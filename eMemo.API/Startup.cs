﻿using DinkToPdf;
using DinkToPdf.Contracts;
using eMemo.API.Middleware;
using eMemo.Helper;
using eMemo.Processes;
using eMemo.Repositories;
using eMemo.Repositories.DBContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace eMemo.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors(o => o.AddPolicy("Policy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials()
                       .SetIsOriginAllowed(c=> true);
            }));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "eMemo API", Version = "v1", Description = "eMemo API" });
            });

            services.AddTransient<AuthorizeFilter>();
            services.AddMvc(option =>
            {
                option.Filters.AddService<AuthorizeFilter>();
            });

            services.AddSingleton<SensitiveDataHelper>();
            services.AddSingleton(typeof(IConverter), new
            SynchronizedConverter(new PdfTools()));
            services.AddScoped<HttpRequestBuilder>();
            services.AddHttpClient("EMEMOAPI").ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
            });
            services.AddScoped<Process>();
            services.AddScoped<Repository>();
            services.AddDbContext<eMemoDBContext>(
                o => o.UseSqlServer(Configuration["ConnectionStrings:eMemoDB"]));
            services.AddDbContext<TGH_EMEMO_DMDBContext>(
              o => o.UseSqlServer(Configuration["ConnectionStrings:TGH_EMEMO_DMDB"]));
            services.AddDbContext<eBaoLSStagingDBContext>(
             o => o.UseSqlServer(Configuration["ConnectionStrings:eBaoLSStagingDB"]));
            services.AddDbContext<AMLSStagingDBContext>(
           o => o.UseSqlServer(Configuration["ConnectionStrings:AMLSStagingDB"]));
            services.AddDbContext<EBAODBContet>(
          o => o.UseOracle(Configuration["ConnectionStrings:eBaoTempDBOR"]));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("Policy");
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/EMEMO_API/swagger/v1/swagger.json", "eMemo API V1");
               // c.SwaggerEndpoint("/swagger/v1/swagger.json", "eMemo API V1");
            });

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMiddleware<RequestResponseLoggingMiddleware>();
            app.UseMvc();
        }
    }
}
