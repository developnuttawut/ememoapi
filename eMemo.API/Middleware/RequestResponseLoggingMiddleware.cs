﻿using eMemo.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Middleware
{
    public class RequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(RequestResponseLoggingMiddleware));
        private readonly SensitiveDataHelper _sensitiveDataHelper;
        private readonly RecyclableMemoryStreamManager _memory;
        public RequestResponseLoggingMiddleware(RequestDelegate next, SensitiveDataHelper sensitiveDataHelper)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _memory = new RecyclableMemoryStreamManager();
            _sensitiveDataHelper = sensitiveDataHelper;
        }

        public async Task Invoke(HttpContext context)
        {

            //Request
            context.Request.EnableBuffering();
            var requestStream = _memory.GetStream();
            await context.Request.Body.CopyToAsync(requestStream);
            var requestData = ReadStreamInChunks(requestStream);
            context.Request.Body.Position = 0;
            requestData = !string.IsNullOrWhiteSpace(requestData) ? _sensitiveDataHelper.SetSensitiveData(requestData) : "";
            //Response
            var originalBodyStream = context.Response.Body;
            var responseBody = _memory.GetStream();
            context.Response.Body = responseBody;

            await _next(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var responseData = await new StreamReader(context.Response.Body).ReadToEndAsync();
            responseData = !string.IsNullOrWhiteSpace(responseData) ? _sensitiveDataHelper.SetSensitiveData(responseData) : "";
            context.Response.Body.Seek(0, SeekOrigin.Begin);


            var request = context.Request;
            if (!request.Path.ToString().Contains("swagger") && !request.Path.ToString().Contains("SaveMemoCode"))
            {
                try
                {
                    var Host = request.Host.ToString();
                    var Path = request.Path;

                    if (request.Path.ToString().Contains("PreviewMemoAttachment")
                        || request.Path.ToString().Contains("GetFileAttachment")
                        || request.Path.ToString().Contains("Export")
                        || request.Path.ToString().Contains("PrintMemoLetterOnly")
                        || request.Path.ToString().Contains("/PrintMemoLetter/Print")
                        )
                    {
                        responseData = "<fileContent>";
                    }

                    _log4net.Info($"{request.Scheme}://{Host}{Path} {requestData} Response : {responseData}");
                }
                catch (Exception ex)
                {
                    _log4net.Error(ex.Message);
                }
            }


            await responseBody.CopyToAsync(originalBodyStream);
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            const int readChunkBufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            var textWriter = new StringWriter();
            var reader = new StreamReader(stream);
            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;
            do
            {
                readChunkLength = reader.ReadBlock(readChunk,
                                                   0,
                                                   readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);
            } while (readChunkLength > 0);
            return textWriter.ToString();
        }
    }
}
