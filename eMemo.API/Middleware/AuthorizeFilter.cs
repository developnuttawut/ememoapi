﻿using eMemo.Processes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace eMemo.API.Middleware
{
    public class AuthorizeFilter : IAsyncActionFilter
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly Process _process;

        public AuthorizeFilter(IHttpContextAccessor httpContextAccessor, Process process)
        {
            _httpContextAccessor = httpContextAccessor;
            _process = process;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            var routeAttribute = controllerActionDescriptor.MethodInfo.GetCustomAttributes(typeof(RouteAttribute), true).FirstOrDefault() as RouteAttribute;
            var template = routeAttribute.Template;

            foreach (var itemRoute in controllerActionDescriptor.RouteValues)
            {
                template = template.Replace($"[{itemRoute.Key}]", itemRoute.Value);
            }

            Regex regex = new Regex(@"\/{.*?\}");
            MatchCollection matches = regex.Matches(template);

            foreach (Match match in matches)
            {
                template = template.Replace($"{match.Value}", "");
            }

            string functionName = $"/{template}";

            if (!functionName.ToLower().Contains("/fileattachment/previewmemoattachment")
                && !functionName.ToLower().Contains("/fileattachment/getfileattachment")
                && !functionName.ToLower().Contains("/fileattachment/previewmemoattachmenthistory")
                && !functionName.ToLower().Contains("/authorization/verifytoken")
                && !functionName.ToLower().Contains("/transaction/retrievetransaction")
                && !functionName.ToLower().Contains("/transaction/updatememotransaction")
                && !functionName.ToLower().Contains("/transaction/autosendmemo")
                && !functionName.ToLower().Contains("/report/export15dayafterprintedreport")
                && !functionName.ToLower().Contains("/report/exportpendingreport")
                && !functionName.ToLower().Contains("/report/exportpendingsummaryreport")
                && !functionName.ToLower().Contains("/report/exportsendingmemoreport")
                && !functionName.ToLower().Contains("/transaction/autosendmemo")
                && !functionName.ToLower().Contains("/transaction/autosendmemo")
                && !functionName.ToLower().Contains("/printmemoletter/printmemoletteronly")
                && !functionName.ToLower().Contains("/printmemoletter/print")
                && !functionName.ToLower().Contains("/transaction/updatedatatemp")
                && !functionName.ToLower().Contains("/transaction/updateagentdata")        
                && !functionName.ToLower().Contains("/alive"))
            {
                var tokenID = _httpContextAccessor.HttpContext.Request.Headers["AuthorizationKey"].ToString();

                if (!string.IsNullOrWhiteSpace(tokenID))
                {
                    var authorizationResponse = await _process.authorization.VerifyToken(tokenID);

                    if (!string.IsNullOrWhiteSpace(authorizationResponse.accessToken))
                    {
                        await next();
                    }
                    else
                    {
                        context.HttpContext.Response.Clear();
                        context.HttpContext.Response.Headers.Add("responsemessage", "Authorization Failed");
                        context.HttpContext.Response.Headers.Add("content-type", "application/json; charset=utf-8");
                        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                }
                else
                {
                    context.HttpContext.Response.Clear();
                    context.HttpContext.Response.Headers.Add("responsemessage", "Authorization Failed");
                    context.HttpContext.Response.Headers.Add("content-type", "application/json; charset=utf-8");
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
