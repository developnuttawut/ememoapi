﻿using eMemo.Helper;
using eMemo.Model.Request.GroupEmailManagement;
using eMemo.Model.Request.MemoMapping;
using eMemo.Model.Response;
using eMemo.Model.Response.EmailManagement;
using eMemo.Model.Response.EMemoTransaction;
using eMemo.Model.Response.MemoMapping;
using eMemo.Model.Response.RuleManagement;
using eMemo.Processes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class MasterDataController : Controller
    {
        private readonly Process process;
        private readonly AppSettingHelper appSettingHelper;
        public MasterDataController(Process process)
        {
            this.process = process;
            appSettingHelper = new AppSettingHelper();
        }

        [HttpPost]
        [Route("[controller]/SaveMemoType")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> SaveMemoType([FromBody] SaveMemoTypeRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoMapping.SaveMemoType(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }           
        }

        [HttpPost]
        [Route("[controller]/DeleteMemoType")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> DeleteMemoType([FromBody] DeleteMemoTypeRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                response = await process.memoMapping.DeleteMemoType(request);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetMemoType")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetMemoTypeResponse))]
        public async Task<IActionResult> GetMemoType()
        {
            GetMemoTypeResponse response = new GetMemoTypeResponse();

            try
            {
                response = await process.memoMapping.GetMemoTypeList();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpPost]
        [Route("[controller]/SaveMemoCode")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> SaveMemoCode([FromForm] SaveMemoCodeRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoMapping.SaveMomoCode(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }              
        }

        [HttpPost]
        [Route("[controller]/DeleteMemoCode")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> DeleteMemoCode([FromBody] DeleteMomoCodeRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                response = await process.memoMapping.DeleteMemoCode(request);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetMemoMapping")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetMemoMappingResponse))]
        public async Task<IActionResult> GetMemoMapping()
        {
            GetMemoMappingResponse response = new GetMemoMappingResponse();

            try
            {
                string hostUrl = appSettingHelper.GetConfiguration("APIPoxyUri");
                response = await process.memoMapping.GetMemoMappingList(hostUrl);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetMemoCodeBy")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetMemoCodeResponse))]
        public async Task<IActionResult> GetMemoCodeBy(string param, string type)
        {
            GetMemoCodeResponse response = new GetMemoCodeResponse();

            try
            {
                response = await process.memoMapping.FindMemoCodeByDescriptionandType(param == null ? "" : param, type);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        #region Rule Management

        [HttpGet]
        [Route("[controller]/GetChannel")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetChannelResponse))]
        public async Task<IActionResult> GetChannel(string param)
        {
            GetChannelResponse response = new GetChannelResponse();

            try
            {
                response = await process.memoMapping.GetChannel();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }
        [HttpGet]
        [Route("[controller]/GetSubChannel")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetSubChannelResponse))]
        public async Task<IActionResult> GetSubChannel(string param)
        {
            GetSubChannelResponse response = new GetSubChannelResponse();

            try
            {
                response = await process.memoMapping.GetSubChannel();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }
        [HttpGet]
        [Route("[controller]/GetBusinessUnit")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetBusinessUnitResponse))]
        public async Task<IActionResult> GetBusinessUnit(string param)
        {
            GetBusinessUnitResponse response = new GetBusinessUnitResponse();

            try
            {
                response = await process.memoMapping.GetBusinessUnit();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }
        [HttpGet]
        [Route("[controller]/GetProductCategory")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetProductCategoryResponse))]
        public async Task<IActionResult> GetProductCategory(string param)
        {
            GetProductCategoryResponse response = new GetProductCategoryResponse();

            try
            {
                response = await process.memoMapping.FindProductCategoryByDescription(param == null ? "" : param);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }
        [HttpGet]
        [Route("[controller]/GetSourceOfBusiness")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetSourceOfBusinessResponse))]
        public async Task<IActionResult> GetSourceOfBusiness(string param)
        {
            GetSourceOfBusinessResponse response = new GetSourceOfBusinessResponse();

            try
            {
                response = await process.memoMapping.GetSourceOfBusiness();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetRuleManagement")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetRuleManagementResponse))]
        public async Task<IActionResult> GetRuleManagement()
        {
            GetRuleManagementResponse response = new GetRuleManagementResponse();

            try
            {
                response = await process.memoMapping.GetRuleManagementList();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }
        [HttpPost]
        [Route("[controller]/SaveRuleManagement")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> SaveRuleManagement([FromBody] SaveRuleManagementRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoMapping.SaveRuleManagement(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
        [HttpPost]
        [Route("[controller]/DeleteRuleManagement")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> DeleteRuleManagement([FromBody] DeleteRuleManagementRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                response = await process.memoMapping.DeleteRuleManagement(request);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        #endregion Rule Management

        #region Email Management
        [HttpGet]
        [Route("[controller]/GetBranchOffice")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetBranchOfficeResponse))]
        public async Task<IActionResult> GetBranchOffice(string param)
        {
            GetBranchOfficeResponse response = new GetBranchOfficeResponse();

            try
            {
                response = await process.memoMapping.GetBranchOffice(param == null ? "" : param);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetBranchOfficeByUnit/{businessUnitID}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetBranchOfficeResponse))]
        public async Task<IActionResult> GetBranchOfficeByUnit(string businessUnitID)
        {
            GetBranchOfficeResponse response = new GetBranchOfficeResponse();

            try
            {
                response = await process.memoMapping.GetBranchOfficeByUnit(businessUnitID);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetGroupEmailManagement")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetGroupEmailManagementResponse))]
        public async Task<IActionResult> GetGroupEmailManagement()
        {
            GetGroupEmailManagementResponse response = new GetGroupEmailManagementResponse();

            try
            {
                response = await process.memoMapping.GetGroupEmailManagementList();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }
        [HttpPost]
        [Route("[controller]/SaveGroupEmailManagement")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> SaveGroupEmailManagement([FromBody] SaveGroupEmailManagementRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoMapping.SaveGroupEmailManagement(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }



        [HttpGet]
        [Route("[controller]/GetAgentUnit")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetAgentUnitResponse))]
        public async Task<IActionResult> GetAgentUnit()
        {
            GetAgentUnitResponse response = new GetAgentUnitResponse();

            try
            {
                response = await process.memoTransaction.GetAgentUnit();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetAgentGroup")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetAgentGroupResponse))]
        public async Task<IActionResult> GetAgentGroup()
        {
            GetAgentGroupResponse response = new GetAgentGroupResponse();

            try
            {
                response = await process.memoTransaction.GetAgentGroup();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        #endregion Email Management

        [HttpGet]
        [Route("[controller]/GetSaleGroup")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetSaleGroupResponse))]
        public async Task<IActionResult> GetSaleGroup()
        {
            GetSaleGroupResponse response = new GetSaleGroupResponse();

            try
            {
                response = await process.memoMapping.GetSaleGroup();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetUnderwritingGroup")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetUnderwritingGroupResponse))]
        public async Task<IActionResult> GetUnderwritingGroup()
        {
            GetUnderwritingGroupResponse response = new GetUnderwritingGroupResponse();

            try
            {
                response = await process.memoMapping.GetUnderwritingGroup();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetProductCategoryReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetProductCategoryResponse))]
        public async Task<IActionResult> GetProductCategoryReport()
        {
            GetProductCategoryResponse response = new GetProductCategoryResponse();

            try
            {
                response = await process.memoMapping.GetProductCategoryForReport();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }
    }
}
