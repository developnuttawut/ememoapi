﻿using eMemo.Helper;
using eMemo.Model.Request.MemoInquiry;
using eMemo.Model.Response;
using eMemo.Model.Response.MemoInquiry;
using eMemo.Processes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class InquiryController : Controller
    {
        private readonly Process process;
        private readonly AppSettingHelper appSettingHelper;
        public InquiryController(Process process)
        {
            this.process = process;
            appSettingHelper = new AppSettingHelper();
        }

        [HttpPost]
        [Route("[controller]/GetMemoInquiry")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetMemoInquiryResponse))]
        public async Task<IActionResult> GetMemoInquiry([FromBody] GetMemoInquiryRequest request)
        {
            if (ModelState.IsValid)
            {
                GetMemoInquiryResponse response = new GetMemoInquiryResponse();

                try
                {
                    response = await process.memoInquiry.GetMemoInquiry(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("[controller]/GetMemoInquiryDetail/{transactionID}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetEmemoInquiryDetailResponse))]
        public async Task<IActionResult> GetMemoInquiryDetail(int transactionID)
        {
            GetEmemoInquiryDetailResponse response = new GetEmemoInquiryDetailResponse();

            try
            {
                string hostUrl = appSettingHelper.GetConfiguration("APIPoxyUri");
                response = await process.memoInquiry.GetMemoInquiryDetail(transactionID, hostUrl);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpPost]
        [Route("[controller]/ReplyMemoItem")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> ReplyMemoItem([FromBody] ReplyMemoItemRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoInquiry.ReplyMemoItem(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("[controller]/ResendMemo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> ResendMemo([FromBody] ResendMemoRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    string hostUrl = appSettingHelper.GetConfiguration("APIPoxyUri");
                    response = await process.memoInquiry.ResendMemo(request, hostUrl);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("[controller]/SendMemo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> SendMemo([FromBody] SendAfterSaveRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoInquiry.SendAfterSave(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

    }
}
