﻿using eMemo.Processes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class AuthorizationController : ControllerBase
    {
        private readonly Process process;
        public AuthorizationController(Process process)
        {
            this.process = process;
        }

        [HttpGet]
        [Route("[controller]/VerifyToken")]
        public async Task<IActionResult> VerifyToken()
        {
            try
            {
                var tokenID = Request.Headers["AuthorizationKey"].ToString();

                var verifyTokenResponse = await process.authorization.Authorization(tokenID);
                if (!string.IsNullOrWhiteSpace(verifyTokenResponse.accessToken))
                {
                    return Ok(verifyTokenResponse);
                }
                else
                {
                    return Unauthorized();
                }     
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
