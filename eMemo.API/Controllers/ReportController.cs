﻿using eMemo.Model.Request.Report;
using eMemo.Model.Response.Report;
using eMemo.Processes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class ReportController : Controller
    {
        private readonly Process process;
        public ReportController(Process process)
        {
            this.process = process;
        }

        [HttpPost]
        [Route("[controller]/Preview15DayAfterPrintedReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> PreviewReport15DayAfterPrinted([FromBody] Report15DayAfterPrintedRequest request)
        {
            if (ModelState.IsValid)
            {
                PreviewReportResponse response = new PreviewReportResponse();
                var responseReport = await process.report.GetReport15DayAfterMemoPrinted(request, true);

                if (responseReport.status.code == ((int)HttpStatusCode.OK).ToString())
                {
                    response.htmlPreview = responseReport.filePreview;

                }

                response.status = responseReport.status;
                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("[controller]/Export15DayAfterPrintedReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> ExportReport15DayAfterPrinted(DateTime memoPrintedDateFrom, DateTime? memoPrintedDateTo, string condition, int printedDay)
        {
            try
            {
                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

                Report15DayAfterPrintedRequest request = new Report15DayAfterPrintedRequest
                {
                    memoPrintedDateFrom = memoPrintedDateFrom,
                    memoPrintedDateTo = memoPrintedDateTo,
                    condition = condition,
                    printedDay = printedDay
                };

                var response = await process.report.GetReport15DayAfterMemoPrinted(request, false);

                if (response.fileContent != null)
                {
                    return File(
                  response.fileContent,
                  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                  $"15DayAfterMemoPrinted_{DateTime.Now.ToString("yyyyMMddHHmmss", _cultureTHInfo)}.xlsx");
                }
                else
                {
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("[controller]/PreviewPendingReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> PreviewPendingReport([FromBody] PendingReportRequest request)
        {
            PreviewReportResponse response = new PreviewReportResponse();
            var responseReport = await process.report.GetReportPending(request, true);

            if (responseReport.status.code == ((int)HttpStatusCode.OK).ToString())
            {
                response.htmlPreview = responseReport.filePreview;

            }

            response.status = responseReport.status;
            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/ExportPendingReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> ExportPendingReport(DateTime asOfDate, string productCategory)
        {
            try
            {
                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

                PendingReportRequest request = new PendingReportRequest
                {
                    asOfDate = asOfDate,
                    productCategory = productCategory
                };

                var response = await process.report.GetReportPending(request, false);

                if (response.fileContent != null)
                {
                    return File(
                  response.fileContent,
                  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                  $"PendingReport_{DateTime.Now.ToString("yyyyMMddHHmmss", _cultureTHInfo)}.xlsx");
                }
                else
                {
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("[controller]/PreviewPendingSummaryReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> PreviewPendingSummaryReport([FromBody] PendingSummaryReportRequest request)
        {
            if (ModelState.IsValid)
            {
                PreviewReportResponse response = new PreviewReportResponse();
                var responseReport = await process.report.GetReportPendingSummary(request, true);

                if (responseReport.status.code == ((int)HttpStatusCode.OK).ToString())
                {
                    response.htmlPreview = responseReport.filePreview;

                }

                response.status = responseReport.status;
                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("[controller]/ExportPendingSummaryReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> ExportPendingSummaryReport(DateTime submissionDateFrom, DateTime? submissionDateTo,string productCategory)
        {
            try
            {
                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

                PendingSummaryReportRequest request = new PendingSummaryReportRequest
                {
                    submissionDateFrom = submissionDateFrom,
                    submissionDateTo = submissionDateTo,
                    productCategory = productCategory
                };

                var response = await process.report.GetReportPendingSummary(request, false);

                if (response.fileContent != null)
                {
                    return File(
                  response.fileContent,
                  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                  $"PendingSummaryReport_{DateTime.Now.ToString("yyyyMMddHHmmss", _cultureTHInfo)}.xlsx");
                }
                else
                {
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("[controller]/PreviewSendingMemoReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> PreviewSendingMemoReport([FromBody] SendingMemoReportRequest request)
        {
            if (ModelState.IsValid)
            {
                PreviewReportResponse response = new PreviewReportResponse();
                var responseReport = await process.report.GetReportSendingMemo(request, true);

                if (responseReport.status.code == ((int)HttpStatusCode.OK).ToString())
                {
                    response.htmlPreview = responseReport.filePreview;

                }

                response.status = responseReport.status;
                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("[controller]/ExportSendingMemoReport")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PreviewReportResponse))]
        public async Task<IActionResult> ExportSendingMemoReport(DateTime memoPrintedDateFrom, DateTime? memoPrintedDateTo, string deliveryStatus, string resendNeed, bool showLatest)
        {
            try
            {
                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

                SendingMemoReportRequest request = new SendingMemoReportRequest
                {
                    memoPrintedDateFrom = memoPrintedDateFrom,
                    memoPrintedDateTo = memoPrintedDateTo,
                    deliveryStatus = deliveryStatus,
                    resendNeed = resendNeed,
                    showLatest = showLatest
                };

                var response = await process.report.GetReportSendingMemo(request, false);

                if (response.fileContent != null)
                {
                    return File(
                  response.fileContent,
                  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                  $"SendingMemoReport_{DateTime.Now.ToString("yyyyMMddHHmmss", _cultureTHInfo)}.xlsx");
                }
                else
                {
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ModelState);
            }
        }
    }
}
