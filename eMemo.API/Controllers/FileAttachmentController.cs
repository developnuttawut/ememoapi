﻿using eMemo.Processes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class FileAttachmentController : ControllerBase
    {
        private readonly Process process;
        public FileAttachmentController(Process process)
        {
            this.process = process;
        }

        [HttpGet]
        [Route("[controller]/GetFileAttachment")]
        public async Task<IActionResult> GetFileAttachment(string fileType, string referenceID)
        {
            try
            {
                var attachmentData = await process.fileAttachment.GetFileAttachmentContent(fileType, referenceID);

                if (attachmentData.fileContent != null)
                {
                    var fileResult = new FileContentResult(attachmentData.fileContent, attachmentData.fileType);
                    fileResult.FileDownloadName = attachmentData.fileName;

                    return new FileContentResult(attachmentData.fileContent, attachmentData.fileType) {
                        FileDownloadName = attachmentData.fileName
                    };
                }
                else
                {
                    return Ok(attachmentData);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("[controller]/PreviewMemoAttachment/{documentNo}")]
        public async Task<IActionResult> PreviewMemoAttachment(string documentNo)
        {
            try
            {
                var attachmentData = await process.fileAttachment.PreviewMemoAttachment(documentNo);
                if (attachmentData.fileContent != null)
                {
                    string filename = "MemoReport_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                    var fileResult = new FileContentResult(attachmentData.fileContent, "application/pdf");
                    fileResult.FileDownloadName = filename;

                    return new FileContentResult(attachmentData.fileContent, "application/pdf");
                }
                else
                {
                    return Ok(attachmentData);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memoDocumentHistoryID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("[controller]/PreviewMemoAttachmentHistory/{documentNo}")]
        public async Task<IActionResult> PreviewMemoAttachmentHistory(string documentNo)
        {
            try
            {
                var attachmentData = await process.fileAttachment.PreviewMemoAttachmentHistory(documentNo);
                if (attachmentData.fileContent != null)
                {
                    string filename = "MemoReport_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                    var fileResult = new FileContentResult(attachmentData.fileContent, "application/pdf");
                    fileResult.FileDownloadName = filename;

                    return new FileContentResult(attachmentData.fileContent, "application/pdf");
                }
                else
                {
                    return Ok(attachmentData);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
