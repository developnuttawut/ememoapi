﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class AliveController : ControllerBase
    {
        [HttpGet]
        [Route("alive")]
        public IActionResult Alive()
        {
            return Ok(true);
        }
    }
}
