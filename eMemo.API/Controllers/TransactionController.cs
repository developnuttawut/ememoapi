﻿using eMemo.Helper;
using eMemo.Model.Entity;
using eMemo.Model.Request.EMemoTransaction;
using eMemo.Model.Response;
using eMemo.Model.Response.EMemoTransaction;
using eMemo.Processes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class TransactionController : Controller
    {
        private readonly Process process;
        private readonly AppSettingHelper appSettingHelper;
        public TransactionController(Process process)
        {
            this.process = process;
            appSettingHelper = new AppSettingHelper();
        }

        [HttpGet]
        [Route("[controller]/RetrieveTransaction")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> RetrieveTransaction()
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoTransaction.RetrieveDataFromTemp();
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("[controller]/SearchMemoTransaction")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SearchMemoTransactionResponse))]
        public async Task<IActionResult> SearchMemoTransaction([FromBody] SearchMemoTransactionRequest request)
        {
            if (ModelState.IsValid)
            {
                SearchMemoTransactionResponse response = new SearchMemoTransactionResponse();

                try
                {
                    response = await process.memoTransaction.SearchMemoTransaction(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("[controller]/UpdateMemoTransaction")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> UpdateMemoTransaction()
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoTransaction.UpdateStatusFromTemp();
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        [Route("[controller]/UpdateDeliveryStatus")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> UpdateDeliveryStatus([FromBody] UpdateDeliveryStatusRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoTransaction.UpdateDeliveryStatus(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("[controller]/GetDeliveryStatus")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetDeliveryStatusResponse))]
        public async Task<IActionResult> GetDeliveryStatus()
        {
            GetDeliveryStatusResponse response = new GetDeliveryStatusResponse();

            try
            {
                response = await process.memoTransaction.GetDeliveryStatusList();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetEBaoStatus")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetEBaoStatusResponse))]
        public async Task<IActionResult> GetEBaoStatus()
        {
            GetEBaoStatusResponse response = new GetEBaoStatusResponse();

            try
            {
                response = await process.memoTransaction.GetEBaoStatusList();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpGet]
        [Route("[controller]/GetEmemoPrerationDetail")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetEmemoPrerationDetailResponse))]
        public async Task<IActionResult> GetMemoTransactionDetail(int transactionID)
        {
            GetEmemoPrerationDetailResponse response = new GetEmemoPrerationDetailResponse();

            try
            {
                string hostUrl = appSettingHelper.GetConfiguration("APIPoxyUri");
                response = await process.memoTransaction.FindEmemoPrerationDetailBy(transactionID, hostUrl);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);
        }

        [HttpPost]
        [Route("[controller]/SaveMemoDocument")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> SaveEMEMODocument([FromBody] SaveMemoDocumentRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                response = await process.memoTransaction.SaveMemoDocument(request);
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);

        }

        //[HttpPost]
        //[Route("[controller]/AddMemoDocument")]
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        //public async Task<IActionResult> AddMemoDocument([FromBody] AddMemoDocumentRequest request)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        SaveDataResponse response = new SaveDataResponse();

        //        try
        //        {
        //            response = await process.memoTransaction.AddMemoDocument(request);
        //        }
        //        catch (Exception ex)
        //        {
        //            response.status = new Model.Base.Status
        //            {
        //                code = ((int)StatusCodes.Status500InternalServerError).ToString(),
        //                message = ex.InnerException?.Message ?? ex.Message
        //            };
        //        }

        //        return Ok(response);
        //    }
        //    else
        //    {
        //        return BadRequest(ModelState);
        //    }
        //}

        //[HttpPost]
        //[Route("[controller]/DeleteMemoDocument")]
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        //public async Task<IActionResult> DeleteMemoDocument([FromBody] DeleteMemoDocumentRequest request)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        SaveDataResponse response = new SaveDataResponse();

        //        try
        //        {
        //            response = await process.memoTransaction.DeleteMemoDocument(request);
        //        }
        //        catch (Exception ex)
        //        {
        //            response.status = new Model.Base.Status
        //            {
        //                code = ((int)StatusCodes.Status500InternalServerError).ToString(),
        //                message = ex.InnerException?.Message ?? ex.Message
        //            };
        //        }

        //        return Ok(response);
        //    }
        //    else
        //    {
        //        return BadRequest(ModelState);
        //    }
        //}

        [HttpPost]
        [Route("[controller]/AdhocSendMemo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> AdhocSendMemo([FromBody] AdhocSendMemoRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    string hostUrl = appSettingHelper.GetConfiguration("APIPoxyUri");
                    response = await process.memoTransaction.AdhocSendMemo(request, hostUrl);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpGet]
        [Route("[controller]/AutoSendMemo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> AutoSendMemo()
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();
                try
                {
                    string hostUrl = appSettingHelper.GetConfiguration("APIPoxyUri");
                    response = await process.memoTransaction.AutoSendMemo(hostUrl);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        //[ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        [Route("[controller]/UpdateDataTemp")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> UpdateDataTemp([FromBody] UpdateTempRequest request)
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    response = await process.memoTransaction.UpdateDataTemp(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("[controller]/UpdateAgentData")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SaveDataResponse))]
        public async Task<IActionResult> UpdateAgentData()
        {

            SaveDataResponse response = new SaveDataResponse();

            try
            {
                response = await process.memoTransaction.UpdateAgentData();
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return Ok(response);

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("[controller]/GetAllTxn")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TTHMemoTransaction>))]
        public async Task<IActionResult> GetAllTxn()
        {
            if (ModelState.IsValid)
            {
                SaveDataResponse response = new SaveDataResponse();

                try
                {
                    var data = await process.memoTransaction.GetAllTXN();
                    return Ok(data);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}
