﻿using eMemo.Model.Request.PrintMemoLetter;
using eMemo.Model.Response.PrintMemoLetter;
using eMemo.Processes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eMemo.API.Controllers
{
    [ApiController]
    [EnableCors("Policy")]
    public class PrintMemoLetterController : Controller
    {
        private readonly Process process;
        public PrintMemoLetterController(Process process)
        {
            this.process = process;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("[controller]/PrintMemoLetterOnly/{documentNo}")]
        public async Task<IActionResult> PrintMemoLetterOnly(string documentNo)
        {
            try
            {
                var attachmentData = await process.printMemoLetter.PrintMemoLetterOnly(documentNo);

                if(attachmentData.fileContent != null)
                {
                    string filename = "MemoLetter_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";

                    return File(
                   attachmentData.fileContent,
                   "application/pdf",
                   filename);

                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("[controller]/Print/{documentNo}")]
        public async Task<IActionResult> Print(string documentNo)
        {
            try
            {
                var attachmentData = await process.printMemoLetter.Print(documentNo);

                if (attachmentData.fileContent != null)
                {
                    string filename = "MemoReport_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";

                    return File(
                   attachmentData.fileContent,
                   "application/pdf",
                    filename);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("[controller]/GetPrintMemoLetter")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetPrintMemoLetterResponse))]
        public async Task<IActionResult> GetPrintMemoLetter([FromBody] GetPrintMemoLetterRequest request)
        {
            if (ModelState.IsValid)
            {
                GetPrintMemoLetterResponse response = new GetPrintMemoLetterResponse();

                try
                {
                    response = await process.printMemoLetter.GetPrintMemoLetter(request);
                }
                catch (Exception ex)
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)StatusCodes.Status500InternalServerError).ToString(),
                        message = ex.InnerException?.Message ?? ex.Message
                    };
                }

                return Ok(response);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}
