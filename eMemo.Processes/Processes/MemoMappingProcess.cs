﻿using eMemo.Helper;
using eMemo.Model.Base;
using eMemo.Model.Entity;
using eMemo.Model.ExternalRequest;
using eMemo.Model.Request.GroupEmailManagement;
using eMemo.Model.Request.MemoMapping;
using eMemo.Model.Response;
using eMemo.Model.Response.EmailManagement;
using eMemo.Model.Response.MemoMapping;
using eMemo.Model.Response.RuleManagement;
using eMemo.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Processes.Processes
{
    public class MemoMappingProcess
    {
        private readonly Repository _repository;
        private AppSettingHelper appSettingHelper;
        private readonly SensitiveDataHelper _sensitiveData;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(MemoMappingProcess));

        public MemoMappingProcess(Repository repository, SensitiveDataHelper sensitiveData)
        {
            _repository = repository;
            appSettingHelper = new AppSettingHelper();
            _sensitiveData = sensitiveData;
        }

        public async Task<SaveDataResponse> SaveMemoType(SaveMemoTypeRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                bool saveResult;
                var memoTypeByName = _repository.memoMapping.FindMemoTypeBy(c => c.MemoType_Name.ToUpper() == request.memoTypeName.ToUpper() && c.Flag_Delete == false);

                // ไม่ซ้ำ หรือ Update ตัวเอง
                if (memoTypeByName == null || memoTypeByName.ID == request.memoTypeID)
                {   // Sequence ห้ามซ้ำ
                    var memoTypeBySequence = _repository.memoMapping.FindMemoTypeBy(e => e.Sequence == request.memoTypeSequence && e.Flag_Delete == false);

                    if (memoTypeBySequence == null || memoTypeBySequence.ID == request.memoTypeID)
                    {
                        if (!request.memoTypeID.HasValue || request.memoTypeID == 0)
                        {
                            saveResult = await _repository.memoMapping.CreateMemoType(request);
                        }
                        else
                        {
                            saveResult = await _repository.memoMapping.UpdateMemoType(request);
                        }

                        response.success = saveResult;
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.OK).ToString(),
                            message = saveResult ? "บันทึกข้อมูลสำเร็จ" : "บันทึกข้อมูลไม่สำเร็จ"
                        };
                    }
                    else
                    {
                        // Sequence ซ้ำ
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.Conflict).ToString(),
                            message = $"Memo Type Sequence {request.memoTypeSequence} ซ้ำกับที่มีอยู่ในระบบ"
                        };
                    }
                }
                else
                {
                    // Memo Type Name ซ้ำ
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.Conflict).ToString(),
                        message = $"Memo Type Name {request.memoTypeName} ซ้ำกับที่มีอยู่ในระบบ"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> DeleteMemoType(DeleteMemoTypeRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                bool saveResult = await _repository.memoMapping.DeleteMemoType(request);

                response.success = saveResult;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = saveResult ? "ลบ Memo Type สำเร็จ" : "ลบ Memo Type ไม่สำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetMemoTypeResponse> GetMemoTypeList()
        {
            GetMemoTypeResponse response = new GetMemoTypeResponse();

            try
            {
                var memoTypeData = await _repository.memoMapping.GetAllMemoType();

                if (memoTypeData.Any())
                {
                    response.data = memoTypeData.Where(e => !e.Flag_Delete).Select(c => new MemoTypeData
                    {
                        memoTypeID = c.ID,
                        memoTypeName = c.MemoType_Name,
                        memoTypeSequence = c.Sequence,

                    }).OrderBy(d => d.memoTypeSequence).ToList();
                }
                else
                {
                    response.data = new List<MemoTypeData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> SaveMomoCode(SaveMemoCodeRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                TMMemoCode newMemoCode = null;
                bool updateSuccess = false;
                string oldMemoCode = string.Empty;

                var memoTypeData = _repository.memoMapping.FindMemoTypeBy(c => c.ID == request.memoTypeID);
                var memoCodeByName = _repository.memoMapping.FindMemoCodeBy(c => c.MemoCode.ToUpper() == request.memoCode.ToUpper() && c.Flag_Delete == false);

                if (memoCodeByName == null || memoCodeByName.ID == request.memoCodeID)
                {
                    var memoCodeBySequence = _repository.memoMapping.FindMemoCodeBy(
                        e => e.Sequence == request.memoCodeSequence
                        && e.MemoType_Name.ToUpper().Trim() == request.memoType.ToUpper().Trim()
                        && e.Flag_Delete == false
                        && e.Status == true);

                    if (memoCodeBySequence == null || memoCodeBySequence.ID == request.memoCodeID)
                    {
                        if (!request.memoCodeID.HasValue || request.memoCodeID == 0)
                        {
                            newMemoCode = await _repository.memoMapping.CreateMemoCode(request);
                            request.memoCodeID = newMemoCode.ID;
                        }
                        else
                        {
                            updateSuccess = await _repository.memoMapping.UpdateMemoCode(request);
                        }

                        if (newMemoCode != null || updateSuccess)
                        {
                            List<TMMemoCodeAttachment> memoCodeAttachmentList = new List<TMMemoCodeAttachment>();

                            await _repository.memoMapping.DeleteMemoCodeAttachment(request.memoCodeID.GetValueOrDefault());

                            if (request.attachments != null)
                            {
                                string applicationName = appSettingHelper.GetConfiguration("ApplicationNameSaveFile");

                                SaveFileRequest saveFileList = new SaveFileRequest();
                                saveFileList.application = applicationName;

                                List<FileDetail> listFileDetail = new List<FileDetail>();
                                foreach (var file in request.attachments)
                                {
                                    //string attachmentPath = Path.Combine(fullPath, file.FileName);
                                    byte[] fileBytes;
                                    using (var ms = new MemoryStream())
                                    {
                                        file.CopyTo(ms);
                                        fileBytes = ms.ToArray();
                                    }

                                    string referenceID = Guid.NewGuid().ToString();
                                    FileDetail fileItem = new FileDetail
                                    {
                                        filename = referenceID + ".pdf",
                                        extension = ".pdf",
                                        data = Convert.ToBase64String(fileBytes)
                                    };

                                    listFileDetail.Add(fileItem);

                                    TMMemoCodeAttachment memoCodeAttachment = new TMMemoCodeAttachment
                                    {
                                        MemoCode_ID = request.memoCodeID.GetValueOrDefault(),
                                        Reference_ID = referenceID,
                                        File_Type = file.ContentType,
                                        File_Size = (Int32)file.Length,
                                        File_Path = "",
                                        File_Name = file.FileName,
                                        Created_By = request.userID,
                                        Created_Date = Utility.GetDateNowThai()
                                    };

                                    memoCodeAttachmentList.Add(memoCodeAttachment);
                                }

                                saveFileList.files = listFileDetail;

                                var fileFromDMC = await _repository.dmcFile.SaveFileDMC(saveFileList);
                                if (fileFromDMC != null && fileFromDMC.operationStatus.statusCode == 200)
                                {
                                    foreach (var file in memoCodeAttachmentList)
                                    {
                                        var fileData = fileFromDMC.files.FirstOrDefault(c => c.fileName.Contains(file.Reference_ID));
                                        if (fileData != null)
                                        {
                                            file.File_Path = fileData.data;
                                            file.File_Extension = fileData.extension;
                                        }

                                    }
                                    var saveMemoCodeAttachmentResult = await _repository.memoMapping.InsertMemoCodeAttachment(memoCodeAttachmentList);


                                    response.success = saveMemoCodeAttachmentResult;
                                    response.status = new Model.Base.Status
                                    {
                                        code = ((int)HttpStatusCode.OK).ToString(),
                                        message = saveMemoCodeAttachmentResult ? "บันทึกข้อมูลสำเร็จ" : "บันทึกข้อมูลเอกสารแนบไม่สำเร็จ"
                                    };
                                }
                                else
                                {
                                    
                                    response.success = false;
                                    response.status = new Model.Base.Status
                                    {
                                        code = ((int)HttpStatusCode.OK).ToString(),
                                        message = fileFromDMC?.operationStatus?.description ?? "บันทึกข้อมูลไม่สำเร็จ"
                                    };
                                }
                            }
                            else
                            {
                                response.success = true;
                                response.status = new Model.Base.Status
                                {
                                    code = ((int)HttpStatusCode.OK).ToString(),
                                    message = "บันทึกข้อมูลสำเร็จ"
                                };
                            }

                            memoCodeAttachmentList = _repository.memoMapping.GetMemoCodeAttachmentBy(c => c.MemoCode_ID == request.memoCodeID);

                            // Memo Detail ที่ยังไม่ได้ Mapping
                            var memoTransactionDetail = _repository.memoTransaction.GetMemoTransactionDetailBy(
                                c => c.MemoCode == request.memoCode
                                && c.MemoCode_Sequence == 999);

                            foreach (var itemDetail in memoTransactionDetail)
                            {
                                itemDetail.MemoCode_Sequence = request.memoCodeSequence;
                                itemDetail.MemoType_Sequence = memoTypeData.Sequence;
                                itemDetail.Updated_By = request.userID;
                                itemDetail.Updated_Date = Utility.GetDateNowThai();

                                var memoDetailAttachment = memoCodeAttachmentList.Select(c => new TTDMemoTransactionAttachment
                                {
                                    MemoTransaction_ID = itemDetail.MemoTransaction_ID,
                                    MemoTransactionDetail_ID = itemDetail.ID,
                                    File_Path = c.File_Path,
                                    File_Type = c.File_Type,
                                    File_Name = c.File_Name,
                                    File_Size = c.File_Size,
                                    File_Extension = c.File_Extension,
                                    Reference_ID = c.Reference_ID,
                                    Created_By = c.Created_By,
                                    Created_Date = c.Created_Date
                                }).ToList();

                                await _repository.memoTransaction.SaveEMemoTransactionDetailAttachment(memoDetailAttachment);
                                //await _repository.memoTransaction.CreateMemoDocumentTransactionHistory(itemDetail.MemoTransaction_ID, itemDetail.DocumentNo, request.userID);
                            }

                            await _repository.memoTransaction.UpdateMemoTransactionDetailList(memoTransactionDetail);
                        }
                        else
                        {
                            response.success = false;
                            response.status = new Model.Base.Status
                            {
                                code = ((int)HttpStatusCode.OK).ToString(),
                                message = "บันทึกข้อมูลไม่สำเร็จ"
                            };
                        }
                    }
                    else
                    {
                        // Sequence ซ้ำ
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.Conflict).ToString(),
                            message = $"Memo Code Sequence ซ้ำกับ {memoCodeBySequence.MemoCode}"
                        };
                    }
                }
                else
                {
                    // Memo Code Name ซ้ำ
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.Conflict).ToString(),
                        message = $"Memo Code {request.memoCode} ซ้ำกับที่มีอยู่ในระบบ"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            var jsonRequest = JsonConvert.SerializeObject(request);
            var jsonResponse = JsonConvert.SerializeObject(response);

            _log4net.Info($"/MasterData/SaveMemoCode {jsonRequest} Response : {jsonResponse}");

            return response;
        }

        public async Task<SaveDataResponse> DeleteMemoCode(DeleteMomoCodeRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                bool saveResult = await _repository.memoMapping.DeleteMemoCode(request);

                response.success = saveResult;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = saveResult ? "ลบ Memo Code สำเร็จ" : "ลบ Memo Code ไม่สำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetMemoMappingResponse> GetMemoMappingList(string hostUrl)
        {
            GetMemoMappingResponse response = new GetMemoMappingResponse();

            try
            {
                var memoMappingData = await _repository.memoMapping.GetMemoMappingList();

                if (memoMappingData.Any())
                {
                    foreach (var itemMemoMapping in memoMappingData)
                    {
                        var memoCodeAttachment = await _repository.memoMapping.GetMemoCodeAttachment(itemMemoMapping.memoCodeID);
                        itemMemoMapping.fileAttachment = memoCodeAttachment.Select(c => new FileAttachment
                        {
                            filePath = $"{hostUrl}/EMEMO_API/FileAttachment/GetFileAttachment?fileType=MEMOCODE&referenceID={c.Reference_ID}",
                            //filePath = $"{hostUrl}/FileAttachment/GetFileAttachment?fileType=MEMOCODE&referenceID={c.Reference_ID}",
                            fileName = c.File_Name,
                            fileSize = (decimal)c.File_Size,
                            fileType = c.File_Type
                        }).ToList();
                    }
                }

                response.data = memoMappingData.OrderBy(c => c.memoTypeSequence).ThenBy(e => e.memoCodeSequence).ToList();
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetMemoCodeResponse> GetMemoCodeList()
        {
            GetMemoCodeResponse response = new GetMemoCodeResponse();

            try
            {
                var memoCodeData = await _repository.memoMapping.GetMemoCodeBy(c => !c.Flag_Delete);
                if (memoCodeData.Any())
                {
                    response.data = memoCodeData.Select(c => new MemoCodeData
                    {
                        memoTypeID = c.MemoType_ID,
                        memoCode = c.MemoCode,
                        memoCodeID = c.ID,
                        description = c.Description

                    }).OrderBy(d => d.memoCode).ToList();
                }
                else
                {
                    response.data = new List<MemoCodeData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }
    
        public async Task<GetMemoCodeResponse> FindMemoCodeByDescriptionandType(string param, string type)
        {
            GetMemoCodeResponse response = new GetMemoCodeResponse();

            try
            {
                var memoCodeData = await _repository.memoMapping.GetMemoCodeBy(
                    c => (c.Description.ToUpper().Contains(param.ToUpper()) || c.MemoCode.ToUpper().Contains(param.ToUpper()))
                    && !c.Flag_Delete
                    && (type == null || (type != null && (c.MemoType_ID == Convert.ToInt32(type)))));

                if (memoCodeData.Any())
                {
                    response.data = memoCodeData.Select(c => new MemoCodeData
                    {
                        memoTypeID = c.MemoType_ID,
                        memoCode = c.MemoCode,
                        memoCodeID = c.ID,
                        description = c.Description

                    }).OrderBy(d => d.memoCode).ToList();
                }
                else
                {
                    response.data = new List<MemoCodeData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        #region Rule Management

        public async Task<GetChannelResponse> GetChannel()
        {
            GetChannelResponse response = new GetChannelResponse();

            try
            {
                var channelData = await _repository.almStaging.GetChannel(c => c.IS_DELETED == 0);
                if (channelData.Any())
                {
                    response.data = channelData.Select(c => new ChannelData
                    {
                        channelID = c.CHANNEL_CODE,
                        description = c.CHANNEL_NAME

                    }).OrderBy(d => d.description).ToList();
                }
                else
                {
                    var channelDataBK = await _repository.almStaging.GetChannelBK(c => c.IS_DELETED == 0);
                    if (channelDataBK.Any())
                    {
                        response.data = channelDataBK.Select(c => new ChannelData
                        {
                            channelID = c.CHANNEL_CODE,
                            description = c.CHANNEL_NAME

                        }).OrderBy(d => d.description).ToList();
                    }
                    else
                    {
                        response.data = new List<ChannelData>();
                    }
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }
        public async Task<GetSubChannelResponse> GetSubChannel()
        {
            GetSubChannelResponse response = new GetSubChannelResponse();

            try
            {
                var channelData = await _repository.almStaging.GetSubChannel(c => c.IS_DELETED == 0);
                if (channelData.Any())
                {
                    response.data = channelData.Select(c => new SubChannelData
                    {
                        subChannelID = (int)c.SUB_CHANNEL_ID,
                        description = c.SUB_CHANNEL_NAME,
                        channelID = (int)c.CHANNEL_ID

                    }).OrderBy(d => d.subChannelID).ToList();
                }
                else
                {
                    var channelDataBK = await _repository.almStaging.GetSubChannelBK(c => c.IS_DELETED == 0);
                    if (channelDataBK.Any())
                    {
                        response.data = channelDataBK.Select(c => new SubChannelData
                        {
                            subChannelID = (int)c.SUB_CHANNEL_ID,
                            description = c.SUB_CHANNEL_NAME,
                            channelID = (int)c.CHANNEL_ID

                        }).OrderBy(d => d.subChannelID).ToList();
                    }
                    else
                    {
                        response.data = new List<SubChannelData>();
                    }

                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }
        public async Task<GetBusinessUnitResponse> GetBusinessUnit()
        {
            GetBusinessUnitResponse response = new GetBusinessUnitResponse();

            try
            {
                var businessUnitData = await _repository.ebaoLSStaging.GetBusinessUnit();
                if (businessUnitData.Any())
                {
                    response.data = businessUnitData.Select(c => new BusinessUnitData
                    {
                        businessUnitID = c.BUSINESS_UNIT,
                        description = c.UNIT_DESC

                    }).OrderBy(d => d.businessUnitID).ToList();
                }
                else
                {
                    var businessUnitDataBK = await _repository.ebaoLSStaging.GetBusinessUnitBK();

                    if (businessUnitDataBK.Any())
                    {
                        response.data = businessUnitDataBK.Select(c => new BusinessUnitData
                        {
                            businessUnitID = c.BUSINESS_UNIT,
                            description = c.UNIT_DESC

                        }).OrderBy(d => d.businessUnitID).ToList();
                    }
                    else
                    {
                        response.data = new List<BusinessUnitData>();
                    }
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }
        public async Task<GetProductCategoryResponse> GetProductCategoryList()
        {
            GetProductCategoryResponse response = new GetProductCategoryResponse();

            try
            {
                var productCategoryData = await _repository.memoMapping.GetProductCategoryList();
                if (productCategoryData.Any())
                {
                    response.data = productCategoryData.Select(c => new ProductCategoryData
                    {
                        description = c.description

                    }).OrderBy(d => d.description).ToList();
                }
                else
                {
                    response.data = new List<ProductCategoryData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }
        public async Task<GetProductCategoryResponse> FindProductCategoryByDescription(string param)
        {
            GetProductCategoryResponse response = new GetProductCategoryResponse();

            try
            {
                var productCategoryData = await _repository.memoMapping.FindProductCategoryByDescription(param);
                if (productCategoryData.Any())
                {
                    response.data = productCategoryData.Select(c => new ProductCategoryData
                    {
                        productCategoryID = c.description,
                        description = c.description

                    }).OrderBy(d => d.description).ToList();
                }
                else
                {
                    response.data = new List<ProductCategoryData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetProductCategoryResponse> GetProductCategoryForReport()
        {
            GetProductCategoryResponse response = new GetProductCategoryResponse();

            try
            {
                List<string> productForReport = new List<string>() { "Individual", "MRTA", "SECAP", "Srisawad", "Group 1 Year Term", "PA" };
                var productCategoryData = await _repository.memoMapping.GetProductCategoryList();
                if (productCategoryData.Any())
                {
                    response.data = productCategoryData.Where(e => productForReport.Contains(e.description)).Select(c => new ProductCategoryData
                    {
                        productCategoryID = c.description,
                        description = c.description

                    }).OrderBy(d => d.description).ToList();
                }
                else
                {
                    response.data = new List<ProductCategoryData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetSourceOfBusinessResponse> GetSourceOfBusiness()
        {
            GetSourceOfBusinessResponse response = new GetSourceOfBusinessResponse();

            try
            {
                var sourceOfBusinessData = await _repository.ebaoLSStaging.GetSourceOfBusiness();
                if (sourceOfBusinessData.Any())
                {
                    response.data = sourceOfBusinessData.Select(c => new SourceOfBusinessData
                    {
                        sourceOfBusinesID = c.BUSINESS_SOURCE,
                        description = c.SOURCE_DESC

                    }).OrderBy(d => d.sourceOfBusinesID).ToList();
                }
                else
                {
                    var sourceOfBusinessDataBK = await _repository.ebaoLSStaging.GetSourceOfBusinessBK();

                    if (sourceOfBusinessDataBK.Any())
                    {
                        response.data = sourceOfBusinessDataBK.Select(c => new SourceOfBusinessData
                        {
                            sourceOfBusinesID = c.BUSINESS_SOURCE,
                            description = c.SOURCE_DESC

                        }).OrderBy(d => d.sourceOfBusinesID).ToList();
                    }
                    else
                    {
                        response.data = new List<SourceOfBusinessData>();
                    }
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetRuleManagementResponse> GetRuleManagementList()
        {
            GetRuleManagementResponse response = new GetRuleManagementResponse();

            try
            {
                var ruleManagementData = await _repository.memoMapping.GetRuleManagementList();

                if (ruleManagementData.Any())
                {
                    foreach (var itemRuleManagement in ruleManagementData)
                    {
                        var productCategoryList = await _repository.memoMapping.GetRuleManagementProductCategory(itemRuleManagement.ruleManagementID);
                        itemRuleManagement.productCategory = productCategoryList.Select(c => c.ProductCategory).ToList();
                    }
                }

                response.data = ruleManagementData;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> SaveRuleManagement(SaveRuleManagementRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                TMRuleManagement newRuleManagement = null;
                bool updateSuccess = false;

                if (!request.ruleManagementID.HasValue || request.ruleManagementID == 0)
                {
                    newRuleManagement = await _repository.memoMapping.CreateRuleManagement(request);
                    if (newRuleManagement != null)
                    {
                        request.ruleManagementID = newRuleManagement.ID;
                    }
                }
                else
                {
                    updateSuccess = await _repository.memoMapping.UpdateRuleManagement(request);
                }

                if (newRuleManagement != null || updateSuccess)
                {
                    List<TMRuleManagementProductCategory> ruleManagementProductCategoryList = new List<TMRuleManagementProductCategory>();

                    await _repository.memoMapping.DeleteRuleManagementProductCategory(request.ruleManagementID.GetValueOrDefault());

                    if (request.productCategory != null && request.productCategory.Count != 0)
                    {
                        foreach (var item in request.productCategory)
                        {
                            TMRuleManagementProductCategory ruleManagementProductCategory = new TMRuleManagementProductCategory
                            {
                                RuleManagement_ID = request.ruleManagementID.GetValueOrDefault(),
                                ProductCategory = item,
                                Created_By = request.userID,
                                Created_Date = Utility.GetDateNowThai()
                            };

                            ruleManagementProductCategoryList.Add(ruleManagementProductCategory);
                        }

                        var saveRuleManagementProductCategory = await _repository.memoMapping.InsertRuleManagementProductCategory(ruleManagementProductCategoryList);

                        response.success = saveRuleManagementProductCategory;
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.OK).ToString(),
                            message = saveRuleManagementProductCategory ? "บันทึกข้อมูลสำเร็จ" : "บันทึกข้อมูล Product Categoryไม่สำเร็จ"
                        };
                    }
                    else
                    {
                        response.success = true;
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.OK).ToString(),
                            message = "บันทึกข้อมูลสำเร็จ"
                        };
                    }
                }
                else
                {
                    response.success = false;
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "บันทึกข้อมูลไม่สำเร็จ"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }
        public async Task<SaveDataResponse> DeleteRuleManagement(DeleteRuleManagementRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                bool saveResult = await _repository.memoMapping.DeleteRuleManagement(request);

                response.success = saveResult;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = saveResult ? "ลบ Rule Management สำเร็จ" : "ลบ Rule Management ไม่สำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        #endregion Rule Management
        #region Email Management
        public async Task<GetBranchOfficeResponse> GetBranchOffice(string param)
        {
            GetBranchOfficeResponse response = new GetBranchOfficeResponse();

            try
            {
                var branchOfficeDataData = await _repository.ebaoLSStaging.GetBranchOffice();
                if (branchOfficeDataData.Any())
                {
                    response.data = branchOfficeDataData.Where(d => d.BUSINESS_UNIT == "3").Select(c => new BranchOfficeData
                    {
                        branchOfficeID = c.BRANCH_CODE,
                        description = c.BRANCH_NAME

                    }).OrderBy(d => d.branchOfficeID).ToList();
                }
                else
                {
                    var branchOfficeDataBK = await _repository.ebaoLSStaging.GetBranchOfficeBK();
                    if (branchOfficeDataBK.Any())
                    {
                        response.data = branchOfficeDataBK.Where(d => d.BUSINESS_UNIT == "3").Select(c => new BranchOfficeData
                        {
                            branchOfficeID = c.BRANCH_CODE,
                            description = c.BRANCH_NAME

                        }).OrderBy(d => d.branchOfficeID).ToList();
                    }
                    else
                    {
                        response.data = new List<BranchOfficeData>();
                    }
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetBranchOfficeResponse> GetBranchOfficeByUnit(string businessUnitID)
        {
            GetBranchOfficeResponse response = new GetBranchOfficeResponse();

            try
            {
                var branchOfficeDataData = await _repository.eBaoMemoLetter.GetBranchBy(c => c.BUSINESS_UNIT == businessUnitID);
                if (branchOfficeDataData.Any())
                {
                    response.data = branchOfficeDataData.Select(c => new BranchOfficeData
                    {
                        branchOfficeID = c.BRANCH_CODE,
                        description = c.BRANCH_NAME

                    }).OrderBy(d => d.branchOfficeID).ToList();
                }
                else
                {
                    response.data = new List<BranchOfficeData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetSaleGroupResponse> GetSaleGroup()
        {
            GetSaleGroupResponse response = new GetSaleGroupResponse();

            try
            {
                var saleGroupData = await _repository.memoMapping.GetSaleGroup(c => c.Flag_Active == true);
                if (saleGroupData.Any())
                {
                    response.data = saleGroupData.Select(c => new SaleGroupData
                    {
                        ID = c.ID,
                        saleGroupName = c.SaleGroup_Name

                    }).OrderBy(d => d.ID).ToList();
                }
                else
                {
                    response.data = new List<SaleGroupData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetUnderwritingGroupResponse> GetUnderwritingGroup()
        {
            GetUnderwritingGroupResponse response = new GetUnderwritingGroupResponse();

            try
            {
                var underwritingGroupData = await _repository.memoMapping.GetUnderwritingGroup(c => c.Flag_Active == true);
                if (underwritingGroupData.Any())
                {
                    response.data = underwritingGroupData.Select(c => new UnderwritingGroupData
                    {
                        ID = c.ID,
                        underwritingGroupName = c.UnderwritingGroup_Name

                    }).OrderBy(d => d.ID).ToList();
                }
                else
                {
                    response.data = new List<UnderwritingGroupData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetGroupEmailManagementResponse> GetGroupEmailManagementList()
        {
            GetGroupEmailManagementResponse response = new GetGroupEmailManagementResponse();

            try
            {
                var dataContent = await _repository.memoMapping.GetGroupEmailManagementList();

                if (dataContent.Any())
                {
                    var groupEmailIDList = dataContent.Select(c => c.ID);
                    var emailManagementList = await _repository.memoMapping.GetEmailManagementListBy(c => groupEmailIDList.Contains(c.GroupEmailManagement_ID));

                    response.data = dataContent.Select(c => new GroupEmailManagementData
                    {
                        groupEmailManagementID = c.ID,
                        emailType = c.EmailType,
                        branchOffice = c.BranchOffice,
                        branchOfficeID = c.BranchOffice_ID,
                        businessUnit = c.BusinessUnit,
                        businessUnitID = c.BusinessUnit_ID,
                        status = c.Status,
                        sendGroup = c.SendGroup,
                        toEmail = emailManagementList.Where(d => d.GroupEmailManagement_ID == c.ID && d.TO_CC == "TO").Select(e => e.EmailAddress).ToList(),
                        ccEmail = emailManagementList.Where(d => d.GroupEmailManagement_ID == c.ID && d.TO_CC == "CC").Select(e => e.EmailAddress).ToList()
                    }).ToList();
                }
                else
                {
                    response.data = new List<GroupEmailManagementData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> SaveGroupEmailManagement(SaveGroupEmailManagementRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                TMGroupEmailManagement newGroupEmailManagement = null;
                bool updateSuccess = false;

                if (!request.ID.HasValue || request.ID == 0)
                {
                    newGroupEmailManagement = await _repository.memoMapping.CreateGroupEmailManagement(request);
                    if (newGroupEmailManagement != null)
                    {
                        request.ID = newGroupEmailManagement.ID;
                    }
                }
                else
                {
                    updateSuccess = await _repository.memoMapping.UpdateGroupEmailManagement(request);
                }

                if (newGroupEmailManagement != null || updateSuccess)
                {
                    List<TMEmailManagement> emailManagementList = new List<TMEmailManagement>();

                    await _repository.memoMapping.DeleteEmailManagement(request.ID.GetValueOrDefault());

                    if (request.groupEmailList != null && request.groupEmailList.Count != 0)
                    {
                        foreach (var item in request.groupEmailList)
                        {
                            TMEmailManagement emailManagement = new TMEmailManagement
                            {
                                GroupEmailManagement_ID = request.ID.GetValueOrDefault(),
                                EmailAddress = item.emailAddress,
                                TO_CC = item.to_cc,
                                Created_By = request.userID,
                                Created_Date = Utility.GetDateNowThai()
                            };

                            emailManagementList.Add(emailManagement);
                        }

                        var saveEmailManagement = await _repository.memoMapping.InsertEmailManagement(emailManagementList);

                        response.success = saveEmailManagement;
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.OK).ToString(),
                            message = saveEmailManagement ? "บันทึกข้อมูลสำเร็จ" : "บันทึกข้อมูล List Group email ไม่สำเร็จ"
                        };
                    }
                    else
                    {
                        response.success = true;
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.OK).ToString(),
                            message = "บันทึกข้อมูลสำเร็จ"
                        };
                    }
                }
                else
                {
                    response.success = false;
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "บันทึกข้อมูลไม่สำเร็จ"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        #endregion Email Management
    }
}
