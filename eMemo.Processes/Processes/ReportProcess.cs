﻿using ClosedXML.Excel;
using eMemo.Helper;
using eMemo.Model.Entity;
using eMemo.Model.Entity.eBaoTemp;
using eMemo.Model.Entity.TGH_EMEMO_DM;
using eMemo.Model.ExternalRequest;
using eMemo.Model.Request.Report;
using eMemo.Model.Response.Report;
using eMemo.Repositories;
using Newtonsoft.Json;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace eMemo.Processes.Processes
{
    public class ReportProcess
    {
        private readonly Repository _repository;
        private AppSettingHelper appSettingHelper;
        private readonly string currencyFormat = "#,##0";
        private readonly string percentFormat = "#0\\.00%";
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(ReportProcess));
        public ReportProcess(Repository repository)
        {
            _repository = repository;
            appSettingHelper = new AppSettingHelper();
        }

        public async Task<GenerateReportResponse> GetReport15DayAfterMemoPrinted(Report15DayAfterPrintedRequest request, bool preview)
        {
            GenerateReportResponse response = new GenerateReportResponse();

            try
            {
                var ebaoStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var ebaoStatusPrinted = ebaoStatusList.FirstOrDefault(c => c.EBaoMemoStatus == "Printed");
                var ebaoStatusIssued = ebaoStatusList.FirstOrDefault(c => c.EBaoMemoStatus == "Issued");

                var dateNow = Utility.GetDateNowThai();

                List<TTHMemoTransaction> dataReport = new List<TTHMemoTransaction>();

                if (request.memoPrintedDateTo.HasValue)
                {
                    dataReport = await _repository.memoTransaction.GetMemoTransactionBy(
                         c => c.TransactionDate.Date >= request.memoPrintedDateFrom.Date
                         && c.TransactionDate.Date <= request.memoPrintedDateTo.GetValueOrDefault().Date
                         && (c.EBaoMemoStatus_ID == ebaoStatusPrinted.ID || c.EBaoMemoStatus_ID == ebaoStatusIssued.ID));
                }
                else
                {
                    dataReport = await _repository.memoTransaction.GetMemoTransactionBy(
                        c => c.TransactionDate.Date == request.memoPrintedDateFrom.Date
                         && (c.EBaoMemoStatus_ID == ebaoStatusPrinted.ID || c.EBaoMemoStatus_ID == ebaoStatusIssued.ID));
                }

                List<TTHMemoTransaction> dataFilter = new List<TTHMemoTransaction>();

                foreach (var itemData in dataReport.OrderBy(c => c.TransactionDate))
                {
                    var diffDate = (dateNow.Date - itemData.TransactionDate.Date).TotalDays;

                    if (request.condition == ">")
                    {
                        if (diffDate > request.printedDay)
                        {
                            dataFilter.Add(itemData);
                        }
                    }
                    else if (request.condition == "<")
                    {
                        if (diffDate < request.printedDay)
                        {
                            dataFilter.Add(itemData);
                        }
                    }
                    else if (request.condition == "=")
                    {
                        if (diffDate == request.printedDay)
                        {
                            dataFilter.Add(itemData);
                        }
                    }
                }

                if (dataFilter.Any())
                {
                    var listTransactionID = dataFilter.Select(s => s.ID);
                    var listDetail = _repository.memoTransaction.GetMemoTransactionDetailBy(
                        c => listTransactionID.Contains(c.MemoTransaction_ID));

                    var listDetailPending = listDetail.Where(c => string.IsNullOrWhiteSpace(c.LetterStatus));

                    var listTransactionIDPending = listDetailPending.Select(c => c.MemoTransaction_ID).Distinct();

                    dataFilter = dataFilter.Where(c => listTransactionIDPending.Contains(c.ID)).ToList();

                    var policyCode = dataFilter.Select(e => e.PolicyNo).ToList();
                    var policyStatusList = await _repository.eBaoMemoLetter.GetPolicyStatus(policyCode);

                    (byte[] fileContent, string filePreview) = GenerateReport15DayAfterPrinted(request, dataFilter, listDetail, policyStatusList, preview);

                    response.fileContent = fileContent;
                    response.filePreview = filePreview;
                    response.success = true;
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "Generate Report Success"
                    };
                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GenerateReportResponse> GetReportSendingMemo(SendingMemoReportRequest request, bool preview)
        {
            GenerateReportResponse response = new GenerateReportResponse();

            try
            {
                var allDeliveryStatus = _repository.memoTransaction.GetAllDeliveryStatus();
                var deliveryStatus = allDeliveryStatus.FirstOrDefault(c => c.ID == request.deliveryStatus);

                List<Transaction_ViewHistory> dataReport = new List<Transaction_ViewHistory>();
                if (request.memoPrintedDateTo.HasValue)
                {
                    dataReport = await _repository.memoTransaction.GetTransactionHistoryReport(
                  c => c.TransactionDate >= request.memoPrintedDateFrom
                  && c.TransactionDate <= request.memoPrintedDateTo
                  && c.Send_Status == deliveryStatus.DeliveryStatus
                  && (string.IsNullOrWhiteSpace(request.resendNeed) ||
                     (!string.IsNullOrWhiteSpace(request.resendNeed) && c.ResendNeeded == request.resendNeed)));
                }
                else
                {
                    dataReport = await _repository.memoTransaction.GetTransactionHistoryReport(
                  c => c.TransactionDate == request.memoPrintedDateFrom
                  && c.Send_Status == deliveryStatus.DeliveryStatus
                  && (string.IsNullOrWhiteSpace(request.resendNeed) ||
                     (!string.IsNullOrWhiteSpace(request.resendNeed) && c.ResendNeeded == request.resendNeed)));
                }

                if (dataReport.Any())
                {
                    var groupByPolicyAndAgentcode = dataReport.GroupBy(c => new { c.PolicyNo }).Select(e => new
                    {
                        PolicyNo = e.Key.PolicyNo
                    }).ToList();

                    List<T_DM_EMEMO_AGENT_INFO_NEW> listAgentInfo = new List<T_DM_EMEMO_AGENT_INFO_NEW>();

                    var policyCodeList = dataReport.Select(c => "'" + c.PolicyNo + "'").Distinct().ToList();
                    listAgentInfo = await _repository.tghEmemo.GetAdditionalDataByPolicyNoList(policyCodeList);

                    var policyCode = dataReport.Select(e => e.PolicyNo).ToList();
                    var policyStatusList = await _repository.eBaoMemoLetter.GetPolicyStatus(policyCode);

                    List<T_CONTRACT_MASTER> contractMaster = new List<T_CONTRACT_MASTER>();
                    List<T_CONTRACT_PRODUCT> contractProduct = new List<T_CONTRACT_PRODUCT>();
                    List<T_CONTRACT_PROPOSAL> contractProposal = new List<T_CONTRACT_PROPOSAL>();

                    contractMaster = await _repository.eBaoMemoLetter.GetContractMasterByPolicyCode(policyCodeList);

                    if (contractMaster.Any())
                    {
                        var policyIDList = contractMaster.Select(d => d.POLICY_ID).ToList();
                        contractProduct = await _repository.eBaoMemoLetter.GetContractProductByPolicyID(policyIDList);
                        contractProposal = await _repository.eBaoMemoLetter.GetContractProposalByPolicyID(policyIDList);
                    }

                    (byte[] fileContent, string filePreview) = GenerateReportSendingMemo(request, dataReport, contractMaster, contractProduct, contractProposal, policyStatusList, preview, listAgentInfo);

                    response.fileContent = fileContent;
                    response.filePreview = filePreview;
                    response.success = true;
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "Generate Report Success"
                    };
                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GenerateReportResponse> GetReportPending(PendingReportRequest request, bool preview)
        {
            GenerateReportResponse response = new GenerateReportResponse();

            try
            {
                Stopwatch sw = Stopwatch.StartNew();

                var pendingDataAllSubmitTask = _repository.eBaoMemoLetter.GetPendingDataAllSubmit(request);
                var pendingDataOtherTask = _repository.eBaoMemoLetter.GetPendingDataOtherData(request);
                var pendingDataConditionAcceptTask = _repository.eBaoMemoLetter.GetPendingDataConditionAccept(request);
                var pendingDataAcceptTask = _repository.eBaoMemoLetter.GetPendingDataAccept(request);
                var pendingDataUnderwritingTask = _repository.eBaoMemoLetter.GetPendingDataUnderwritingInprogress(request);
                var pendingDataUpdateAtTask = _repository.eBaoMemoLetter.GetPendingDataUpdateAt(request);
                var pendingDataUpdateLiabilityAtTask = _repository.eBaoMemoLetter.GetPendingDataUpdateLiabilityAt(request);

                await Task.WhenAll(pendingDataAllSubmitTask, pendingDataOtherTask, pendingDataConditionAcceptTask, pendingDataAcceptTask, pendingDataUnderwritingTask, pendingDataUpdateAtTask, pendingDataUpdateLiabilityAtTask);

                var pendingDataAllSubmit = pendingDataAllSubmitTask.Result;
                var pendingDataOther = pendingDataOtherTask.Result;
                var pendingDataConditionAccept = pendingDataConditionAcceptTask.Result;
                var pendingDataAccept = pendingDataAcceptTask.Result;
                var pendingDataUnderwriting = pendingDataUnderwritingTask.Result;
                var pendingDataUpdateAt = pendingDataUpdateAtTask.Result;
                var pendingDataUpdateLiabilityAt = pendingDataUpdateLiabilityAtTask.Result;
                sw.Stop();

                List<Pending_Rawdata> allTransaction = new List<Pending_Rawdata>();
                allTransaction.AddRange(pendingDataAllSubmit);
                allTransaction.AddRange(pendingDataOther);
                allTransaction.AddRange(pendingDataConditionAccept);
                allTransaction.AddRange(pendingDataAccept);
                allTransaction.AddRange(pendingDataUnderwriting);
                allTransaction.AddRange(pendingDataUpdateAt);
                allTransaction.AddRange(pendingDataUpdateLiabilityAt);

                if (allTransaction.Any())
                {
                    pendingDataAllSubmit.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));
                    pendingDataOther.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));
                    pendingDataConditionAccept.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));
                    pendingDataAccept.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));
                    pendingDataUnderwriting.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));
                    pendingDataUpdateAt.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));
                    pendingDataUpdateLiabilityAt.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));

                    if (!string.IsNullOrWhiteSpace(request.productCategory))
                    {
                        pendingDataAllSubmit = pendingDataAllSubmit.Where(c => c.productCategory == request.productCategory).ToList();
                        pendingDataOther = pendingDataOther.Where(c => c.productCategory == request.productCategory).ToList();
                        pendingDataConditionAccept = pendingDataConditionAccept.Where(c => c.productCategory == request.productCategory).ToList();
                        pendingDataAccept = pendingDataAccept.Where(c => c.productCategory == request.productCategory).ToList();
                        pendingDataUnderwriting = pendingDataUnderwriting.Where(c => c.productCategory == request.productCategory).ToList();
                        pendingDataUpdateAt = pendingDataUpdateAt.Where(c => c.productCategory == request.productCategory).ToList();
                        pendingDataUpdateLiabilityAt = pendingDataUpdateLiabilityAt.Where(c => c.productCategory == request.productCategory).ToList();
                    }

                    Stopwatch sw1 = Stopwatch.StartNew();
                    List<string> policyCodeList = new List<string>();

                    policyCodeList.AddRange(pendingDataAllSubmit.Select(c => c.POLICY_CODE).Distinct());
                    policyCodeList.AddRange(pendingDataUnderwriting.Select(c => c.POLICY_CODE).Distinct());
                    policyCodeList.AddRange(pendingDataConditionAccept.Select(c => c.POLICY_CODE).Distinct());
                    policyCodeList.AddRange(pendingDataAccept.Select(c => c.POLICY_CODE).Distinct());

                    var listTransaction = await _repository.memoTransaction.GetTransactionReportByPolicy(policyCodeList);

                    //listTransaction = listTransaction.Where(c => policyCodeList.Contains(c.PolicyNo)).ToList();

                    pendingDataAllSubmit = pendingDataAllSubmit.GroupBy(c => c.POLICY_CODE).Select(e => e.FirstOrDefault()).ToList();
                    pendingDataOther = pendingDataOther.GroupBy(c => c.POLICY_CODE).Select(e => e.FirstOrDefault()).ToList();
                    pendingDataConditionAccept = pendingDataConditionAccept.GroupBy(c => c.POLICY_CODE).Select(e => e.FirstOrDefault()).ToList();
                    pendingDataAccept = pendingDataAccept.GroupBy(c => c.POLICY_CODE).Select(e => e.FirstOrDefault()).ToList();
                    pendingDataUnderwriting = pendingDataUnderwriting.GroupBy(c => c.POLICY_CODE).Select(e => e.FirstOrDefault()).ToList();
                    pendingDataUpdateAt = pendingDataUpdateAt.GroupBy(c => c.POLICY_CODE).Select(e => e.FirstOrDefault()).ToList();
                    pendingDataUpdateLiabilityAt = pendingDataUpdateLiabilityAt.GroupBy(c => c.POLICY_CODE).Select(e => e.FirstOrDefault()).ToList();

                    (byte[] fileContent, string filePreview) = await GenerateReportPending(request, listTransaction, pendingDataAllSubmit, pendingDataOther, pendingDataConditionAccept,
                        pendingDataAccept, pendingDataUnderwriting, pendingDataUpdateAt, pendingDataUpdateLiabilityAt, preview);

                    sw1.Stop();
                    response.fileContent = fileContent;
                    response.filePreview = filePreview;
                    response.success = true;
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "Generate Report Success"
                    };

                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                var exjson = JsonConvert.SerializeObject(ex);

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = exjson
                };
            }

            return response;
        }

        public async Task<GenerateReportResponse> GetReportPendingSummary(PendingSummaryReportRequest request, bool preview)
        {
            GenerateReportResponse response = new GenerateReportResponse();

            try
            {
                List<Pending_Summary_Rawdata> listContractMaster = new List<Pending_Summary_Rawdata>();

                listContractMaster = await _repository.eBaoMemoLetter.GetPendingSummaryData(request.submissionDateFrom, request.submissionDateTo);

                if (listContractMaster.Any())
                {

                    listContractMaster.ForEach(c => c.productCategory = Utility.CheckProductCategory(c.POLICY_CODE, c.unit_desc));
                    if (!string.IsNullOrWhiteSpace(request.productCategory))
                    {
                        listContractMaster = listContractMaster.Where(c => c.productCategory == request.productCategory).ToList();
                    }

                    if (listContractMaster.Any())
                    {
                        List<Transaction_View> listDataReport = new List<Transaction_View>();
                        List<T_CONTRACT_PRODUCT> contractProduct = new List<T_CONTRACT_PRODUCT>();
                        List<T_CONTRACT_PROPOSAL> contractProposal = new List<T_CONTRACT_PROPOSAL>();
                        List<T_PENDING_CAUSE_POLICY> pendingCauseList = new List<T_PENDING_CAUSE_POLICY>();
                        List<T_DM_EMEMO_AGENT_INFO> agentInfoData = new List<T_DM_EMEMO_AGENT_INFO>();
                        List<T_DM_EMEMO_AGENT_INFO_NEW> listAgentInfo = new List<T_DM_EMEMO_AGENT_INFO_NEW>();

                        var policyCodeList = listContractMaster.Select(c => c.POLICY_CODE).Distinct().ToList();
                        var policyCodeQuery = listContractMaster.Select(c => "'" + c.POLICY_CODE + "'").Distinct().ToList();
                        listAgentInfo = await _repository.tghEmemo.GetAdditionalDataByPolicyNoList(policyCodeQuery);
                        //foreach (var itemGroupData in policyCodeQuery)
                        //{
                        //    var agentInfo = _repository.tghEmemo.GetAdditionalDataByPolicyNo(itemGroupData);

                        //    if (agentInfo != null)
                        //    {
                        //        listAgentInfo.Add(agentInfo);
                        //    }
                        //}

                        listDataReport = await _repository.memoTransaction.GetTransactionReport(
                            c => policyCodeList.Contains(c.PolicyNo));

                        var agentCodeList = listDataReport.Select(c => c.AgentCode).Distinct().ToList();

                        agentInfoData = _repository.tghEmemo.GetAgentInfoBy(c => agentCodeList.Contains(c.AGENT_CODE));

                        var policyIDList = listContractMaster.Select(d => d.POLICY_ID).ToList();
                        contractProposal = await _repository.eBaoMemoLetter.GetContractProposalByPolicyID(policyIDList);

                        var listTransactionID = listDataReport.Select(s => s.TransactionID).Distinct();
                        var listHistory = await _repository.memoTransaction.FindMemoTransactionHistoryBy(c => listTransactionID.Contains(c.MemoTransaction_ID));

                        (byte[] fileContent, string filePreview) = await GenerateReportPendingSummary(request, listDataReport, contractProposal, listContractMaster, listHistory, listAgentInfo, agentInfoData, preview);

                        response.fileContent = fileContent;
                        response.filePreview = filePreview;
                        response.success = true;
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.OK).ToString(),
                            message = "Generate Report Success"
                        };
                    }
                    else
                    {
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.NotFound).ToString(),
                            message = "ไม่พบข้อมูล"
                        };
                    }
                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        private (byte[], string) GenerateReport15DayAfterPrinted(Report15DayAfterPrintedRequest request, List<TTHMemoTransaction> listTransaction,
            List<TTDMemoTransactionDetail> listTransactionDetail, List<PolicyStatus> listPolicyStatus, bool preview)
        {
            System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

            using (var workbook = new XLWorkbook())
            {
                Color columnHead = Color.FromArgb(77, 121, 242);
                XLColor columnHeadXL = XLColor.FromArgb(columnHead.A, columnHead.R, columnHead.G, columnHead.B);

                var worksheet = workbook.Worksheets.Add("Report15DayAfterPrinted");

                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 13)).Merge();
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 13)).Merge();
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 13)).Merge();
                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 13)).Style.Font.Bold = true;
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 13)).Style.Font.Bold = true;
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 13)).Style.Font.Bold = true;
                worksheet.Range(worksheet.Cell(4, 1), worksheet.Cell(4, 13)).Merge();

                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 13)).Value = "บริษัท อาคเนย์ประกันชีวิต จำกัด (มหาชน)";
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 13)).Value = "15 Days after memo printed report สำหรับฝ่ายรับประกัน";
                string printDateTo = request.memoPrintedDateTo.HasValue ? request.memoPrintedDateTo.GetValueOrDefault().ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty;
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 12)).Value = $"Memo Printed Date From {request.memoPrintedDateFrom.ToString("dd/MM/yyyy", _cultureTHInfo)} to Memo Printed Date {printDateTo}";

                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 13)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 13)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 13)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(1, 14).SetValue($"วันที่พิมพ์ : {DateTime.Now.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                worksheet.Cell(2, 14).SetValue($"เวลาที่พิมพ์ : {DateTime.Now.ToString("HH:mm", _cultureTHInfo)}");
                worksheet.Cell(3, 14).SetValue($"หน้าที่ : 1/1");

                worksheet.Range(worksheet.Cell(5, 1), worksheet.Cell(5, 14)).Style.Fill.BackgroundColor = columnHeadXL;
                worksheet.Cell(5, 1).Value = "No.";
                worksheet.Cell(5, 2).Value = "Policy No.";
                worksheet.Cell(5, 3).Value = "Proposal Date";
                worksheet.Cell(5, 4).Value = "Life Assured Name";
                worksheet.Cell(5, 5).Value = "Life Assured Age at Entry";
                worksheet.Cell(5, 6).Value = "Proposer Name";
                worksheet.Cell(5, 7).Value = "Memo printed";
                worksheet.Cell(5, 8).Value = "Memo User name (last)";
                worksheet.Cell(5, 9).Value = "Last Reply Date&Time (Last)";
                worksheet.Cell(5, 10).Value = "Reply User name (Last)";
                worksheet.Cell(5, 11).Value = $"{request.printedDay} days from Memo printed date";
                worksheet.Cell(5, 12).Value = "30 days from proposal date";
                worksheet.Cell(5, 13).Value = "Policy Status";
                worksheet.Cell(5, 14).Value = "Address Detail";

                worksheet.Cell(5, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 14).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(5, 1).Style.Font.Bold = true;
                worksheet.Cell(5, 2).Style.Font.Bold = true;
                worksheet.Cell(5, 3).Style.Font.Bold = true;
                worksheet.Cell(5, 4).Style.Font.Bold = true;
                worksheet.Cell(5, 5).Style.Font.Bold = true;
                worksheet.Cell(5, 6).Style.Font.Bold = true;
                worksheet.Cell(5, 7).Style.Font.Bold = true;
                worksheet.Cell(5, 8).Style.Font.Bold = true;
                worksheet.Cell(5, 9).Style.Font.Bold = true;
                worksheet.Cell(5, 10).Style.Font.Bold = true;
                worksheet.Cell(5, 11).Style.Font.Bold = true;
                worksheet.Cell(5, 12).Style.Font.Bold = true;
                worksheet.Cell(5, 13).Style.Font.Bold = true;
                worksheet.Cell(5, 14).Style.Font.Bold = true;

                worksheet.Cell(5, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 9).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 12).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 13).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 14).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Column(1).Width = 10;
                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 22;
                worksheet.Column(4).Width = 40;
                worksheet.Column(5).Width = 22;
                worksheet.Column(6).Width = 40;
                worksheet.Column(7).Width = 22;
                worksheet.Column(8).Width = 22;
                worksheet.Column(9).Width = 25;
                worksheet.Column(10).Width = 22;
                worksheet.Column(11).Width = 28;
                worksheet.Column(12).Width = 28;
                worksheet.Column(13).Width = 22;
                worksheet.Column(14).Width = 90;

                int startRow = 6;
                for (int i = 0; i < listTransaction.Count; i++)
                {
                    var itemData = listTransaction[i];
                    var itemDetail = listTransactionDetail.Where(c => c.MemoTransaction_ID == itemData.ID);
                    var lastedReply = itemDetail.OrderByDescending(e => e.Reply_Date).FirstOrDefault(c => c.Reply_Date != null);

                    DateTime date15DayAfterPrint = itemData.TransactionDate.AddDays(request.printedDay);
                    if (lastedReply != null)
                    {
                        date15DayAfterPrint = lastedReply.Reply_Date.Value.AddDays(request.printedDay);
                    }

                    string policyStauts = itemData.PolicyStatus;
                    var policyStatusData = listPolicyStatus.FirstOrDefault(c => c.policy_code == itemData.PolicyNo);

                    if (policyStatusData != null)
                    {
                        policyStauts = policyStatusData.status_desc;
                    }

                    worksheet.Cell(startRow, 1).Value = i + 1;
                    worksheet.Cell(startRow, 2).SetValue(itemData.PolicyNo);
                    worksheet.Cell(startRow, 3).SetValue(itemData.ProposalDate.ToString("dd/MM/yyyy"));
                    worksheet.Cell(startRow, 4).SetValue($"{itemData.LifeAssuredName}");
                    worksheet.Cell(startRow, 5).SetValue($"{itemData.EntryAgeLifeAssured} ปี");
                    worksheet.Cell(startRow, 6).Value = itemData.PolicyName;
                    worksheet.Cell(startRow, 7).SetValue(itemData.TransactionDate.ToString("dd/MM/yyyy", _cultureTHInfo));
                    worksheet.Cell(startRow, 8).Value = !string.IsNullOrWhiteSpace(itemData.Updated_By) ? itemData.Updated_By : itemData.Created_By;
                    worksheet.Cell(startRow, 9).SetValue(lastedReply != null ? lastedReply.Reply_Date.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : "");
                    worksheet.Cell(startRow, 10).Value = lastedReply != null ? lastedReply.Reply_By : "";
                    worksheet.Cell(startRow, 11).SetValue(date15DayAfterPrint.ToString("dd/MM/yyyy", _cultureTHInfo));
                    worksheet.Cell(startRow, 12).SetValue(itemData.ProposalDate.AddDays(30).ToString("dd/MM/yyyy"));
                    worksheet.Cell(startRow, 13).Value = policyStauts;

                    itemData.LAAddress = itemData.LAAddress != null ? itemData.LAAddress.Replace("|", "") : string.Empty;
                    worksheet.Cell(startRow, 14).Value = itemData.LAAddress;

                    worksheet.Cell(startRow, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 9).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 12).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 13).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(startRow, 14).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                    worksheet.Cell(startRow, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(startRow, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheet.Cell(startRow, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheet.Cell(startRow, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(startRow, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(startRow, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(startRow, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(startRow, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(startRow, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(startRow, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    startRow++;
                }


                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    string htmlBody = string.Empty;

                    if (preview)
                    {
                        Workbook workbookC = new Workbook();
                        workbookC.LoadFromStream(stream);
                        Worksheet sheet = workbookC.Worksheets[0];

                        string fileSave = $"Report1{Guid.NewGuid()}";

                        sheet.SaveToHtml(fileSave);

                        string excelHtmlPath = Path.GetFullPath(Path.Combine(fileSave));
                        using (StreamReader reader = File.OpenText(excelHtmlPath))
                        {
                            htmlBody = reader.ReadToEnd();
                        }

                        var regex = new Regex(@"<[hH][2][^>]*>[^<]*</[hH][2]\s*>", RegexOptions.Compiled | RegexOptions.Multiline);
                        htmlBody = regex.Replace(htmlBody, "");

                        File.Delete(excelHtmlPath);
                    }

                    return (content, htmlBody);
                }
            }
        }

        private (byte[], string) GenerateReportSendingMemo(SendingMemoReportRequest request, List<Transaction_ViewHistory> listTransaction,
            List<T_CONTRACT_MASTER> contractMasterList, List<T_CONTRACT_PRODUCT> contractProductList, List<T_CONTRACT_PROPOSAL> contractProposalList,
            List<PolicyStatus> policyStatusList, bool preview, List<T_DM_EMEMO_AGENT_INFO_NEW> listAgentInfo)
        {
            System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

            using (var workbook = new XLWorkbook())
            {
                Color columnHead = Color.FromArgb(77, 121, 242);
                XLColor columnHeadXL = XLColor.FromArgb(columnHead.A, columnHead.R, columnHead.G, columnHead.B);

                var worksheet = workbook.Worksheets.Add("ReportSendingMemo");

                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 19)).Merge();
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 19)).Merge();
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 19)).Merge();
                worksheet.Range(worksheet.Cell(4, 1), worksheet.Cell(4, 19)).Merge();
                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 19)).Style.Font.Bold = true;
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 19)).Style.Font.Bold = true;
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 19)).Style.Font.Bold = true;

                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 19)).Value = "บริษัท อาคเนย์ประกันชีวิต จำกัด(มหาชน)";
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 19)).Value = "Sending Memo Report : สำหรับฝ่ายรับประกัน";
                string printDateTo = request.memoPrintedDateTo.HasValue ? request.memoPrintedDateTo.GetValueOrDefault().ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty;
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 19)).Value = $"Memo Printed Date From {request.memoPrintedDateFrom.ToString("dd/MM/yyyy", _cultureTHInfo)} to Memo Printed Date {printDateTo}";

                worksheet.Range(worksheet.Cell(1, 1), worksheet.Cell(1, 19)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(2, 19)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(3, 19)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);


                worksheet.Cell(1, 20).SetValue($"วันที่พิมพ์ : {DateTime.Now.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                worksheet.Cell(2, 20).SetValue($"เวลาที่พิมพ์ : {DateTime.Now.ToString("HH:mm", _cultureTHInfo)}");
                worksheet.Cell(3, 20).SetValue($"หน้าที่ : 1/1");

                worksheet.Range(worksheet.Cell(5, 1), worksheet.Cell(5, 20)).Style.Fill.BackgroundColor = columnHeadXL;
                worksheet.Cell(5, 1).Value = "No.";
                worksheet.Cell(5, 2).Value = "Policy No.";
                worksheet.Cell(5, 3).Value = "Life Assured Name";
                worksheet.Cell(5, 4).Value = "Life Assured Age at Entry";
                worksheet.Cell(5, 5).Value = "Proposer Name";
                worksheet.Cell(5, 6).Value = "Submit Date";
                worksheet.Cell(5, 7).Value = "Memo Date & Time";
                worksheet.Cell(5, 8).Value = "Policy Status";
                worksheet.Cell(5, 9).Value = "Auto Memo Sending (Y/N)";
                worksheet.Cell(5, 10).Value = "Date & Time";
                worksheet.Cell(5, 11).Value = "Phone No.";
                worksheet.Cell(5, 12).Value = "Email";
                worksheet.Cell(5, 13).Value = "Resend Needes";
                worksheet.Cell(5, 14).Value = "Servicing Branch";
                worksheet.Cell(5, 15).Value = "Application Branch";
                worksheet.Cell(5, 16).Value = "Branch Code";
                worksheet.Cell(5, 17).Value = "Agent Code";
                worksheet.Cell(5, 18).Value = "Issue Agent";
                worksheet.Cell(5, 19).Value = "Prospect Producer";
                worksheet.Cell(5, 20).Value = "Sending Totals";

                worksheet.Cell(5, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 14).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 15).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 16).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 18).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 19).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(5, 1).Style.Font.Bold = true;
                worksheet.Cell(5, 2).Style.Font.Bold = true;
                worksheet.Cell(5, 3).Style.Font.Bold = true;
                worksheet.Cell(5, 4).Style.Font.Bold = true;
                worksheet.Cell(5, 5).Style.Font.Bold = true;
                worksheet.Cell(5, 6).Style.Font.Bold = true;
                worksheet.Cell(5, 7).Style.Font.Bold = true;
                worksheet.Cell(5, 8).Style.Font.Bold = true;
                worksheet.Cell(5, 9).Style.Font.Bold = true;
                worksheet.Cell(5, 10).Style.Font.Bold = true;
                worksheet.Cell(5, 11).Style.Font.Bold = true;
                worksheet.Cell(5, 12).Style.Font.Bold = true;
                worksheet.Cell(5, 13).Style.Font.Bold = true;
                worksheet.Cell(5, 14).Style.Font.Bold = true;
                worksheet.Cell(5, 15).Style.Font.Bold = true;
                worksheet.Cell(5, 16).Style.Font.Bold = true;
                worksheet.Cell(5, 17).Style.Font.Bold = true;
                worksheet.Cell(5, 18).Style.Font.Bold = true;
                worksheet.Cell(5, 19).Style.Font.Bold = true;
                worksheet.Cell(5, 20).Style.Font.Bold = true;

                worksheet.Cell(5, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 9).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 12).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 13).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 14).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 15).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 16).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 17).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 18).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 19).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 20).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Column(2).Width = 15;
                worksheet.Column(3).Width = 22;
                worksheet.Column(4).Width = 22;
                worksheet.Column(5).Width = 30;
                worksheet.Column(6).Width = 22;
                worksheet.Column(7).Width = 22;
                worksheet.Column(8).Width = 22;
                worksheet.Column(9).Width = 25;
                worksheet.Column(10).Width = 22;
                worksheet.Column(11).Width = 22;
                worksheet.Column(12).Width = 25;
                worksheet.Column(13).Width = 22;
                worksheet.Column(14).Width = 22;
                worksheet.Column(15).Width = 22;
                worksheet.Column(16).Width = 11;
                worksheet.Column(17).Width = 11;
                worksheet.Column(18).Width = 22;
                worksheet.Column(19).Width = 22;
                worksheet.Column(20).Width = 22;

                int startRow = 6;
                var dataGroup = listTransaction.OrderByDescending(c => c.Send_DateTime).GroupBy(e => e.PolicyNo).ToList();
                int index = 1;

                for (int i = 0; i < dataGroup.Count; i++)
                {
                    var itemGroup = dataGroup[i];

                    var contractMaster = contractMasterList.FirstOrDefault(c => c.POLICY_CODE == itemGroup.Key);
                    var contractProduct = contractProductList.FirstOrDefault(c => c.POLICY_ID == contractMaster?.POLICY_ID);
                    var contractProposal = contractProposalList.FirstOrDefault(c => c.POLICY_ID == contractMaster?.POLICY_ID);

                    if (request.showLatest)
                    {
                        string sms = string.Empty;
                        string email = string.Empty;

                        var itemData = itemGroup.OrderByDescending(e => e.Send_DateTime).FirstOrDefault();
                        var policyStatusData = policyStatusList.FirstOrDefault(c => c.policy_code == itemData.PolicyNo);
                        if (policyStatusData != null)
                        {
                            itemData.PolicyStatus = policyStatusData.status_desc;
                        }

                        if (itemData.ChannelSend == "SMS")
                        {
                            sms = itemData.Description;
                        }
                        else if (itemData.ChannelSend == "E-Mail")
                        {
                            email = itemData.Description;
                        }

                        var agentInfo = listAgentInfo.FirstOrDefault(c => c.POLICY_CODE == itemData.PolicyNo);

                        worksheet.Cell(startRow, 1).Value = i + 1;
                        worksheet.Cell(startRow, 2).SetValue(itemData.PolicyNo);
                        worksheet.Cell(startRow, 3).Value = itemData.LifeAssuredName;
                        worksheet.Cell(startRow, 4).SetValue($"{itemData.EntryAgeLifeAssured} ปี");
                        worksheet.Cell(startRow, 5).SetValue(itemData.PolicyName);
                        worksheet.Cell(startRow, 6).SetValue(contractMaster != null && contractMaster.SUBMISSION_DATE.HasValue ? contractMaster.SUBMISSION_DATE.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                        worksheet.Cell(startRow, 7).SetValue(itemData.TransactionDate.ToString("dd/MM/yyyy", _cultureTHInfo));
                        worksheet.Cell(startRow, 8).Value = itemData.PolicyStatus;
                        worksheet.Cell(startRow, 9).Value = itemData.AutoMemoSending;
                        worksheet.Cell(startRow, 10).SetValue(itemData.Send_DateTime.HasValue ? itemData.Send_DateTime.Value.ToString("dd/MM/yyyy HH:mm", _cultureTHInfo) + " น." : "");
                        worksheet.Cell(startRow, 11).SetValue(sms);
                        worksheet.Cell(startRow, 12).SetValue(email);
                        worksheet.Cell(startRow, 13).Value = string.IsNullOrWhiteSpace(itemData.ResendNeeded) ? "-" : itemData.ResendNeeded;
                        worksheet.Cell(startRow, 14).Value = itemData.ServicingBranch;
                        worksheet.Cell(startRow, 15).Value = agentInfo != null ? agentInfo.APPLICATION_BRANCH_NAME : string.Empty;
                        worksheet.Cell(startRow, 16).Value = itemData.BranchCode; // BranchCode
                        worksheet.Cell(startRow, 17).Value = itemData.AgentCode;
                        worksheet.Cell(startRow, 18).Value = contractProduct != null ? contractProduct.ISSUE_AGENT : string.Empty; // Issue Agent select p.issue_agent from t_contract_product p
                        worksheet.Cell(startRow, 19).Value = contractProposal != null ? contractProposal.PROSPECT_PRODUCER : string.Empty; // Prospect select p.prospect_producer  from t_contract_proposal p;
                        worksheet.Cell(startRow, 20).Value = itemGroup.Count(); // Sending Total

                        worksheet.Cell(startRow, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 9).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 12).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 13).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 14).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 15).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 16).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 17).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 18).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 19).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(startRow, 20).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                        worksheet.Cell(startRow, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 16).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(startRow, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        startRow++;
                    }
                    else
                    {
                        foreach (var itemData in itemGroup.OrderByDescending(e => e.Send_DateTime))
                        {
                            string sms = string.Empty;
                            string email = string.Empty;

                            if (itemData.ChannelSend == "SMS")
                            {
                                sms = itemData.Description;
                            }
                            else if (itemData.ChannelSend == "E-Mail")
                            {
                                email = itemData.Description;
                            }

                            var policyStatusData = policyStatusList.FirstOrDefault(c => c.policy_code == itemData.PolicyNo);
                            if (policyStatusData != null)
                            {
                                itemData.PolicyStatus = policyStatusData.status_desc;
                            }

                            var agentInfo = listAgentInfo.FirstOrDefault(c => c.POLICY_CODE == itemData.PolicyNo);
                            worksheet.Cell(startRow, 1).Value = index;
                            worksheet.Cell(startRow, 2).SetValue(itemData.PolicyNo);
                            worksheet.Cell(startRow, 3).Value = itemData.LifeAssuredName;
                            worksheet.Cell(startRow, 4).SetValue($"{itemData.EntryAgeLifeAssured} ปี");
                            worksheet.Cell(startRow, 5).SetValue(itemData.PolicyName);
                            worksheet.Cell(startRow, 6).SetValue(contractMaster != null && contractMaster.SUBMISSION_DATE.HasValue ? contractMaster.SUBMISSION_DATE.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                            worksheet.Cell(startRow, 7).SetValue(itemData.TransactionDate.ToString("dd/MM/yyyy", _cultureTHInfo));
                            worksheet.Cell(startRow, 8).Value = itemData.PolicyStatus;
                            worksheet.Cell(startRow, 9).Value = itemData.AutoMemoSending;
                            worksheet.Cell(startRow, 10).SetValue(itemData.Send_DateTime.HasValue ? itemData.Send_DateTime.Value.ToString("dd/MM/yyyy HH:mm", _cultureTHInfo) + " น." : "");
                            worksheet.Cell(startRow, 11).SetValue(sms);
                            worksheet.Cell(startRow, 12).SetValue(email);
                            worksheet.Cell(startRow, 13).Value = string.IsNullOrWhiteSpace(itemData.ResendNeeded) ? "-" : itemData.ResendNeeded;
                            worksheet.Cell(startRow, 14).Value = itemData.ServicingBranch;
                            worksheet.Cell(startRow, 15).Value = agentInfo != null ? agentInfo.APPLICATION_BRANCH_NAME : string.Empty;
                            worksheet.Cell(startRow, 16).Value = itemData.BranchCode; // BranchCode
                            worksheet.Cell(startRow, 17).Value = itemData.AgentCode;
                            worksheet.Cell(startRow, 18).Value = contractProduct != null ? contractProduct.ISSUE_AGENT : string.Empty; // Issue Agent select p.issue_agent from t_contract_product p
                            worksheet.Cell(startRow, 19).Value = contractProposal != null ? contractProposal.PROSPECT_PRODUCER : string.Empty; // Prospect select p.prospect_producer  from t_contract_proposal p;
                            worksheet.Cell(startRow, 20).Value = itemGroup.Count(); // Sending Total

                            worksheet.Cell(startRow, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 9).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 12).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 13).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 14).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 15).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 16).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 17).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 18).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 19).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(startRow, 20).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                            worksheet.Cell(startRow, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 16).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheet.Cell(startRow, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            index++;
                            startRow++;
                        }
                    }

                }


                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    string htmlBody = string.Empty;

                    if (preview)
                    {
                        Workbook workbookC = new Workbook();
                        workbookC.LoadFromStream(stream);
                        Worksheet sheet = workbookC.Worksheets[0];

                        string fileSave = $"Report2{Guid.NewGuid()}";

                        sheet.SaveToHtml(fileSave);

                        string excelHtmlPath = Path.GetFullPath(Path.Combine(fileSave));
                        using (StreamReader reader = File.OpenText(excelHtmlPath))
                        {
                            htmlBody = reader.ReadToEnd();
                        }

                        var regex = new Regex(@"<[hH][2][^>]*>[^<]*</[hH][2]\s*>", RegexOptions.Compiled | RegexOptions.Multiline);
                        htmlBody = regex.Replace(htmlBody, "");

                        File.Delete(excelHtmlPath);
                    }

                    return (content, htmlBody);
                }
            }
        }

        private async Task<(byte[], string)> GenerateReportPending(PendingReportRequest request, List<Transaction_View> listData
            , List<Pending_Rawdata> pendingDataAllSubmit, List<Pending_Rawdata> pendingDataOther, List<Pending_Rawdata> pendingDataConditionAccept,
            List<Pending_Rawdata> pendingDataAccept, List<Pending_Rawdata> pendingDataUnderwriting, List<Pending_Rawdata> pendingDataUpdateAt,
            List<Pending_Rawdata> pendingDataUpdateLiabilityAt, bool preview)
        {
            System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

            using (var workbook = new XLWorkbook())
            {
                Color columnHead = Color.FromArgb(77, 121, 242);
                XLColor columnHeadXL = XLColor.FromArgb(columnHead.A, columnHead.R, columnHead.G, columnHead.B);

                #region Total

                #region Total Case

                int dataDuplicated = 0;
                List<Pending_Rawdata> dataDuplicatedList = new List<Pending_Rawdata>();
                var submittedData = pendingDataAllSubmit;

                List<Pending_Rawdata> issuedList = new List<Pending_Rawdata>();
                var submitIssue = pendingDataAllSubmit.Where(c => c.LIABILITY_STATE == 1);
                var pendingIssue = pendingDataUpdateLiabilityAt.Where(c => c.LIABILITY_STATE == 1);

                issuedList.AddRange(submitIssue);
                issuedList.AddRange(pendingIssue);

                var groupIssue = issuedList.GroupBy(c => c.POLICY_CODE);
                var issuedData = groupIssue.Select(c => c.FirstOrDefault());

                dataDuplicatedList.AddRange(groupIssue.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                dataDuplicated += groupIssue.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> declinePostponeList = new List<Pending_Rawdata>();
                var submitDecline = pendingDataAllSubmit.Where(c => c.PROPOSAL_STATUS == "82" || c.PROPOSAL_STATUS == "84");
                var pendingDecline = pendingDataUpdateAt.Where(c => c.PROPOSAL_STATUS == "82" || c.PROPOSAL_STATUS == "84");

                declinePostponeList.AddRange(submitDecline);
                declinePostponeList.AddRange(pendingDecline);

                var groupDecline = declinePostponeList.GroupBy(c => c.POLICY_CODE);
                dataDuplicatedList.AddRange(groupDecline.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                var declinePostponedData = groupDecline.Select(c => c.FirstOrDefault());

                dataDuplicated += groupDecline.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> withdrawList = new List<Pending_Rawdata>();
                var submitWithdraw = pendingDataAllSubmit.Where(c => c.PROPOSAL_STATUS == "86");
                var pendingwithdraw = pendingDataUpdateAt.Where(c => c.PROPOSAL_STATUS == "86");

                withdrawList.AddRange(submitWithdraw);
                withdrawList.AddRange(pendingwithdraw);

                var groupWithdraw = withdrawList.GroupBy(c => c.POLICY_CODE);
                dataDuplicatedList.AddRange(groupWithdraw.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                var withdrawData = groupWithdraw.Select(c => c.FirstOrDefault());

                dataDuplicated += groupWithdraw.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> terminateFreelookList = new List<Pending_Rawdata>();
                var submitTerminateF = pendingDataAllSubmit.Where(c => c.LIABILITY_STATE == 3 && c.END_CAUSE == 51);
                var pendingTerminateF = pendingDataUpdateLiabilityAt.Where(c => c.LIABILITY_STATE == 3 && c.END_CAUSE == 51);

                terminateFreelookList.AddRange(submitTerminateF);
                terminateFreelookList.AddRange(pendingTerminateF);

                var groupTerminateF = terminateFreelookList.GroupBy(c => c.POLICY_CODE);
                dataDuplicatedList.AddRange(groupTerminateF.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                var terminateFreelookData = groupTerminateF.Select(c => c.FirstOrDefault()); //Termination Reason = Financial Difficulty

                dataDuplicated += groupTerminateF.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> terminateOtherList = new List<Pending_Rawdata>();
                var submitTerminateOther = pendingDataAllSubmit.Where(c => c.LIABILITY_STATE == 3 && c.END_CAUSE != 51);
                var pendingTerminateOther = pendingDataUpdateLiabilityAt.Where(c => c.LIABILITY_STATE == 3 && c.END_CAUSE != 51);

                terminateOtherList.AddRange(submitTerminateOther);
                terminateOtherList.AddRange(pendingTerminateOther);

                var groupTerminate = terminateOtherList.GroupBy(c => c.POLICY_CODE);
                dataDuplicatedList.AddRange(groupTerminate.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                var terminateOtherData = groupTerminate.Select(c => c.FirstOrDefault()); // Termination Reason = ไมใช่่ Financial Difficulty       

                dataDuplicated += groupTerminate.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> OtherDataList = new List<Pending_Rawdata>();
                var submitOtherData = pendingDataAllSubmit.Where(c => c.PROPOSAL_STATUS == "10"
                            || c.PROPOSAL_STATUS == "11"
                            || c.PROPOSAL_STATUS == "20"
                            || c.PROPOSAL_STATUS == "21"
                            || c.PROPOSAL_STATUS == "31"
                            || c.PROPOSAL_STATUS == "40");

                OtherDataList.AddRange(pendingDataOther);
                OtherDataList.AddRange(submitOtherData);

                var groupOther = OtherDataList.GroupBy(c => c.POLICY_CODE);
                dataDuplicatedList.AddRange(groupOther.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                var otherData = groupOther.Select(c => c.FirstOrDefault());
                dataDuplicated += groupOther.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> conditionAcceptList = new List<Pending_Rawdata>();
                var submitConditionAccept = pendingDataAllSubmit.Where(c => c.PROPOSAL_STATUS == "81");

                conditionAcceptList.AddRange(pendingDataConditionAccept);
                conditionAcceptList.AddRange(submitConditionAccept);

                var groupConAccept = conditionAcceptList.GroupBy(c => c.POLICY_CODE);

                var dataDuplicate = groupConAccept.Where(e => e.Count() > 1).SelectMany(c => c).ToList();

                pendingDataConditionAccept = pendingDataConditionAccept.Where(e => !dataDuplicate.Select(c => c.POLICY_CODE).Contains(e.POLICY_CODE)).ToList();
                dataDuplicatedList.AddRange(dataDuplicate);
                var conditionAcceptedData = groupConAccept.Select(c => c.FirstOrDefault());

                dataDuplicated += groupConAccept.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> acceptList = new List<Pending_Rawdata>();
                var submitAccept = pendingDataAllSubmit.Where(c => c.PROPOSAL_STATUS == "80");

                acceptList.AddRange(pendingDataAccept);
                acceptList.AddRange(submitAccept);

                var groupAccept = acceptList.GroupBy(c => c.POLICY_CODE);
                dataDuplicatedList.AddRange(groupAccept.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                var acceptedData = groupAccept.Select(c => c.FirstOrDefault());
                dataDuplicated += groupAccept.Where(e => e.Count() > 1).Count();

                List<Pending_Rawdata> totalPendingList = new List<Pending_Rawdata>();
                var submitPending = pendingDataAllSubmit.Where(c => c.PROPOSAL_STATUS == "32");

                totalPendingList.AddRange(pendingDataUnderwriting);
                totalPendingList.AddRange(submitPending);

                var groupPending = totalPendingList.GroupBy(c => c.POLICY_CODE);
                dataDuplicatedList.AddRange(groupPending.Where(e => e.Count() > 1).SelectMany(c => c).ToList());
                var totalPendingData = groupPending.Select(c => c.FirstOrDefault());//Pending Reason = Y & N
                dataDuplicated += groupPending.Where(e => e.Count() > 1).Count();

                var worksheet = workbook.Worksheets.Add("Total");

                var policyDuplicate = dataDuplicatedList.Select(c => c.POLICY_CODE).Distinct();
                List<Pending_Rawdata> prevPendingList = new List<Pending_Rawdata>();
                prevPendingList.AddRange(pendingDataOther);
                prevPendingList.AddRange(pendingDataConditionAccept);
                prevPendingList.AddRange(pendingDataAccept);
                prevPendingList.AddRange(pendingDataUnderwriting);
                prevPendingList.AddRange(pendingIssue);
                prevPendingList.AddRange(pendingDecline);
                prevPendingList.AddRange(pendingwithdraw);
                prevPendingList.AddRange(pendingTerminateF);
                prevPendingList.AddRange(pendingTerminateOther);

                var prevPendingData = prevPendingList.Where(c => !policyDuplicate.Contains(c.POLICY_CODE) && c.SUBMISSION_DATE.GetValueOrDefault().Date != request.asOfDate.Date);

                int totalCase = issuedData.Count()
                    + declinePostponedData.Count() + withdrawData.Count() + terminateFreelookData.Count()
                    + terminateOtherData.Count() + otherData.Count() + conditionAcceptedData.Count()
                    + acceptedData.Count() + totalPendingData.Count();


                worksheet.Column(1).Width = 15;
                worksheet.Column(2).Width = 22;
                worksheet.Column(3).Width = 22;
                worksheet.Column(4).Width = 15;
                worksheet.Column(5).Width = 15;
                worksheet.Column(6).Width = 15;
                worksheet.Column(7).Width = 15;

                worksheet.Range(worksheet.Cell(1, 2), worksheet.Cell(1, 3)).Merge();
                worksheet.Range(worksheet.Cell(1, 2), worksheet.Cell(1, 3)).Value = $"Memo report  as of {request.asOfDate.ToString("dd/MM/yyyy", _cultureTHInfo)}";
                worksheet.Range(worksheet.Cell(1, 2), worksheet.Cell(1, 3)).Style.Font.Bold = true;

                worksheet.Range(worksheet.Cell(2, 2), worksheet.Cell(2, 3)).Merge();
                worksheet.Range(worksheet.Cell(2, 2), worksheet.Cell(2, 3)).Value = "";
                worksheet.Range(worksheet.Cell(2, 2), worksheet.Cell(2, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(2, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(2, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(2, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(2, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(2, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(2, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(2, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(2, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(2, 4).Style.Font.Bold = true;
                worksheet.Cell(2, 5).Style.Font.Bold = true;
                worksheet.Cell(2, 6).Style.Font.Bold = true;
                worksheet.Cell(2, 7).Style.Font.Bold = true;

                worksheet.Cell(2, 4).Value = "Case(s)";
                worksheet.Cell(2, 5).Value = "%Case";
                worksheet.Cell(2, 6).Value = "APE(THB.)";
                worksheet.Cell(2, 7).Value = "%APE";

                worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(3, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(3, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(3, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(3, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(3, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(3, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(3, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(3, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3, 3)).Merge();
                worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3, 3)).Value = "Prev. Pending";
                worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3, 3)).Style.Font.Bold = true;

                worksheet.Cell(3, 4).Value = prevPendingData.Count();
                worksheet.Cell(3, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(3, 4), worksheet.Cell(13, 4)).Style.NumberFormat.Format = currencyFormat;
                worksheet.Range(worksheet.Cell(3, 4), worksheet.Cell(13, 4)).DataType = XLDataType.Number;

                worksheet.Range(worksheet.Cell(3, 6), worksheet.Cell(13, 6)).Style.NumberFormat.Format = currencyFormat;
                worksheet.Range(worksheet.Cell(3, 6), worksheet.Cell(13, 6)).DataType = XLDataType.Number;

                worksheet.Range(worksheet.Cell(3, 7), worksheet.Cell(13, 7)).Style.NumberFormat.Format = percentFormat;
                worksheet.Range(worksheet.Cell(3, 7), worksheet.Cell(13, 7)).DataType = XLDataType.Number;

                var percentCasePrevPending = prevPendingData.Count() > 0 ?
                    Math.Round((prevPendingData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(3, 5).SetValue(percentCasePrevPending);
                worksheet.Cell(3, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(3, 5).DataType = XLDataType.Number;
                worksheet.Cell(3, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                decimal totalSumAPEPrev = 0;
                if (prevPendingData.Any())
                {
                    totalSumAPEPrev = prevPendingData.Sum(c => c.APE).GetValueOrDefault();
                }

                worksheet.Cell(3, 6).Value = totalSumAPEPrev;
                worksheet.Cell(3, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Range(worksheet.Cell(4, 2), worksheet.Cell(4, 3)).Merge();
                worksheet.Range(worksheet.Cell(4, 2), worksheet.Cell(4, 3)).Value = "Submitted";

                worksheet.Cell(4, 4).Value = submittedData.Count();

                worksheet.Range(worksheet.Cell(4, 2), worksheet.Cell(4, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(4, 2), worksheet.Cell(4, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range(worksheet.Cell(4, 2), worksheet.Cell(4, 3)).Style.Font.Bold = true;

                worksheet.Cell(4, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(4, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(4, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(4, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(4, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(4, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(4, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(4, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                var percentCaseSubmitted = submittedData.Count() > 0 ?
                   Math.Round((submittedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(4, 5).SetValue(percentCaseSubmitted);
                worksheet.Cell(4, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(4, 5).DataType = XLDataType.Number;

                decimal totalSumSubmitted = 0;
                if (submittedData.Any())
                {
                    totalSumSubmitted = submittedData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumSubmitted = totalSumSubmitted + sumAPE;

                    //}
                }

                worksheet.Cell(4, 6).Value = totalSumSubmitted;

                worksheet.Range(worksheet.Cell(5, 2), worksheet.Cell(5, 3)).Merge();
                worksheet.Range(worksheet.Cell(5, 2), worksheet.Cell(5, 3)).Value = "Issued";
                worksheet.Range(worksheet.Cell(5, 2), worksheet.Cell(5, 3)).Style.Font.Bold = true;

                worksheet.Cell(5, 4).Value = issuedData.Count();

                worksheet.Range(worksheet.Cell(5, 2), worksheet.Cell(5, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(5, 2), worksheet.Cell(5, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(5, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(5, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(5, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseIssue = issuedData.Count() > 0 ?
                    Math.Round((issuedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(5, 5).SetValue(percentCaseIssue);
                worksheet.Cell(5, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(5, 5).DataType = XLDataType.Number;

                decimal totalSumIssue = 0;
                if (issuedData.Any())
                {
                    var groupPolicyID = issuedData.GroupBy(c => c.POLICY_ID);
                    totalSumIssue = issuedData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumIssue = totalSumIssue + sumAPE;

                    //}
                }

                worksheet.Cell(5, 6).Value = totalSumIssue;

                worksheet.Range(worksheet.Cell(6, 2), worksheet.Cell(6, 3)).Merge();
                worksheet.Range(worksheet.Cell(6, 2), worksheet.Cell(6, 3)).Value = "Declined / Postponed";
                worksheet.Range(worksheet.Cell(6, 2), worksheet.Cell(6, 3)).Style.Font.Bold = true;

                worksheet.Cell(6, 4).Value = declinePostponedData.Count();

                worksheet.Range(worksheet.Cell(6, 2), worksheet.Cell(6, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(6, 2), worksheet.Cell(6, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(6, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(6, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(6, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(6, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(6, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(6, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(6, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(6, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseDecline = declinePostponedData.Count() > 0 ?
                   Math.Round((declinePostponedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(6, 5).SetValue(percentCaseDecline);
                worksheet.Cell(6, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(6, 5).DataType = XLDataType.Number;

                decimal totalSumDecline = 0;
                if (declinePostponedData.Any())
                {
                    totalSumDecline = declinePostponedData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumDecline = totalSumDecline + sumAPE;

                    //}
                }

                worksheet.Cell(6, 6).Value = totalSumDecline;

                worksheet.Range(worksheet.Cell(7, 2), worksheet.Cell(7, 3)).Merge();
                worksheet.Range(worksheet.Cell(7, 2), worksheet.Cell(7, 3)).Value = "Withdraw";
                worksheet.Range(worksheet.Cell(7, 2), worksheet.Cell(7, 3)).Style.Font.Bold = true;

                worksheet.Cell(7, 4).Value = withdrawData.Count();

                worksheet.Range(worksheet.Cell(7, 2), worksheet.Cell(7, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(7, 2), worksheet.Cell(7, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(7, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(7, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(7, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(7, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(7, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(7, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(7, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(7, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseWithdraw = withdrawData.Count() > 0 ?
                    Math.Round((withdrawData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(7, 5).SetValue(percentCaseWithdraw);
                worksheet.Cell(7, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(7, 5).DataType = XLDataType.Number;

                decimal totalSumWithdraw = 0;
                if (withdrawData.Any())
                {
                    totalSumWithdraw = withdrawData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumWithdraw = totalSumWithdraw + sumAPE;

                    //}
                }

                worksheet.Cell(7, 6).Value = totalSumWithdraw;

                worksheet.Range(worksheet.Cell(8, 2), worksheet.Cell(8, 3)).Merge();
                worksheet.Range(worksheet.Cell(8, 2), worksheet.Cell(8, 3)).Value = "Terminate Freelook";
                worksheet.Range(worksheet.Cell(8, 2), worksheet.Cell(8, 3)).Style.Font.Bold = true;

                worksheet.Cell(8, 4).Value = terminateFreelookData.Count();

                worksheet.Range(worksheet.Cell(8, 2), worksheet.Cell(8, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(8, 2), worksheet.Cell(8, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(8, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(8, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(8, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(8, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(8, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(8, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(8, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(8, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseTerminateF = terminateFreelookData.Count() > 0 ?
                 Math.Round((terminateFreelookData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(8, 5).SetValue(percentCaseTerminateF);
                worksheet.Cell(8, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(8, 5).DataType = XLDataType.Number;

                decimal totalSumTeminateF = 0;
                if (terminateFreelookData.Any())
                {
                    totalSumTeminateF = terminateFreelookData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumTeminateF = totalSumTeminateF + sumAPE;

                    //}
                }

                worksheet.Cell(8, 6).Value = totalSumTeminateF;

                worksheet.Range(worksheet.Cell(9, 2), worksheet.Cell(9, 3)).Merge();
                worksheet.Range(worksheet.Cell(9, 2), worksheet.Cell(9, 3)).Value = "Terminate";
                worksheet.Range(worksheet.Cell(9, 2), worksheet.Cell(9, 3)).Style.Font.Bold = true;

                worksheet.Cell(9, 4).Value = terminateOtherData.Count();

                worksheet.Range(worksheet.Cell(9, 2), worksheet.Cell(9, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(9, 2), worksheet.Cell(9, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(9, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(9, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(9, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(9, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(9, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(9, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(9, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(9, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseTerminateO = terminateOtherData.Count() > 0 ?
                 Math.Round((terminateOtherData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(9, 5).SetValue(percentCaseTerminateF);
                worksheet.Cell(9, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(9, 5).DataType = XLDataType.Number;

                decimal totalSumTeminateO = 0;
                if (terminateOtherData.Any())
                {
                    totalSumTeminateO = terminateOtherData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumTeminateO = totalSumTeminateO + sumAPE;
                    //}
                }

                worksheet.Cell(9, 6).Value = totalSumTeminateO;

                worksheet.Range(worksheet.Cell(10, 2), worksheet.Cell(10, 3)).Merge();
                worksheet.Range(worksheet.Cell(10, 2), worksheet.Cell(10, 3)).Value = "Other (data entry, Ver, waiting UW)";
                worksheet.Range(worksheet.Cell(10, 2), worksheet.Cell(10, 3)).Style.Font.Bold = true;

                worksheet.Cell(10, 4).Value = otherData.Count();

                worksheet.Range(worksheet.Cell(10, 2), worksheet.Cell(10, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(10, 2), worksheet.Cell(10, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(10, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(10, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(10, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(10, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(10, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(10, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(10, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(10, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseOtherData = otherData.Count() > 0 ?
                    Math.Round((otherData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(10, 5).SetValue(percentCaseOtherData);
                worksheet.Cell(10, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(10, 5).DataType = XLDataType.Number;

                decimal totalSumOtherData = 0;
                if (otherData.Any())
                {
                    totalSumOtherData = otherData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumOtherData = totalSumOtherData + sumAPE;
                    //}
                }

                worksheet.Cell(10, 6).Value = totalSumOtherData;

                worksheet.Range(worksheet.Cell(11, 2), worksheet.Cell(11, 3)).Merge();
                worksheet.Range(worksheet.Cell(11, 2), worksheet.Cell(11, 3)).Value = "Conditional Acceptance";
                worksheet.Range(worksheet.Cell(11, 2), worksheet.Cell(11, 3)).Style.Font.Bold = true;

                worksheet.Cell(11, 4).Value = conditionAcceptedData.Count();

                worksheet.Range(worksheet.Cell(11, 2), worksheet.Cell(11, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(11, 2), worksheet.Cell(11, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(11, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(11, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(11, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(11, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(11, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(11, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(11, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(11, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseConAccept = conditionAcceptedData.Count() > 0 ?
                    Math.Round((conditionAcceptedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(11, 5).SetValue(percentCaseConAccept);
                worksheet.Cell(11, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(11, 5).DataType = XLDataType.Number;

                decimal totalSumConAccept = 0;
                if (conditionAcceptedData.Any())
                {
                    totalSumConAccept = conditionAcceptedData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumConAccept = totalSumConAccept + sumAPE;
                    //}
                }

                worksheet.Cell(11, 6).Value = totalSumConAccept;

                worksheet.Range(worksheet.Cell(12, 2), worksheet.Cell(12, 3)).Merge();
                worksheet.Range(worksheet.Cell(12, 2), worksheet.Cell(12, 3)).Value = "Accepted (premium not paid)";
                worksheet.Range(worksheet.Cell(12, 2), worksheet.Cell(12, 3)).Style.Font.Bold = true;

                worksheet.Cell(12, 4).Value = acceptedData.Count();

                worksheet.Range(worksheet.Cell(12, 2), worksheet.Cell(12, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(12, 2), worksheet.Cell(12, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(12, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(12, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(12, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(12, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(12, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(12, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(12, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(12, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseAccept = acceptedData.Count() > 0 ?
                   Math.Round((acceptedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(12, 5).SetValue(percentCaseAccept);
                worksheet.Cell(12, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(12, 5).DataType = XLDataType.Number;

                decimal totalSumAccept = 0;
                if (acceptedData.Any())
                {
                    totalSumAccept = acceptedData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumAccept = totalSumAccept + sumAPE;
                    //}
                }

                worksheet.Cell(12, 6).Value = totalSumAccept;

                worksheet.Range(worksheet.Cell(13, 2), worksheet.Cell(13, 3)).Merge();
                worksheet.Range(worksheet.Cell(13, 2), worksheet.Cell(13, 3)).Value = "Underwriting in Progress ";
                worksheet.Range(worksheet.Cell(13, 2), worksheet.Cell(13, 3)).Style.Font.Bold = true;

                worksheet.Cell(13, 4).Value = totalPendingData.Count();

                worksheet.Range(worksheet.Cell(13, 2), worksheet.Cell(13, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                worksheet.Range(worksheet.Cell(13, 2), worksheet.Cell(13, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(13, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(13, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(13, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(13, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(13, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(13, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(13, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(13, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                var percentCaseTotal = totalPendingData.Count() > 0 ?
                        Math.Round((totalPendingData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(13, 5).SetValue(percentCaseTotal);
                worksheet.Cell(13, 5).Style.NumberFormat.Format = percentFormat;
                worksheet.Cell(13, 5).DataType = XLDataType.Number;

                decimal totalSumTotalPending = 0;
                if (totalPendingData.Any())
                {
                    totalSumTotalPending = totalPendingData.Sum(c => c.APE).GetValueOrDefault();
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }

                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumTotalPending = totalSumTotalPending + sumAPE;
                    //}
                }

                worksheet.Cell(13, 6).Value = totalSumTotalPending;

                decimal totalSumAPE = totalSumWithdraw + totalSumTotalPending + totalSumTeminateO + totalSumTeminateF
                    + totalSumOtherData + totalSumIssue + totalSumDecline + totalSumConAccept + totalSumAccept;

                var percentAPE1 = totalSumAPEPrev > 0 ?
                       Math.Round((totalSumAPEPrev / totalSumAPE) * 100, 2) : 0;

                var percentAPE2 = totalSumSubmitted > 0 ?
                        Math.Round((totalSumSubmitted / totalSumAPE) * 100, 2) : 0;

                var percentAPE3 = totalSumIssue > 0 ?
                       Math.Round((totalSumIssue / totalSumAPE) * 100, 2) : 0;

                var percentAPE4 = totalSumDecline > 0 ?
                       Math.Round((totalSumDecline / totalSumAPE) * 100, 2) : 0;

                var percentAPE5 = totalSumWithdraw > 0 ?
                       Math.Round((totalSumWithdraw / totalSumAPE) * 100, 2) : 0;

                var percentAPE6 = totalSumTeminateF > 0 ?
                       Math.Round((totalSumTeminateF / totalSumAPE) * 100, 2) : 0;

                var percentAPE7 = totalSumTeminateO > 0 ?
                       Math.Round((totalSumTeminateO / totalSumAPE) * 100, 2) : 0;

                var percentAPE8 = totalSumOtherData > 0 ?
                       Math.Round((totalSumOtherData / totalSumAPE) * 100, 2) : 0;

                var percentAPE9 = totalSumConAccept > 0 ?
                       Math.Round((totalSumConAccept / totalSumAPE) * 100, 2) : 0;

                var percentAPE10 = totalSumAccept > 0 ?
                       Math.Round((totalSumAccept / totalSumAPE) * 100, 2) : 0;

                var percentAPE11 = totalSumTotalPending > 0 ?
                       Math.Round((totalSumTotalPending / totalSumAPE) * 100, 2) : 0;

                worksheet.Cell(3, 7).Value = percentAPE1;
                worksheet.Cell(4, 7).Value = percentAPE2;
                worksheet.Cell(5, 7).Value = percentAPE3;
                worksheet.Cell(6, 7).Value = percentAPE4;
                worksheet.Cell(7, 7).Value = percentAPE5;
                worksheet.Cell(8, 7).Value = percentAPE6;
                worksheet.Cell(9, 7).Value = percentAPE7;
                worksheet.Cell(10, 7).Value = percentAPE8;
                worksheet.Cell(11, 7).Value = percentAPE9;
                worksheet.Cell(12, 7).Value = percentAPE10;
                worksheet.Cell(13, 7).Value = percentAPE11;
                #endregion


                var allPendingData = pendingDataUnderwriting;
                allPendingData.AddRange(submitPending);

                var groupByProduct = allPendingData.GroupBy(c => c.productCategory);
                var allPendingCode = await _repository.eBaoMemoLetter.GetPendingCode();
                allPendingCode = allPendingCode.OrderBy(c => c.PENDING_ID).ToList();

                worksheet.Range(worksheet.Cell(15, 2), worksheet.Cell(15, 7)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(15, 2), worksheet.Cell(15, 7)).Style.Font.Bold = true;

                worksheet.Cell(15, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range(worksheet.Cell(15, 3), worksheet.Cell(15, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(15, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(15, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(15, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(15, 2).Value = "Product";
                worksheet.Range(worksheet.Cell(15, 3), worksheet.Cell(15, 4)).Merge();
                worksheet.Range(worksheet.Cell(15, 3), worksheet.Cell(15, 4)).Value = "Pending Reason";
                worksheet.Cell(15, 5).Value = "Case(s)";
                worksheet.Cell(15, 6).Value = "API(Bath)";
                worksheet.Cell(15, 7).Value = "APE(Bath)";

                int RowProduct = 16;

                decimal grandTotalSumAPIProduct = 0;
                decimal grandTotalSumAPEProduct = 0;
                int grandTotalCaseProduct = 0;

                foreach (var itemProduct in groupByProduct)
                {
                    //var policyCodeinProduct = itemProduct.Select(c => c.POLICY_CODE).Distinct();
                    //var contractMasterInProduct = pendingDataUnderwriting.Where(c => policyCodeinProduct.Contains(c.POLICY_CODE)).Select(d => d.POLICY_ID);
                    //var pendingCauseInProduct = pendingDataUnderwriting.Where(e => contractMasterInProduct.Contains(e.POLICY_ID));

                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Merge();
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Style.Font.Bold = true;
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Value = itemProduct.Key;

                    // for Pending Reason
                    decimal totalSumAPIProduct = 0;
                    decimal totalSumAPEProduct = 0;
                    int totalCaseProduct = 0;

                    foreach (var itemPendingCode in allPendingCode)
                    {
                        var dataCase = itemProduct.Where(c => c.pending_id == itemPendingCode.PENDING_ID);

                        int countCase = dataCase.Count();
                        decimal sumAPIProduct = 0;
                        decimal sumAPEProduct = 0;

                        sumAPIProduct = dataCase.Sum(c => c.total_prem_af.GetValueOrDefault());
                        sumAPEProduct = dataCase.Sum(c => c.APE.GetValueOrDefault());

                        worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Merge();
                        worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Value = $"{itemPendingCode.PENDING_ID} {itemPendingCode.PEND_CAUSE}";
                        worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;


                        worksheet.Cell(RowProduct, 5).Value = countCase;
                        worksheet.Cell(RowProduct, 6).Value = sumAPIProduct;
                        worksheet.Cell(RowProduct, 7).Value = sumAPEProduct;

                        worksheet.Cell(RowProduct, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(RowProduct, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(RowProduct, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                        worksheet.Cell(RowProduct, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(RowProduct, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(RowProduct, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        worksheet.Cell(RowProduct, 5).Style.NumberFormat.Format = currencyFormat;
                        worksheet.Cell(RowProduct, 5).DataType = XLDataType.Number;

                        worksheet.Cell(RowProduct, 6).Style.NumberFormat.Format = currencyFormat;
                        worksheet.Cell(RowProduct, 6).DataType = XLDataType.Number;

                        worksheet.Cell(RowProduct, 7).Style.NumberFormat.Format = currencyFormat;
                        worksheet.Cell(RowProduct, 7).DataType = XLDataType.Number;

                        RowProduct++;
                        totalSumAPIProduct = totalSumAPIProduct + sumAPIProduct;
                        totalSumAPEProduct = totalSumAPEProduct + sumAPEProduct;
                        totalCaseProduct = totalCaseProduct + countCase;
                    }

                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Merge();
                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Font.Bold = true;
                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Value = "Total";
                    worksheet.Cell(RowProduct, 5).Value = totalCaseProduct;
                    worksheet.Cell(RowProduct, 6).Value = totalSumAPIProduct;
                    worksheet.Cell(RowProduct, 7).Value = totalSumAPEProduct;


                    worksheet.Cell(RowProduct, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(RowProduct, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(RowProduct, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                    worksheet.Cell(RowProduct, 5).Style.Font.Bold = true;
                    worksheet.Cell(RowProduct, 6).Style.Font.Bold = true;
                    worksheet.Cell(RowProduct, 7).Style.Font.Bold = true;

                    worksheet.Cell(RowProduct, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(RowProduct, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(RowProduct, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheet.Cell(RowProduct, 5).Style.NumberFormat.Format = currencyFormat;
                    worksheet.Cell(RowProduct, 5).DataType = XLDataType.Number;

                    worksheet.Cell(RowProduct, 6).Style.NumberFormat.Format = currencyFormat;
                    worksheet.Cell(RowProduct, 6).DataType = XLDataType.Number;

                    worksheet.Cell(RowProduct, 7).Style.NumberFormat.Format = currencyFormat;
                    worksheet.Cell(RowProduct, 7).DataType = XLDataType.Number;

                    RowProduct++;
                    grandTotalCaseProduct = grandTotalCaseProduct + totalCaseProduct;
                    grandTotalSumAPIProduct = grandTotalSumAPIProduct + totalSumAPIProduct;
                    grandTotalSumAPEProduct = grandTotalSumAPEProduct + totalSumAPEProduct;
                }

                worksheet.Cell(RowProduct, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Merge();
                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Value = "Grand Total";
                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Font.Bold = true;

                worksheet.Cell(RowProduct, 5).Value = grandTotalCaseProduct;
                worksheet.Cell(RowProduct, 6).Value = grandTotalSumAPIProduct;
                worksheet.Cell(RowProduct, 7).Value = grandTotalSumAPEProduct;


                worksheet.Cell(RowProduct, 5).Style.Font.Bold = true;
                worksheet.Cell(RowProduct, 6).Style.Font.Bold = true;
                worksheet.Cell(RowProduct, 7).Style.Font.Bold = true;

                worksheet.Cell(RowProduct, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(RowProduct, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(RowProduct, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(RowProduct, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(RowProduct, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(RowProduct, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(RowProduct, 5).Style.NumberFormat.Format = currencyFormat;
                worksheet.Cell(RowProduct, 5).DataType = XLDataType.Number;

                worksheet.Cell(RowProduct, 6).Style.NumberFormat.Format = currencyFormat;
                worksheet.Cell(RowProduct, 6).DataType = XLDataType.Number;

                worksheet.Cell(RowProduct, 7).Style.NumberFormat.Format = currencyFormat;
                worksheet.Cell(RowProduct, 7).DataType = XLDataType.Number;

                #endregion

                if (!preview)
                {
                    var documentList = listData.Select(c => c.DocumentNo).Distinct();
                    var agentCodeList = listData.Select(c => c.AgentCode).Distinct();
                    var policyCodeList = listData.Select(c => "'" + c.PolicyNo + "'").Distinct().ToList();

                    var documentJoinData = await _repository.eBaoMemoLetter.GetDocumentJoin(documentList.ToList());
                    var documentData = await _repository.eBaoMemoLetter.GetDocumentData(documentList.ToList());
                    var agentList = await _repository.eBaoMemoLetter.GetAgent(agentCodeList.ToList());
                    var agentInfoNew = await _repository.tghEmemo.GetAdditionalDataByPolicyNoList(policyCodeList);
                    var agentInfo = _repository.tghEmemo.GetAgentInfoBy(c => agentCodeList.Contains(c.AGENT_CODE));

                    List<DataNotInMemo> dataNotInMemoPending = new List<DataNotInMemo>();

                    var policyInTransaction = listData.Select(e => e.PolicyNo).Distinct();
                    var listDataNotInEmemoPending = totalPendingData.Where(c => !policyInTransaction.Contains(c.POLICY_CODE)).Select(e => e.POLICY_CODE);

                    if (listDataNotInEmemoPending.Any())
                    {
                        dataNotInMemoPending = await _repository.eBaoMemoLetter.GetDataNotInMemo(listDataNotInEmemoPending.ToList());
                        var documentListNotin = dataNotInMemoPending.Select(c => c.document_no).Distinct();
                        var documentJoinDataNotin = await _repository.eBaoMemoLetter.GetDocumentJoin(documentListNotin.ToList());
                        var documentDataNotin = await _repository.eBaoMemoLetter.GetDocumentData(documentListNotin.ToList());

                        var policyCodeListtNotin = dataNotInMemoPending.Select(c => "'" + c.POLICY_CODE + "'").Distinct().ToList();
                        var agentInfoNewNotin = await _repository.tghEmemo.GetAdditionalDataByPolicyNoList(policyCodeListtNotin);

                        var agentCodeListNotin = agentInfoNewNotin.Select(c => c.AGENT_CODE).Distinct();
                        var agentListNotin = await _repository.eBaoMemoLetter.GetAgent(agentCodeListNotin.ToList());
                        var agentInfoNotin = _repository.tghEmemo.GetAgentInfoBy(c => agentCodeListNotin.Contains(c.AGENT_CODE));

                        documentJoinData.AddRange(documentJoinDataNotin);
                        documentData.AddRange(documentDataNotin);
                        agentList.AddRange(agentListNotin);
                        agentInfoNew.AddRange(agentInfoNewNotin);
                        agentInfo.AddRange(agentInfoNotin);
                    }

                    #region Pending
                    var worksheetPending = workbook.Worksheets.Add("Pending");

                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 27)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 27)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 27)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(4, 1), worksheetPending.Cell(4, 27)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 27)).Style.Font.Bold = true;
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 27)).Style.Font.Bold = true;
                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 27)).Style.Font.Bold = true;

                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 27)).Value = "บริษัท อาคเนย์ประกันชีวิต จำกัด(มหาชน)";
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 27)).Value = "Pending Memo Details Reports : สำหรับฝ่ายรับประกัน";

                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 27)).Value = $"as of Date {request.asOfDate.ToString("dd/MM/yyyy", _cultureTHInfo)}";

                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 27)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 27)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 27)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);


                    worksheetPending.Cell(1, 28).SetValue($"วันที่พิมพ์ : {DateTime.Now.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                    worksheetPending.Cell(2, 28).SetValue($"เวลาที่พิมพ์ : {DateTime.Now.ToString("HH:mm", _cultureTHInfo)}");
                    worksheetPending.Cell(3, 28).SetValue($"หน้าที่ : 1/3");

                    worksheetPending.Range(worksheetPending.Cell(5, 1), worksheetPending.Cell(5, 28)).Style.Fill.BackgroundColor = columnHeadXL;

                    worksheetPending.Range(worksheetPending.Cell(5, 1), worksheetPending.Cell(5, 28)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetPending.Range(worksheetPending.Cell(5, 1), worksheetPending.Cell(5, 28)).Style.Font.Bold = true;

                    for (int i = 1; i <= 28; i++)
                    {
                        worksheetPending.Cell(5, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    }

                    worksheetPending.Cell(5, 1).Value = "Memo Duration";
                    worksheetPending.Cell(5, 2).Value = "Memo Printed Date";
                    worksheetPending.Cell(5, 3).Value = "Submission Date";
                    worksheetPending.Cell(5, 4).Value = "Application Duration";
                    worksheetPending.Cell(5, 5).Value = "Policy No.";
                    worksheetPending.Cell(5, 6).Value = "Proposer Name";
                    worksheetPending.Cell(5, 7).Value = "Life Assured Name";
                    worksheetPending.Cell(5, 8).Value = "Life Assured Age";
                    worksheetPending.Cell(5, 9).Value = "Policy Status";
                    worksheetPending.Cell(5, 10).Value = "NB Pending Reason";
                    worksheetPending.Cell(5, 11).Value = "Letter Type";
                    worksheetPending.Cell(5, 12).Value = "Memo Type";
                    worksheetPending.Cell(5, 13).Value = "Memo Code";
                    worksheetPending.Cell(5, 14).Value = "Memo Description";
                    worksheetPending.Cell(5, 15).Value = "Application Branch";
                    worksheetPending.Cell(5, 16).Value = "Service Branch";
                    worksheetPending.Cell(5, 17).Value = "Agent Region";
                    worksheetPending.Cell(5, 18).Value = "Group Name";
                    worksheetPending.Cell(5, 19).Value = "Agent Unit";
                    worksheetPending.Cell(5, 20).Value = "Agent Code";
                    worksheetPending.Cell(5, 21).Value = "Agent Name";
                    worksheetPending.Cell(5, 22).Value = "ตัวแทนมีสิทธิ NonMed แล้วแสดงค่า N";
                    worksheetPending.Cell(5, 23).Value = "Benefit Code";
                    worksheetPending.Cell(5, 24).Value = "Benefit Name";
                    worksheetPending.Cell(5, 25).Value = "Payment Frequency";
                    worksheetPending.Cell(5, 26).Value = "Premium (API)";
                    worksheetPending.Cell(5, 27).Value = "Premium APE";
                    worksheetPending.Cell(5, 28).Value = "Underwriting User (Last)";

                    worksheetPending.Column(1).Width = 15;
                    worksheetPending.Column(2).Width = 22;
                    worksheetPending.Column(3).Width = 15;
                    worksheetPending.Column(4).Width = 22;
                    worksheetPending.Column(5).Width = 15;
                    worksheetPending.Column(6).Width = 40;
                    worksheetPending.Column(7).Width = 30;
                    worksheetPending.Column(8).Width = 15;
                    worksheetPending.Column(9).Width = 25;
                    worksheetPending.Column(10).Width = 30;
                    worksheetPending.Column(11).Width = 22;
                    worksheetPending.Column(12).Width = 25;
                    worksheetPending.Column(13).Width = 25;
                    worksheetPending.Column(14).Width = 60;
                    worksheetPending.Column(15).Width = 25;
                    worksheetPending.Column(16).Width = 25;
                    worksheetPending.Column(17).Width = 22;
                    worksheetPending.Column(18).Width = 30;
                    worksheetPending.Column(19).Width = 30;
                    worksheetPending.Column(20).Width = 22;
                    worksheetPending.Column(21).Width = 30;
                    worksheetPending.Column(22).Width = 30;
                    worksheetPending.Column(23).Width = 20;
                    worksheetPending.Column(24).Width = 40;
                    worksheetPending.Column(25).Width = 20;
                    worksheetPending.Column(26).Width = 20;
                    worksheetPending.Column(27).Width = 20;
                    worksheetPending.Column(28).Width = 22;


                    int startRowDataPending = 6;

                    var groupDataPending = totalPendingData.GroupBy(c => c.POLICY_CODE);
                    foreach (var itemGroup in groupDataPending)
                    {
                        var itemPending = itemGroup.FirstOrDefault();
                        var agentInfoNewData = agentInfoNew.FirstOrDefault(c => c.POLICY_CODE == itemPending.POLICY_CODE);

                        var contractMasterData = totalPendingData.FirstOrDefault(c => c.POLICY_ID == itemPending.POLICY_ID);
                        var allTransactionData = listData.Where(c => c.PolicyNo == contractMasterData.POLICY_CODE);
                        var transactionData = allTransactionData.FirstOrDefault();

                        var submissionDate = contractMasterData.SUBMISSION_DATE;
                        double? applicationDuration = null;

                        if (submissionDate.HasValue)
                        {
                            applicationDuration = (request.asOfDate.Date - submissionDate.Value.Date).TotalDays;
                        }

                        var nbPending = contractMasterData?.PEND_CAUSE;

                        var sumAPI = contractMasterData.Institute_code == 1 ? contractMasterData.total_prem_af * 2 : contractMasterData.total_prem_af;

                        double? memoDuration = null;

                        var lastMemoPrinted = contractMasterData.CREATE_DATE;
                        if (lastMemoPrinted.HasValue)
                        {
                            memoDuration = (request.asOfDate.Date - lastMemoPrinted.GetValueOrDefault().Date).TotalDays;
                        }

                        var benefitCode = contractMasterData?.internal_id;
                        var benefitName = contractMasterData?.product_name;
                        var sumAPE = contractMasterData.APE;
                        var agentRegion = MappingAgentRegion(agentInfoNewData.BRANCH_CO_CODE);

                        if (allTransactionData.Any())
                        {
                            foreach (var itemTransaction in allTransactionData)
                            {
                                var agentInfoData = agentInfo.FirstOrDefault(c => c.AGENT_CODE == itemTransaction.AgentCode);
                                var letterType = documentJoinData.FirstOrDefault(c => c.document_no == itemTransaction.DocumentNo)?.template_name;
                                var medNonMed = agentList.FirstOrDefault(c => c.agent_code == itemTransaction.AgentCode)?.med_indi;

                                worksheetPending.Cell(startRowDataPending, 1).Value = memoDuration != null ? $"{memoDuration} วัน" : "";
                                worksheetPending.Cell(startRowDataPending, 2).SetValue($"{transactionData.TransactionDate.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                                worksheetPending.Cell(startRowDataPending, 3).SetValue(submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                                worksheetPending.Cell(startRowDataPending, 4).Value = applicationDuration != null ? $"{applicationDuration} วัน" : string.Empty;
                                worksheetPending.Cell(startRowDataPending, 5).SetValue(transactionData.PolicyNo);
                                worksheetPending.Cell(startRowDataPending, 6).Value = transactionData.PolicyName;
                                worksheetPending.Cell(startRowDataPending, 7).Value = $"{transactionData.LifeAssuredName}";
                                worksheetPending.Cell(startRowDataPending, 8).Value = $"{transactionData.EntryAgeLifeAssured} ปี";
                                worksheetPending.Cell(startRowDataPending, 9).Value = contractMasterData.STATUS_DESC;
                                worksheetPending.Cell(startRowDataPending, 10).Value = nbPending; // NB PEN
                                worksheetPending.Cell(startRowDataPending, 11).Value = letterType; // LETTER Type
                                worksheetPending.Cell(startRowDataPending, 12).Value = !string.IsNullOrWhiteSpace(itemTransaction.MemoType_Name) ? itemTransaction.MemoType_Name : itemTransaction.MemoType;
                                worksheetPending.Cell(startRowDataPending, 13).Value = itemTransaction.MemoCode;
                                worksheetPending.Cell(startRowDataPending, 14).Value = !string.IsNullOrWhiteSpace(itemTransaction.Description) ? itemTransaction.Description : itemTransaction.MemoCodeDescription;
                                worksheetPending.Cell(startRowDataPending, 15).Value = agentInfoNewData != null ? agentInfoNewData.APPLICATION_BRANCH_NAME : string.Empty;
                                worksheetPending.Cell(startRowDataPending, 16).Value = agentInfoNewData != null ? agentInfoNewData.BRANCH_NAME : string.Empty; //service
                                worksheetPending.Cell(startRowDataPending, 17).Value = agentRegion;
                                worksheetPending.Cell(startRowDataPending, 18).Value = agentInfoData != null ? agentInfoData.AGENT_GROUP : string.Empty;
                                worksheetPending.Cell(startRowDataPending, 19).Value = itemTransaction.AgentUnit;
                                worksheetPending.Cell(startRowDataPending, 20).Value = itemTransaction.AgentCode;
                                worksheetPending.Cell(startRowDataPending, 21).Value = itemTransaction.AgentName;
                                worksheetPending.Cell(startRowDataPending, 22).Value = medNonMed;
                                worksheetPending.Cell(startRowDataPending, 23).Value = benefitCode;
                                worksheetPending.Cell(startRowDataPending, 24).Value = benefitName;
                                worksheetPending.Cell(startRowDataPending, 25).Value = contractMasterData.RENEWAL_TYPE;
                                worksheetPending.Cell(startRowDataPending, 26).Value = sumAPI;
                                worksheetPending.Cell(startRowDataPending, 27).Value = sumAPE;
                                worksheetPending.Cell(startRowDataPending, 28).Value = contractMasterData.user_name;

                                worksheetPending.Cell(startRowDataPending, 26).Style.NumberFormat.Format = currencyFormat;
                                worksheetPending.Cell(startRowDataPending, 26).DataType = XLDataType.Number;
                                worksheetPending.Cell(startRowDataPending, 27).Style.NumberFormat.Format = currencyFormat;
                                worksheetPending.Cell(startRowDataPending, 27).DataType = XLDataType.Number;

                                for (int i = 1; i <= 28; i++)
                                {
                                    worksheetPending.Cell(startRowDataPending, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                }

                                worksheetPending.Cell(startRowDataPending, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                                worksheetPending.Cell(startRowDataPending, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 18).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                                worksheetPending.Cell(startRowDataPending, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 21).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                                worksheetPending.Cell(startRowDataPending, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 23).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                                worksheetPending.Cell(startRowDataPending, 25).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 28).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                startRowDataPending++;
                            }
                        }
                        else
                        {
                            var dataNotInMemoPolicy = dataNotInMemoPending.FirstOrDefault(c => c.POLICY_CODE == contractMasterData.POLICY_CODE);
                            memoDuration = (request.asOfDate.Date - dataNotInMemoPolicy.insert_time.Date).TotalDays;
                            var agentInfoData = agentInfo.FirstOrDefault(c => c.AGENT_CODE == agentInfoNewData?.AGENT_CODE);
                            var letterType = documentJoinData.FirstOrDefault(c => c.document_no == dataNotInMemoPolicy?.document_no)?.template_name;
                            var medNonMed = agentList.FirstOrDefault(c => c.agent_code == agentInfoNewData?.AGENT_CODE)?.med_indi;

                            worksheetPending.Cell(startRowDataPending, 1).Value = memoDuration != null ? $"{memoDuration} วัน" : "";
                            worksheetPending.Cell(startRowDataPending, 2).Value = dataNotInMemoPolicy != null ? $"{dataNotInMemoPolicy.insert_time.ToString("dd/MM/yyyy", _cultureTHInfo)}" : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 3).Value = submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 4).Value = applicationDuration != null ? $"{applicationDuration} วัน" : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 5).SetValue(itemPending?.POLICY_CODE);
                            worksheetPending.Cell(startRowDataPending, 6).Value = dataNotInMemoPolicy?.ProposerName;
                            worksheetPending.Cell(startRowDataPending, 7).Value = $"{dataNotInMemoPolicy?.LifeAssuredName}";
                            worksheetPending.Cell(startRowDataPending, 8).Value = dataNotInMemoPolicy?.LifeAssuredAge != null ? $"{dataNotInMemoPolicy.LifeAssuredAge} ปี" : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 9).Value = contractMasterData.STATUS_DESC;
                            worksheetPending.Cell(startRowDataPending, 10).Value = nbPending; // NB PEN
                            worksheetPending.Cell(startRowDataPending, 11).Value = letterType; // LETTER Type
                            worksheetPending.Cell(startRowDataPending, 12).Value = string.Empty;
                            worksheetPending.Cell(startRowDataPending, 13).Value = string.Empty;
                            worksheetPending.Cell(startRowDataPending, 14).Value = string.Empty;
                            worksheetPending.Cell(startRowDataPending, 15).Value = agentInfoNewData != null ? agentInfoNewData.APPLICATION_BRANCH_NAME : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 16).Value = agentInfoNewData != null ? agentInfoNewData.BRANCH_NAME : string.Empty; //service
                            worksheetPending.Cell(startRowDataPending, 17).Value = agentRegion;
                            worksheetPending.Cell(startRowDataPending, 18).Value = agentInfoData != null ? agentInfoData.AGENT_GROUP : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 19).Value = agentInfoNewData?.UNIT_NAME;
                            worksheetPending.Cell(startRowDataPending, 20).Value = agentInfoNewData?.AGENT_CODE;
                            worksheetPending.Cell(startRowDataPending, 21).Value = $"{agentInfoNewData?.FIRST_NAME} {agentInfoNewData?.LAST_NAME}";
                            worksheetPending.Cell(startRowDataPending, 22).Value = medNonMed;
                            worksheetPending.Cell(startRowDataPending, 23).Value = benefitCode;
                            worksheetPending.Cell(startRowDataPending, 24).Value = benefitName;
                            worksheetPending.Cell(startRowDataPending, 25).Value = contractMasterData.RENEWAL_TYPE;
                            worksheetPending.Cell(startRowDataPending, 26).Value = sumAPI;
                            worksheetPending.Cell(startRowDataPending, 27).Value = sumAPE;
                            worksheetPending.Cell(startRowDataPending, 28).Value = contractMasterData.user_name;

                            worksheetPending.Cell(startRowDataPending, 26).Style.NumberFormat.Format = currencyFormat;
                            worksheetPending.Cell(startRowDataPending, 26).DataType = XLDataType.Number;
                            worksheetPending.Cell(startRowDataPending, 27).Style.NumberFormat.Format = currencyFormat;
                            worksheetPending.Cell(startRowDataPending, 27).DataType = XLDataType.Number;

                            for (int i = 1; i <= 28; i++)
                            {
                                worksheetPending.Cell(startRowDataPending, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            }

                            worksheetPending.Cell(startRowDataPending, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                            worksheetPending.Cell(startRowDataPending, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 18).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                            worksheetPending.Cell(startRowDataPending, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 21).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                            worksheetPending.Cell(startRowDataPending, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 23).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                            worksheetPending.Cell(startRowDataPending, 25).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 28).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            startRowDataPending++;
                        }

                    }
                    #endregion

                    #region Condition Acceptance

                    List<DataNotInMemo> dataNotInMemoConAccept = new List<DataNotInMemo>();
                    var listDataNotInEmemoConAccept = conditionAcceptedData.Select(e => e.POLICY_CODE);

                    if (listDataNotInEmemoConAccept.Any())
                    {
                        dataNotInMemoConAccept = await _repository.eBaoMemoLetter.GetDataNotInMemo(listDataNotInEmemoConAccept.ToList());
                        var documentListNotin = dataNotInMemoConAccept.Select(c => c.document_no).Distinct();
                        var documentJoinDataNotin = await _repository.eBaoMemoLetter.GetDocumentJoin(documentListNotin.ToList());
                        //var documentDataNotin = await _repository.eBaoMemoLetter.GetDocumentData(documentListNotin.ToList());

                        var policyCodeListtNotin = dataNotInMemoConAccept.Select(c => "'" + c.POLICY_CODE + "'").Distinct().ToList();
                        var agentInfoNewNotin = await _repository.tghEmemo.GetAdditionalDataByPolicyNoList(policyCodeListtNotin);

                        var agentCodeListNotin = agentInfoNewNotin.Select(c => c.AGENT_CODE).Distinct();
                        var agentListNotin = await _repository.eBaoMemoLetter.GetAgent(agentCodeListNotin.ToList());
                        var agentInfoNotin = _repository.tghEmemo.GetAgentInfoBy(c => agentCodeListNotin.Contains(c.AGENT_CODE));

                        documentJoinData.AddRange(documentJoinDataNotin);
                        //documentData.AddRange(documentDataNotin);
                        agentList.AddRange(agentListNotin);
                        agentInfoNew.AddRange(agentInfoNewNotin);
                        agentInfo.AddRange(agentInfoNotin);
                    }

                    var worksheetCon = workbook.Worksheets.Add("Conditional Acceptance");

                    worksheetCon.Range(worksheetCon.Cell(1, 1), worksheetCon.Cell(1, 29)).Merge();
                    worksheetCon.Range(worksheetCon.Cell(2, 1), worksheetCon.Cell(2, 29)).Merge();
                    worksheetCon.Range(worksheetCon.Cell(3, 1), worksheetCon.Cell(3, 29)).Merge();
                    worksheetCon.Range(worksheetCon.Cell(4, 1), worksheetCon.Cell(4, 29)).Merge();
                    worksheetCon.Range(worksheetCon.Cell(1, 1), worksheetCon.Cell(1, 29)).Style.Font.Bold = true;
                    worksheetCon.Range(worksheetCon.Cell(2, 1), worksheetCon.Cell(2, 29)).Style.Font.Bold = true;
                    worksheetCon.Range(worksheetCon.Cell(3, 1), worksheetCon.Cell(3, 29)).Style.Font.Bold = true;

                    worksheetCon.Range(worksheetCon.Cell(1, 1), worksheetCon.Cell(1, 29)).Value = "บริษัท อาคเนย์ประกันชีวิต จำกัด(มหาชน)";
                    worksheetCon.Range(worksheetCon.Cell(2, 1), worksheetCon.Cell(2, 29)).Value = "Pending Conditional Acceptance Details Reports : สำหรับฝ่ายรับประกัน";

                    worksheetCon.Range(worksheetCon.Cell(3, 1), worksheetCon.Cell(3, 29)).Value = $"as of Date {request.asOfDate.ToString("dd/MM/yyyy", _cultureTHInfo)}";

                    worksheetCon.Range(worksheetCon.Cell(1, 1), worksheetCon.Cell(1, 29)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetCon.Range(worksheetCon.Cell(2, 1), worksheetCon.Cell(2, 29)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetCon.Range(worksheetCon.Cell(3, 1), worksheetCon.Cell(3, 29)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheetCon.Cell(1, 30).SetValue($"วันที่พิมพ์ : {DateTime.Now.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                    worksheetCon.Cell(2, 30).SetValue($"เวลาที่พิมพ์ : {DateTime.Now.ToString("HH:mm", _cultureTHInfo)}");
                    worksheetCon.Cell(3, 30).SetValue($"หน้าที่ : 2/3");

                    worksheetCon.Range(worksheetCon.Cell(5, 1), worksheetCon.Cell(5, 30)).Style.Fill.BackgroundColor = columnHeadXL;
                    worksheetCon.Range(worksheetCon.Cell(5, 1), worksheetCon.Cell(5, 30)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetCon.Range(worksheetCon.Cell(5, 1), worksheetCon.Cell(5, 30)).Style.Font.Bold = true;

                    for (int i = 1; i <= 30; i++)
                    {
                        worksheetCon.Cell(5, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    }

                    worksheetCon.Cell(5, 1).Value = "LCA Duration";
                    worksheetCon.Cell(5, 2).Value = "LCA Printed Date";
                    worksheetCon.Cell(5, 3).Value = "Submission Date";
                    worksheetCon.Cell(5, 4).Value = "Application Duration";
                    worksheetCon.Cell(5, 5).Value = "Policy No.";
                    worksheetCon.Cell(5, 6).Value = "Proposer Name";
                    worksheetCon.Cell(5, 7).Value = "Life Assured Name";
                    worksheetCon.Cell(5, 8).Value = "Life Assured Age";
                    worksheetCon.Cell(5, 9).Value = "Policy Status";
                    worksheetCon.Cell(5, 10).Value = "NB Pending Reason";
                    worksheetCon.Cell(5, 11).Value = "Letter Type";
                    worksheetCon.Cell(5, 12).Value = "Memo Type";
                    worksheetCon.Cell(5, 13).Value = "Memo Code";
                    worksheetCon.Cell(5, 14).Value = "Memo Description";
                    worksheetCon.Cell(5, 15).Value = "Application Branch";
                    worksheetCon.Cell(5, 16).Value = "Service Branch";
                    worksheetCon.Cell(5, 17).Value = "Agent Region";
                    worksheetCon.Cell(5, 18).Value = "Group Name";
                    worksheetCon.Cell(5, 19).Value = "Agent Unit";
                    worksheetCon.Cell(5, 20).Value = "Agent Code";
                    worksheetCon.Cell(5, 21).Value = "Agent Name";
                    worksheetCon.Cell(5, 22).Value = "ตัวแทนมีสิทธิ NonMed แล้วแสดงค่า N";
                    worksheetCon.Cell(5, 23).Value = "Benefit Code";
                    worksheetCon.Cell(5, 24).Value = "Benefit Name";
                    worksheetCon.Cell(5, 25).Value = "Payment Frequency";
                    worksheetCon.Cell(5, 26).Value = "Premium (API)";
                    worksheetCon.Cell(5, 27).Value = "Premium APE";
                    worksheetCon.Cell(5, 28).Value = "Underwriting User (Last)";
                    worksheetCon.Cell(5, 29).Value = "Std Life Indicator (Y/N)";
                    worksheetCon.Cell(5, 30).Value = "Exclusion Code (Y/N)";

                    worksheetCon.Column(1).Width = 15;
                    worksheetCon.Column(2).Width = 22;
                    worksheetCon.Column(3).Width = 22;
                    worksheetCon.Column(4).Width = 20;
                    worksheetCon.Column(5).Width = 15;
                    worksheetCon.Column(6).Width = 40;
                    worksheetCon.Column(7).Width = 30;
                    worksheetCon.Column(8).Width = 20;
                    worksheetCon.Column(9).Width = 22;
                    worksheetCon.Column(10).Width = 25;
                    worksheetCon.Column(11).Width = 20;
                    worksheetCon.Column(12).Width = 25;
                    worksheetCon.Column(13).Width = 25;
                    worksheetCon.Column(14).Width = 60;
                    worksheetCon.Column(15).Width = 30;
                    worksheetCon.Column(16).Width = 30;
                    worksheetCon.Column(17).Width = 22;
                    worksheetCon.Column(18).Width = 22;
                    worksheetCon.Column(19).Width = 22;
                    worksheetCon.Column(20).Width = 22;
                    worksheetCon.Column(21).Width = 30;
                    worksheetCon.Column(22).Width = 40;
                    worksheetCon.Column(23).Width = 22;
                    worksheetCon.Column(24).Width = 40;
                    worksheetCon.Column(25).Width = 22;
                    worksheetCon.Column(26).Width = 22;
                    worksheetCon.Column(27).Width = 25;
                    worksheetCon.Column(28).Width = 25;
                    worksheetCon.Column(29).Width = 25;
                    worksheetCon.Column(30).Width = 25;

                    var policyIDConAccept = conditionAcceptedData.Select(c => c.POLICY_ID);
                    List<T_INSURED_LIST> insuredListData = new List<T_INSURED_LIST>();
                    List<T_UW_EXCLUSION> exclusionListData = new List<T_UW_EXCLUSION>();

                    if (policyIDConAccept.Any())
                    {
                        insuredListData = await _repository.eBaoMemoLetter.GetInsuredList(policyIDConAccept.ToList());
                        exclusionListData = await _repository.eBaoMemoLetter.GetUWExclusionList(policyIDConAccept.ToList());
                    }

                    int startRowDataConAccept = 6;

                    var groupDataConAccept = conditionAcceptedData.GroupBy(c => c.POLICY_CODE);
                    foreach (var itemGroup in groupDataConAccept)
                    {
                        var itemPending = itemGroup.FirstOrDefault();
                        var agentInfoNewData = agentInfoNew.FirstOrDefault(c => c.POLICY_CODE == itemPending.POLICY_CODE);
                        //var allTransactionData = listData.Where(c => c.PolicyNo == itemPending.POLICY_CODE);
                        //var transactionData = allTransactionData.FirstOrDefault();
                        var submissionDate = itemPending.SUBMISSION_DATE;
                        decimal? applicationDuration = null;
                        decimal? LCADuration = null;

                        if (submissionDate.HasValue)
                        {
                            applicationDuration = (decimal)(request.asOfDate.Date - submissionDate.Value.Date).TotalDays;
                            applicationDuration = Math.Ceiling((decimal)applicationDuration.GetValueOrDefault());
                        }

                        string stdLife = insuredListData.FirstOrDefault(c => c.policy_id == itemPending.POLICY_ID)?.stand_life;
                        var exclusionData = exclusionListData.Where(c => c.policy_id == itemPending.POLICY_ID);

                        var nbPending = itemPending?.PEND_CAUSE;

                        var sumAPI = itemPending.Institute_code == 1 ? itemPending.total_prem_af * 2 : itemPending.total_prem_af;

                        var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemPending.POLICY_ID);
                        var sumAPE = itemPending.APE;

                        var benefitCode = itemPending?.internal_id;
                        var benefitName = itemPending?.product_name;
                        var paymentFrequency = itemPending?.RENEWAL_TYPE;

                        #region Data in Transaction
                        //if (allTransactionData.Any())
                        //{
                        //    foreach (var itemTransaction in allTransactionData)
                        //    {
                        //        var docData = documentData.FirstOrDefault(c => c.document_no == transactionData.DocumentNo);

                        //        if (docData != null && docData.insert_time.HasValue && docData.end_time.HasValue)
                        //        {
                        //            LCADuration = (decimal)(docData.end_time.GetValueOrDefault().Date - docData.insert_time.GetValueOrDefault().Date).TotalDays;
                        //            LCADuration = Math.Ceiling(LCADuration.GetValueOrDefault());
                        //        }

                        //        var agentInfoData = agentInfo.FirstOrDefault(c => c.AGENT_CODE == itemTransaction.AgentCode);
                        //        var letterType = documentJoinData.FirstOrDefault(c => c.document_no == itemTransaction.DocumentNo)?.template_name;
                        //        var medNonMed = agentList.FirstOrDefault(c => c.agent_code == itemTransaction.AgentCode)?.med_indi;

                        //        worksheetCon.Cell(startRowDataConAccept, 1).Value = LCADuration != null ? $"{LCADuration} วัน" : "";
                        //        worksheetCon.Cell(startRowDataConAccept, 2).Value = itemPending.CREATE_DATE != null ? itemPending.CREATE_DATE.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty;
                        //        worksheetCon.Cell(startRowDataConAccept, 3).Value = submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty;
                        //        worksheetCon.Cell(startRowDataConAccept, 4).Value = applicationDuration != null ? $"{applicationDuration} วัน" : string.Empty;
                        //        worksheetCon.Cell(startRowDataConAccept, 5).SetValue(transactionData.PolicyNo);
                        //        worksheetCon.Cell(startRowDataConAccept, 6).Value = transactionData.PolicyName;
                        //        worksheetCon.Cell(startRowDataConAccept, 7).Value = transactionData.LifeAssuredName;
                        //        worksheetCon.Cell(startRowDataConAccept, 8).Value = $"{transactionData.EntryAgeLifeAssured} ปี";
                        //        worksheetCon.Cell(startRowDataConAccept, 9).Value = itemPending.STATUS_DESC;
                        //        worksheetCon.Cell(startRowDataConAccept, 10).Value = nbPending; // NB PEN
                        //        worksheetCon.Cell(startRowDataConAccept, 11).Value = letterType; // LETTER Type
                        //        worksheetCon.Cell(startRowDataConAccept, 12).Value = "-";
                        //        worksheetCon.Cell(startRowDataConAccept, 13).Value = "-";
                        //        worksheetCon.Cell(startRowDataConAccept, 14).Value = "-";
                        //        worksheetCon.Cell(startRowDataConAccept, 15).Value = agentInfoNewData != null ? agentInfoNewData.APPLICATION_BRANCH_NAME : string.Empty;
                        //        worksheetCon.Cell(startRowDataConAccept, 16).Value = agentInfoData != null ? agentInfoData.AGENT_GROUP : string.Empty;
                        //        worksheetCon.Cell(startRowDataConAccept, 17).Value = itemTransaction.AgentUnit;
                        //        worksheetCon.Cell(startRowDataConAccept, 18).Value = itemTransaction.AgentCode;
                        //        worksheetCon.Cell(startRowDataConAccept, 19).Value = itemTransaction.AgentName;
                        //        worksheetCon.Cell(startRowDataConAccept, 20).Value = medNonMed;
                        //        worksheetCon.Cell(startRowDataConAccept, 21).Value = benefitCode;
                        //        worksheetCon.Cell(startRowDataConAccept, 22).Value = benefitName;
                        //        worksheetCon.Cell(startRowDataConAccept, 23).Value = paymentFrequency;
                        //        worksheetCon.Cell(startRowDataConAccept, 24).Value = sumAPI;
                        //        worksheetCon.Cell(startRowDataConAccept, 25).Value = sumAPE;
                        //        worksheetCon.Cell(startRowDataConAccept, 26).Value = itemPending.user_name;
                        //        worksheetCon.Cell(startRowDataConAccept, 27).Value = stdLife;
                        //        worksheetCon.Cell(startRowDataConAccept, 28).Value = exclusionData.Any() ? "Y" : "N";

                        //        worksheetCon.Cell(startRowDataConAccept, 24).Style.NumberFormat.Format = currencyFormat;
                        //        worksheetCon.Cell(startRowDataConAccept, 24).DataType = XLDataType.Number;
                        //        worksheetCon.Cell(startRowDataConAccept, 25).Style.NumberFormat.Format = currencyFormat;
                        //        worksheetCon.Cell(startRowDataConAccept, 25).DataType = XLDataType.Number;

                        //        worksheetCon.Cell(startRowDataConAccept, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 9).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 12).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 13).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 14).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 15).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 16).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 17).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 18).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 19).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 20).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 21).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 22).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 23).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 24).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 25).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 26).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 27).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        //        worksheetCon.Cell(startRowDataConAccept, 28).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                        //        worksheetCon.Cell(startRowDataConAccept, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 14).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 18).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 21).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 23).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 25).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //        worksheetCon.Cell(startRowDataConAccept, 28).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        //        startRowDataConAccept++;
                        //    }
                        //}
                        #endregion

                        var dataNotInMemoPolicy = dataNotInMemoConAccept.FirstOrDefault(c => c.POLICY_CODE == itemPending.POLICY_CODE);
                        var agentInfoData = agentInfo.FirstOrDefault(c => c.AGENT_CODE == agentInfoNewData?.AGENT_CODE);
                        var letterType = documentJoinData.FirstOrDefault(c => c.document_no == dataNotInMemoPolicy?.document_no)?.template_name;
                        var medNonMed = agentList.FirstOrDefault(c => c.agent_code == agentInfoNewData?.AGENT_CODE)?.med_indi;
                        //var docData = documentData.FirstOrDefault(c => c.document_no == dataNotInMemoPolicy?.document_no);
                        var agentRegion = MappingAgentRegion(agentInfoNewData.BRANCH_CO_CODE);

                        if (itemPending.lca_printeddate.HasValue)
                        {
                            LCADuration = (decimal)(request.asOfDate.Date - itemPending.lca_printeddate.GetValueOrDefault().Date).TotalDays;
                            LCADuration = Math.Ceiling(LCADuration.GetValueOrDefault());
                        }

                        worksheetCon.Cell(startRowDataConAccept, 1).Value = LCADuration != null ? $"{LCADuration} วัน" : "";
                        worksheetCon.Cell(startRowDataConAccept, 2).SetValue(itemPending.lca_printeddate != null ? itemPending.lca_printeddate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                        //worksheetCon.Cell(startRowDataConAccept, 2).Value = itemPending.lca_printeddate != null ? itemPending.lca_printeddate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty;
                        worksheetCon.Cell(startRowDataConAccept, 3).SetValue(submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                        worksheetCon.Cell(startRowDataConAccept, 4).Value = applicationDuration != null ? $"{applicationDuration} วัน" : string.Empty;
                        worksheetCon.Cell(startRowDataConAccept, 5).SetValue(itemPending?.POLICY_CODE);
                        worksheetCon.Cell(startRowDataConAccept, 6).Value = dataNotInMemoPolicy?.ProposerName;
                        worksheetCon.Cell(startRowDataConAccept, 7).Value = dataNotInMemoPolicy?.LifeAssuredName;
                        worksheetCon.Cell(startRowDataConAccept, 8).Value = dataNotInMemoPolicy?.LifeAssuredAge != null ? $"{dataNotInMemoPolicy.LifeAssuredAge} ปี" : string.Empty;
                        worksheetCon.Cell(startRowDataConAccept, 9).Value = itemPending.STATUS_DESC;
                        worksheetCon.Cell(startRowDataConAccept, 10).Value = nbPending; // NB PEN
                        worksheetCon.Cell(startRowDataConAccept, 11).Value = letterType; // LETTER Type
                        worksheetCon.Cell(startRowDataConAccept, 12).Value = "-";
                        worksheetCon.Cell(startRowDataConAccept, 13).Value = "-";
                        worksheetCon.Cell(startRowDataConAccept, 14).Value = "-";
                        worksheetCon.Cell(startRowDataConAccept, 15).Value = agentInfoNewData != null ? agentInfoNewData.APPLICATION_BRANCH_NAME : string.Empty;
                        worksheetCon.Cell(startRowDataConAccept, 16).Value = agentInfoNewData != null ? agentInfoNewData.BRANCH_NAME : string.Empty;
                        worksheetCon.Cell(startRowDataConAccept, 17).Value = agentRegion;
                        worksheetCon.Cell(startRowDataConAccept, 18).Value = agentInfoData != null ? agentInfoData.AGENT_GROUP : string.Empty;
                        worksheetCon.Cell(startRowDataConAccept, 19).Value = agentInfoNewData.UNIT_NAME;
                        worksheetCon.Cell(startRowDataConAccept, 20).Value = agentInfoNewData.AGENT_CODE;
                        worksheetCon.Cell(startRowDataConAccept, 21).Value = $"{agentInfoNewData?.FIRST_NAME} {agentInfoNewData?.LAST_NAME}";
                        worksheetCon.Cell(startRowDataConAccept, 22).Value = medNonMed;
                        worksheetCon.Cell(startRowDataConAccept, 23).Value = benefitCode;
                        worksheetCon.Cell(startRowDataConAccept, 24).Value = benefitName;
                        worksheetCon.Cell(startRowDataConAccept, 25).Value = paymentFrequency;
                        worksheetCon.Cell(startRowDataConAccept, 26).Value = sumAPI;
                        worksheetCon.Cell(startRowDataConAccept, 27).Value = sumAPE;
                        worksheetCon.Cell(startRowDataConAccept, 28).Value = itemPending.user_name;
                        worksheetCon.Cell(startRowDataConAccept, 29).Value = stdLife;
                        worksheetCon.Cell(startRowDataConAccept, 30).Value = exclusionData.Any() ? "Y" : "N";

                        worksheetCon.Cell(startRowDataConAccept, 26).Style.NumberFormat.Format = currencyFormat;
                        worksheetCon.Cell(startRowDataConAccept, 26).DataType = XLDataType.Number;
                        worksheetCon.Cell(startRowDataConAccept, 27).Style.NumberFormat.Format = currencyFormat;
                        worksheetCon.Cell(startRowDataConAccept, 27).DataType = XLDataType.Number;

                        for (int i = 1; i <= 30; i++)
                        {
                            worksheetCon.Cell(startRowDataConAccept, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        }

                        worksheetCon.Cell(startRowDataConAccept, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 14).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 23).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 25).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 28).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 29).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheetCon.Cell(startRowDataConAccept, 30).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        startRowDataConAccept++;


                    }

                    #endregion

                    #region Accepted
                    var worksheetAccepred = workbook.Worksheets.Add("Accepted (premium not paid)");

                    worksheetAccepred.Range(worksheetAccepred.Cell(1, 1), worksheetAccepred.Cell(1, 29)).Merge();
                    worksheetAccepred.Range(worksheetAccepred.Cell(2, 1), worksheetAccepred.Cell(2, 29)).Merge();
                    worksheetAccepred.Range(worksheetAccepred.Cell(3, 1), worksheetAccepred.Cell(3, 29)).Merge();
                    worksheetAccepred.Range(worksheetAccepred.Cell(4, 1), worksheetAccepred.Cell(4, 29)).Merge();
                    worksheetAccepred.Range(worksheetAccepred.Cell(1, 1), worksheetAccepred.Cell(1, 29)).Style.Font.Bold = true;
                    worksheetAccepred.Range(worksheetAccepred.Cell(2, 1), worksheetAccepred.Cell(2, 29)).Style.Font.Bold = true;
                    worksheetAccepred.Range(worksheetAccepred.Cell(3, 1), worksheetAccepred.Cell(3, 29)).Style.Font.Bold = true;

                    worksheetAccepred.Range(worksheetAccepred.Cell(1, 1), worksheetAccepred.Cell(1, 29)).Value = "บริษัท อาคเนย์ประกันชีวิต จำกัด(มหาชน)";
                    worksheetAccepred.Range(worksheetAccepred.Cell(2, 1), worksheetAccepred.Cell(2, 29)).Value = "Pending Accept (Premium Not Paid) Details Report : สำหรับฝ่ายรับประกัน";

                    worksheetAccepred.Range(worksheetAccepred.Cell(3, 1), worksheetAccepred.Cell(3, 29)).Value = $"as of Date {request.asOfDate.ToString("dd/MM/yyyy", _cultureTHInfo)}";

                    worksheetAccepred.Range(worksheetAccepred.Cell(1, 1), worksheetAccepred.Cell(1, 29)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetAccepred.Range(worksheetAccepred.Cell(2, 1), worksheetAccepred.Cell(2, 29)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetAccepred.Range(worksheetAccepred.Cell(3, 1), worksheetAccepred.Cell(3, 29)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);


                    worksheetAccepred.Cell(1, 30).SetValue($"วันที่พิมพ์ : {DateTime.Now.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                    worksheetAccepred.Cell(2, 30).SetValue($"เวลาที่พิมพ์ : {DateTime.Now.ToString("HH:mm", _cultureTHInfo)}");
                    worksheetAccepred.Cell(3, 30).SetValue($"หน้าที่ : 3/3");

                    worksheetAccepred.Range(worksheetAccepred.Cell(5, 1), worksheetAccepred.Cell(5, 30)).Style.Fill.BackgroundColor = columnHeadXL;
                    worksheetAccepred.Range(worksheetAccepred.Cell(5, 1), worksheetAccepred.Cell(5, 30)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetAccepred.Range(worksheetAccepred.Cell(5, 1), worksheetAccepred.Cell(5, 30)).Style.Font.Bold = true;

                    for (int i = 1; i <= 30; i++)
                    {
                        worksheetAccepred.Cell(5, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    }

                    worksheetAccepred.Cell(5, 1).Value = "Accept Duration";
                    worksheetAccepred.Cell(5, 2).Value = "Accept Date";
                    worksheetAccepred.Cell(5, 3).Value = "Submission Date";
                    worksheetAccepred.Cell(5, 4).Value = "Application Duration";
                    worksheetAccepred.Cell(5, 5).Value = "Policy No.";
                    worksheetAccepred.Cell(5, 6).Value = "Proposer Name";
                    worksheetAccepred.Cell(5, 7).Value = "Life Assured Name";
                    worksheetAccepred.Cell(5, 8).Value = "Life Assured Age";
                    worksheetAccepred.Cell(5, 9).Value = "Policy Status";
                    worksheetAccepred.Cell(5, 10).Value = "NB Pending Reason";
                    worksheetAccepred.Cell(5, 11).Value = "Letter Type";
                    worksheetAccepred.Cell(5, 12).Value = "Memo Type";
                    worksheetAccepred.Cell(5, 13).Value = "Memo Code";
                    worksheetAccepred.Cell(5, 14).Value = "Memo Description";
                    worksheetAccepred.Cell(5, 15).Value = "Application Branch";
                    worksheetAccepred.Cell(5, 16).Value = "Service Branch";
                    worksheetAccepred.Cell(5, 17).Value = "Agent Region";
                    worksheetAccepred.Cell(5, 18).Value = "Group Name";
                    worksheetAccepred.Cell(5, 19).Value = "Agent Unit";
                    worksheetAccepred.Cell(5, 20).Value = "Agent Code";
                    worksheetAccepred.Cell(5, 21).Value = "Agent Name";
                    worksheetAccepred.Cell(5, 22).Value = "ตัวแทนมีสิทธิ NonMed แล้วแสดงค่า N";
                    worksheetAccepred.Cell(5, 23).Value = "Benefit Code";
                    worksheetAccepred.Cell(5, 24).Value = "Benefit Name";
                    worksheetAccepred.Cell(5, 25).Value = "Payment Frequency";
                    worksheetAccepred.Cell(5, 26).Value = "Premium (API)";
                    worksheetAccepred.Cell(5, 27).Value = "Premium APE";
                    worksheetAccepred.Cell(5, 28).Value = "Underwriting User (Last)";
                    worksheetAccepred.Cell(5, 29).Value = "Amount Received";
                    worksheetAccepred.Cell(5, 30).Value = "Balance to Turn Policy In force";

                    worksheetAccepred.Column(1).Width = 22;
                    worksheetAccepred.Column(2).Width = 22;
                    worksheetAccepred.Column(3).Width = 22;
                    worksheetAccepred.Column(4).Width = 22;
                    worksheetAccepred.Column(5).Width = 22;
                    worksheetAccepred.Column(6).Width = 40;
                    worksheetAccepred.Column(7).Width = 30;
                    worksheetAccepred.Column(8).Width = 22;
                    worksheetAccepred.Column(9).Width = 25;
                    worksheetAccepred.Column(10).Width = 30;
                    worksheetAccepred.Column(11).Width = 22;
                    worksheetAccepred.Column(12).Width = 25;
                    worksheetAccepred.Column(13).Width = 25;
                    worksheetAccepred.Column(14).Width = 60;
                    worksheetAccepred.Column(15).Width = 30;
                    worksheetAccepred.Column(16).Width = 30;
                    worksheetAccepred.Column(17).Width = 22;
                    worksheetAccepred.Column(18).Width = 25;
                    worksheetAccepred.Column(19).Width = 25;
                    worksheetAccepred.Column(20).Width = 25;
                    worksheetAccepred.Column(21).Width = 30;
                    worksheetAccepred.Column(22).Width = 40;
                    worksheetAccepred.Column(23).Width = 25;
                    worksheetAccepred.Column(24).Width = 40;
                    worksheetAccepred.Column(25).Width = 22;
                    worksheetAccepred.Column(26).Width = 22;
                    worksheetAccepred.Column(27).Width = 22;
                    worksheetAccepred.Column(28).Width = 30;
                    worksheetAccepred.Column(29).Width = 22;
                    worksheetAccepred.Column(30).Width = 35;

                    int startRowDataAccept = 6;
                    List<DataNotInMemo> dataNotInMemoAccept = new List<DataNotInMemo>();
                    var listDataNotInEmemoAccept = acceptedData.Where(c => !policyInTransaction.Contains(c.POLICY_CODE)).Select(e => e.POLICY_CODE);

                    if (listDataNotInEmemoAccept.Any())
                    {
                        dataNotInMemoAccept = await _repository.eBaoMemoLetter.GetDataNotInMemo(listDataNotInEmemoAccept.ToList());
                        var documentListNotin = dataNotInMemoAccept.Select(c => c.document_no).Distinct();
                        var documentJoinDataNotin = await _repository.eBaoMemoLetter.GetDocumentJoin(documentListNotin.ToList());
                        var documentDataNotin = await _repository.eBaoMemoLetter.GetDocumentData(documentListNotin.ToList());

                        var policyCodeListtNotin = dataNotInMemoAccept.Select(c => "'" + c.POLICY_CODE + "'").Distinct().ToList();
                        var agentInfoNewNotin = await _repository.tghEmemo.GetAdditionalDataByPolicyNoList(policyCodeListtNotin);

                        var agentCodeListNotin = agentInfoNewNotin.Select(c => c.AGENT_CODE).Distinct();
                        var agentListNotin = await _repository.eBaoMemoLetter.GetAgent(agentCodeListNotin.ToList());
                        var agentInfoNotin = _repository.tghEmemo.GetAgentInfoBy(c => agentCodeListNotin.Contains(c.AGENT_CODE));

                        documentJoinData.AddRange(documentJoinDataNotin);
                        documentData.AddRange(documentDataNotin);
                        agentList.AddRange(agentListNotin);
                        agentInfoNew.AddRange(agentInfoNewNotin);
                        agentInfo.AddRange(agentInfoNotin);
                    }

                    foreach (var itemPending in acceptedData)
                    {
                        var contractMasterData = acceptedData.FirstOrDefault(c => c.POLICY_ID == itemPending.POLICY_ID);
                        var allTransactionData = listData.Where(c => c.PolicyNo == contractMasterData.POLICY_CODE);
                        var transactionData = allTransactionData.FirstOrDefault();
                        var submissionDate = contractMasterData.SUBMISSION_DATE;
                        var agentInfoNewData = agentInfoNew.FirstOrDefault(c => c.POLICY_CODE == itemPending.POLICY_CODE);

                        double? applicationDuration = null;

                        if (submissionDate.HasValue)
                        {
                            applicationDuration = (request.asOfDate.Date - submissionDate.Value.Date).TotalDays;
                        }

                        var nbPending = allPendingCode.FirstOrDefault(c => c.PENDING_ID == itemPending.pending_id)?.PEND_CAUSE;

                        var lastMemoPrinted = contractMasterData.CREATE_DATE;
                        double? memoDuration = null;

                        if (lastMemoPrinted.HasValue)
                        {
                            memoDuration = (request.asOfDate.Date - lastMemoPrinted.GetValueOrDefault().Date).TotalDays;
                        }

                        string underWritingLast = contractMasterData.user_name;
                        var sumAPI = itemPending.Institute_code == 1 ? itemPending.total_prem_af * 2 : itemPending.total_prem_af;
                        var sumAPE = itemPending.APE;

                        decimal amountRecieved = itemPending.amount_recieved.GetValueOrDefault();

                        var benefitCode = contractMasterData?.internal_id;
                        var benefitName = contractMasterData?.product_name;
                        var paymentFrequency = contractMasterData?.RENEWAL_TYPE;
                        var agentRegion = MappingAgentRegion(agentInfoNewData.BRANCH_CO_CODE);

                        if (allTransactionData.Any())
                        {
                            foreach (var itemTransaction in allTransactionData)
                            {
                                var agentInfoData = agentInfo.FirstOrDefault(c => c.AGENT_CODE == itemTransaction.AgentCode);
                                var letterType = documentJoinData.FirstOrDefault(c => c.document_no == itemTransaction.DocumentNo)?.template_name;
                                var medNonMed = agentList.FirstOrDefault(c => c.agent_code == itemTransaction.AgentCode)?.med_indi;

                                worksheetAccepred.Cell(startRowDataAccept, 1).Value = memoDuration != null ? $"{memoDuration} วัน" : "";
                                worksheetAccepred.Cell(startRowDataAccept, 2).SetValue($"{transactionData.TransactionDate.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                                worksheetAccepred.Cell(startRowDataAccept, 3).SetValue(submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                                worksheetAccepred.Cell(startRowDataAccept, 4).Value = applicationDuration != null ? $"{applicationDuration} วัน" : string.Empty;
                                worksheetAccepred.Cell(startRowDataAccept, 5).SetValue(transactionData.PolicyNo);
                                worksheetAccepred.Cell(startRowDataAccept, 6).Value = transactionData.PolicyName;
                                worksheetAccepred.Cell(startRowDataAccept, 7).Value = transactionData.LifeAssuredName;
                                worksheetAccepred.Cell(startRowDataAccept, 8).Value = $"{transactionData.EntryAgeLifeAssured} ปี";
                                worksheetAccepred.Cell(startRowDataAccept, 9).Value = itemPending.STATUS_DESC;
                                worksheetAccepred.Cell(startRowDataAccept, 10).Value = nbPending; // NB PEN
                                worksheetAccepred.Cell(startRowDataAccept, 11).Value = letterType; // LETTER Type
                                worksheetAccepred.Cell(startRowDataAccept, 12).Value = !string.IsNullOrWhiteSpace(itemTransaction.MemoType_Name) ? itemTransaction.MemoType_Name : itemTransaction.MemoType;
                                worksheetAccepred.Cell(startRowDataAccept, 13).Value = itemTransaction.MemoCode;
                                worksheetAccepred.Cell(startRowDataAccept, 14).Value = !string.IsNullOrWhiteSpace(itemTransaction.Description) ? itemTransaction.Description : itemTransaction.MemoCodeDescription;
                                worksheetAccepred.Cell(startRowDataAccept, 15).Value = agentInfoNewData != null ? agentInfoNewData.APPLICATION_BRANCH_NAME : string.Empty;
                                worksheetAccepred.Cell(startRowDataAccept, 16).Value = agentInfoNewData != null ? agentInfoNewData.BRANCH_NAME : string.Empty;
                                worksheetAccepred.Cell(startRowDataAccept, 17).Value = agentRegion;
                                worksheetAccepred.Cell(startRowDataAccept, 18).Value = agentInfoData != null ? agentInfoData.AGENT_GROUP : string.Empty;
                                worksheetAccepred.Cell(startRowDataAccept, 19).Value = itemTransaction.AgentUnit;
                                worksheetAccepred.Cell(startRowDataAccept, 20).Value = itemTransaction.AgentCode;
                                worksheetAccepred.Cell(startRowDataAccept, 21).Value = itemTransaction.AgentName;
                                worksheetAccepred.Cell(startRowDataAccept, 22).Value = medNonMed;
                                worksheetAccepred.Cell(startRowDataAccept, 23).Value = benefitCode;
                                worksheetAccepred.Cell(startRowDataAccept, 24).Value = benefitName;
                                worksheetAccepred.Cell(startRowDataAccept, 25).Value = paymentFrequency;
                                worksheetAccepred.Cell(startRowDataAccept, 26).Value = sumAPI;
                                worksheetAccepred.Cell(startRowDataAccept, 27).Value = sumAPE;
                                worksheetAccepred.Cell(startRowDataAccept, 28).Value = underWritingLast;
                                worksheetAccepred.Cell(startRowDataAccept, 29).Value = amountRecieved;
                                worksheetAccepred.Cell(startRowDataAccept, 30).Value = sumAPI - amountRecieved;


                                worksheetAccepred.Cell(startRowDataAccept, 26).Style.NumberFormat.Format = currencyFormat;
                                worksheetAccepred.Cell(startRowDataAccept, 26).DataType = XLDataType.Number;
                                worksheetAccepred.Cell(startRowDataAccept, 27).Style.NumberFormat.Format = currencyFormat;
                                worksheetAccepred.Cell(startRowDataAccept, 27).DataType = XLDataType.Number;
                                worksheetAccepred.Cell(startRowDataAccept, 29).Style.NumberFormat.Format = currencyFormat;
                                worksheetAccepred.Cell(startRowDataAccept, 29).DataType = XLDataType.Number;
                                worksheetAccepred.Cell(startRowDataAccept, 30).Style.NumberFormat.Format = currencyFormat;
                                worksheetAccepred.Cell(startRowDataAccept, 30).DataType = XLDataType.Number;

                                for (int i = 1; i <= 30; i++)
                                {
                                    worksheetAccepred.Cell(startRowDataAccept, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                }

                                worksheetAccepred.Cell(startRowDataAccept, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 23).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 25).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 28).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 29).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetAccepred.Cell(startRowDataAccept, 30).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                startRowDataAccept++;
                            }
                        }
                        else
                        {
                            var dataNotInMemoPolicy = dataNotInMemoAccept.FirstOrDefault(c => c.POLICY_CODE == itemPending.POLICY_CODE);
                            var agentInfoData = agentInfo.FirstOrDefault(c => c.AGENT_CODE == agentInfoNewData?.AGENT_CODE);
                            var letterType = documentJoinData.FirstOrDefault(c => c.document_no == dataNotInMemoPolicy.document_no)?.template_name;
                            var medNonMed = agentList.FirstOrDefault(c => c.agent_code == agentInfoNewData.AGENT_CODE)?.med_indi;
                            memoDuration = (request.asOfDate.Date - dataNotInMemoPolicy.insert_time.Date).TotalDays;

                            worksheetAccepred.Cell(startRowDataAccept, 1).Value = memoDuration != null ? $"{memoDuration} วัน" : "";
                            worksheetAccepred.Cell(startRowDataAccept, 2).SetValue($"{dataNotInMemoPolicy.insert_time.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                            worksheetAccepred.Cell(startRowDataAccept, 3).SetValue(submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                            worksheetAccepred.Cell(startRowDataAccept, 4).Value = applicationDuration != null ? $"{applicationDuration} วัน" : string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 5).SetValue(dataNotInMemoPolicy.POLICY_CODE);
                            worksheetAccepred.Cell(startRowDataAccept, 6).Value = dataNotInMemoPolicy.ProposerName;
                            worksheetAccepred.Cell(startRowDataAccept, 7).Value = dataNotInMemoPolicy.LifeAssuredName;
                            worksheetAccepred.Cell(startRowDataAccept, 8).Value = $"{dataNotInMemoPolicy.LifeAssuredAge} ปี";
                            worksheetAccepred.Cell(startRowDataAccept, 9).Value = itemPending.STATUS_DESC;
                            worksheetAccepred.Cell(startRowDataAccept, 10).Value = nbPending; // NB PEN
                            worksheetAccepred.Cell(startRowDataAccept, 11).Value = letterType; // LETTER Type
                            worksheetAccepred.Cell(startRowDataAccept, 12).Value = string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 13).Value = string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 14).Value = string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 15).Value = agentInfoNewData != null ? agentInfoNewData.APPLICATION_BRANCH_NAME : string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 16).Value = agentInfoNewData != null ? agentInfoNewData.BRANCH_NAME : string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 17).Value = agentRegion;
                            worksheetAccepred.Cell(startRowDataAccept, 18).Value = agentInfoData != null ? agentInfoData.AGENT_GROUP : string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 19).Value = agentInfoNewData != null ? agentInfoNewData.UNIT_NAME : string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 20).Value = agentInfoNewData != null ? agentInfoNewData.AGENT_CODE : string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 21).Value = agentInfoNewData != null ? $"{agentInfoNewData?.FIRST_NAME} {agentInfoNewData?.LAST_NAME}" : string.Empty;
                            worksheetAccepred.Cell(startRowDataAccept, 22).Value = medNonMed;
                            worksheetAccepred.Cell(startRowDataAccept, 23).Value = benefitCode;
                            worksheetAccepred.Cell(startRowDataAccept, 24).Value = benefitName;
                            worksheetAccepred.Cell(startRowDataAccept, 25).Value = paymentFrequency;
                            worksheetAccepred.Cell(startRowDataAccept, 26).Value = sumAPI;
                            worksheetAccepred.Cell(startRowDataAccept, 27).Value = sumAPE;
                            worksheetAccepred.Cell(startRowDataAccept, 28).Value = underWritingLast;
                            worksheetAccepred.Cell(startRowDataAccept, 29).Value = amountRecieved;
                            worksheetAccepred.Cell(startRowDataAccept, 30).Value = sumAPI - amountRecieved;


                            worksheetAccepred.Cell(startRowDataAccept, 26).Style.NumberFormat.Format = currencyFormat;
                            worksheetAccepred.Cell(startRowDataAccept, 26).DataType = XLDataType.Number;
                            worksheetAccepred.Cell(startRowDataAccept, 27).Style.NumberFormat.Format = currencyFormat;
                            worksheetAccepred.Cell(startRowDataAccept, 27).DataType = XLDataType.Number;
                            worksheetAccepred.Cell(startRowDataAccept, 29).Style.NumberFormat.Format = currencyFormat;
                            worksheetAccepred.Cell(startRowDataAccept, 29).DataType = XLDataType.Number;
                            worksheetAccepred.Cell(startRowDataAccept, 30).Style.NumberFormat.Format = currencyFormat;
                            worksheetAccepred.Cell(startRowDataAccept, 30).DataType = XLDataType.Number;

                            for (int i = 1; i <= 30; i++)
                            {
                                worksheetAccepred.Cell(startRowDataAccept, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            }

                            worksheetAccepred.Cell(startRowDataAccept, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 17).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 20).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 23).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 25).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 28).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 29).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetAccepred.Cell(startRowDataAccept, 30).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            startRowDataAccept++;
                        }
                    }

                    #endregion
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    string htmlBody = string.Empty;

                    if (preview)
                    {
                        Workbook workbookC = new Workbook();
                        workbookC.LoadFromStream(stream);
                        Worksheet sheet = workbookC.Worksheets[0];

                        string fileSave = $"Report3{Guid.NewGuid()}";

                        sheet.SaveToHtml(fileSave);

                        string excelHtmlPath = Path.GetFullPath(Path.Combine(fileSave));
                        using (StreamReader reader = File.OpenText(excelHtmlPath))
                        {
                            htmlBody = reader.ReadToEnd();
                        }

                        var regex = new Regex(@"<[hH][2][^>]*>[^<]*</[hH][2]\s*>", RegexOptions.Compiled | RegexOptions.Multiline);
                        htmlBody = regex.Replace(htmlBody, "");

                        File.Delete(excelHtmlPath);
                    }

                    return (content, htmlBody);
                }
            }
        }

        private async Task<(byte[], string)> GenerateReportPendingSummary(PendingSummaryReportRequest request, List<Transaction_View> listData
            , List<T_CONTRACT_PROPOSAL> listContractProposal, List<Pending_Summary_Rawdata> contractMaster, List<TTDMemoTransactionHistory> listTransactionHistory,
            List<T_DM_EMEMO_AGENT_INFO_NEW> listAgentInfo, List<T_DM_EMEMO_AGENT_INFO> agentInfoData, bool preview)
        {
            System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");

            using (var workbook = new XLWorkbook())
            {
                Color columnHead = Color.FromArgb(77, 121, 242);
                XLColor columnHeadXL = XLColor.FromArgb(columnHead.A, columnHead.R, columnHead.G, columnHead.B);

                var allPolicyID = contractMaster.Select(c => c.POLICY_ID).ToList();
                var Institute_code = await _repository.eBaoMemoLetter.GetInstitute_code(allPolicyID);

                var submittedData = contractMaster;
                var issuedData = contractMaster.Where(c => c.LIABILITY_STATE == 1 && c.LIABILITY_STATE.HasValue);
                var declinePostponedData = contractMaster.Where(c => c.PROPOSAL_STATUS == "82" || c.PROPOSAL_STATUS == "84");
                var withdrawData = contractMaster.Where(c => c.PROPOSAL_STATUS == "86");
                var terminateFreelookData = contractMaster.Where(c => c.LIABILITY_STATE == 3 && c.END_CAUSE == 51); //Termination Reason = Financial Difficulty
                var terminateOtherData = contractMaster.Where(c => c.LIABILITY_STATE == 3 && c.END_CAUSE != 51); // Termination Reason = ไมใช่่ Financial Difficulty       
                var otherData = contractMaster.Where(
                            c => c.PROPOSAL_STATUS == "10"
                            || c.PROPOSAL_STATUS == "11"
                            || c.PROPOSAL_STATUS == "20"
                            || c.PROPOSAL_STATUS == "21"
                            || c.PROPOSAL_STATUS == "31"
                            || c.PROPOSAL_STATUS == "40");

                var conditionAcceptedData = contractMaster.Where(c => c.PROPOSAL_STATUS == "81");
                var acceptedData = contractMaster.Where(c => c.PROPOSAL_STATUS == "80");
                var totalPendingData = contractMaster.Where(c => c.PROPOSAL_STATUS == "32"); //Pending Reason = Y & N

                int totalCase = issuedData.Count()
                   + declinePostponedData.Count() + withdrawData.Count() + terminateFreelookData.Count()
                   + terminateOtherData.Count() + otherData.Count() + conditionAcceptedData.Count()
                   + acceptedData.Count() + totalPendingData.Count();

                #region Total

                #region Summary Case
                var worksheet = workbook.Worksheets.Add("Total");
                worksheet.Column(1).Width = 15;
                worksheet.Column(2).Width = 22;
                worksheet.Column(3).Width = 22;
                worksheet.Column(4).Width = 15;
                worksheet.Column(5).Width = 15;
                worksheet.Column(6).Width = 15;
                worksheet.Column(7).Width = 15;

                worksheet.Range(worksheet.Cell(1, 2), worksheet.Cell(1, 4)).Merge();
                worksheet.Range(worksheet.Cell(1, 2), worksheet.Cell(1, 4)).Value = $"Memo summary report from submission date {request.submissionDateFrom.ToString("dd/MM/yyyy", _cultureTHInfo)} to {request.submissionDateTo.Value.ToString("dd/MM/yyyy", _cultureTHInfo)}";
                worksheet.Range(worksheet.Cell(1, 2), worksheet.Cell(1, 4)).Style.Font.Bold = true;

                worksheet.Range(worksheet.Cell(2, 2), worksheet.Cell(2, 3)).Merge();
                worksheet.Range(worksheet.Cell(2, 2), worksheet.Cell(2, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(2, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(2, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(2, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(2, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(2, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(2, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(2, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(2, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(2, 4).Style.Font.Bold = true;
                worksheet.Cell(2, 5).Style.Font.Bold = true;
                worksheet.Cell(2, 6).Style.Font.Bold = true;
                worksheet.Cell(2, 7).Style.Font.Bold = true;

                worksheet.Cell(2, 4).Value = "Case(s)";
                worksheet.Cell(2, 5).Value = "%Case";
                worksheet.Cell(2, 6).Value = "APE(THB.)";
                worksheet.Cell(2, 7).Value = "%APE";

                worksheet.Range(worksheet.Cell(3, 6), worksheet.Cell(12, 6)).Style.NumberFormat.Format = currencyFormat;
                worksheet.Range(worksheet.Cell(3, 6), worksheet.Cell(12, 6)).DataType = XLDataType.Number;

                worksheet.Range(worksheet.Cell(3, 7), worksheet.Cell(12, 7)).Style.NumberFormat.Format = percentFormat;
                worksheet.Range(worksheet.Cell(3, 7), worksheet.Cell(12, 7)).DataType = XLDataType.Number;

                worksheet.Range(worksheet.Cell(3, 4), worksheet.Cell(12, 4)).Style.NumberFormat.Format = currencyFormat;
                worksheet.Range(worksheet.Cell(3, 4), worksheet.Cell(12, 4)).DataType = XLDataType.Number;

                worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3, 3)).Merge();
                worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3, 3)).Value = "Total submitted";

                worksheet.Cell(3, 4).Value = submittedData.Count();

                var percentCaseSubmitted = submittedData.Count() > 0 ?
                  Math.Round((submittedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(3, 5).Value = percentCaseSubmitted + "%";

                decimal totalSumSubmitted = 0;
                if (submittedData.Any())
                {
                    totalSumSubmitted = submittedData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = submittedData.GroupBy(c => c.POLICY_ID);

                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;

                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumSubmitted = totalSumSubmitted + sumAPE;

                    //}
                }

                worksheet.Cell(3, 6).Value = totalSumSubmitted;

                worksheet.Range(worksheet.Cell(4, 2), worksheet.Cell(4, 3)).Merge();
                worksheet.Range(worksheet.Cell(4, 2), worksheet.Cell(4, 3)).Value = "Issued";
                worksheet.Cell(4, 4).Value = issuedData.Count();

                var percentCaseIssued = issuedData.Count() > 0 ?
                    Math.Round((issuedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(4, 5).Value = percentCaseIssued + "%";

                decimal totalSumIssued = 0;
                if (issuedData.Any())
                {
                    totalSumIssued = issuedData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = issuedData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumIssued = totalSumIssued + sumAPE;

                    //}
                }

                worksheet.Cell(4, 6).Value = totalSumIssued;

                worksheet.Range(worksheet.Cell(5, 2), worksheet.Cell(5, 3)).Merge();
                worksheet.Range(worksheet.Cell(5, 2), worksheet.Cell(5, 3)).Value = "Declined / Postponed";
                worksheet.Cell(5, 4).Value = declinePostponedData.Count();

                var percentCaseDecline = declinePostponedData.Count() > 0 ?
                    Math.Round((declinePostponedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(5, 5).Value = percentCaseDecline + "%";

                decimal totalSumDecline = 0;
                if (declinePostponedData.Any())
                {
                    totalSumDecline = declinePostponedData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = declinePostponedData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumDecline = totalSumDecline + sumAPE;

                    //}
                }

                worksheet.Cell(5, 6).Value = totalSumDecline;

                worksheet.Range(worksheet.Cell(6, 2), worksheet.Cell(6, 3)).Merge();
                worksheet.Range(worksheet.Cell(6, 2), worksheet.Cell(6, 3)).Value = "Withdraw";
                worksheet.Cell(6, 4).Value = withdrawData.Count();

                var percentCaseWithdraw = withdrawData.Count() > 0 ?
                    Math.Round((withdrawData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(6, 5).Value = percentCaseWithdraw + "%";

                decimal totalSumWithdraw = 0;
                if (withdrawData.Any())
                {
                    totalSumWithdraw = withdrawData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = withdrawData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumWithdraw = totalSumWithdraw + sumAPE;

                    //}
                }

                worksheet.Cell(6, 6).Value = totalSumWithdraw;

                worksheet.Range(worksheet.Cell(7, 2), worksheet.Cell(7, 3)).Merge();
                worksheet.Range(worksheet.Cell(7, 2), worksheet.Cell(7, 3)).Value = "Terminate (Freelook)";
                worksheet.Cell(7, 4).Value = terminateFreelookData.Count();

                var percentCaseTerminateF = terminateFreelookData.Count() > 0 ?
                    Math.Round((terminateFreelookData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(7, 5).Value = percentCaseTerminateF + "%";

                decimal totalSumTerminateF = 0;
                if (terminateFreelookData.Any())
                {
                    totalSumTerminateF = terminateFreelookData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = terminateFreelookData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumTerminateF = totalSumTerminateF + sumAPE;

                    //}
                }

                worksheet.Cell(7, 6).Value = totalSumTerminateF;

                worksheet.Range(worksheet.Cell(8, 2), worksheet.Cell(8, 3)).Merge();
                worksheet.Range(worksheet.Cell(8, 2), worksheet.Cell(8, 3)).Value = "Terminated (Other)";
                worksheet.Cell(8, 4).Value = terminateOtherData.Count();

                var percentCaseTerminateO = terminateOtherData.Count() > 0 ?
                     Math.Round((terminateOtherData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(8, 5).Value = percentCaseTerminateO + "%";

                decimal totalSumTerminateO = 0;
                if (terminateOtherData.Any())
                {
                    totalSumTerminateO = terminateOtherData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = terminateOtherData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumTerminateO = totalSumTerminateO + sumAPE;

                    //}
                }

                worksheet.Cell(8, 6).Value = totalSumTerminateO;

                worksheet.Range(worksheet.Cell(9, 2), worksheet.Cell(9, 3)).Merge();
                worksheet.Range(worksheet.Cell(9, 2), worksheet.Cell(9, 3)).Value = "Other (data entry, Ver, waiting UW)";
                worksheet.Cell(9, 4).Value = otherData.Count();

                var percentCaseOther = otherData.Count() > 0 ?
                     Math.Round((otherData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(9, 5).Value = percentCaseOther + "%";

                decimal totalSumOther = 0;
                if (otherData.Any())
                {
                    //var itemi = otherData.Select(c => c.POLICY_CODE).ToString();
                    totalSumOther = otherData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = otherData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumOther = totalSumOther + sumAPE;

                    //}
                }

                worksheet.Cell(9, 6).Value = totalSumOther;

                worksheet.Range(worksheet.Cell(10, 2), worksheet.Cell(10, 3)).Merge();
                worksheet.Range(worksheet.Cell(10, 2), worksheet.Cell(10, 3)).Value = "Conditional Acceptance";
                worksheet.Cell(10, 4).Value = conditionAcceptedData.Count();

                var percentCaseConAccept = conditionAcceptedData.Count() > 0 ?
                    Math.Round((conditionAcceptedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(10, 5).Value = percentCaseConAccept + "%";

                decimal totalSumConAccept = 0;
                if (conditionAcceptedData.Any())
                {
                    totalSumConAccept = conditionAcceptedData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = conditionAcceptedData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumConAccept = totalSumConAccept + sumAPE;

                    //}
                }

                worksheet.Cell(10, 6).Value = totalSumConAccept;

                worksheet.Range(worksheet.Cell(11, 2), worksheet.Cell(11, 3)).Merge();
                worksheet.Range(worksheet.Cell(11, 2), worksheet.Cell(11, 3)).Value = "Accepted (premium not paid)";
                worksheet.Cell(11, 4).Value = acceptedData.Count();

                var percentCaseAccept = acceptedData.Count() > 0 ?
                    Math.Round((acceptedData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(11, 5).Value = percentCaseAccept + "%";

                decimal totalSumAccept = 0;
                if (acceptedData.Any())
                {
                    totalSumAccept = acceptedData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = acceptedData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumAccept = totalSumAccept + sumAPE;

                    //}
                }

                worksheet.Cell(11, 6).Value = totalSumAccept;

                worksheet.Range(worksheet.Cell(12, 2), worksheet.Cell(12, 3)).Merge();
                worksheet.Range(worksheet.Cell(12, 2), worksheet.Cell(12, 3)).Value = "Total pending";
                worksheet.Cell(12, 4).Value = totalPendingData.Count();

                var percentCaseTotalPending = totalPendingData.Count() > 0 ?
                        Math.Round((totalPendingData.Count() / (decimal)totalCase) * 100, 2) : 0;

                worksheet.Cell(12, 5).Value = percentCaseTotalPending + "%";

                decimal totalSumTotalPending = 0;
                if (totalPendingData.Any())
                {
                    totalSumTotalPending = totalPendingData.Sum(c => c.APE.GetValueOrDefault());
                    //var groupPolicyID = totalPendingData.GroupBy(c => c.POLICY_ID);
                    //foreach (var itemGroup in groupPolicyID)
                    //{
                    //    var APIValue = apiList.FirstOrDefault(c => c.POLICY_ID == itemGroup.Key);
                    //    var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                    //    var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemGroup.Key);
                    //    if (initiueCode != null)
                    //    {
                    //        //sumAPI = sumAPI * 2;
                    //    }

                    //    var paymentFrequency = itemGroup.FirstOrDefault()?.RENEWAL_TYPE;
                    //    int frequency = 1;
                    //    if (!string.IsNullOrWhiteSpace(paymentFrequency))
                    //    {
                    //        frequency = Int32.Parse(paymentFrequency);
                    //    }
                    //    var sumAPE = CalculateFrequency(frequency, sumAPI);

                    //    totalSumTotalPending = totalSumTotalPending + sumAPE;

                    //}
                }

                worksheet.Cell(12, 6).Value = totalSumTotalPending;


                decimal totalSumAPE = totalSumWithdraw + totalSumTotalPending + totalSumTerminateO + totalSumTerminateF
                    + totalSumOther + totalSumIssued + totalSumDecline + totalSumConAccept + totalSumAccept;

                var percentAPE1 = totalSumSubmitted > 0 ?
                       Math.Round((totalSumSubmitted / totalSumAPE) * 100, 2) : 0;

                var percentAPE2 = totalSumIssued > 0 ?
                        Math.Round((totalSumIssued / totalSumAPE) * 100, 2) : 0;

                var percentAPE3 = totalSumDecline > 0 ?
                       Math.Round((totalSumDecline / totalSumAPE) * 100, 2) : 0;

                var percentAPE4 = totalSumWithdraw > 0 ?
                       Math.Round((totalSumWithdraw / totalSumAPE) * 100, 2) : 0;

                var percentAPE5 = totalSumTerminateF > 0 ?
                       Math.Round((totalSumTerminateF / totalSumAPE) * 100, 2) : 0;

                var percentAPE6 = totalSumTerminateO > 0 ?
                       Math.Round((totalSumTerminateO / totalSumAPE) * 100, 2) : 0;

                var percentAPE7 = totalSumOther > 0 ?
                       Math.Round((totalSumOther / totalSumAPE) * 100, 2) : 0;

                var percentAPE8 = totalSumConAccept > 0 ?
                       Math.Round((totalSumConAccept / totalSumAPE) * 100, 2) : 0;

                var percentAPE9 = totalSumAccept > 0 ?
                       Math.Round((totalSumAccept / totalSumAPE) * 100, 2) : 0;

                var percentAPE10 = totalSumTotalPending > 0 ?
                       Math.Round((totalSumTotalPending / totalSumAPE) * 100, 2) : 0;

                worksheet.Cell(3, 7).Value = percentAPE1 + "%";
                worksheet.Cell(4, 7).Value = percentAPE2 + "%";
                worksheet.Cell(5, 7).Value = percentAPE3 + "%";
                worksheet.Cell(6, 7).Value = percentAPE4 + "%";
                worksheet.Cell(7, 7).Value = percentAPE5 + "%";
                worksheet.Cell(8, 7).Value = percentAPE6 + "%";
                worksheet.Cell(9, 7).Value = percentAPE7 + "%";
                worksheet.Cell(10, 7).Value = percentAPE8 + "%";
                worksheet.Cell(11, 7).Value = percentAPE9 + "%";
                worksheet.Cell(12, 7).Value = percentAPE10 + "%";

                #endregion

                for (int i = 3; i <= 12; i++)
                {
                    worksheet.Range(worksheet.Cell(i, 2), worksheet.Cell(i, 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(i, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(i, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(i, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(i, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(i, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(i, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(i, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(i, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(i, 2), worksheet.Cell(i, 3)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    worksheet.Range(worksheet.Cell(i, 2), worksheet.Cell(i, 3)).Style.Font.Bold = true;
                }

                listData.ForEach(c => c.ProductCategory = Utility.CheckProductCategory(c.PolicyNo, c.BusinessUnit));
                var groupByProduct = totalPendingData.GroupBy(c => c.productCategory);
                var allPendingCode = await _repository.eBaoMemoLetter.GetPendingCode();

                worksheet.Range(worksheet.Cell(14, 2), worksheet.Cell(14, 7)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Range(worksheet.Cell(14, 2), worksheet.Cell(14, 7)).Style.Font.Bold = true;

                worksheet.Cell(14, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range(worksheet.Cell(14, 3), worksheet.Cell(14, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(14, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(14, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(14, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(14, 2).Value = "Product";
                worksheet.Range(worksheet.Cell(14, 3), worksheet.Cell(14, 4)).Merge();
                worksheet.Range(worksheet.Cell(14, 3), worksheet.Cell(14, 4)).Value = "Pending Reason";
                worksheet.Cell(14, 5).Value = "Case(s)";
                worksheet.Cell(14, 6).Value = "API(Bath)";
                worksheet.Cell(14, 7).Value = "APE(Bath)";

                int RowProduct = 15;

                decimal grandTotalSumAPIProduct = 0;
                decimal grandTotalSumAPEProduct = 0;
                int grandTotalCaseProduct = 0;

                List<string> productCateGory = new List<string> { "", "" };

                allPendingCode = allPendingCode.OrderBy(c => c.PENDING_ID).ToList();

                foreach (var itemProduct in groupByProduct)
                {
                    var policyCodeinProduct = itemProduct.Select(c => c.POLICY_CODE);
                    var count = policyCodeinProduct.Count();
                    //var contractMasterInProduct = totalPendingData.Where(c => policyCodeinProduct.Contains(c.POLICY_CODE));
                    //var pendingCauseInProduct = pendingCauseList.Where(e => contractMasterInProduct.Contains(e.policy_id));

                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Merge();
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Style.Font.Bold = true;
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
                    worksheet.Range(worksheet.Cell(RowProduct, 2), worksheet.Cell(RowProduct + allPendingCode.Count, 2)).Value = itemProduct.Key;

                    // for Pending Reason
                    decimal totalSumAPIProduct = 0;
                    decimal totalSumAPEProduct = 0;
                    int totalCaseProduct = 0;

                    foreach (var itemPendingCode in allPendingCode)
                    {
                        var dataCase = itemProduct.Where(c => c.pending_id == itemPendingCode.PENDING_ID);

                        int countCase = dataCase.Count();
                        decimal sumAPIProduct = 0;
                        decimal sumAPEProduct = 0;

                        sumAPEProduct = dataCase.Sum(c => c.APE.GetValueOrDefault());
                        foreach (var itemCase in dataCase)
                        {
                            //var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemCase.POLICY_ID);
                            decimal sumAPI = 0;

                            //var paymentFrequency = itemCase.RENEWAL_TYPE;
                            //int frequency = 1;
                            //if (!string.IsNullOrWhiteSpace(paymentFrequency))
                            //{
                            //    frequency = Int32.Parse(paymentFrequency);
                            //}
                            //var sumAPE = CalculateFrequency(frequency, sumAPI);

                            //var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemCase.POLICY_ID);
                            if (itemCase.Institute_code == 1)
                            {
                                sumAPI = itemCase.total_prem_af.GetValueOrDefault() * 2;
                            }
                            else
                            {
                                sumAPI = itemCase.total_prem_af.GetValueOrDefault();
                            }

                            sumAPIProduct = sumAPIProduct + sumAPI;

                        }

                        worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Merge();
                        worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Value = $"{itemPendingCode.PENDING_ID} {itemPendingCode.PEND_CAUSE}";
                        worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                        worksheet.Cell(RowProduct, 5).Value = countCase;
                        worksheet.Cell(RowProduct, 6).Value = sumAPIProduct;
                        worksheet.Cell(RowProduct, 7).Value = sumAPEProduct;

                        worksheet.Cell(RowProduct, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(RowProduct, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        worksheet.Cell(RowProduct, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                        worksheet.Cell(RowProduct, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(RowProduct, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        worksheet.Cell(RowProduct, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);


                        worksheet.Cell(RowProduct, 5).Style.NumberFormat.Format = currencyFormat;
                        worksheet.Cell(RowProduct, 5).DataType = XLDataType.Number;

                        worksheet.Cell(RowProduct, 6).Style.NumberFormat.Format = currencyFormat;
                        worksheet.Cell(RowProduct, 6).DataType = XLDataType.Number;

                        worksheet.Cell(RowProduct, 7).Style.NumberFormat.Format = currencyFormat;
                        worksheet.Cell(RowProduct, 7).DataType = XLDataType.Number;

                        RowProduct++;
                        totalSumAPIProduct = totalSumAPIProduct + sumAPIProduct;
                        totalSumAPEProduct = totalSumAPEProduct + sumAPEProduct;
                        totalCaseProduct = totalCaseProduct + countCase;
                    }

                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Merge();
                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Font.Bold = true;
                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Value = "Total";

                    worksheet.Cell(RowProduct, 5).Value = totalCaseProduct;
                    worksheet.Cell(RowProduct, 6).Value = totalSumAPIProduct;
                    worksheet.Cell(RowProduct, 7).Value = totalSumAPEProduct;

                    worksheet.Cell(RowProduct, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(RowProduct, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(RowProduct, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                    worksheet.Cell(RowProduct, 5).Style.Font.Bold = true;
                    worksheet.Cell(RowProduct, 6).Style.Font.Bold = true;
                    worksheet.Cell(RowProduct, 7).Style.Font.Bold = true;

                    worksheet.Cell(RowProduct, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(RowProduct, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheet.Cell(RowProduct, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheet.Cell(RowProduct, 5).Style.NumberFormat.Format = currencyFormat;
                    worksheet.Cell(RowProduct, 5).DataType = XLDataType.Number;

                    worksheet.Cell(RowProduct, 6).Style.NumberFormat.Format = currencyFormat;
                    worksheet.Cell(RowProduct, 6).DataType = XLDataType.Number;

                    worksheet.Cell(RowProduct, 7).Style.NumberFormat.Format = currencyFormat;
                    worksheet.Cell(RowProduct, 7).DataType = XLDataType.Number;

                    RowProduct++;
                    grandTotalCaseProduct = grandTotalCaseProduct + totalCaseProduct;
                    grandTotalSumAPIProduct = grandTotalSumAPIProduct + totalSumAPIProduct;
                    grandTotalSumAPEProduct = grandTotalSumAPEProduct + totalSumAPEProduct;
                }

                worksheet.Cell(RowProduct, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Merge();
                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Value = "Grand Total";
                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Range(worksheet.Cell(RowProduct, 3), worksheet.Cell(RowProduct, 4)).Style.Font.Bold = true;

                worksheet.Cell(RowProduct, 5).Value = grandTotalCaseProduct;
                worksheet.Cell(RowProduct, 6).Value = grandTotalSumAPIProduct;
                worksheet.Cell(RowProduct, 7).Value = grandTotalSumAPEProduct;

                worksheet.Cell(RowProduct, 5).Style.Font.Bold = true;
                worksheet.Cell(RowProduct, 6).Style.Font.Bold = true;
                worksheet.Cell(RowProduct, 7).Style.Font.Bold = true;

                worksheet.Cell(RowProduct, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(RowProduct, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(RowProduct, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                worksheet.Cell(RowProduct, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(RowProduct, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.Cell(RowProduct, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                worksheet.Cell(RowProduct, 5).Style.NumberFormat.Format = currencyFormat;
                worksheet.Cell(RowProduct, 5).DataType = XLDataType.Number;

                worksheet.Cell(RowProduct, 6).Style.NumberFormat.Format = currencyFormat;
                worksheet.Cell(RowProduct, 6).DataType = XLDataType.Number;

                worksheet.Cell(RowProduct, 7).Style.NumberFormat.Format = currencyFormat;
                worksheet.Cell(RowProduct, 7).DataType = XLDataType.Number;
                #endregion

                if (!preview)
                {
                    #region Pending
                    var documentList = listData.Select(c => c.DocumentNo).Distinct();
                    var agentCodeList = listData.Select(c => c.AgentCode).Distinct();
                    var policyCodeList = contractMaster.Select(c => c.POLICY_CODE).Distinct();
                    var policyIDList = contractMaster.Select(c => c.POLICY_ID).Distinct();

                    var contractMasterJoinData = await _repository.eBaoMemoLetter.GetContractMasterJoin(policyCodeList.ToList());
                    var documentJoinData = await _repository.eBaoMemoLetter.GetDocumentJoin(documentList.ToList());
                    var documentData = await _repository.eBaoMemoLetter.GetDocumentData(documentList.ToList());
                    var agentList = await _repository.eBaoMemoLetter.GetAgent(agentCodeList.ToList());
                    var uwPolicyList = await _repository.eBaoMemoLetter.GetUWPolicyList(policyIDList.ToList());
                    var memoLetterData = await _repository.eBaoMemoLetter.GetTempMemoLetterByPolicyCodeList(policyCodeList.ToList());

                    var worksheetPending = workbook.Worksheets.Add("Memo Report");

                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 31)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 31)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 31)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(4, 1), worksheetPending.Cell(4, 31)).Merge();
                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 31)).Style.Font.Bold = true;
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 31)).Style.Font.Bold = true;
                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 31)).Style.Font.Bold = true;

                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 31)).Value = "บริษัท อาคเนย์ประกันชีวิต จำกัด(มหาชน)";
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 31)).Value = "Memo Reports : สำหรับฝ่ายรับประกัน";

                    var headerDate = $"Submission Date from {request.submissionDateFrom.ToString("dd/MM/yyyy", _cultureTHInfo)}";
                    if (request.submissionDateTo.HasValue)
                    {
                        headerDate += $" to {request.submissionDateTo.Value.ToString("dd/MM/yyyy", _cultureTHInfo)}";
                    }

                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 31)).Value = headerDate;
                    worksheetPending.Range(worksheetPending.Cell(1, 1), worksheetPending.Cell(1, 31)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetPending.Range(worksheetPending.Cell(2, 1), worksheetPending.Cell(2, 31)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetPending.Range(worksheetPending.Cell(3, 1), worksheetPending.Cell(3, 31)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);


                    worksheetPending.Cell(1, 32).SetValue($"วันที่พิมพ์ : {DateTime.Now.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                    worksheetPending.Cell(2, 32).SetValue($"เวลาที่พิมพ์ : {DateTime.Now.ToString("HH:mm", _cultureTHInfo)}");
                    worksheetPending.Cell(3, 32).SetValue($"หน้าที่ : 1/1");

                    worksheetPending.Range(worksheetPending.Cell(5, 1), worksheetPending.Cell(5, 32)).Style.Fill.BackgroundColor = columnHeadXL;

                    worksheetPending.Range(worksheetPending.Cell(5, 1), worksheetPending.Cell(5, 32)).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    worksheetPending.Range(worksheetPending.Cell(5, 1), worksheetPending.Cell(5, 32)).Style.Font.Bold = true;

                    for (int i = 1; i <= 32; i++)
                    {
                        worksheetPending.Cell(5, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    }

                    worksheetPending.Cell(5, 1).Value = "Submission Date";
                    worksheetPending.Cell(5, 2).Value = "Memo Printed Date (First)";
                    worksheetPending.Cell(5, 3).Value = "Reply Date (First)";
                    worksheetPending.Cell(5, 4).Value = "Memo Printed Date (Last)";
                    worksheetPending.Cell(5, 5).Value = "Reply Date (Last)";
                    worksheetPending.Cell(5, 6).Value = "Policy No.";
                    worksheetPending.Cell(5, 7).Value = "Proposer Name";
                    worksheetPending.Cell(5, 8).Value = "Life Assured Name";
                    worksheetPending.Cell(5, 9).Value = "Life Assured Age";
                    worksheetPending.Cell(5, 10).Value = "Policy Status";
                    worksheetPending.Cell(5, 11).Value = "Accepted Date (First)";
                    worksheetPending.Cell(5, 12).Value = "Issue Date";
                    worksheetPending.Cell(5, 13).Value = "Withdraw Date";
                    worksheetPending.Cell(5, 14).Value = "NB Pending Reason";
                    worksheetPending.Cell(5, 15).Value = "Letter Type";
                    worksheetPending.Cell(5, 16).Value = "Memo Type";
                    worksheetPending.Cell(5, 17).Value = "Memo Code";
                    worksheetPending.Cell(5, 18).Value = "Memo Description";
                    worksheetPending.Cell(5, 19).Value = "Application Branch";
                    worksheetPending.Cell(5, 20).Value = "Service Branch";
                    worksheetPending.Cell(5, 21).Value = "Agent Region";
                    worksheetPending.Cell(5, 22).Value = "Group Name";
                    worksheetPending.Cell(5, 23).Value = "Agent Unit";
                    worksheetPending.Cell(5, 24).Value = "Agent Code";
                    worksheetPending.Cell(5, 25).Value = "Agent Name";
                    worksheetPending.Cell(5, 26).Value = "ตัวแทนมีสิทธิ NonMed แล้วแสดงค่า N";
                    worksheetPending.Cell(5, 27).Value = "Benefit Code";
                    worksheetPending.Cell(5, 28).Value = "Benefit Name";
                    worksheetPending.Cell(5, 29).Value = "Payment Frequency";
                    worksheetPending.Cell(5, 30).Value = "Premium (API)";
                    worksheetPending.Cell(5, 31).Value = "Premium APE";
                    worksheetPending.Cell(5, 32).Value = "ชื่อ User (status ล่าสุด)";

                    worksheetPending.Column(1).Width = 22;
                    worksheetPending.Column(2).Width = 22;
                    worksheetPending.Column(3).Width = 22;
                    worksheetPending.Column(4).Width = 22;
                    worksheetPending.Column(5).Width = 22;
                    worksheetPending.Column(6).Width = 22;
                    worksheetPending.Column(7).Width = 40;
                    worksheetPending.Column(8).Width = 40;
                    worksheetPending.Column(9).Width = 22;
                    worksheetPending.Column(10).Width = 22;
                    worksheetPending.Column(11).Width = 22;
                    worksheetPending.Column(12).Width = 22;
                    worksheetPending.Column(13).Width = 22;
                    worksheetPending.Column(14).Width = 25;
                    worksheetPending.Column(15).Width = 22;
                    worksheetPending.Column(16).Width = 25;
                    worksheetPending.Column(17).Width = 25;
                    worksheetPending.Column(18).Width = 80;
                    worksheetPending.Column(19).Width = 30;
                    worksheetPending.Column(20).Width = 30;
                    worksheetPending.Column(21).Width = 25;
                    worksheetPending.Column(22).Width = 25;
                    worksheetPending.Column(23).Width = 25;
                    worksheetPending.Column(24).Width = 25;
                    worksheetPending.Column(25).Width = 30;
                    worksheetPending.Column(26).Width = 35;
                    worksheetPending.Column(27).Width = 25;
                    worksheetPending.Column(28).Width = 40;
                    worksheetPending.Column(29).Width = 22;
                    worksheetPending.Column(30).Width = 30;
                    worksheetPending.Column(31).Width = 22;
                    worksheetPending.Column(32).Width = 30;

                    List<DataNotInMemo> dataNotInMemo = new List<DataNotInMemo>();
                    int startRowDataPending = 6;
                    var policyInTransaction = listData.Select(e => e.PolicyNo).Distinct();
                    var listDataNotInEmemo = contractMaster.Where(c => !policyInTransaction.Contains(c.POLICY_CODE)).Select(e => e.POLICY_CODE);

                    if (listDataNotInEmemo.Any())
                    {
                        dataNotInMemo = await _repository.eBaoMemoLetter.GetDataNotInMemo(listDataNotInEmemo.ToList());
                    }


                    foreach (var itemPending in listContractProposal)
                    {
                        DateTime? firstReply = null;
                        DateTime? lastedReply = null;
                        var contractMasterData = contractMaster.FirstOrDefault(c => c.POLICY_ID == itemPending.POLICY_ID);
                        var submissionDate = contractMasterData.SUBMISSION_DATE;

                        var memoData = memoLetterData.FirstOrDefault(c => c.POLICY_CODE == contractMasterData.POLICY_CODE);
                        var nbPending = contractMaster.FirstOrDefault(c => c.POLICY_ID == itemPending.POLICY_ID)?.PEND_CAUSE;

                        var APIValue = await _repository.eBaoMemoLetter.GetAPI(itemPending.POLICY_ID);

                        var sumAPI = APIValue != null ? APIValue.PolPREM : 0;
                        var contractJoinData = contractMasterJoinData.FirstOrDefault(c => c.policy_code == contractMasterData.POLICY_CODE);
                        var benefitCode = contractJoinData?.internal_id;
                        var benefitName = contractJoinData?.product_name;
                        var paymentFrequency = contractJoinData?.renewal_type;
                        int frequency = 1;

                        if (!string.IsNullOrWhiteSpace(paymentFrequency))
                        {
                            frequency = Int32.Parse(paymentFrequency);
                        }
                        var sumAPE = CalculateFrequency(frequency, sumAPI);

                        var initiueCode = Institute_code.FirstOrDefault(c => c.policy_id == itemPending.POLICY_ID);
                        if (initiueCode != null)
                        {
                            sumAPI = sumAPI * 2;
                        }

                        T_DM_EMEMO_AGENT_INFO_NEW agentInfo = new T_DM_EMEMO_AGENT_INFO_NEW();
                        if (listAgentInfo != null)
                        {
                            agentInfo = listAgentInfo.FirstOrDefault(c => c.POLICY_CODE == contractMasterData?.POLICY_CODE);
                        }


                        var uwPolicyData = uwPolicyList.Where(c => c.policy_id == itemPending.POLICY_ID && c.underwrite_time.HasValue).OrderBy(e => e.underwrite_time).FirstOrDefault();
                        DateTime? acceptDateFirst = null;
                        DateTime? withDrawDate = null;

                        if (uwPolicyData != null)
                        {
                            acceptDateFirst = uwPolicyData.underwrite_time;
                        }

                        if (itemPending.PROPOSAL_STATUS == "86")
                        {
                            withDrawDate = itemPending.update_time;
                        }


                        var allTransactionData = listData.Where(c => c.PolicyNo == contractMasterData?.POLICY_CODE);
                        var agentRegion = MappingAgentRegion(agentInfo?.BRANCH_CO_CODE);

                        if (allTransactionData.Any())
                        {
                            var transactionData = allTransactionData.FirstOrDefault();
                            firstReply = allTransactionData.Where(c => c.LetterStatus == "Replied" && c.Reply_Date.HasValue).OrderBy(e => e.Reply_Date).FirstOrDefault()?.Reply_Date;
                            lastedReply = allTransactionData.Where(c => c.LetterStatus == "Replied" && c.Reply_Date.HasValue).OrderByDescending(e => e.Reply_Date).FirstOrDefault()?.Reply_Date;

                            foreach (var itemTransaction in allTransactionData)
                            {
                                T_DM_EMEMO_AGENT_INFO agentInfoValue = null;
                                if (agentInfoData != null)
                                {
                                    agentInfoValue = agentInfoData.FirstOrDefault(c => c.AGENT_CODE == agentInfo?.AGENT_CODE);
                                }
                                DateTime? printedDateLast = null;

                                var history = listTransactionHistory.Where(c => c.MemoTransaction_ID == itemTransaction.TransactionID).OrderByDescending(e => e.Send_DateTime).FirstOrDefault();

                                if (history != null)
                                {
                                    printedDateLast = history.Send_DateTime;
                                }

                                var letterType = documentJoinData.FirstOrDefault(c => c.document_no == itemTransaction.DocumentNo)?.template_name;

                                var medNonMed = agentList.FirstOrDefault(c => c.agent_code == itemTransaction.AgentCode)?.med_indi;

                                worksheetPending.Cell(startRowDataPending, 1).SetValue(submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                                worksheetPending.Cell(startRowDataPending, 2).SetValue($"{transactionData.TransactionDate.ToString("dd/MM/yyyy", _cultureTHInfo)}");
                                worksheetPending.Cell(startRowDataPending, 3).SetValue(firstReply.HasValue ? firstReply.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                                worksheetPending.Cell(startRowDataPending, 4).SetValue(printedDateLast.HasValue ? printedDateLast.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                                worksheetPending.Cell(startRowDataPending, 5).SetValue(lastedReply.HasValue ? lastedReply.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);

                                worksheetPending.Cell(startRowDataPending, 6).SetValue(transactionData.PolicyNo);
                                worksheetPending.Cell(startRowDataPending, 7).Value = transactionData.PolicyName;
                                worksheetPending.Cell(startRowDataPending, 8).Value = transactionData.LifeAssuredName;
                                worksheetPending.Cell(startRowDataPending, 9).Value = $"{ transactionData.EntryAgeLifeAssured} ปี";
                                worksheetPending.Cell(startRowDataPending, 10).Value = contractMasterData.STATUS_DESC;

                                worksheetPending.Cell(startRowDataPending, 11).SetValue(acceptDateFirst.HasValue ? acceptDateFirst.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                                worksheetPending.Cell(startRowDataPending, 12).SetValue(contractMasterData.ISSUE_DATE.HasValue ? contractMasterData.ISSUE_DATE.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                                worksheetPending.Cell(startRowDataPending, 13).SetValue(withDrawDate.HasValue ? withDrawDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);

                                worksheetPending.Cell(startRowDataPending, 14).Value = nbPending; // NB PEN
                                worksheetPending.Cell(startRowDataPending, 15).Value = letterType; // LETTER Type
                                worksheetPending.Cell(startRowDataPending, 16).Value = !string.IsNullOrWhiteSpace(itemTransaction.MemoType_Name) ? itemTransaction.MemoType_Name : itemTransaction.MemoType;
                                worksheetPending.Cell(startRowDataPending, 17).Value = itemTransaction.MemoCode;
                                worksheetPending.Cell(startRowDataPending, 18).Value = !string.IsNullOrWhiteSpace(itemTransaction.Description) ? itemTransaction.Description : itemTransaction.MemoCodeDescription;
                                worksheetPending.Cell(startRowDataPending, 19).Value = agentInfo != null ? agentInfo.APPLICATION_BRANCH_NAME : string.Empty;
                                worksheetPending.Cell(startRowDataPending, 20).Value = agentInfo != null ? agentInfo.BRANCH_NAME : string.Empty;
                                worksheetPending.Cell(startRowDataPending, 21).Value = agentRegion;
                                worksheetPending.Cell(startRowDataPending, 22).Value = agentInfoValue != null ? agentInfoValue.AGENT_GROUP : string.Empty;
                                worksheetPending.Cell(startRowDataPending, 23).Value = itemTransaction.AgentUnit;
                                worksheetPending.Cell(startRowDataPending, 24).Value = itemTransaction.AgentCode;
                                worksheetPending.Cell(startRowDataPending, 25).Value = itemTransaction.AgentName;

                                worksheetPending.Cell(startRowDataPending, 26).Value = medNonMed;
                                worksheetPending.Cell(startRowDataPending, 27).Value = benefitCode;
                                worksheetPending.Cell(startRowDataPending, 28).Value = benefitName;
                                worksheetPending.Cell(startRowDataPending, 29).Value = paymentFrequency;
                                worksheetPending.Cell(startRowDataPending, 30).Value = sumAPI;
                                worksheetPending.Cell(startRowDataPending, 31).Value = sumAPE;
                                worksheetPending.Cell(startRowDataPending, 32).Value = contractMasterData.user_name;

                                worksheetPending.Cell(startRowDataPending, 31).Style.NumberFormat.Format = currencyFormat;
                                worksheetPending.Cell(startRowDataPending, 31).DataType = XLDataType.Number;
                                worksheetPending.Cell(startRowDataPending, 30).Style.NumberFormat.Format = currencyFormat;
                                worksheetPending.Cell(startRowDataPending, 30).DataType = XLDataType.Number;
                                worksheetPending.Cell(startRowDataPending, 29).Style.NumberFormat.Format = currencyFormat;
                                worksheetPending.Cell(startRowDataPending, 29).DataType = XLDataType.Number;

                                for (int i = 1; i <= 32; i++)
                                {
                                    worksheetPending.Cell(startRowDataPending, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                }

                                worksheetPending.Cell(startRowDataPending, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 15).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 29).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 30).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 31).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                worksheetPending.Cell(startRowDataPending, 32).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                                startRowDataPending++;
                            }
                        }
                        else
                        {
                            var dataNotInMemoPolicy = dataNotInMemo.FirstOrDefault(c => c.POLICY_CODE == contractMasterData?.POLICY_CODE);
                            var medNonMed = agentList.FirstOrDefault(c => c.agent_code == agentInfo?.AGENT_CODE)?.med_indi;

                            T_DM_EMEMO_AGENT_INFO agentInfoValue = null;
                            if(agentInfoData != null)
                            {
                                agentInfoValue = agentInfoData.FirstOrDefault(c => c.AGENT_CODE == agentInfo?.AGENT_CODE);
                            }

                            
                            var letterType = documentJoinData.FirstOrDefault(c => c.document_no == dataNotInMemoPolicy?.document_no)?.template_name;

                            worksheetPending.Cell(startRowDataPending, 1).SetValue(submissionDate.HasValue ? submissionDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                            worksheetPending.Cell(startRowDataPending, 2).SetValue(dataNotInMemoPolicy.insert_time.ToString("dd/MM/yyyy", _cultureTHInfo));
                            worksheetPending.Cell(startRowDataPending, 3).SetValue(firstReply.HasValue ? firstReply.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                            worksheetPending.Cell(startRowDataPending, 4).SetValue(string.Empty);
                            worksheetPending.Cell(startRowDataPending, 5).SetValue(lastedReply.HasValue ? lastedReply.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);

                            worksheetPending.Cell(startRowDataPending, 6).SetValue(contractMasterData?.POLICY_CODE);
                            worksheetPending.Cell(startRowDataPending, 7).Value = dataNotInMemoPolicy.ProposerName != null ? dataNotInMemoPolicy.ProposerName.Trim() : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 8).Value = dataNotInMemoPolicy.LifeAssuredName != null ? dataNotInMemoPolicy.LifeAssuredName.Trim() : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 9).Value = $"{ dataNotInMemoPolicy.LifeAssuredAge} ปี";
                            worksheetPending.Cell(startRowDataPending, 10).Value = contractMasterData?.STATUS_DESC;

                            worksheetPending.Cell(startRowDataPending, 11).SetValue(acceptDateFirst.HasValue ? acceptDateFirst.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                            worksheetPending.Cell(startRowDataPending, 12).SetValue(contractMasterData != null && contractMasterData.ISSUE_DATE.HasValue ? contractMasterData.ISSUE_DATE.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);
                            worksheetPending.Cell(startRowDataPending, 13).SetValue(withDrawDate.HasValue ? withDrawDate.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : string.Empty);

                            worksheetPending.Cell(startRowDataPending, 14).Value = nbPending; // NB PEN
                            worksheetPending.Cell(startRowDataPending, 15).Value = letterType; // LETTER Type
                            worksheetPending.Cell(startRowDataPending, 16).Value = string.Empty;
                            worksheetPending.Cell(startRowDataPending, 17).Value = string.Empty;
                            worksheetPending.Cell(startRowDataPending, 18).Value = string.Empty;
                            worksheetPending.Cell(startRowDataPending, 19).Value = agentInfo != null ? agentInfo.APPLICATION_BRANCH_NAME : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 20).Value = agentInfo != null ? agentInfo.BRANCH_NAME : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 21).Value = agentRegion;
                            worksheetPending.Cell(startRowDataPending, 22).Value = agentInfoValue != null ? agentInfoValue.AGENT_GROUP : string.Empty;
                            worksheetPending.Cell(startRowDataPending, 23).Value = agentInfo?.UNIT_NAME;
                            worksheetPending.Cell(startRowDataPending, 24).Value = agentInfo?.AGENT_CODE;
                            worksheetPending.Cell(startRowDataPending, 25).Value = $"{agentInfo?.FIRST_NAME} {agentInfo?.LAST_NAME}";

                            worksheetPending.Cell(startRowDataPending, 26).Value = medNonMed;
                            worksheetPending.Cell(startRowDataPending, 27).Value = benefitCode;
                            worksheetPending.Cell(startRowDataPending, 28).Value = benefitName;
                            worksheetPending.Cell(startRowDataPending, 29).Value = paymentFrequency;
                            worksheetPending.Cell(startRowDataPending, 30).Value = sumAPI;
                            worksheetPending.Cell(startRowDataPending, 31).Value = sumAPE;
                            worksheetPending.Cell(startRowDataPending, 32).Value = dataNotInMemoPolicy.user_name;


                            worksheetPending.Cell(startRowDataPending, 31).Style.NumberFormat.Format = currencyFormat;
                            worksheetPending.Cell(startRowDataPending, 31).DataType = XLDataType.Number;
                            worksheetPending.Cell(startRowDataPending, 30).Style.NumberFormat.Format = currencyFormat;
                            worksheetPending.Cell(startRowDataPending, 30).DataType = XLDataType.Number;
                            worksheetPending.Cell(startRowDataPending, 29).Style.NumberFormat.Format = currencyFormat;
                            worksheetPending.Cell(startRowDataPending, 29).DataType = XLDataType.Number;

                            for (int i = 1; i <= 32; i++)
                            {
                                worksheetPending.Cell(startRowDataPending, i).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            }

                            worksheetPending.Cell(startRowDataPending, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 11).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 12).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 13).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 15).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 26).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 27).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 29).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 30).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 31).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            worksheetPending.Cell(startRowDataPending, 32).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                            startRowDataPending++;
                        }



                    }
                    #endregion
                }


                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    string htmlBody = string.Empty;

                    if (preview)
                    {
                        Workbook workbookC = new Workbook();
                        workbookC.LoadFromStream(stream);
                        Worksheet sheet = workbookC.Worksheets[0];

                        string fileSave = $"Report4{Guid.NewGuid()}";

                        sheet.SaveToHtml(fileSave);

                        string excelHtmlPath = Path.GetFullPath(Path.Combine(fileSave));
                        using (StreamReader reader = File.OpenText(excelHtmlPath))
                        {
                            htmlBody = reader.ReadToEnd();
                        }

                        var regex = new Regex(@"<[hH][2][^>]*>[^<]*</[hH][2]\s*>", RegexOptions.Compiled | RegexOptions.Multiline);
                        htmlBody = regex.Replace(htmlBody, "");

                        File.Delete(excelHtmlPath);
                    }

                    return (content, htmlBody);
                }
            }
        }

        private string MappingAgentRegion(string branchCOCode)
        {
            string agentRegion = string.Empty;

            if (branchCOCode == "1")
            {
                agentRegion = "Agency Team A";
            }
            else if (branchCOCode == "2")
            {
                agentRegion = "Agency Team B";
            }
            else if (branchCOCode == "3")
            {
                agentRegion = "Agency Digital";
            }
            else if (branchCOCode == "4")
            {
                agentRegion = "Agency Upper Northeast";
            }
            else if (branchCOCode == "5")
            {
                agentRegion = "Agency Lower Northeast";
            }
            else if (branchCOCode == "6")
            {
                agentRegion = "Wealth Partners";
            }

            return agentRegion;
        }

        private decimal CalculateFrequency(int? frequency, decimal? api)
        {
            decimal? sumAPE = 0;
            if (frequency == 1)
            {
                sumAPE = api * 1;
            }
            else if (frequency == 2)
            {
                sumAPE = api * 2;
            }
            else if (frequency == 3)
            {
                sumAPE = api * 4;
            }
            else if (frequency == 4)
            {
                sumAPE = api * 12;
            }
            else if (frequency == 5)
            {
                sumAPE = api * 1;
            }
            else
            {
                sumAPE = api * 1;
            }

            return sumAPE.GetValueOrDefault();
        }
    }

}


