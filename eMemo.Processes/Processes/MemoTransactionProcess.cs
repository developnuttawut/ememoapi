﻿using DinkToPdf;
using DinkToPdf.Contracts;
using eMemo.Helper;
using eMemo.Model.Base;
using eMemo.Model.Entity;
using eMemo.Model.Entity.eBaoTemp;
using eMemo.Model.ExternalRequest;
using eMemo.Model.Request.EMemoTransaction;
using eMemo.Model.Request.MemoMapping;
using eMemo.Model.Response;
using eMemo.Model.Response.EMemoTransaction;
using eMemo.Model.Response.MemoMapping;
using eMemo.Model.Response.RuleManagement;
using eMemo.Repositories;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Processes.Processes
{
    public class MemoTransactionProcess
    {
        private readonly Repository _repository;
        private AppSettingHelper appSettingHelper;
        private readonly IConverter _converter;
        private IHttpContextAccessor _httpContextAccessor;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(MemoTransactionProcess));
        public MemoTransactionProcess(Repository repository, IConverter converter, IHttpContextAccessor httpContextAccessor)
        {
            _repository = repository;
            appSettingHelper = new AppSettingHelper();
            _converter = converter;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<SaveDataResponse> RetrieveDataFromTemp()
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                //var dataNotTransfer = await _repository.eBaoMemoLetter.GetTempMemoLetterBy(
                //   c => c.IS_TRANSFER != "Y" && c.MEMO_STATUS == "Printed"
                //   && c.POLICY_STATUS == "Underwriting in Progress");

                var dataNotTransfer = await _repository.eBaoMemoLetter.GetTempMemoLetterNotTransfer();
                if (dataNotTransfer.Any())
                {
                    var listDocument = dataNotTransfer.Select(c => c.DOCUMENT_NO);
                    var allStagingDetail = await _repository.eBaoMemoLetter.GetTempStagingDetailByDocument(listDocument.ToList());

                    List<CreateMemoTransactionRequest> listCreateRequest = new List<CreateMemoTransactionRequest>();

                    foreach (var itemData in dataNotTransfer)
                    {
                        var stagingDetail = allStagingDetail.Where(c => c.DOCUMENT_NO == itemData.DOCUMENT_NO);

                        List<MemoDetail> listDetail = new List<MemoDetail>();

                        if (stagingDetail.Any())
                        {
                            listDetail = stagingDetail.Select(c => new MemoDetail
                            {
                                memoCode = !string.IsNullOrWhiteSpace(c.MEMO_CODE) ? c.MEMO_CODE.Trim().Replace(" ", "") : c.MEMO_CODE,
                                memoType = c.MEMO_ITEM_TYPE,
                                memoDescription = c.MEMO_DESCRIPTION,
                                memoComment = c.COMMENTS
                            }).ToList();
                        }

                        CreateMemoTransactionRequest request = new CreateMemoTransactionRequest
                        {
                            agentCode = itemData.AGENT_CODE,
                            agentName = itemData.AGENT_NAME,
                            documentNo = itemData.DOCUMENT_NO,
                            citizenID = itemData.CITIZEN_ID,
                            mainPlan = itemData.PRODUCT_CODE,
                            proposalNo = itemData.PROPOSAL_CODE,
                            proposalDate = itemData.PROPOSAL_DATE.GetValueOrDefault(),
                            policyName = itemData.POLICY_NAME,
                            policyNo = itemData.POLICY_CODE,
                            userID = itemData.CREATE_USER,
                            transactionDate = itemData.CREATE_DATE.GetValueOrDefault(),
                            eBaoMemoStatus = itemData.MEMO_STATUS,
                            policyStatus = itemData.POLICY_STATUS,
                            detail = listDetail,
                            entryAgeLifeAssured = itemData.LA_AGE.ToString(),
                            laAddress = itemData.LA_ADDRESS,
                            laEmail = itemData.LA_EMAIL,
                            laPhoneNo = itemData.LA_PHONE,
                            lifeAssuredName = itemData.LA_NAME,
                            phAddress = itemData.PH_ADDRESS,
                            phEmail = itemData.PH_EMAIL,
                            phPhoneNo = itemData.PH_PHONE,
                            round = 1,
                            partyID = itemData.CITIZEN_ID,
                        };

                        listCreateRequest.Add(request);
                    }

                    response = await CreateMemoTransaction(listCreateRequest, dataNotTransfer);
                }

                if(response.status == null)
                {
                    response.success = true;
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "Retrieve Success"
                    };
                }        
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }
            finally
            {
                _httpContextAccessor.HttpContext.Response.OnCompleted(async () =>
                {
                    await UpdateStatusFromTemp();
                    await UpdateAgentData();
                });
            }

            return response;
        }

        public async Task<SaveDataResponse> UpdateStatusFromTemp()
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                //var dataUpdateStatus = await _repository.eBaoMemoLetter.GetTempMemoLetterBy(
                //   c => c.IS_TRANSFER == "Y" && (c.MEMO_STATUS != "Printed" || c.POLICY_STATUS != "Underwriting In progress"));

                var dataUpdateStatus = await _repository.eBaoMemoLetter.GetTempMemoLetterUpdateStatus();

                var ebaoStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var ebaoStatusReplied = ebaoStatusList.FirstOrDefault(e => e.EBaoMemoStatus == "Replied");
                var ebaoStatusDiscard = ebaoStatusList.FirstOrDefault(e => e.EBaoMemoStatus == "Discard");
                var ebaoStatusIssued = ebaoStatusList.FirstOrDefault(e => e.EBaoMemoStatus == "Issued");
                var deliveryStautsList = _repository.memoTransaction.GetAllDeliveryStatus();
                var deliveryStautsSuccess = deliveryStautsList.FirstOrDefault(c => c.DeliveryStatus == "Successfully");

                if (dataUpdateStatus.Any())
                {
                    var listDocument = dataUpdateStatus.Select(c => c.DOCUMENT_NO);
                    var transactionHeaderByDocument = await _repository.memoTransaction.GetMemoTransactionBy(c => listDocument.Contains(c.DocumentNo));

                    var memoIDUpdate = transactionHeaderByDocument.Select(c => c.ID);
                    var allMemoDetailUpdateData = _repository.memoTransaction.GetMemoTransactionDetailBy(c => memoIDUpdate.Contains(c.MemoTransaction_ID) && string.IsNullOrWhiteSpace(c.LetterStatus));

                    foreach (var itemData in transactionHeaderByDocument)
                    {
                        var dataTemp = dataUpdateStatus.FirstOrDefault(e => e.DOCUMENT_NO == itemData.DocumentNo);
                        var ebaoStatusUpdate = ebaoStatusList.FirstOrDefault(e => e.EBaoMemoStatus == dataTemp.MEMO_STATUS);

                        if ((itemData.EBaoMemoStatus_ID != ebaoStatusUpdate?.ID) || itemData.PolicyStatus != dataTemp.POLICY_STATUS)
                        {
                            if (dataTemp.MEMO_STATUS == "Replied")
                            {
                                itemData.EBaoMemoStatus_ID = ebaoStatusReplied.ID;
                                itemData.EBaoRepliedDate = dataTemp.STATUS_DATE;
                                itemData.EBaoRepliedUser = dataTemp.REPLIED_USER;
                            }
                            else if (dataTemp.MEMO_STATUS == "Discarded")
                            {
                                itemData.EBaoMemoStatus_ID = ebaoStatusDiscard.ID;
                            }
                            else if (dataTemp.MEMO_STATUS == "Issued")
                            {
                                itemData.EBaoMemoStatus_ID = ebaoStatusIssued.ID;
                            }

                            itemData.PolicyStatus = dataTemp.POLICY_STATUS;
                            if (itemData.EMemoStatus == "New")
                            {
                                itemData.EMemoStatus = string.Empty;
                            }

                            itemData.Updated_By = "eMemo";
                            itemData.Updated_Date = Utility.GetDateNowThai();

                            if (dataTemp.MEMO_STATUS == "Replied" || dataTemp.MEMO_STATUS == "Discarded")
                            {
                                List<TTDMemoTransactionDetail> memoDetailUpdateData = new List<TTDMemoTransactionDetail>();

                                itemData.EMemoStatus = dataTemp.MEMO_STATUS;
                                itemData.DeliveryStatus_ID = deliveryStautsSuccess.ID;
                                itemData.Round = null;
                                itemData.EstimateTime = null;

                                memoDetailUpdateData = allMemoDetailUpdateData.Where(c => c.MemoTransaction_ID == itemData.ID && string.IsNullOrWhiteSpace(c.LetterStatus)).ToList();
                                foreach (var itemDetail in memoDetailUpdateData)
                                {
                                    itemDetail.LetterStatus = dataTemp.MEMO_STATUS;
                                    itemDetail.Reply_By = !string.IsNullOrWhiteSpace(dataTemp.REPLIED_USER) ? dataTemp.REPLIED_USER : dataTemp.UPDATE_USER;
                                    itemDetail.Source = "eBao";
                                    itemDetail.Reply_Date = dataTemp.STATUS_DATE;
                                }
                                await _repository.memoTransaction.UpdateMemoTransactionDetailList(memoDetailUpdateData);
                            }

                            await _repository.memoTransaction.UpdateMemoTransactionHeaderList(itemData);
                        }
                    }
                }

                response.success = true;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "บันทึกข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SearchMemoTransactionResponse> SearchMemoTransaction(SearchMemoTransactionRequest request)
        {
            SearchMemoTransactionResponse response = new SearchMemoTransactionResponse();

            try
            {
                bool bSearch = false;
                var eboaStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var eboaStatusPrinted = eboaStatusList.FirstOrDefault(c => c.EBaoMemoStatus == "Printed");

                var result = await _repository.memoTransaction.GetMemoTransactionBy(
                    c => (c.PolicyStatus == "Underwriting in Progress")
                    && c.EBaoMemoStatus_ID == eboaStatusPrinted.ID);

                var deliveryStatusList = _repository.memoTransaction.GetAllDeliveryStatus();

                if (!string.IsNullOrEmpty(request.documentNo))
                {
                    result = result.Where(w => w.DocumentNo.ToUpper().EndsWith(request.documentNo.ToUpper())).ToList();
                    bSearch = true;
                }

                if (request.transactionDateTo != null)
                {
                    result = result.Where(w => w.TransactionDate.Date >= request.transactionDateForm.Value.Date && w.TransactionDate.Date <= request.transactionDateTo.Value.Date).ToList();
                    bSearch = true;
                }
                else if (request.transactionDateForm != null)
                {
                    result = result.Where(w => w.TransactionDate.Date == request.transactionDateForm.Value.Date).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.policyNo))
                {
                    result = result.Where(w => w.PolicyNo.ToUpper().Equals(request.policyNo.ToUpper())).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.proposalNo))
                {
                    result = result.Where(w => w.ProposalNo.ToUpper().Equals(request.proposalNo.ToUpper())).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrWhiteSpace(request.ebaoMemoStatusID))
                {
                    result = result.Where(w => w.EBaoMemoStatus_ID == request.ebaoMemoStatusID).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrWhiteSpace(request.deliveryStatus))
                {
                    result = result.Where(w => w.DeliveryStatus_ID == request.deliveryStatus).ToList();
                    bSearch = true;
                }

                if (request.deliveryRound.HasValue)
                {
                    result = result.Where(w => w.Round.Equals(request.deliveryRound)).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.citizenID))
                {
                    result = result.Where(w => w.CitizenID.ToUpper().Equals(request.citizenID.ToUpper())).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.partyID))
                {
                    result = result.Where(w => w.PartyID.ToUpper().Equals(request.partyID.ToUpper())).ToList();
                    bSearch = true;
                }


                if (!string.IsNullOrEmpty(request.customerName))
                {
                    result = result.Where(w => w.PolicyName.ToUpper().Contains($"{request.customerName.ToUpper()}")).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.customerSurname))
                {
                    result = result.Where(w => w.PolicyName.ToUpper().Contains($"{request.customerSurname.ToUpper()}")).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.agentCode))
                {
                    result = result.Where(w => w.AgentCode.ToUpper().Contains(request.agentCode.ToUpper())).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.agentName))
                {
                    result = result.Where(w => w.AgentName.ToUpper().Contains($"{request.agentName.ToUpper()}")).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentSurname))
                {
                    result = result.Where(w => w.AgentName.ToUpper().Contains($"{request.agentSurname.ToUpper()}")).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentUnit))
                {
                    result = result.Where(w => w.AgentUnit.ToUpper().Contains(request.agentUnit.ToUpper())).ToList();
                    bSearch = true;
                }

                if (!string.IsNullOrEmpty(request.gentGroup))
                {
                    result = result.Where(w => w.AgentGroup.ToUpper().Contains(request.gentGroup.ToUpper())).ToList();
                    bSearch = true;
                }

                if (result.Any() && bSearch)
                {
                    var data = result.Select(s => new MemoTransactionData()
                    {
                        transactionID = s.ID,
                        documentNo = s.DocumentNo,
                        transactionDate = s.TransactionDate,
                        policyNo = s.PolicyNo,
                        mainPlan = s.MainPlan,
                        proposalNo = s.ProposalNo,
                        proposalDate = s.ProposalDate,
                        policyName = s.PolicyName,
                        citizenID = s.CitizenID,
                        policyStatus = s.PolicyStatus,
                        agentCode = s.AgentCode,
                        agentName = s.AgentName,
                        agentUnit = s.AgentUnit,
                        agentGroup = s.AgentGroup,
                        memoStatus = s.EMemoStatus,
                        eBaoMemoStatusID = s.EBaoMemoStatus_ID,
                        eBaoMemoStatus = eboaStatusList.FirstOrDefault(c => c.ID == s.EBaoMemoStatus_ID)?.EBaoMemoStatus,
                        round = s.Round.GetValueOrDefault(),
                        estimateTime = s.EstimateTime,
                        deliveryStatusID = s.DeliveryStatus_ID,
                        deliveryStatus = deliveryStatusList.FirstOrDefault(c => c.ID == s.DeliveryStatus_ID)?.DeliveryStatus
                    }).ToList();

                    response.data = data;
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "ค้นหาข้อมูลสำเร็จ"
                    };
                }
                else
                {
                    response.data = new List<MemoTransactionData>();
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> UpdateDeliveryStatus(UpdateDeliveryStatusRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                List<bool> saveResult = new List<bool>();

                foreach (var itemRequest in request.updateData)
                {
                    bool result = await _repository.memoTransaction.UpdateDeliveryStatus(itemRequest, request.userID);
                    saveResult.Add(result);
                }

                response.success = saveResult.TrueForAll(c => c);
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = saveResult.TrueForAll(c => c) ? "บันทึกข้อมูลสำเร็จ" : "บันทึกข้อมูลไม่สำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> SaveMemoDocument(SaveMemoDocumentRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                string errorMessae = string.Empty;
                var allMemoTransactionDetail = _repository.memoTransaction.GetMemoTransactionDetailBy(c => c.MemoTransaction_ID == request.memoTransactionID);
                var memoDetailIDUpdate = request.memoDocumentData.Where(c => c.edit).Select(e => e.memoDetailID.GetValueOrDefault());
                var memoCodeAdd = request.memoDocumentData.Where(c => c.add).Select(e => e.memoCode);
                var memoCodeDelete = request.memoDocumentData.Where(c => c.delete).Select(e => e.memoDetailID);
                var allMemoType = await _repository.memoMapping.GetAllMemoType();
                var allMemoCode = await _repository.memoMapping.GetAllMemoCode();

                var listMemoCodeID = allMemoCode.Select(e => e.ID);
                var allMemoCodeAttachmentList = _repository.memoMapping.GetMemoCodeAttachmentBy(d => listMemoCodeID.Contains(d.MemoCode_ID));

                var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.ID == request.memoTransactionID);

                // Add
                if (memoCodeAdd.Any())
                {
                    foreach (var itemDetail in request.memoDocumentData.Where(c => c.add))
                    {
                        var memoTypeData = allMemoType.Where(c => !c.Flag_Delete && c.Status).FirstOrDefault(c => c.MemoType_Name == itemDetail.memoType);
                        var memoMappingData = allMemoCode.Where(c => !c.Flag_Delete && c.Status).FirstOrDefault(c => c.MemoCode == itemDetail.memoCode);

                        TTDMemoTransactionDetail dataDetail = new TTDMemoTransactionDetail
                        {
                            MemoType = itemDetail.memoType,
                            MemoCode = itemDetail.memoCode,
                            MemoTransaction_ID = request.memoTransactionID,
                            DocumentNo = transactionData.DocumentNo,
                            MemoType_Sequence = memoTypeData.Sequence,
                            MemoCode_Sequence = memoMappingData.Sequence,
                            MemoCodeDescription = memoMappingData.Description,
                            Comment = itemDetail.comment,
                            Created_Date = Utility.GetDateNowThai(),
                            Created_By = request.userID
                        };

                        var saveDetailResult = await _repository.memoTransaction.AddMemoTransactionDetail(dataDetail);

                        if (saveDetailResult != null)
                        {
                            var memoCodeAttachmentList = allMemoCodeAttachmentList.Where(c => c.MemoCode_ID == memoMappingData.ID);

                            if (memoCodeAttachmentList.Any())
                            {
                                // Insert Attachment
                                string applicationName = appSettingHelper.GetConfiguration("ApplicationNameSaveFile");

                                SaveFileRequest saveFileList = new SaveFileRequest();
                                saveFileList.application = applicationName;

                                List<FileDetail> listFileDetail = new List<FileDetail>();
                                List<TTDMemoTransactionAttachment> listFileAtachment = new List<TTDMemoTransactionAttachment>();

                                foreach (var itemAttachment in memoCodeAttachmentList.OrderBy(c => c.Created_Date))
                                {
                                    var fileFromDMC = await _repository.dmcFile.GetFileDMC(itemAttachment.File_Path);
                                    string referenceID = Guid.NewGuid().ToString();
                                    itemAttachment.Reference_ID = referenceID;

                                    if (fileFromDMC != null && fileFromDMC.responseCode.resultCode == "Ok")
                                    {
                                        FileDetail fileData = new FileDetail
                                        {
                                            filename = referenceID + ".pdf",
                                            extension = itemAttachment.File_Extension,
                                            data = fileFromDMC.base64
                                        };

                                        listFileDetail.Add(fileData);
                                    }
                                    else
                                    {
                                        _log4net.Error(fileFromDMC.responseCode.description);
                                    }

                                    TTDMemoTransactionAttachment memoCodeAttachment = new TTDMemoTransactionAttachment
                                    {
                                        Reference_ID = referenceID,
                                        File_Extension = ".pdf",
                                        File_Type = itemAttachment.File_Type,
                                        File_Size = itemAttachment.File_Size,
                                        File_Path = "",
                                        File_Name = itemAttachment.File_Name,
                                        Created_By = request.userID,
                                        Created_Date = Utility.GetDateNowThai()
                                    };

                                    listFileAtachment.Add(memoCodeAttachment);

                                }

                                saveFileList.files = listFileDetail;

                                var saveFileResponse = await _repository.dmcFile.SaveFileDMC(saveFileList);

                                if (saveFileResponse != null && saveFileResponse.operationStatus != null && saveFileResponse.operationStatus.statusCode == 200)
                                {
                                    var saveFileResult = saveFileResponse.files;

                                    List<TTDMemoTransactionAttachment> listSaveAttachmentDetail = listFileAtachment.Select(c => new TTDMemoTransactionAttachment
                                    {
                                        MemoTransaction_ID = request.memoTransactionID,
                                        Reference_ID = c.Reference_ID,
                                        File_Name = c.File_Name,
                                        File_Path = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.data,
                                        File_Size = c.File_Size,
                                        File_Type = c.File_Type,
                                        File_Extension = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.extension,
                                        MemoTransactionDetail_ID = saveDetailResult.ID,
                                        Created_By = request.userID,
                                        Created_Date = Utility.GetDateNowThai()
                                    }).ToList();

                                    await _repository.memoTransaction.SaveEMemoTransactionDetailAttachment(listSaveAttachmentDetail);
                                }
                                else
                                {
                                    _log4net.Error(saveFileResponse.operationStatus.description);
                                    errorMessae += $"{itemDetail.memoCode} Save Attachment ไม่สำเร็จ {saveFileResponse?.operationStatus?.description}\n";
                                }
                            }

                            if (itemDetail.attachments != null && itemDetail.attachments.Any())
                            {
                                // Insert Attachment
                                string applicationName = appSettingHelper.GetConfiguration("ApplicationNameSaveFile");

                                SaveFileRequest saveFileList = new SaveFileRequest();
                                saveFileList.application = applicationName;

                                List<FileDetail> listFileDetail = new List<FileDetail>();
                                List<TTDMemoTransactionAttachment> listFileAtachment = new List<TTDMemoTransactionAttachment>();

                                foreach (var file in itemDetail.attachments)
                                {
                                    var base64Data = file.fileBase64.Split(",")[1];
                                    string referenceID = Guid.NewGuid().ToString();
                                    FileDetail fileItem = new FileDetail
                                    {
                                        filename = referenceID + ".pdf",
                                        extension = ".pdf",
                                        data = base64Data
                                    };

                                    listFileDetail.Add(fileItem);

                                    TTDMemoTransactionAttachment memoCodeAttachment = new TTDMemoTransactionAttachment
                                    {
                                        Reference_ID = referenceID,
                                        File_Extension = ".pdf",
                                        File_Type = file.contentType,
                                        File_Size = (Int32)file.fileSize,
                                        File_Path = "",
                                        File_Name = file.fileName,
                                        Created_By = request.userID,
                                        Created_Date = Utility.GetDateNowThai()
                                    };

                                    listFileAtachment.Add(memoCodeAttachment);

                                }

                                saveFileList.files = listFileDetail;

                                var saveFileResponse = await _repository.dmcFile.SaveFileDMC(saveFileList);

                                if (saveFileResponse != null && saveFileResponse.operationStatus != null && saveFileResponse.operationStatus.statusCode == 200)
                                {
                                    var saveFileResult = saveFileResponse.files;

                                    List<TTDMemoTransactionAttachment> listSaveAttachmentDetail = listFileAtachment.Select(c => new TTDMemoTransactionAttachment
                                    {
                                        MemoTransaction_ID = request.memoTransactionID,
                                        Reference_ID = c.Reference_ID,
                                        File_Name = c.File_Name,
                                        File_Path = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.data,
                                        File_Size = c.File_Size,
                                        File_Type = c.File_Type,
                                        File_Extension = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.extension,
                                        MemoTransactionDetail_ID = saveDetailResult.ID,
                                        Created_By = request.userID,
                                        Created_Date = Utility.GetDateNowThai()
                                    }).ToList();

                                    await _repository.memoTransaction.SaveEMemoTransactionDetailAttachment(listSaveAttachmentDetail);
                                }
                                else
                                {
                                    _log4net.Error(saveFileResponse.operationStatus.description);
                                    errorMessae += $"{itemDetail.memoCode} Save Attachment ไม่สำเร็จ {saveFileResponse?.operationStatus?.description}\n";
                                }
                            }
                        }
                        else
                        {
                            errorMessae += $"{itemDetail.memoCode} Add ไม่สำเร็จ\n";
                        }
                    }
                }

                // Edit
                if (memoDetailIDUpdate.Any())
                {
                    var memoTransactionUpdate = allMemoTransactionDetail.Where(e => memoDetailIDUpdate.Contains(e.ID)).ToList();

                    foreach (var itemDetail in memoTransactionUpdate.OrderBy(c => c.MemoType_Sequence).ThenBy(e => e.MemoCode_Sequence))
                    {
                        var dataUpdate = request.memoDocumentData.FirstOrDefault(c => c.memoDetailID == itemDetail.ID);

                        if (dataUpdate != null)
                        {
                            List<TTDMemoTransactionDetail> listDetailUpdate = new List<TTDMemoTransactionDetail>();
                            itemDetail.Updated_Date = Utility.GetDateNowThai();
                            itemDetail.Updated_By = request.userID;
                            itemDetail.Comment = dataUpdate.comment;

                            listDetailUpdate.Add(itemDetail);

                            var updateDataDetailResult = await _repository.memoTransaction.UpdateMemoTransactionDetailList(listDetailUpdate);
                            if (!updateDataDetailResult)
                            {
                                errorMessae += $"{dataUpdate.memoCode} Update ไม่สำเร็จ\n";
                            }
                            else
                            {

                                var memoAttachmentDelete = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => c.MemoTransactionDetail_ID == itemDetail.ID);
                                if (memoAttachmentDelete.Any())
                                {
                                    await _repository.memoTransaction.DeleteMemoTransactionAttachmentList(memoAttachmentDelete);
                                }

                                if (dataUpdate.attachments != null)
                                {
                                    // Insert Attachment
                                    string applicationName = appSettingHelper.GetConfiguration("ApplicationNameSaveFile");

                                    SaveFileRequest saveFileList = new SaveFileRequest();
                                    saveFileList.application = applicationName;

                                    List<FileDetail> listFileDetail = new List<FileDetail>();
                                    List<TTDMemoTransactionAttachment> listFileAtachment = new List<TTDMemoTransactionAttachment>();

                                    foreach (var file in dataUpdate.attachments)
                                    {
                                        var base64Data = file.fileBase64.Split(",")[1];
                                        string referenceID = Guid.NewGuid().ToString();
                                        FileDetail fileItem = new FileDetail
                                        {
                                            filename = referenceID + ".pdf",
                                            extension = ".pdf",
                                            data = base64Data
                                        };

                                        listFileDetail.Add(fileItem);

                                        TTDMemoTransactionAttachment memoCodeAttachment = new TTDMemoTransactionAttachment
                                        {
                                            Reference_ID = referenceID,
                                            File_Extension = ".pdf",
                                            File_Type = file.contentType,
                                            File_Size = (Int32)file.fileSize,
                                            File_Path = "",
                                            File_Name = file.fileName,
                                            Created_By = request.userID,
                                            Created_Date = Utility.GetDateNowThai()
                                        };

                                        listFileAtachment.Add(memoCodeAttachment);
                                    }

                                    saveFileList.files = listFileDetail;

                                    var saveFileResponse = await _repository.dmcFile.SaveFileDMC(saveFileList);

                                    if (saveFileResponse != null && saveFileResponse.operationStatus != null && saveFileResponse.operationStatus.statusCode == 200)
                                    {
                                        var saveFileResult = saveFileResponse.files;

                                        List<TTDMemoTransactionAttachment> listSaveAttachmentDetail = listFileAtachment.Select(c => new TTDMemoTransactionAttachment
                                        {
                                            MemoTransaction_ID = request.memoTransactionID,
                                            Reference_ID = c.Reference_ID,
                                            File_Name = c.File_Name,
                                            File_Path = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.data,
                                            File_Size = c.File_Size,
                                            File_Type = c.File_Type,
                                            File_Extension = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.extension,
                                            MemoTransactionDetail_ID = itemDetail.ID,
                                            Created_By = request.userID,
                                            Created_Date = Utility.GetDateNowThai()
                                        }).ToList();

                                        await _repository.memoTransaction.SaveEMemoTransactionDetailAttachment(listSaveAttachmentDetail);
                                    }
                                    else
                                    {
                                        _log4net.Error(saveFileResponse.operationStatus.description);
                                        errorMessae += $"{itemDetail.MemoCode} Save Attachment ไม่สำเร็จ {saveFileResponse?.operationStatus?.description}\n";
                                    }
                                }
                            }
                        }
                    }


                }

                // Delete
                if (memoCodeDelete.Any())
                {
                    var memoTransactionDelete = allMemoTransactionDetail.Where(c => memoCodeDelete.Contains(c.ID));
                    var memoAttachmentDelete = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => memoTransactionDelete.Select(e => e.ID).Contains(c.MemoTransactionDetail_ID));

                    if (memoAttachmentDelete.Any())
                    {
                        await _repository.memoTransaction.DeleteMemoTransactionAttachmentList(memoAttachmentDelete);
                    }

                    var result = await _repository.memoTransaction.DeleteMemoTransactionDetailList(memoTransactionDelete.ToList());
                    if (!result)
                    {
                        errorMessae += $"{memoTransactionDelete.Select(c => c.MemoCode).Join(",")} ลบไม่สำเร็จ\n";
                    }
                }

                string noteUpdate = string.Join("|", request.noteList.Where(c => !string.IsNullOrEmpty(c.Trim())));
                bool updateNote = false;

                if (noteUpdate != transactionData.Note)
                {
                    updateNote = true;
                    await _repository.memoTransaction.UpdateMemoTransactionNote(request.memoTransactionID, noteUpdate, request.userID);
                }

                if (memoCodeAdd.Any() || memoCodeDelete.Any() || memoDetailIDUpdate.Any() || updateNote)
                {
                    await _repository.memoTransaction.CreateMemoDocumentTransactionHistory(request.memoTransactionID, transactionData.DocumentNo, request.userID, noteUpdate);
                }

                response.success = string.IsNullOrEmpty(errorMessae) ? true : false;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = string.IsNullOrEmpty(errorMessae) ? "บันทึกข้อมูลสำเร็จ" : errorMessae
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(ex.InnerException?.Message ?? ex.Message);
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetDeliveryStatusResponse> GetDeliveryStatusList()
        {
            GetDeliveryStatusResponse response = new GetDeliveryStatusResponse();

            try
            {
                var deliveryStatusData = _repository.memoTransaction.GetAllDeliveryStatus();

                if (deliveryStatusData.Any())
                {
                    response.data = deliveryStatusData.Select(c => new DeliveryStatusData
                    {
                        deliveryStatusID = c.ID,
                        description = c.DeliveryStatus

                    }).ToList();
                }
                else
                {
                    response.data = new List<DeliveryStatusData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetEBaoStatusResponse> GetEBaoStatusList()
        {
            GetEBaoStatusResponse response = new GetEBaoStatusResponse();

            try
            {
                var deliveryStatusData = await _repository.memoTransaction.GetAllEBaoStatus();

                if (deliveryStatusData.Any())
                {
                    response.data = deliveryStatusData.Select(c => new EBaoStatusData
                    {
                        eBaoStatusID = c.ID,
                        description = c.EBaoMemoStatus

                    }).ToList();
                }
                else
                {
                    response.data = new List<EBaoStatusData>();
                }

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetAgentUnitResponse> GetAgentUnit()
        {
            GetAgentUnitResponse response = new GetAgentUnitResponse();

            try
            {
                var data = await _repository.memoTransaction.GetMemoTransactionHeader();

                if (data.Any())
                {
                    var agentUnitList = data.GroupBy(
                        x => new
                        {
                            x.AgentUnit,
                            x.AgentUnitCode,
                        })
                    .Select(e => new AgentUnitDataResult
                    {
                        agentUnitName = e.Key.AgentUnit,
                        agentUnitCode = e.Key.AgentUnitCode,
                    }).OrderBy(z => z.agentUnitName).ToList();


                    //List<AgentUnitDataResult> y = new List<AgentUnitDataResult>();
                    //y.Add(new AgentUnitDataResult { agentUnitCode = "001", agentUnitName = "Hello" });


                    response.data = agentUnitList;
                }


                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetAgentGroupResponse> GetAgentGroup()
        {
            GetAgentGroupResponse response = new GetAgentGroupResponse();

            try
            {
                var data = await _repository.memoTransaction.GetMemoTransactionHeader();

                if (data.Any())
                {
                    var agentGroupList = data.GroupBy(
                        x => new
                        {
                            x.AgentGroupCode,
                            x.AgentGroup,
                        })
                    .Select(e => new AgentGroupDataResult
                    {
                        agentGroupCode = e.Key.AgentGroupCode,
                        agentGroupName = e.Key.AgentGroup,
                    }).OrderBy(z => z.agentGroupName).ToList();

                    response.data = agentGroupList;
                }


                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "ค้นหาข้อมูลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetEmemoPrerationDetailResponse> FindEmemoPrerationDetailBy(int transactionID, string hostUrl)
        {
            GetEmemoPrerationDetailResponse response = new GetEmemoPrerationDetailResponse();

            try
            {
                var memoTransactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.ID == transactionID);
                var ebaoStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var memoDocumentHistory = await _repository.memoTransaction.GetMemoDocumentHistoryBy(c => c.MemoTransaction_ID == transactionID);


                if (memoTransactionData != null)
                {
                    var basicInformation = new PreparationBasicInfomation();
                    basicInformation.citizenID = memoTransactionData.CitizenID;
                    basicInformation.agentName = memoTransactionData.AgentCode + " : " + memoTransactionData.AgentName;
                    basicInformation.group = memoTransactionData.AgentGroup;
                    basicInformation.unit = memoTransactionData.AgentUnit;
                    basicInformation.policyNo = memoTransactionData.PolicyNo;
                    basicInformation.policyName = memoTransactionData.PolicyName;
                    basicInformation.policyStatus = memoTransactionData.PolicyStatus;
                    basicInformation.proposalNo = memoTransactionData.ProposalNo;
                    basicInformation.proposalDate = memoTransactionData.ProposalDate.ToString("dd/MM/yyyy");
                    basicInformation.mainPlan = memoTransactionData.MainPlan;
                    basicInformation.mainPlan = memoTransactionData.MainPlan;

                    var memoDocumentInformation = new PreparationMemoDocumentInfomation();
                    memoDocumentInformation.ebaoDocumentNo = memoTransactionData.DocumentNo;
                    memoDocumentInformation.transactionDate = memoTransactionData.TransactionDate.ToString("dd/MM/yyyy");
                    memoDocumentInformation.createdUser = memoTransactionData.Created_By;
                    memoDocumentInformation.ebaoMemoStatus = ebaoStatusList.FirstOrDefault(c => c.ID == memoTransactionData.EBaoMemoStatus_ID)?.EBaoMemoStatus;
                    memoDocumentInformation.ebaoRelpliedUser = memoTransactionData.EBaoRepliedUser;
                    memoDocumentInformation.ebaoRelpliedDate = memoTransactionData.EBaoRepliedDate == null ? "-" : Convert.ToDateTime(memoTransactionData.EBaoRepliedDate).ToString("dd/MM/yyyy");

                    var memoAmendmentInformation = new PreparationMemoAmendmentInformation();
                    List<MemoDocumentHistory> listMemoDocumentHistory = new List<MemoDocumentHistory>();

                    var memoTransactionDetailData = await _repository.memoTransaction.FindMemoTransactionDetailBy(c => c.MemoTransaction_ID == transactionID);
                    if (memoTransactionDetailData.Any())
                    {
                        var memoAttachment = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => c.MemoTransaction_ID == transactionID);

                        memoAmendmentInformation.documents = memoTransactionDetailData.OrderBy(d => d.MemoType_Sequence).ThenBy(e => e.MemoCode_Sequence).Select(c => new PreparationMemoDocument
                        {
                            memoDetailID = c.ID,
                            memoCode = c.MemoCode,
                            memoDescription = c.MemoCodeDescription,
                            comment = c.Comment,
                            fileAttachments = memoAttachment.Where(d => d.MemoTransactionDetail_ID == c.ID).Select(e => new FileAttachment
                            {
                                filePath = $"{hostUrl}/EMEMO_API/FileAttachment/GetFileAttachment?fileType=MEMOCODETXN&referenceID={e.ID}",
                                fileName = e.File_Name,
                                fileType = e.File_Type,
                                fileSize = (decimal)e.File_Size,
                            }).ToList()
                        }).ToList();

                        int sequence = 1;
                        foreach (var itemDoc in memoAmendmentInformation.documents)
                        {
                            itemDoc.sequence = sequence;
                            sequence++;
                        }
                    }
                    else
                    {
                        memoAmendmentInformation.documents = new List<PreparationMemoDocument>();
                    }

                    var memoTransactionHistoryData = await _repository.memoTransaction.FindMemoTransactionHistoryBy(c => c.MemoTransaction_ID == transactionID);
                    if (memoTransactionHistoryData.Any())
                    {
                        memoAmendmentInformation.history = memoTransactionHistoryData.OrderByDescending(e => e.Send_DateTime).Select(c => new PreparationMemoHistory
                        {
                            sendTo = c.SendTo,
                            channel = c.Channel,
                            description = c.Description,
                            sendStatus = c.Send_Status,
                            sendBy = c.Send_By,
                            sendDate = c.Send_DateTime.ToString("dd/MM/yyyy"),
                            sendTime = c.Send_DateTime.ToString("HH:mm")
                        }).ToList();
                    }
                    else
                    {
                        memoAmendmentInformation.history = new List<PreparationMemoHistory>();
                    }

                    if (memoDocumentHistory.Any())
                    {
                        var memoHistoryIDList = memoDocumentHistory.Select(c => c.ID);
                        var memoDumentHistoryAttachment = await _repository.memoTransaction.GetMemoDocumentHistoryDetailBy(c => memoHistoryIDList.Contains(c.MemoDocumentHistory_ID));

                        listMemoDocumentHistory = memoDocumentHistory.OrderBy(e => e.SequenceEditDocumentNo).Select(c => new MemoDocumentHistory
                        {
                            documentNo = c.EMEMODocumentNo + "-" + c.SequenceEditDocumentNo,
                            transactionDate = c.TransactionDate.ToString("dd/MM/yyyy"),
                            osMemo = memoDumentHistoryAttachment.Where(d => d.MemoDocumentHistory_ID == c.ID).Count(),
                            memoDocumentHistoryID = c.ID,
                            printedBy = c.Created_By
                        }).ToList();
                    }

                    List<string> noteList = new List<string>();
                    if (memoTransactionData.Note != null)
                    {
                        noteList = memoTransactionData.Note.Split("|").ToList();
                    }
                    response.data = new EmemoPrerationDetailData
                    {
                        memoDocumentInformation = memoDocumentInformation,
                        basicInformation = basicInformation,
                        memoAmendmentInformation = memoAmendmentInformation,
                        memoTransactionID = memoTransactionData.ID,
                        memoDocumentHistory = listMemoDocumentHistory,
                        noteList = noteList
                    };
                    response.status = new Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "ค้นหาข้อมูลสำเร็จ"
                    };
                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "ค้นหาข้อมูลไม่สำเร็จ"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        //2201210000007
        public async Task<SaveDataResponse> AdhocSendMemo(AdhocSendMemoRequest request, string hostUrl)
        {
            SaveDataResponse response = new SaveDataResponse();
            List<TTHMemoTransaction> transactionAdhocSend = new List<TTHMemoTransaction>();
            List<RuleManagementData> allRuleManagement = new List<RuleManagementData>();
            List<TMDeliveryStatus> deliveryStatus = new List<TMDeliveryStatus>();
            List<TMGroupEmailManagement> groupEmailManagement = new List<TMGroupEmailManagement>();
            List<TMEmailManagement> allEmailConfig = new List<TMEmailManagement>();
            List<FileStreamTransaction> fileStream = new List<FileStreamTransaction>();
            List<RoundAndRuleData> listRoundAndRule = new List<RoundAndRuleData>();

            try
            {
                transactionAdhocSend = await _repository.memoTransaction.GetMemoTransactionBy(c => request.transactionIDList.Contains(c.ID));
                var allTransactionDetail = await _repository.memoTransaction.FindMemoTransactionDetailBy(c => request.transactionIDList.Contains(c.MemoTransaction_ID));

                foreach (var itemTran in transactionAdhocSend)
                {
                    RoundAndRuleData round = new RoundAndRuleData
                    {
                        transactionID = itemTran.ID,
                        round = itemTran.Round.GetValueOrDefault(),
                        SecondTier = itemTran.SecondTier
                    };
                    listRoundAndRule.Add(round);
                }

                #region Prepare MasterData
                allRuleManagement = await _repository.memoMapping.GetRuleManagementList();
                allRuleManagement = allRuleManagement.Where(c => c.status).ToList();

                if (allRuleManagement.Any())
                {
                    foreach (var itemRuleManagement in allRuleManagement)
                    {
                        var productCategoryList = await _repository.memoMapping.GetRuleManagementProductCategory(itemRuleManagement.ruleManagementID);
                        itemRuleManagement.productCategory = productCategoryList.Select(c => c.ProductCategory).ToList();
                    }
                }
                else
                {
                    response.status = new Status
                    {
                        code = ((int)HttpStatusCode.UnprocessableEntity).ToString(),
                        message = "ไม่พบข้อมูล RuleManagement"
                    };

                    return response;
                }

                #endregion

                string message = string.Empty;

                foreach (var itemTransaction in transactionAdhocSend)
                {
                    var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                    List<MemoDetailData> memoDetailData = transactionDetail.Select(e => new MemoDetailData
                    {
                        Comment = e.Comment,
                        DocumentNo = e.DocumentNo,
                        LetterStatus = e.LetterStatus,
                        MemoCode = e.MemoCode,
                        MemoCodeDescription = e.MemoCodeDescription,
                        MemoCode_Sequence = e.MemoCode_Sequence,
                        MemoType = e.MemoType,
                        MemoType_Sequence = e.MemoType_Sequence,
                        Reply_By = e.Reply_By,
                        Reply_Date = e.Reply_Date,
                        Source = e.Source,
                        MemoTransaction_ID = e.MemoTransaction_ID
                    }).ToList();

                    (RuleManagementData ruleManagementData, string errorMessage) = FindRuleData(allRuleManagement, itemTransaction);

                    string customerPhoneNo = !string.IsNullOrWhiteSpace(itemTransaction.PHPhoneNo)
                                            ? itemTransaction.PHPhoneNo : !string.IsNullOrWhiteSpace(itemTransaction.LAPhoneNo)
                                            ? itemTransaction.LAPhoneNo : string.Empty;

                    string customerEmail = !string.IsNullOrWhiteSpace(itemTransaction.PHEmail)
                                            ? itemTransaction.PHEmail : !string.IsNullOrWhiteSpace(itemTransaction.LAEmail)
                                            ? itemTransaction.LAEmail : string.Empty;

                    if (ruleManagementData != null)
                    {
                        // Send First Tier
                        if (itemTransaction.Round == 1)
                        {
                            if (ruleManagementData.firstTier == "Agent")
                            {
                                if (ruleManagementData.firstTierChannel == "SMS")
                                {
                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Phone Number\n";
                                    }
                                }
                                else if (ruleManagementData.firstTierChannel == "E-Mail")
                                {
                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Email\n";
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Phone Number\n";
                                    }

                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Email\n";
                                    }

                                }
                            }
                            else if (ruleManagementData.firstTier == "Customer")
                            {
                                if (ruleManagementData.firstTierChannel == "SMS")
                                {
                                    if (string.IsNullOrWhiteSpace(customerPhoneNo))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Phone Number\n";
                                    }
                                }
                                else if (ruleManagementData.firstTierChannel == "E-Mail")
                                {
                                    if (string.IsNullOrWhiteSpace(customerEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Email\n";
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrWhiteSpace(customerPhoneNo))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Phone Number\n";
                                    }

                                    if (string.IsNullOrWhiteSpace(customerEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Email\n";
                                    }

                                }
                            }

                            if (ruleManagementData.secondTier != null)
                            {
                                DateTime sendSecondTier = DateTime.Now;

                                try
                                {
                                    sendSecondTier = await CheckDateWorkingSpace(ruleManagementData.spaceWorkingDays.GetValueOrDefault(), itemTransaction.EstimateTime);
                                }
                                catch
                                {
                                    sendSecondTier.AddDays(ruleManagementData.spaceWorkingDays.GetValueOrDefault());
                                }

                                itemTransaction.EstimateTime = sendSecondTier;
                                itemTransaction.EMemoStatus = "Successfully";
                                itemTransaction.Round = 2;
                                itemTransaction.SecondTier = ruleManagementData.secondTier;
                                itemTransaction.SecondTierChannel = ruleManagementData.secondTierChannel;
                                itemTransaction.DatetimeSendSecondTier = sendSecondTier;
                            }
                            else
                            {
                                itemTransaction.EstimateTime = null;
                                itemTransaction.Round = null;
                                itemTransaction.EMemoStatus = "Successfully";
                                itemTransaction.DeliveryStatus_ID = "38aa4fc5-7a3f-4d69-abf4-c947b080b8f6";
                            }
                        }
                        else if (itemTransaction.Round == 2)
                        {
                            if (ruleManagementData.secondTier == "Agent")
                            {
                                if (ruleManagementData.secondTierChannel == "SMS")
                                {
                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Phone Number\n";
                                    }
                                }
                                else if (ruleManagementData.secondTierChannel == "E-Mail")
                                {
                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Email\n";
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Phone Number\n";
                                    }

                                    if (string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Agent Email\n";
                                    }

                                }
                            }
                            else if (ruleManagementData.secondTier == "Customer")
                            {
                                if (ruleManagementData.secondTierChannel == "SMS")
                                {
                                    if (string.IsNullOrWhiteSpace(customerPhoneNo))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Phone Number\n";
                                    }
                                }
                                else if (ruleManagementData.secondTierChannel == "E-Mail")
                                {
                                    if (string.IsNullOrWhiteSpace(customerEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Email\n";
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrWhiteSpace(customerPhoneNo))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Phone Number\n";
                                    }

                                    if (string.IsNullOrWhiteSpace(customerEmail))
                                    {
                                        message += $"{itemTransaction.PolicyNo} ไม่มีข้อมูล Customer Email\n";
                                    }

                                }
                            }

                            itemTransaction.EstimateTime = null;
                            itemTransaction.Round = null;
                            itemTransaction.EMemoStatus = "Successfully";
                            itemTransaction.DeliveryStatus_ID = "38aa4fc5-7a3f-4d69-abf4-c947b080b8f6";
                        }

                        Stream fileAttachment = null;
                        ///
                        if (ruleManagementData.firstTierChannel.Contains("E-Mail") ||
                            (ruleManagementData.secondTierChannel != null && ruleManagementData.secondTierChannel.Contains("E-Mail")))
                        {
                            var coverPage = _repository.sendMemo.GenerateCoverPage(itemTransaction, memoDetailData, itemTransaction.Note);
                            fileAttachment = await GenerateMemoAttachStream(coverPage, itemTransaction.ID);

                            FileStreamTransaction fileData = new FileStreamTransaction
                            {
                                transactionID = itemTransaction.ID,
                                fileStream = fileAttachment
                            };

                            fileStream.Add(fileData);
                        }

                        await _repository.memoTransaction.UpdateMemoTransactionHeaderList(itemTransaction);

                        message += $"{itemTransaction.PolicyNo} ทำรายการ Adhoc Send สำเร็จ";

                    }
                    else
                    {
                        message += !string.IsNullOrEmpty(errorMessage) ? errorMessage : $"{itemTransaction.PolicyNo} ไม่พบ Rule สำหรับส่งเอกสารที่ทำการสร้างไว้ตรงกับข้อมูล กธ. นี้\n";
                    }
                }


                groupEmailManagement = await _repository.memoMapping.GetGroupEmailManagementList();
                groupEmailManagement = groupEmailManagement.Where(c => c.Status).ToList();

                var allGroupEmailManagementID = groupEmailManagement.Where(e => e.Status).Select(c => c.ID);
                allEmailConfig = await _repository.memoMapping.GetEmailManagementListBy(c => allGroupEmailManagementID.Contains(c.GroupEmailManagement_ID));

                deliveryStatus = _repository.memoTransaction.GetAllDeliveryStatus();

                response.success = true;
                response.status = new Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = message
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(ex.InnerException?.Message ?? ex.Message);
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }
            finally
            {
                #region Send Document

                _httpContextAccessor.HttpContext.Response.OnCompleted(async () =>
                {
                    try
                    {
                        var allTransactionDetail = await _repository.memoTransaction.FindMemoTransactionDetailBy(c => request.transactionIDList.Contains(c.MemoTransaction_ID));
                        var deliverySuccess = deliveryStatus.FirstOrDefault(c => c.DeliveryStatus == "Successfully");
                        var deliveryUnSuccess = deliveryStatus.FirstOrDefault(c => c.DeliveryStatus == "Unsuccessfully");

                        foreach (var itemTransaction in transactionAdhocSend)
                        {
                            List<TTDMemoTransactionHistory> transactionHistory = new List<TTDMemoTransactionHistory>();
                            List<string> documentSendSuccess = new List<string>();
                            List<UpdateStatusTransactionRequest> listUpdateTransaction = new List<UpdateStatusTransactionRequest>();

                            string customerPhoneNo = !string.IsNullOrWhiteSpace(itemTransaction.PHPhoneNo)
                            ? itemTransaction.PHPhoneNo : !string.IsNullOrWhiteSpace(itemTransaction.LAPhoneNo)
                            ? itemTransaction.LAPhoneNo : string.Empty;

                            string customerEmail = !string.IsNullOrWhiteSpace(itemTransaction.PHEmail)
                            ? itemTransaction.PHEmail : !string.IsNullOrWhiteSpace(itemTransaction.LAEmail)
                            ? itemTransaction.LAEmail : string.Empty;

                            bool sendResult = false;
                            bool sendResult2 = false;
                            bool missData = false;
                            bool sendCompleteTier = false;
                            string productCategory = Utility.CheckProductCategory(itemTransaction.PolicyNo, itemTransaction.SourceOfBusiness);
                            string policyNameSMS = string.Empty;

                            if (productCategory == "Individual" || productCategory == "PA")
                            {
                                if (Convert.ToInt32(itemTransaction.EntryAgeLifeAssured) > 19)
                                {
                                    policyNameSMS = itemTransaction.LifeAssuredName;
                                }
                                else
                                {
                                    policyNameSMS = itemTransaction.PolicyName + " ผู้ปกครองของ " + itemTransaction.LifeAssuredName;
                                }
                            }
                            else if (productCategory == "MRTA" || productCategory == "Group 1 Year Term")
                            {
                                policyNameSMS = itemTransaction.LifeAssuredName;
                            }
                            else
                            {
                                policyNameSMS = itemTransaction.PolicyName;
                            }

                            (RuleManagementData ruleManagementData, string errorMessage) = FindRuleData(allRuleManagement, itemTransaction);

                            UpdateStatusTransactionRequest updateStautsTransactionRequest = new UpdateStatusTransactionRequest();

                            if (ruleManagementData != null)
                            {
                                string sendTo = string.Empty;
                                string channel = string.Empty;
                                string description = string.Empty;
                                string sendStatus = string.Empty;
                                var ruleOriginal = listRoundAndRule.FirstOrDefault(c => c.transactionID == itemTransaction.ID);

                                TTDMemoTransactionHistory history = new TTDMemoTransactionHistory
                                {
                                    MemoTransaction_ID = itemTransaction.ID,
                                    Send_By = request.userID,
                                    Send_DateTime = Utility.GetDateNowThai(),
                                };

                                TTDMemoTransactionHistory history2 = null;

                                List<string> emailTo = new List<string>();
                                List<string> emailCC = new List<string>();
                                string pathDocument = $"{hostUrl}/EMEMO_API/FileAttachment/PreviewMemoAttachment/{itemTransaction.DocumentNo}";

                                Stream fileAttachment = null;
                                if (ruleManagementData.firstTierChannel.Contains("E-Mail") ||
                                 (ruleManagementData.secondTierChannel != null && ruleManagementData.secondTierChannel.Contains("E-Mail")))
                                {
                                    fileAttachment = fileStream.FirstOrDefault(c => c.transactionID == itemTransaction.ID)?.fileStream;
                                }

                                // Send First Tier
                                if (ruleOriginal.round == 1)
                                {
                                    // ส่งหา Agent
                                    if (ruleManagementData.firstTier == "Agent")
                                    {
                                        history.SendTo = "Agent";

                                        if (ruleManagementData.firstTierChannel == "SMS")
                                        {
                                            history.Channel = "SMS";
                                            history.Description = itemTransaction.AgentPhoneNumber;

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารเพิ่มเติมเพื่อพิจารณาการรับประกันกธ. {itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(itemTransaction.AgentPhoneNumber, messageSMS);
                                                sendResult = responseSend.resultCode == "1";
                                            }

                                            // ส่ง SMS ไม่ผ่านส่งด้วย Email
                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Agent",
                                                    Channel = "E-Mail",
                                                    Description = itemTransaction.AgentEmail,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                                {
                                                    emailTo.Add(itemTransaction.AgentEmail);
                                                    var bodyEmail = _repository.sendMemo.GetBodyEmail("Agent", itemTransaction);

                                                    if (fileAttachment.Length == 0)
                                                    {
                                                        sendResult2 = false;
                                                    }
                                                    else
                                                    {
                                                        sendResult2 = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                    }

                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.firstTierChannel == "E-Mail")
                                        {
                                            history.Channel = "E-Mail";
                                            history.Description = itemTransaction.AgentEmail;

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                            {
                                                // Email To Agent with PDF
                                                emailTo.Add(itemTransaction.AgentEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Agent", itemTransaction);
                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            // ส่ง Email ไม่ผ่านส่งด้วย SMS
                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Agent",
                                                    Channel = "SMS",
                                                    Description = itemTransaction.AgentPhoneNumber,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                                {
                                                    string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารเพิ่มเติมเพื่อพิจารณาการรับประกันกธ. {itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                    var responseSend = await _repository.sendMemo.SendSMS(itemTransaction.AgentPhoneNumber, messageSMS);
                                                    sendResult2 = responseSend.resultCode == "1";
                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.firstTierChannel == "SMS&E-Mail")
                                        {
                                            history2 = new TTDMemoTransactionHistory
                                            {
                                                MemoTransaction_ID = itemTransaction.ID,
                                                Send_By = request.userID,
                                                SendTo = "Agent",
                                                Channel = "SMS",
                                                Description = itemTransaction.AgentPhoneNumber,
                                                Send_DateTime = Utility.GetDateNowThai(),
                                            };

                                            history.Channel = "E-Mail";
                                            history.Description = itemTransaction.AgentEmail;

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                            {
                                                // Email To Agent with PDF
                                                emailTo.Add(itemTransaction.AgentEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Agent", itemTransaction);

                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารเพิ่มเติมเพื่อพิจารณาการรับประกันกธ. {itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(itemTransaction.AgentPhoneNumber, messageSMS);
                                                sendResult2 = responseSend.resultCode == "1";
                                            }

                                            if (string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber) && string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                            {
                                                missData = true;
                                            }
                                        }
                                    }
                                    // ส่งหา Customer
                                    else if (ruleManagementData.firstTier == "Customer")
                                    {
                                        history.SendTo = "Customer";
                                        if (ruleManagementData.firstTierChannel == "SMS")
                                        {
                                            history.Channel = "SMS";
                                            history.Description = customerPhoneNo;

                                            if (!string.IsNullOrWhiteSpace(customerPhoneNo))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารจาก {policyNameSMS} เพิ่มเติมเพื่อพิจารณาการรับประกันกธ. { itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(customerPhoneNo, messageSMS);
                                                sendResult = responseSend.resultCode == "1";
                                            }

                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Customer",
                                                    Channel = "E-Mail",
                                                    Description = customerEmail,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(customerEmail))
                                                {
                                                    // Email To Customer with PDF
                                                    emailTo.Add(customerEmail);
                                                    var bodyEmail = _repository.sendMemo.GetBodyEmail("Customer", itemTransaction);
                                                    if (fileAttachment.Length == 0)
                                                    {
                                                        sendResult2 = false;
                                                    }
                                                    else
                                                    {
                                                        sendResult2 = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                    }
                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.firstTierChannel == "E-Mail")
                                        {
                                            history.Channel = "E-Mail";
                                            history.Description = customerEmail;

                                            if (!string.IsNullOrWhiteSpace(customerEmail))
                                            {
                                                // Email To Customer with PDF
                                                emailTo.Add(customerEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Customer", itemTransaction);
                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Customer",
                                                    Channel = "SMS",
                                                    Description = customerPhoneNo,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(customerPhoneNo))
                                                {
                                                    string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารจาก {policyNameSMS} เพิ่มเติมเพื่อพิจารณาการรับประกันกธ. { itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                    var responseSend = await _repository.sendMemo.SendSMS(customerPhoneNo, messageSMS);
                                                    sendResult2 = responseSend.resultCode == "1";
                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.firstTierChannel == "SMS&E-Mail")
                                        {
                                            history2 = new TTDMemoTransactionHistory
                                            {
                                                MemoTransaction_ID = itemTransaction.ID,
                                                Send_By = request.userID,
                                                SendTo = "Customer",
                                                Channel = "SMS",
                                                Description = customerPhoneNo,
                                                Send_DateTime = Utility.GetDateNowThai(),
                                            };

                                            history.Channel = "E-Mail";
                                            history.Description = customerEmail;

                                            if (!string.IsNullOrWhiteSpace(customerEmail))
                                            {
                                                // Email To Agent with PDF
                                                emailTo.Add(customerEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Customer", itemTransaction);
                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            if (!string.IsNullOrWhiteSpace(customerPhoneNo))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารจาก {policyNameSMS} เพิ่มเติมเพื่อพิจารณาการรับประกันกธ. { itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(customerPhoneNo, messageSMS);
                                                sendResult2 = responseSend.resultCode == "1";
                                            }

                                            if (string.IsNullOrWhiteSpace(customerPhoneNo) && string.IsNullOrWhiteSpace(customerEmail))
                                            {
                                                missData = true;
                                            }
                                        }
                                    }

                                    if (ruleManagementData.secondTier == null)
                                    {
                                        sendCompleteTier = true;
                                        itemTransaction.EstimateTime = null;
                                        itemTransaction.Round = null;
                                        itemTransaction.EMemoStatus = "Successfully";
                                        itemTransaction.DeliveryStatus_ID = "38aa4fc5-7a3f-4d69-abf4-c947b080b8f6";
                                    }
                                    else
                                    {
                                        DateTime sendSecondTier = DateTime.Now;

                                        try
                                        {
                                            sendSecondTier = await CheckDateWorkingSpace(ruleManagementData.spaceWorkingDays.GetValueOrDefault(), itemTransaction.EstimateTime);
                                        }
                                        catch
                                        {
                                            sendSecondTier.AddDays(ruleManagementData.spaceWorkingDays.GetValueOrDefault());
                                        }

                                        itemTransaction.EstimateTime = sendSecondTier;
                                        itemTransaction.Round = 2;
                                        itemTransaction.SecondTier = ruleManagementData.secondTier;
                                        itemTransaction.SecondTierChannel = ruleManagementData.secondTierChannel;

                                        // วันทำการ ?
                                        itemTransaction.DatetimeSendSecondTier = sendSecondTier;
                                    }

                                    if ((sendResult || sendResult2) && ruleManagementData.secondTier == null)
                                    {
                                        documentSendSuccess.Add(itemTransaction.DocumentNo);
                                    }

                                    await _repository.memoTransaction.UpdateMemoTransactionHeaderList(itemTransaction);
                                }
                                else if (ruleOriginal.round == 2)
                                {
                                    // Send Second Tier
                                    if (itemTransaction.SecondTier == "Agent")
                                    {
                                        history.SendTo = "Agent";

                                        if (ruleManagementData.secondTierChannel == "SMS")
                                        {
                                            history.Channel = "SMS";
                                            history.Description = itemTransaction.AgentPhoneNumber;

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารเพิ่มเติมเพื่อพิจารณาการรับประกันกธ. {itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(itemTransaction.AgentPhoneNumber, messageSMS);
                                                sendResult = responseSend.resultCode == "1";
                                            }

                                            // ส่ง SMS ไม่ผ่านส่งด้วย Email
                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Agent",
                                                    Channel = "E-Mail",
                                                    Description = itemTransaction.AgentEmail,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                                {
                                                    emailTo.Add(itemTransaction.AgentEmail);
                                                    var bodyEmail = _repository.sendMemo.GetBodyEmail("Agent", itemTransaction);
                                                    if (fileAttachment.Length == 0)
                                                    {
                                                        sendResult2 = false;
                                                    }
                                                    else
                                                    {
                                                        sendResult2 = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                    }
                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.secondTierChannel == "E-Mail")
                                        {
                                            history.Channel = "E-Mail";
                                            history.Description = itemTransaction.AgentEmail;

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                            {
                                                // Email To Agent with PDF
                                                emailTo.Add(itemTransaction.AgentEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Agent", itemTransaction);
                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            // ส่ง Email ไม่ผ่านส่งด้วย SMS
                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Agent",
                                                    Channel = "SMS",
                                                    Description = itemTransaction.AgentPhoneNumber,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                                {
                                                    string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารเพิ่มเติมเพื่อพิจารณาการรับประกันกธ. {itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                    var responseSend = await _repository.sendMemo.SendSMS(itemTransaction.AgentPhoneNumber, messageSMS);
                                                    sendResult2 = responseSend.resultCode == "1";
                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.secondTierChannel == "SMS&E-Mail")
                                        {
                                            history2 = new TTDMemoTransactionHistory
                                            {
                                                MemoTransaction_ID = itemTransaction.ID,
                                                Send_By = request.userID,
                                                SendTo = "Agent",
                                                Channel = "SMS",
                                                Description = itemTransaction.AgentPhoneNumber,
                                                Send_DateTime = Utility.GetDateNowThai(),
                                            };

                                            history.Channel = "E-Mail";
                                            history.Description = itemTransaction.AgentEmail;

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                            {
                                                // Email To Agent with PDF
                                                emailTo.Add(itemTransaction.AgentEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Agent", itemTransaction);
                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            if (!string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารเพิ่มเติมเพื่อพิจารณาการรับประกันกธ. {itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(itemTransaction.AgentPhoneNumber, messageSMS);
                                                sendResult2 = responseSend.resultCode == "1";
                                            }

                                            if (string.IsNullOrWhiteSpace(itemTransaction.AgentPhoneNumber) && string.IsNullOrWhiteSpace(itemTransaction.AgentEmail))
                                            {
                                                missData = true;
                                            }
                                        }
                                    }
                                    else if (itemTransaction.SecondTier == "Customer")
                                    {
                                        history.SendTo = "Customer";

                                        if (ruleManagementData.secondTierChannel == "SMS")
                                        {
                                            history.Channel = "SMS";
                                            history.Description = customerPhoneNo;

                                            if (!string.IsNullOrWhiteSpace(customerPhoneNo))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารจาก {policyNameSMS} เพิ่มเติมเพื่อพิจารณาการรับประกันกธ. { itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(customerPhoneNo, messageSMS);
                                                sendResult = responseSend.resultCode == "1";
                                            }

                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Customer",
                                                    Channel = "E-Mail",
                                                    Description = customerEmail,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(customerEmail))
                                                {
                                                    // Email To Customer with PDF
                                                    emailTo.Add(customerEmail);
                                                    var bodyEmail = _repository.sendMemo.GetBodyEmail("Customer", itemTransaction);
                                                    if (fileAttachment.Length == 0)
                                                    {
                                                        sendResult2 = false;
                                                    }
                                                    else
                                                    {
                                                        sendResult2 = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                    }
                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.secondTierChannel == "E-Mail")
                                        {
                                            history.Channel = "E-Mail";
                                            history.Description = customerEmail;

                                            if (!string.IsNullOrWhiteSpace(customerEmail))
                                            {
                                                // Email To Customer with PDF
                                                emailTo.Add(customerEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Customer", itemTransaction);
                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            if (!sendResult)
                                            {
                                                history2 = new TTDMemoTransactionHistory
                                                {
                                                    MemoTransaction_ID = itemTransaction.ID,
                                                    Send_By = request.userID,
                                                    SendTo = "Customer",
                                                    Channel = "SMS",
                                                    Description = customerPhoneNo,
                                                    Send_DateTime = Utility.GetDateNowThai(),
                                                };

                                                if (!string.IsNullOrWhiteSpace(customerPhoneNo))
                                                {
                                                    string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารจาก {policyNameSMS} เพิ่มเติมเพื่อพิจารณาการรับประกันกธ. { itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                    var responseSend = await _repository.sendMemo.SendSMS(customerPhoneNo, messageSMS);
                                                    sendResult2 = responseSend.resultCode == "1";
                                                }
                                                else
                                                {
                                                    sendResult2 = false;
                                                }
                                            }
                                            else
                                            {
                                                sendResult2 = true;
                                            }
                                        }
                                        else if (ruleManagementData.secondTierChannel == "SMS&E-Mail")
                                        {
                                            history2 = new TTDMemoTransactionHistory
                                            {
                                                MemoTransaction_ID = itemTransaction.ID,
                                                Send_By = request.userID,
                                                SendTo = "Customer",
                                                Channel = "SMS",
                                                Description = customerPhoneNo,
                                                Send_DateTime = Utility.GetDateNowThai(),
                                            };

                                            history.Channel = "E-Mail";
                                            history.Description = customerEmail;

                                            if (!string.IsNullOrWhiteSpace(customerEmail))
                                            {
                                                // Email To Agent with PDF
                                                emailTo.Add(customerEmail);
                                                var bodyEmail = _repository.sendMemo.GetBodyEmail("Customer", itemTransaction);
                                                if (fileAttachment.Length == 0)
                                                {
                                                    sendResult = false;
                                                }
                                                else
                                                {
                                                    sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                                                }
                                            }

                                            if (!string.IsNullOrWhiteSpace(customerPhoneNo))
                                            {
                                                string messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารจาก {policyNameSMS} เพิ่มเติมเพื่อพิจารณาการรับประกันกธ. { itemTransaction.PolicyNo} คลิก {pathDocument}";
                                                var responseSend = await _repository.sendMemo.SendSMS(customerPhoneNo, messageSMS);
                                                sendResult2 = responseSend.resultCode == "1";
                                            }

                                            if (string.IsNullOrWhiteSpace(customerPhoneNo) && string.IsNullOrWhiteSpace(customerEmail))
                                            {
                                                missData = true;
                                            }
                                        }
                                    }

                                    itemTransaction.EstimateTime = null;
                                    itemTransaction.Round = null;
                                    itemTransaction.DeliveryStatus_ID = "38aa4fc5-7a3f-4d69-abf4-c947b080b8f6";
                                    await _repository.memoTransaction.UpdateMemoTransactionHeaderList(itemTransaction);

                                    if (sendResult || sendResult2)
                                    {
                                        documentSendSuccess.Add(itemTransaction.DocumentNo);
                                    }

                                    sendCompleteTier = true;
                                }

                                history.AutoMemoSending = request.userID == "eMemo" ? "Y" : "N";

                                if (history2 != null)
                                {
                                    history2.AutoMemoSending = request.userID == "eMemo" ? "Y" : "N";
                                }

                                if (sendResult && sendResult2)
                                {
                                    history.Send_Status = deliverySuccess.DeliveryStatus;

                                    if (history2 != null)
                                    {
                                        history2.Send_Status = deliverySuccess.DeliveryStatus;
                                        history2.ResendNeeded = request.userID == "eMemo" ? "N" : string.Empty;
                                    }

                                    if (sendCompleteTier)
                                    {
                                        updateStautsTransactionRequest.deliveryStatusID = deliverySuccess.ID;
                                    }

                                    history.ResendNeeded = request.userID == "eMemo" ? "N" : string.Empty;
                                    updateStautsTransactionRequest.ememoStatus = deliverySuccess.DeliveryStatus;
                                }
                                else
                                {
                                    if (sendResult)
                                    {
                                        history.Send_Status = deliverySuccess.DeliveryStatus;
                                    }
                                    else
                                    {
                                        history.Send_Status = deliveryUnSuccess.DeliveryStatus;
                                    }

                                    if (history2 != null)
                                    {
                                        if (sendResult2)
                                        {
                                            history2.Send_Status = deliverySuccess.DeliveryStatus;
                                        }
                                        else
                                        {
                                            history2.Send_Status = deliveryUnSuccess.DeliveryStatus;
                                        }

                                        history2.ResendNeeded = request.userID == "eMemo" ? "Y" : string.Empty;
                                    }

                                    updateStautsTransactionRequest.ememoStatus = deliveryUnSuccess.DeliveryStatus;
                                    history.ResendNeeded = request.userID == "eMemo" ? "Y" : string.Empty;

                                    if (sendCompleteTier)
                                    {
                                        updateStautsTransactionRequest.deliveryStatusID = deliveryUnSuccess.ID;
                                    }
                                }

                                transactionHistory.Add(history);
                                if (history2 != null)
                                {
                                    transactionHistory.Add(history2);
                                }

                                updateStautsTransactionRequest.transactionID = itemTransaction.ID;
                                updateStautsTransactionRequest.updateBy = request.userID;
                                updateStautsTransactionRequest.updateDate = Utility.GetDateNowThai();

                                listUpdateTransaction.Add(updateStautsTransactionRequest);


                                var tempStaging = await _repository.eBaoMemoLetter.GetTempMemoLetterBy(c => documentSendSuccess.Contains(c.DOCUMENT_NO));

                                await _repository.eBaoMemoLetter.UpdateMemoStatus(tempStaging, request.userID, "Issued");
                                await _repository.memoTransaction.UpdateMemoTransactionStatusList(listUpdateTransaction);
                                await _repository.memoTransaction.CreateMemoTransactionHistory(transactionHistory);

                                if (!string.IsNullOrWhiteSpace(ruleManagementData.sendEmailToSaleSupport)
                                && (ruleManagementData.sendEmailToSaleSupport != "Y" && ruleManagementData.sendEmailToSaleSupport != "N"))
                                {
                                    TMGroupEmailManagement groupByBusinessUnit = null;

                                    // Group Sale นี้ทั้งหมด
                                    var groupEmailAll = groupEmailManagement.FirstOrDefault(c => c.EmailType == "Sale Support"
                                                         && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                                         && string.IsNullOrWhiteSpace(c.BusinessUnit)
                                                         && string.IsNullOrWhiteSpace(c.BranchOffice) && c.Status);

                                    if (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit))
                                    {
                                        // หาจาก Group ที่ระบุ Unit ไม่ระบุ Branch
                                        groupByBusinessUnit = groupEmailManagement.FirstOrDefault(c =>
                                                        !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                                        && c.EmailType == "Sale Support"
                                                        && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                                        && c.BusinessUnit.Contains(itemTransaction.BusinessUnit)
                                                        && string.IsNullOrWhiteSpace(c.BranchOffice)
                                                        && c.Status);
                                    }

                                    // Branh เหมือนกันส่งอันเดียว
                                    if (itemTransaction.ServicingBranch == itemTransaction.ApplicationBranch)
                                    {
                                        // หาจาก Group ที่ระบุทั้ง Business Unit และ Branch
                                        var groupEmailSend = groupEmailManagement.FirstOrDefault(
                                              c => (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit)
                                                  && !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                                  && c.BusinessUnit.Contains(itemTransaction.BusinessUnit))
                                              && c.EmailType == "Sale Support"
                                              && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                              && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                                  && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                                  && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                        if (groupEmailSend == null)
                                        {
                                            // หาจาก Group ที่ระบุ Branch แต่ไม่ระบุ Unit
                                            groupEmailSend = groupEmailManagement.FirstOrDefault(
                                                 c => string.IsNullOrWhiteSpace(c.BusinessUnit)
                                                 && c.EmailType == "Sale Support"
                                                 && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                                 && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                                     && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                                     && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                            if (groupEmailSend == null)
                                            {
                                                if (groupByBusinessUnit != null)
                                                {
                                                    groupEmailSend = groupByBusinessUnit;
                                                }
                                                else
                                                {
                                                    groupEmailSend = groupEmailAll;
                                                }
                                            }

                                        }

                                        if (groupEmailSend != null)
                                        {
                                            var emailConfigData = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSend.ID);

                                            emailTo = emailConfigData.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                            emailCC = emailConfigData.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                            var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                            Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                            var bodyEmail = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                            _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachmentSend);
                                        }

                                    }
                                    //คนละ Branch ส่ง 2 Email
                                    else
                                    {
                                        // หาจาก Group ที่ระบุทั้ง Business Unit และ Branch ServicingBranch
                                        var groupEmailSendServiceBranch = groupEmailManagement.FirstOrDefault(
                                              c => c.EmailType == "Sale Support"
                                              && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                              && (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit)
                                              && !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                              && c.BusinessUnit.Contains(itemTransaction.BusinessUnit))
                                              && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                              && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                              && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                        // หาจาก Group ที่ระบุทั้ง Business Unit และ Branch ApplicationBranch
                                        var groupEmailSendApplicationBranch = groupEmailManagement.FirstOrDefault(
                                              c => (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit)
                                              && !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                              && c.BusinessUnit.Contains(itemTransaction.BusinessUnit))
                                              && c.EmailType == "Sale Support"
                                              && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                              && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                              && !string.IsNullOrWhiteSpace(itemTransaction.ApplicationBranch)
                                              && c.BranchOffice.Contains(itemTransaction.ApplicationBranch)));

                                        if (groupEmailSendServiceBranch == null && groupEmailSendApplicationBranch == null)
                                        {
                                            // หาจาก Group ที่ระบุ Branch แต่ไม่ระบุ Unit ServicingBranch
                                            groupEmailSendServiceBranch = groupEmailManagement.FirstOrDefault(
                                             c => string.IsNullOrWhiteSpace(c.BusinessUnit)
                                             && c.EmailType == "Sale Support"
                                             && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                             && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                             && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                             && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                            // หาจาก Group ที่ระบุ Branch แต่ไม่ระบุ Unit ApplicationBranch
                                            groupEmailSendApplicationBranch = groupEmailManagement.FirstOrDefault(
                                             c => string.IsNullOrWhiteSpace(c.BusinessUnit)
                                             && c.EmailType == "Sale Support"
                                             && c.SendGroup == ruleManagementData.sendEmailToSaleSupport
                                             && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                             && !string.IsNullOrWhiteSpace(itemTransaction.ApplicationBranch)
                                             && c.BranchOffice.Contains(itemTransaction.ApplicationBranch)));

                                            if (groupEmailSendServiceBranch == null && groupEmailSendApplicationBranch == null)
                                            {
                                                if (groupByBusinessUnit != null)
                                                {
                                                    groupEmailSendServiceBranch = groupByBusinessUnit;
                                                }
                                                else
                                                {
                                                    groupEmailSendServiceBranch = groupEmailAll;
                                                }

                                                if (groupEmailSendServiceBranch != null)
                                                {
                                                    var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendServiceBranch.ID);

                                                    emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                    emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                    var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                    Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                    var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                    _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                                }

                                            }
                                            else
                                            {
                                                if (groupEmailSendServiceBranch != null)
                                                {
                                                    var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendServiceBranch.ID);

                                                    emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                    emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();
                                                    var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                    Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                    var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                    _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                                }

                                                if (groupEmailSendApplicationBranch != null)
                                                {
                                                    var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendApplicationBranch.ID);

                                                    emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                    emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();
                                                    var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                    Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                    var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                    _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (groupEmailSendServiceBranch != null)
                                            {
                                                var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendServiceBranch.ID);

                                                emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                            }

                                            if (groupEmailSendApplicationBranch != null)
                                            {
                                                var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendApplicationBranch.ID);

                                                emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                            }
                                        }

                                    }

                                }

                                //Underwriting
                                if (!string.IsNullOrWhiteSpace(ruleManagementData.sendEmailToSaleUnderwriting)
                                 && (ruleManagementData.sendEmailToSaleUnderwriting != "Y" && ruleManagementData.sendEmailToSaleUnderwriting != "N"))
                                {
                                    TMGroupEmailManagement groupByBusinessUnit = null;

                                    var groupEmailAll = groupEmailManagement.FirstOrDefault(c => string.IsNullOrWhiteSpace(c.BusinessUnit)
                                                         && c.EmailType == "Underwriting"
                                                         && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                                         && string.IsNullOrWhiteSpace(c.BranchOffice) && c.Status);

                                    if (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit))
                                    {
                                        // หาจาก Group ที่ระบุ Unit ไม่ระบุ Branch
                                        groupByBusinessUnit = groupEmailManagement.FirstOrDefault(c => !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                                        && c.EmailType == "Underwriting"
                                                        && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                                        && c.BusinessUnit.Contains(itemTransaction.BusinessUnit)
                                                        && string.IsNullOrWhiteSpace(c.BranchOffice)
                                                        && c.Status);
                                    }

                                    // Branh เหมือนกันส่งอันเดียว
                                    if (itemTransaction.ServicingBranch == itemTransaction.ApplicationBranch)
                                    {
                                        // หาจาก Group ที่ระบุทั้ง Business Unit และ Branch
                                        var groupEmailSend = groupEmailManagement.FirstOrDefault(
                                              c => (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit)
                                              && !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                              && c.BusinessUnit.Contains(itemTransaction.BusinessUnit))
                                              && c.EmailType == "Underwriting"
                                              && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                              && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                              && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                              && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                        if (groupEmailSend == null)
                                        {
                                            // หาจาก Group ที่ระบุ Branch แต่ไม่ระบุ Unit
                                            groupEmailSend = groupEmailManagement.FirstOrDefault(
                                             c => string.IsNullOrWhiteSpace(c.BusinessUnit)
                                             && c.EmailType == "Underwriting"
                                             && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                             && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                             && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                             && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                            if (groupEmailSend == null)
                                            {
                                                if (groupByBusinessUnit != null)
                                                {
                                                    groupEmailSend = groupByBusinessUnit;
                                                }
                                                else
                                                {
                                                    groupEmailSend = groupEmailAll;
                                                }
                                            }

                                        }

                                        if (groupEmailSend != null)
                                        {
                                            var emailConfigData = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSend.ID);

                                            emailTo = emailConfigData.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                            emailCC = emailConfigData.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                            var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                            Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                            var bodyEmail = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                            _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachmentSend);
                                        }

                                    }
                                    //คนละ Branch ส่ง 2 Email
                                    else
                                    {
                                        // หาจาก Group ที่ระบุทั้ง Business Unit และ Branch
                                        var groupEmailSendServiceBranch = groupEmailManagement.FirstOrDefault(
                                              c => (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit)
                                              && !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                              && c.BusinessUnit.Contains(itemTransaction.BusinessUnit))
                                              && c.EmailType == "Underwriting"
                                              && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                              && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                              && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                              && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                        // หาจาก Group ที่ระบุทั้ง Business Unit และ Branch
                                        var groupEmailSendApplicationBranch = groupEmailManagement.FirstOrDefault(
                                              c => (!string.IsNullOrWhiteSpace(itemTransaction.BusinessUnit)
                                              && !string.IsNullOrWhiteSpace(c.BusinessUnit)
                                              && c.BusinessUnit.Contains(itemTransaction.BusinessUnit))
                                              && c.EmailType == "Underwriting"
                                              && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                              && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                              && !string.IsNullOrWhiteSpace(itemTransaction.ApplicationBranch)
                                              && c.BranchOffice.Contains(itemTransaction.ApplicationBranch)));

                                        if (groupEmailSendServiceBranch == null && groupEmailSendApplicationBranch == null)
                                        {
                                            // หาจาก Group ที่ระบุ Branch แต่ไม่ระบุ Unit
                                            groupEmailSendServiceBranch = groupEmailManagement.FirstOrDefault(
                                             c => string.IsNullOrWhiteSpace(c.BusinessUnit)
                                             && c.EmailType == "Underwriting"
                                             && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                             && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                             && !string.IsNullOrWhiteSpace(itemTransaction.ServicingBranch)
                                             && c.BranchOffice.Contains(itemTransaction.ServicingBranch)));

                                            // หาจาก Group ที่ระบุ Branch แต่ไม่ระบุ Unit
                                            groupEmailSendApplicationBranch = groupEmailManagement.FirstOrDefault(
                                             c => string.IsNullOrWhiteSpace(c.BusinessUnit)
                                             && c.EmailType == "Underwriting"
                                             && c.SendGroup == ruleManagementData.sendEmailToSaleUnderwriting
                                             && (!string.IsNullOrWhiteSpace(c.BranchOffice)
                                             && !string.IsNullOrWhiteSpace(itemTransaction.ApplicationBranch)
                                             && c.BranchOffice.Contains(itemTransaction.ApplicationBranch)));

                                            if (groupEmailSendServiceBranch == null && groupEmailSendApplicationBranch == null)
                                            {
                                                if (groupByBusinessUnit != null)
                                                {
                                                    groupEmailSendServiceBranch = groupByBusinessUnit;
                                                }
                                                else
                                                {
                                                    groupEmailSendServiceBranch = groupEmailAll;
                                                }

                                                if (groupEmailSendServiceBranch != null)
                                                {
                                                    var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendServiceBranch.ID);

                                                    emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                    emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                    var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                    Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                    var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                    _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                                }

                                            }
                                            else
                                            {
                                                if (groupEmailSendServiceBranch != null)
                                                {
                                                    var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendServiceBranch.ID);

                                                    emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                    emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                    var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                    Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                    var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                    _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                                }

                                                if (groupEmailSendApplicationBranch != null)
                                                {
                                                    var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendApplicationBranch.ID);

                                                    emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                    emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                    var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                    Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                    var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                    _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (groupEmailSendServiceBranch != null)
                                            {
                                                var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendServiceBranch.ID);

                                                emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                            }

                                            if (groupEmailSendApplicationBranch != null)
                                            {
                                                var emailConfigDataSend = allEmailConfig.Where(c => c.GroupEmailManagement_ID == groupEmailSendApplicationBranch.ID);

                                                emailTo = emailConfigDataSend.Where(e => e.TO_CC == "TO").Select(c => c.EmailAddress).ToList();
                                                emailCC = emailConfigDataSend.Where(e => e.TO_CC == "CC").Select(c => c.EmailAddress).ToList();

                                                var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
                                                Stream fileAttachmentSend = await GenerateFile(transactionDetail, itemTransaction);

                                                var bodyEmailSend = _repository.sendMemo.GetBodyEmail("SaleSupport", itemTransaction);
                                                _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmailSend, fileAttachmentSend);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //var tempStaging = await _repository.eBaoMemoLetter.GetTempMemoLetterBy(c => documentSendSuccess.Contains(c.DOCUMENT_NO));

                        //await _repository.eBaoMemoLetter.UpdateMemoStatus(tempStaging, request.userID, "Issued");
                        //await _repository.memoTransaction.UpdateMemoTransactionStatusList(listUpdateTransaction);
                        //await _repository.memoTransaction.CreateMemoTransactionHistory(transactionHistory);
                    }
                    catch (Exception ex)
                    {
                        _log4net.Error(JsonConvert.SerializeObject(ex));
                    }
                });

                #endregion
            }

            return response;
        }

        public async Task<DateTime> CheckDateWorkingSpace(int spaceWorkingDay, DateTime? timeFirst)
        {
            //Utility.GetDateNowThai().AddDays(ruleManagementData.spaceWorkingDays.GetValueOrDefault())
            string applicationCode = appSettingHelper.GetConfiguration("ApplicationCode");
            string functionCode = appSettingHelper.GetConfiguration("FunctionCode");

            var reqHoliday = new GetHolidayRequest
            {
                ApplicationCode = applicationCode,
                FunctionCode = functionCode,
                StartDate = Utility.GetDateNowThai().ToString("yyyy-MM-dd"),
                EndDate = Utility.GetDateNowThai().AddDays((spaceWorkingDay * 2) + 30).ToString("yyyy-MM-dd")
            };

            string nextSendDate = "";
            var listHoliday = await _repository.holiday.GetHoliday(reqHoliday);

            foreach (var item in listHoliday)
            {
                if (item.IsHoliday == false)
                {
                    spaceWorkingDay -= 1;
                }

                //Date
                if (spaceWorkingDay == 0)
                {
                    nextSendDate = item.Date;
                    break;
                }

            }

            return nextSendDate == "" ? Utility.GetDateNowThai().AddDays(spaceWorkingDay) : Convert.ToDateTime(nextSendDate).AddHours(timeFirst.Value.Hour).AddMinutes(timeFirst.Value.Minute);

        }

        public async Task<SaveDataResponse> AutoSendMemo(string hostUrl)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                var deliveryStatus = _repository.memoTransaction.GetAllDeliveryStatus();
                var deliveryStatusWaitingProcess = deliveryStatus.FirstOrDefault(c => c.DeliveryStatus == "Waiting for process");

                var ebaoStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var ebaoStatusPrinted = ebaoStatusList.FirstOrDefault(c => c.EBaoMemoStatus == "Printed");

                var transactionForSend = await _repository.memoTransaction.GetMemoTransactionBy(
                    c => c.DeliveryStatus_ID == deliveryStatusWaitingProcess.ID
                    && c.PolicyStatus == "Underwriting In progress"
                    && c.EBaoMemoStatus_ID == ebaoStatusPrinted.ID
                    && c.Round != null && c.EstimateTime.HasValue
                    && c.EstimateTime <= Utility.GetDateNowThai());

                //var policyCode = transactionForSend.Select(c => c.PolicyNo);
                //var policyStatusList = await _repository.eBaoMemoLetter.GetPolicyStatus(policyCode.ToList());

                //transactionForSend.ForEach(c => c.PolicyStatus = policyStatusList.FirstOrDefault(e => e.policy_code == c.PolicyNo)?.status_desc);

                //transactionForSend = transactionForSend.Where(c => c.PolicyStatus == "Underwriting In progress").ToList();

                if (transactionForSend.Any())
                {
                    AdhocSendMemoRequest request = new AdhocSendMemoRequest
                    {
                        transactionIDList = transactionForSend.Select(c => c.ID).ToList(),
                        userID = "eMemo"
                    };

                    await AdhocSendMemo(request, hostUrl);
                }

                response.success = true;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "Success"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> UpdateDataTemp(UpdateTempRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                await _repository.eBaoMemoLetter.UpdateTempTable(request);
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<List<TTHMemoTransaction>> GetAllTXN()
        {
            return await _repository.memoTransaction.GetMemoTransactionMockUp();
        }

        public async Task<SaveDataResponse> UpdateAgentData()
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                var allTransaction = await _repository.memoTransaction.GetMemoTransactionBy(c => c.ID != 0 && string.IsNullOrWhiteSpace(c.Channel));
                foreach (var item in allTransaction)
                {
                    var agentInfoData = _repository.tghEmemo.GetAdditionalData(item.AgentCode, item.PolicyNo);
                    if (agentInfoData != null)
                    {

                        item.AgentUnitCode = agentInfoData.UNIT_CODE;
                        item.AgentUnit = agentInfoData.UNIT_NAME;
                        item.AgentGroupCode = agentInfoData.GROUP_CODE;
                        item.AgentGroup = agentInfoData.GROUP_NAME;
                        item.AgentEmail = agentInfoData.EMAIL;
                        item.BusinessUnit = agentInfoData.BUSINESS_UNIT_NAME;
                        item.AgentPhoneNumber = agentInfoData.MOBILE;
                        item.ApplicationBranch = agentInfoData.APPLICATION_BRANCH_NAME;
                        item.Channel = agentInfoData.CHANNEL_NAME;
                        item.ProductCategory = agentInfoData.PRODUCT_CATEGORY_NAME;
                        item.ServicingBranch = agentInfoData.BRANCH_NAME;
                        item.BranchCode = agentInfoData.BRANCH_CODE;
                        item.SourceOfBusiness = agentInfoData.SOURCE_OF_BUSINESS_NAME;
                        item.SubChannel = agentInfoData.SUB_CHANNEL_NAME;
                        item.ApplicationBranchCode = agentInfoData.APPLICATION_BRANCH_CODE;
                        item.BranchCOCode = agentInfoData.BRANCH_CO_CODE;

                        await _repository.memoTransaction.UpdateMemoTransactionHeader(item);

                    }
                }

                response.success = true;
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = "Success"
                };

                _log4net.Info("Update AgentData");

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        #region Private Function
        private async Task<Stream> GenerateMemoAttachStream(byte[] coverpage, int transactionID)
        {
            List<byte[]> allPDF = new List<byte[]>();
            var memoDetail = _repository.memoTransaction.GetMemoTransactionDetail(transactionID);
            var memoDetailIDList = memoDetail.OrderBy(e => e.MemoType_Sequence).ThenBy(d => d.MemoCode_Sequence).Select(c => c.ID);

            allPDF.Add(coverpage);

            var allMemoCodeAttach = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => c.MemoTransaction_ID == transactionID);

            foreach (var detailID in memoDetailIDList)
            {
                var memoCodeAttach = allMemoCodeAttach.Where(e => e.MemoTransactionDetail_ID == detailID);
                foreach (var itemAttach in memoCodeAttach.OrderBy(c => c.Created_Date))
                {
                    var fileFromDMC = await _repository.dmcFile.GetFileDMC(itemAttach.File_Path);
                    var byteFile = Convert.FromBase64String(fileFromDMC.base64);

                    allPDF.Add(byteFile);
                }
            }

            var mergePDF = ConcatAndAddContent(allPDF);
            return new MemoryStream(mergePDF);
        }

        private static byte[] ConcatAndAddContent(List<byte[]> pdfByteContent)
        {

            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();

                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {

                            //Create a PdfReader bound to that byte array
                            using (var reader = new PdfReader(p))
                            {

                                //Add the entire document instead of page-by-page
                                copy.AddDocument(reader);
                            }
                        }

                        doc.Close();
                    }
                }

                //Return just before disposing
                return ms.ToArray();
            }
        }

        private async Task<SaveDataResponse> CreateMemoTransaction(List<CreateMemoTransactionRequest> listRequest, List<T_MEMO_LETTER> dataNotTransfer)
        {
            SaveDataResponse response = new SaveDataResponse();
            List<string> documentSuccess = new List<string>();

            try
            {
                bool result = false;
                var deliveryStatusList = _repository.memoTransaction.GetAllDeliveryStatus();
                var deliveryStatusWaitingProcess = deliveryStatusList.FirstOrDefault(c => c.DeliveryStatus == "Waiting for process");
                var deliveryStatusUnMapping = deliveryStatusList.FirstOrDefault(c => c.DeliveryStatus == "Unmapping");

                var ebaoStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var ebaoStatusPrinted = ebaoStatusList.FirstOrDefault(c => c.EBaoMemoStatus == "Printed");

                var allMemoMappingList = await _repository.memoMapping.GetMemoMappingList();
                allMemoMappingList = allMemoMappingList.Where(c => c.status == true).ToList();

                var listMemoCodeID = allMemoMappingList.Select(e => e.memoCodeID);
                var memoCodeAttachmentList = _repository.memoMapping.GetMemoCodeAttachmentBy(d => listMemoCodeID.Contains(d.MemoCode_ID));

                foreach (var request in listRequest)
                {
                    request.eBaoMemoStatus = ebaoStatusPrinted.ID;
                    request.memoStatus = "New";

                    // TODO : Check Product Category
                    var additionalData = _repository.tghEmemo.GetAdditionalData(request.agentCode, request.policyNo);
                    //var additionalDataByPolicy = _repository.tghEmemo.GetAdditionalDataByPolicyNo(request.policyNo);

                    //if (additionalDataByPolicy != null)
                    //{
                    //    additionalData.PRODUCT_CATEGORY_CODE = additionalDataByPolicy.PRODUCT_CATEGORY_CODE;
                    //    additionalData.PRODUCT_CATEGORY_NAME = additionalDataByPolicy.PRODUCT_CATEGORY_NAME;
                    //}
                    //else
                    //{
                    //    var policyDigit3 = request.policyNo.Substring(2, 1);
                    //    if (policyDigit3 == "1")
                    //    {
                    //        additionalData.PRODUCT_CATEGORY_NAME = "Individual";
                    //    }
                    //    else if (policyDigit3 == "2")
                    //    {
                    //        if (additionalData.SOURCE_OF_BUSINESS_NAME != "SECAP" && additionalData.SOURCE_OF_BUSINESS_NAME != "Srisawad")
                    //        {
                    //            additionalData.PRODUCT_CATEGORY_NAME = "MRTA";
                    //        }
                    //        else if (additionalData.SOURCE_OF_BUSINESS_NAME == "SECAP")
                    //        {
                    //            additionalData.PRODUCT_CATEGORY_NAME = "SECAP";
                    //        }
                    //        else if (additionalData.SOURCE_OF_BUSINESS_NAME == "Srisawad")
                    //        {
                    //            additionalData.PRODUCT_CATEGORY_NAME = "Srisawad";
                    //        }
                    //    }
                    //    else if (policyDigit3 == "3")
                    //    {
                    //        additionalData.PRODUCT_CATEGORY_NAME = "Group 1 Year Term";
                    //    }
                    //    else if (policyDigit3 == "4")
                    //    {
                    //        additionalData.PRODUCT_CATEGORY_NAME = "PA";
                    //    }
                    //    else
                    //    {
                    //        additionalData.PRODUCT_CATEGORY_NAME = "Individual";
                    //    }
                    //}

                    var saveResult = await _repository.memoTransaction.CreateMemoTransactionHeader(request, additionalData);

                    if (saveResult != null && saveResult.ID != 0)
                    {
                        foreach (var itemDetail in request.detail)
                        {
                            // Mapping Memo Detail กับ Maste ใน eMemo
                            //var memoMappingData = allMemoMappingList.FirstOrDefault(
                            //   c => (itemDetail.memoCode != null && (c.memoCode.ToLower() == itemDetail.memoCode.ToLower()))
                            //   && c.memoTypeName.ToLower() == itemDetail.memoType.ToLower());

                            var memoMappingData = allMemoMappingList.FirstOrDefault(
                                c => itemDetail.memoCode != null && c.memoCode.ToLower().Trim().Replace(" ", "") == itemDetail.memoCode.ToLower().Trim().Replace(" ", ""));

                            if (memoMappingData != null)
                            {
                                itemDetail.memoTypeSequence = memoMappingData.memoTypeSequence;
                                itemDetail.memoCodeSequence = memoMappingData.memoCodeSequence;
                            }
                            else
                            {
                                itemDetail.memoTypeSequence = 999;
                                itemDetail.memoCodeSequence = 999;
                            }
                        }

                        if (request.detail.Any())
                        {
                            result = await _repository.memoTransaction.CreateMemoTransactionDetail(saveResult.ID, request);
                        }

                        if (!result)
                        {
                            await _repository.memoTransaction.DeleteMemoTransactionHeader(saveResult.ID);
                            response.status = new Model.Base.Status
                            {
                                code = ((int)HttpStatusCode.NotImplemented).ToString(),
                                message = "Save Transaction Detail Failed"
                            };
                        }
                        else
                        {
                            documentSuccess.Add(request.documentNo);
                            var memoTransactionDetail = _repository.memoTransaction.GetMemoTransactionDetail(saveResult.ID);

                            var resultMapping = await MappingEMemoAttachment(allMemoMappingList, memoTransactionDetail, memoCodeAttachmentList, saveResult.ID, request.userID, request.documentNo);

                            UpdateDeliveryStatusData updateStatusRequest = new UpdateDeliveryStatusData
                            {
                                transactionID = saveResult.ID,
                                deliveryStatus = resultMapping ? deliveryStatusWaitingProcess.ID : deliveryStatusUnMapping.ID
                            };

                            await _repository.memoTransaction.UpdateDeliveryStatus(updateStatusRequest, request.userID);

                            var itemUpdate = dataNotTransfer.FirstOrDefault(x => x.DOCUMENT_NO == request.documentNo);
                            await _repository.eBaoMemoLetter.UpdateTransferFlagPeritem(itemUpdate, "eMemo");
                        }
                    }
                    else
                    {
                        response.status = new Model.Base.Status
                        {
                            code = ((int)HttpStatusCode.NotImplemented).ToString(),
                            message = "Save Transaction Header Failed"
                        };
                    }
                }

                if (documentSuccess.Any())
                {
                    foreach (var itemData in dataNotTransfer.Where(c => documentSuccess.Contains(c.DOCUMENT_NO)))
                    {
                        itemData.IS_TRANSFER = "Y";
                    }

                    _repository.eBaoMemoLetter.UpdateTransferFlag(dataNotTransfer, "eMemo");
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        private async Task<bool> MappingEMemoAttachment(List<MemoMappingData> allMemoMappingList, List<TTDMemoTransactionDetail> detail,
            List<TMMemoCodeAttachment> memoCodeAttachmentList, int transactionID, string userID, string documentNo)
        {
            try
            {
                DateTime dateNow = Utility.GetDateNowThai();
                bool mappingiResult = true;

                List<TTDMemoTransactionAttachment> allAttachmentList = new List<TTDMemoTransactionAttachment>();

                foreach (var itemDetail in detail.Where(w => !string.IsNullOrWhiteSpace(w.MemoCode)).OrderBy(c => c.MemoType_Sequence).ThenBy(e => e.MemoCode_Sequence))
                {
                    //var memoMappingData = allMemoMappingList.FirstOrDefault(
                    //            c => (itemDetail.MemoCode != null && (c.memoCode.ToLower() == itemDetail.MemoCode.ToLower()))
                    //            && c.memoTypeName.ToLower() == itemDetail.MemoType.ToLower());

                    var memoMappingData = allMemoMappingList.FirstOrDefault(
                             c => itemDetail.MemoCode != null && c.memoCode.ToLower() == itemDetail.MemoCode.ToLower());

                    if (memoMappingData != null)
                    {
                        var memoAttachment = memoCodeAttachmentList.Where(s => s.MemoCode_ID == memoMappingData.memoCodeID);
                        if (memoAttachment.Any())
                        {
                            List<FileDetail> listFileAttachment = new List<FileDetail>();

                            foreach (var itemAttachment in memoAttachment.OrderBy(c => c.Created_Date))
                            {
                                // Get Attachment จาก Memo mapping master มาบันทึกไว้ที่ Transaction
                                // Get File External
                                var fileFromDMC = await _repository.dmcFile.GetFileDMC(itemAttachment.File_Path);

                                // Save FileExternal
                                string referenceID = Guid.NewGuid().ToString();
                                itemAttachment.Reference_ID = referenceID;

                                if (fileFromDMC != null && fileFromDMC.responseCode.resultCode == "Ok")
                                {
                                    FileDetail fileData = new FileDetail
                                    {
                                        filename = referenceID + ".pdf",
                                        extension = itemAttachment.File_Extension,
                                        data = fileFromDMC.base64
                                    };

                                    listFileAttachment.Add(fileData);
                                }
                                else
                                {
                                    _log4net.Error(fileFromDMC.responseCode.description);
                                }
                            }

                            SaveFileRequest saveFileRequest = new SaveFileRequest
                            {
                                application = "Ememo",
                                files = listFileAttachment
                            };

                            var saveFileResponse = await _repository.dmcFile.SaveFileDMC(saveFileRequest);

                            if (saveFileResponse != null && saveFileResponse.operationStatus != null && saveFileResponse.operationStatus.statusCode == 200)
                            {
                                var saveFileResult = saveFileResponse.files;

                                List<TTDMemoTransactionAttachment> listSaveAttachmentDetail = memoAttachment.OrderBy(e => e.Created_Date).Select(c => new TTDMemoTransactionAttachment
                                {
                                    MemoTransaction_ID = transactionID,
                                    Reference_ID = c.Reference_ID,
                                    File_Name = c.File_Name,
                                    File_Path = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.data,
                                    File_Size = c.File_Size,
                                    File_Type = c.File_Type,
                                    File_Extension = saveFileResult.FirstOrDefault(e => e.fileName.Contains(c.Reference_ID))?.extension,
                                    MemoTransactionDetail_ID = itemDetail.ID,
                                    Created_By = userID,
                                    Created_Date = c.Created_Date
                                }).ToList();

                                await _repository.memoTransaction.SaveEMemoTransactionDetailAttachment(listSaveAttachmentDetail);
                                allAttachmentList.AddRange(listSaveAttachmentDetail);
                            }
                            else
                            {
                                _log4net.Error(saveFileResponse.operationStatus.description);
                            }
                        }
                    }
                    else
                    {
                        mappingiResult = false;
                    }
                }

                TTHMemoDocumentHistory memoHistory = new TTHMemoDocumentHistory
                {
                    EMEMODocumentNo = documentNo,
                    MemoTransaction_ID = transactionID,
                    SequenceEditDocumentNo = 1,
                    TransactionDate = Utility.GetDateNowThai(),
                    Created_Date = Utility.GetDateNowThai(),
                    Created_By = userID
                };

                var saveHistoryResult = await _repository.memoTransaction.CreateMemoDocumentHistory(memoHistory);

                foreach (var itemDetail in detail)
                {
                    var memoAttachmentByID = allAttachmentList.Where(c => c.MemoTransactionDetail_ID == itemDetail.ID);

                    TTDMemoDocumentHistoryDetail historyDocumentDetail = new TTDMemoDocumentHistoryDetail
                    {
                        Comment = itemDetail.Comment,
                        Created_By = itemDetail.Created_By,
                        Created_Date = Utility.GetDateNowThai(),
                        DocumentNo = itemDetail.DocumentNo,
                        MemoCode = itemDetail.MemoCode,
                        MemoCodeDescription = itemDetail.MemoCodeDescription,
                        MemoCode_Sequence = itemDetail.MemoCode_Sequence,
                        MemoDocumentHistory_ID = saveHistoryResult.ID,
                        LetterStatus = itemDetail.LetterStatus,
                        MemoType = itemDetail.MemoType,
                        MemoType_Sequence = itemDetail.MemoType_Sequence,
                        Source = itemDetail.Source,
                        MemoTransaction_ID = itemDetail.MemoTransaction_ID,
                        Reply_By = itemDetail.Reply_By,
                        Reply_Date = itemDetail.Reply_Date
                    };

                    var saveDetailResponse = await _repository.memoTransaction.CreateMemoDocumentHistoryDetail(historyDocumentDetail);

                    var memoHistoryAttachment = memoAttachmentByID.Select(c => new TTDMemoDocumentHistoryAttachment
                    {
                        MemoDocumentHistoryDetail_ID = saveDetailResponse.ID,
                        MemoDocumentHistory_ID = saveHistoryResult.ID,
                        File_Name = c.File_Name,
                        Reference_ID = c.Reference_ID,
                        File_Extension = c.File_Extension,
                        File_Path = c.File_Path,
                        File_Size = c.File_Size,
                        File_Type = c.File_Type,
                        Created_By = userID,
                        Created_Date = Utility.GetDateNowThai(),
                        Sequence = c.Sequence.GetValueOrDefault()
                    }).ToList();

                    await _repository.memoTransaction.SaveMemoDocumentHistoryAttchment(memoHistoryAttachment);
                }

                return mappingiResult;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return false;
            }
        }


        private async Task<Stream> GenerateFile(List<TTDMemoTransactionDetail> allTransactionDetail, TTHMemoTransaction itemTransaction)
        {
            var transactionDetail = allTransactionDetail.Where(c => c.MemoTransaction_ID == itemTransaction.ID).ToList();
            List<MemoDetailData> memoDetailData = transactionDetail.Select(e => new MemoDetailData
            {
                Comment = e.Comment,
                DocumentNo = e.DocumentNo,
                LetterStatus = e.LetterStatus,
                MemoCode = e.MemoCode,
                MemoCodeDescription = e.MemoCodeDescription,
                MemoCode_Sequence = e.MemoCode_Sequence,
                MemoType = e.MemoType,
                MemoType_Sequence = e.MemoType_Sequence,
                Reply_By = e.Reply_By,
                Reply_Date = e.Reply_Date,
                Source = e.Source,
                MemoTransaction_ID = e.MemoTransaction_ID
            }).ToList();


            var coverPage = _repository.sendMemo.GenerateCoverPage(itemTransaction, memoDetailData, itemTransaction.Note);
            var fileAttachment = await GenerateMemoAttachStream(coverPage, itemTransaction.ID);
            return fileAttachment;
        }

        private (RuleManagementData, string) FindRuleData(List<RuleManagementData> allRuleManagement, TTHMemoTransaction itemTransaction)
        {
            string errorMessage = string.Empty;
            RuleManagementData ruleManagementData = null;

            if (itemTransaction.Channel != null)
            {
                var allRuleManagementMatchChannel = allRuleManagement.Where(
                               c => c.channel.Contains(itemTransaction.Channel)
                               && (itemTransaction.SubChannel != null && c.subChannel.Contains(itemTransaction.SubChannel)));

                if (!allRuleManagementMatchChannel.Any())
                {
                    allRuleManagementMatchChannel = allRuleManagement.Where(
                                        c => c.channel.Contains(itemTransaction.Channel));
                }

                if (allRuleManagementMatchChannel.Any())
                {
                    var ruleManagementMatchSOB = allRuleManagementMatchChannel.Where(
                                      c => (itemTransaction.SourceOfBusiness == null || (c.sob != null && itemTransaction.SourceOfBusiness != null && c.sob.Contains(itemTransaction.SourceOfBusiness)))
                                      && (itemTransaction.BusinessUnit == null || (c.businessUnit != null && itemTransaction.BusinessUnit != null && c.businessUnit.Contains(itemTransaction.BusinessUnit))));

                    if (ruleManagementMatchSOB.Any())
                    {
                        ruleManagementData = ruleManagementMatchSOB.Where(c => c.productCategory.Contains(itemTransaction.ProductCategory)).FirstOrDefault();

                        if (ruleManagementData == null)
                        {
                            ruleManagementData = ruleManagementMatchSOB.FirstOrDefault(c => c.productCategory.Count == 0);
                        }
                    }

                    if (ruleManagementData == null)
                    {
                        ruleManagementData = allRuleManagementMatchChannel.Where(c => c.productCategory.Contains(itemTransaction.ProductCategory)).FirstOrDefault();

                        if (ruleManagementData == null)
                        {
                            ruleManagementData = allRuleManagementMatchChannel.FirstOrDefault(c => c.productCategory.Count == 0);
                        }
                    }

                    if (ruleManagementData == null)
                    {
                        errorMessage = $"ไม่พบ Rule ที่ทำการ Set ไว้สำหรับ Channel {itemTransaction.Channel} และ Product Category {itemTransaction.ProductCategory}";
                    }
                }
                else
                {
                    errorMessage = $"ไม่พบ Rule ที่ทำการ Set ไว้สำหรับ Channel {itemTransaction.Channel}";
                }

            }
            else
            {
                errorMessage = $"กธ. {itemTransaction.PolicyNo} ไม่มีข้อมูล Channel";
            }

            return (ruleManagementData, errorMessage);
        }
        #endregion
    }
}
