﻿using eMemo.Helper;
using eMemo.Model.Base;
using eMemo.Model.Entity;
using eMemo.Model.Request.MemoInquiry;
using eMemo.Model.Response;
using eMemo.Model.Response.EMemoTransaction;
using eMemo.Model.Response.MemoInquiry;
using eMemo.Repositories;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Processes.Processes
{
    public class MemoInquiryProcess
    {
        private readonly Repository _repository;
        private AppSettingHelper appSettingHelper;
        private IHttpContextAccessor _httpContextAccessor;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(MemoInquiryProcess));
        public MemoInquiryProcess(Repository repository, IHttpContextAccessor httpContextAccessor)
        {
            _repository = repository;
            appSettingHelper = new AppSettingHelper();
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<GetMemoInquiryResponse> GetMemoInquiry(GetMemoInquiryRequest request)
        {
            GetMemoInquiryResponse response = new GetMemoInquiryResponse();

            try
            {
                var result = await _repository.memoTransaction.GetMemoTransactionBy(c => c.EMemoStatus != "New");

                var eboaStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var deliveryStatusList = _repository.memoTransaction.GetAllDeliveryStatus();

                if (!string.IsNullOrEmpty(request.documentNo))
                {
                    result = result.Where(w => w.DocumentNo.ToUpper().EndsWith(request.documentNo.ToUpper())).ToList();
                }

                if (request.transactionDateTo != null && request.transactionDateForm == null)
                {
                    result = result.Where(w => w.TransactionDate.Date == request.transactionDateForm.Value.Date).ToList();
                }

                if (request.transactionDateTo != null && request.transactionDateForm != null)
                {
                    result = result.Where(w => w.TransactionDate.Date >= request.transactionDateForm.Value.Date && w.TransactionDate.Date <= request.transactionDateTo.Value.Date).ToList();
                }

                if (!string.IsNullOrEmpty(request.policyNo))
                {
                    result = result.Where(w => w.PolicyNo.ToUpper().Equals(request.policyNo.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.proposalNo))
                {
                    result = result.Where(w => w.ProposalNo.ToUpper().Equals(request.proposalNo.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.ebaoMemoStatusID))
                {
                    result = result.Where(w => w.EBaoMemoStatus_ID == request.ebaoMemoStatusID).ToList();
                }

                if (!string.IsNullOrEmpty(request.citizenID))
                {
                    result = result.Where(w => w.CitizenID.ToUpper().Equals(request.citizenID.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.partyID))
                {
                    result = result.Where(w => w.PartyID.ToUpper().Equals(request.partyID.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.customerName))
                {
                    result = result.Where(w => w.PolicyName.ToUpper().Contains(request.customerName.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.customerSurname))
                {
                    result = result.Where(w => w.PolicyName.ToUpper().Contains(request.customerSurname.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentCode))
                {
                    result = result.Where(w => w.AgentCode.ToUpper().Contains(request.agentCode.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentName))
                {
                    result = result.Where(w => w.AgentName.ToUpper().Contains($"{request.agentName.ToUpper()}")).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentSurname))
                {
                    result = result.Where(w => w.AgentName.ToUpper().Contains($"{request.agentSurname.ToUpper()}")).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentUnit))
                {
                    result = result.Where(w => w.AgentUnit.ToUpper().Contains(request.agentUnit.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentGroup))
                {
                    result = result.Where(w => w.AgentGroup.ToUpper().Contains(request.agentGroup.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.sending))
                {
                    var sendingStatus = deliveryStatusList.FirstOrDefault(c => c.ID == request.sending);

                    result = result.Where(w => w.EMemoStatus == sendingStatus.DeliveryStatus).ToList();
                }

                if (result.Any())
                {
                    var transactionIDList = result.Select(c => c.ID);
                    var allMemoDetail = _repository.memoTransaction.GetMemoTransactionDetailBy(c => transactionIDList.Contains(c.MemoTransaction_ID));

                    var data = result.Select(s => new MemoInquiryData()
                    {
                        transactionID = s.ID,
                        documentNo = s.DocumentNo,
                        transactionDate = s.TransactionDate.ToString("dd/MM/yyyy"),
                        policyNo = s.PolicyNo,
                        proposalNo = s.ProposalNo,
                        policyName = s.PolicyName,
                        agentCode = s.AgentCode,
                        agentName = s.AgentName,
                        agentUnit = s.AgentUnit,
                        agentGroup = s.AgentGroup,
                        eBaoMemoStatusID = s.EBaoMemoStatus_ID,
                        ebaoMemoStatus = eboaStatusList.FirstOrDefault(c => c.ID == s.EBaoMemoStatus_ID)?.EBaoMemoStatus,
                        sending = deliveryStatusList.FirstOrDefault(c => c.DeliveryStatus == s.EMemoStatus)?.DeliveryStatus,
                        osMemo = allMemoDetail.Count(e => e.MemoTransaction_ID == s.ID && string.IsNullOrWhiteSpace(e.LetterStatus))
                    }).ToList();


                    if (!string.IsNullOrWhiteSpace(request.osMemo))
                    {
                        if (request.osMemo == "Y")
                        {
                            data = data.Where(w => w.osMemo > 0).ToList();
                        }
                        else
                        {
                            data = data.Where(w => w.osMemo == 0).ToList();
                        }
                    }

                    response.data = data;
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "ค้นหาข้อมูลสำเร็จ"
                    };
                }
                else
                {
                    response.data = new List<MemoInquiryData>();
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetEmemoInquiryDetailResponse> GetMemoInquiryDetail(int transactionID, string hostUrl)
        {
            GetEmemoInquiryDetailResponse response = new GetEmemoInquiryDetailResponse();

            try
            {
                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");
                var memoTransactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.ID == transactionID);
                var ebaoStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var memoDocumentHistory = await _repository.memoTransaction.GetMemoDocumentHistoryBy(c => c.MemoTransaction_ID == transactionID);

                if (memoTransactionData != null)
                {
                    List<string> plicyCode = new List<string>() { memoTransactionData.PolicyNo };
                    var policyStatus = await _repository.eBaoMemoLetter.GetPolicyStatus(plicyCode);

                    if (policyStatus != null)
                    {
                        memoTransactionData.PolicyStatus = policyStatus.FirstOrDefault().status_desc;
                    }

                    var basicInformation = new InquiryBasicInfomation();
                    basicInformation.citizenID = memoTransactionData.CitizenID;
                    basicInformation.agentName = memoTransactionData.AgentCode + " : " + memoTransactionData.AgentName;
                    basicInformation.group = memoTransactionData.AgentGroup;
                    basicInformation.unit = memoTransactionData.AgentUnit;
                    basicInformation.policyNo = memoTransactionData.PolicyNo;
                    basicInformation.policyName = memoTransactionData.PolicyName;
                    basicInformation.policyStatus = memoTransactionData.PolicyStatus;
                    basicInformation.proposalNo = memoTransactionData.ProposalNo;
                    basicInformation.proposalDate = memoTransactionData.ProposalDate.ToString("dd/MM/yyyy");
                    basicInformation.mainPlan = memoTransactionData.MainPlan;
                    basicInformation.mainPlan = memoTransactionData.MainPlan;
                    basicInformation.agentEmail = memoTransactionData.AgentEmail;
                    basicInformation.agentPhoneNumber = memoTransactionData.AgentPhoneNumber;
                    basicInformation.customerEmail = memoTransactionData.PHEmail;
                    basicInformation.customerPhoneNumber = memoTransactionData.PHPhoneNo;

                    var memoDocumentInformation = new InquiryMemoDocumentInfomation();
                    memoDocumentInformation.ebaoDocumentNo = memoTransactionData.DocumentNo;
                    memoDocumentInformation.transactionDate = memoTransactionData.TransactionDate.ToString("dd/MM/yyyy");
                    memoDocumentInformation.createdUser = memoTransactionData.Created_By;
                    memoDocumentInformation.ebaoMemoStatus = ebaoStatusList.FirstOrDefault(c => c.ID == memoTransactionData.EBaoMemoStatus_ID)?.EBaoMemoStatus;
                    memoDocumentInformation.ebaoRelpliedUser = memoTransactionData.EBaoRepliedUser;
                    memoDocumentInformation.ebaoRelpliedDate = memoTransactionData.EBaoRepliedDate == null ? "-" : Convert.ToDateTime(memoTransactionData.EBaoRepliedDate).ToString("dd/MM/yyyy");

                    var memoAmendmentInformation = new InquiryMemoAmendmentInformation();
                    List<MemoDocumentHistory> listMemoDocumentHistory = new List<MemoDocumentHistory>();

                    var memoTransactionDetailData = await _repository.memoTransaction.FindMemoTransactionDetailBy(c => c.MemoTransaction_ID == transactionID);
                    if (memoTransactionDetailData.Any())
                    {
                        var memoAttachment = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => c.MemoTransaction_ID == transactionID);

                        memoAmendmentInformation.documents = memoTransactionDetailData.OrderBy(d => d.MemoType_Sequence).ThenBy(e => e.MemoCode_Sequence).Select(c => new InquiryMemoDocument
                        {
                            memoDetailID = c.ID,
                            memoCode = c.MemoCode,
                            memoDescription = c.MemoCodeDescription,
                            comment = c.Comment,
                            letterStatus = c.LetterStatus == "Discarded" ? "Discard" : c.LetterStatus,
                            replyBy = c.Reply_By,
                            replyDate = c.Reply_Date,
                            source = c.Source,
                            createdDate = c.Created_Date.ToString("dd/MM/yyyy", _cultureTHInfo),
                            modifyDate = c.Updated_Date.HasValue ? c.Updated_Date.Value.ToString("dd/MM/yyyy", _cultureTHInfo) : null,
                            fileAttachments = memoAttachment.Where(d => d.MemoTransactionDetail_ID == c.ID).Select(e => new FileAttachment
                            {
                                filePath = $"{hostUrl}/EMEMO_API/FileAttachment/GetFileAttachment?fileType=MEMOCODETXN&referenceID={e.ID}",
                                fileName = e.File_Name,
                                fileType = e.File_Type,
                                fileSize = (decimal)e.File_Size,
                            }).ToList()
                        }).ToList();
                    }
                    else
                    {
                        memoAmendmentInformation.documents = new List<InquiryMemoDocument>();
                    }


                    int sequence = 1;
                    foreach (var itemDoc in memoAmendmentInformation.documents)
                    {
                        itemDoc.sequence = sequence;
                        sequence++;
                    }

                    var memoTransactionHistoryData = await _repository.memoTransaction.FindMemoTransactionHistoryBy(c => c.MemoTransaction_ID == transactionID);
                    if (memoTransactionHistoryData.Any())
                    {
                        memoAmendmentInformation.history = memoTransactionHistoryData.OrderByDescending(e => e.Send_DateTime).Select(c => new InquiryMemoHistory
                        {
                            sendTo = c.SendTo,
                            channel = c.Channel,
                            description = c.Description,
                            sendStatus = c.Send_Status,
                            sendBy = c.Send_By,
                            sendDate = c.Send_DateTime.ToString("dd/MM/yyyy"),
                            sendTime = c.Send_DateTime.ToString("HH:mm")
                        }).ToList();
                    }
                    else
                    {
                        memoAmendmentInformation.history = new List<InquiryMemoHistory>();
                    }

                    if (memoDocumentHistory.Any())
                    {
                        var memoHistoryIDList = memoDocumentHistory.Select(c => c.ID);
                        var memoDumentHistoryDetail = await _repository.memoTransaction.GetMemoDocumentHistoryDetailBy(c => memoHistoryIDList.Contains(c.MemoDocumentHistory_ID));

                        listMemoDocumentHistory = memoDocumentHistory.OrderBy(e => e.SequenceEditDocumentNo).Select(c => new MemoDocumentHistory
                        {
                            documentNo = c.EMEMODocumentNo + "-" + c.SequenceEditDocumentNo,
                            transactionDate = c.TransactionDate.ToString("dd/MM/yyyy"),
                            osMemo = memoDumentHistoryDetail.Where(d => d.MemoDocumentHistory_ID == c.ID).Count(),
                            memoDocumentHistoryID = c.ID,
                            printedBy = c.Created_By
                        }).ToList();
                    }

                    List<string> noteList = new List<string>();
                    if (memoTransactionData.Note != null)
                    {
                        noteList = memoTransactionData.Note.Split("|").ToList();
                    }

                    response.data = new EmemoInquiryDetailData
                    {
                        memoDocumentInformation = memoDocumentInformation,
                        basicInformation = basicInformation,
                        memoAmendmentInformation = memoAmendmentInformation,
                        memoTransactionID = memoTransactionData.ID,
                        memoDocumentHistory = listMemoDocumentHistory,
                        noteList = noteList
                    };
                    response.status = new Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "ค้นหาข้อมูลสำเร็จ"
                    };
                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> ResendMemo(ResendMemoRequest request, string hostUrl)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                string errorMessage = string.Empty;

                if (string.IsNullOrWhiteSpace(request.information))
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.UnprocessableEntity).ToString(),
                        message = "กรุณาระบุ information"
                    };

                    return response;    
                }

                if (string.IsNullOrWhiteSpace(request.via))
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.UnprocessableEntity).ToString(),
                        message = "กรุณาระบุ via"
                    };

                    return response;
                }

                if (string.IsNullOrWhiteSpace(request.channel))
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.UnprocessableEntity).ToString(),
                        message = "กรุณาระบุ channel"
                    };

                    return response;
                }

                var historyData = await _repository.memoTransaction.FindMemoDocumentHistoryBy(c => c.ID == request.memoDocumentHistoryID);

                if (historyData != null)
                {
                    var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.ID == historyData.MemoTransaction_ID);
                    var memoDetail = await _repository.memoTransaction.GetMemoDocumentHistoryDetailBy(c => c.MemoDocumentHistory_ID == request.memoDocumentHistoryID);

                    var deliveryStatus = _repository.memoTransaction.GetAllDeliveryStatus();
                    var deliverySuccess = deliveryStatus.FirstOrDefault(c => c.DeliveryStatus == "Successfully");
                    var deliveryUnSuccess = deliveryStatus.FirstOrDefault(c => c.DeliveryStatus == "Unsuccessfully");

                    var ebaoStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                    var ebaoStatusPrinted = ebaoStatusList.FirstOrDefault(c => c.EBaoMemoStatus == "Printed");

                    List<MemoDetailData> memoDetailData = memoDetail.Select(e => new MemoDetailData
                    {
                        Comment = e.Comment,
                        DocumentNo = e.DocumentNo,
                        LetterStatus = e.LetterStatus,
                        MemoCode = e.MemoCode,
                        MemoCodeDescription = e.MemoCodeDescription,
                        MemoCode_Sequence = e.MemoCode_Sequence,
                        MemoType = e.MemoType,
                        MemoType_Sequence = e.MemoType_Sequence,
                        Reply_By = e.Reply_By,
                        Reply_Date = e.Reply_Date,
                        Source = e.Source,
                        MemoTransaction_ID = e.MemoTransaction_ID
                    }).ToList();

                    if (request.channel == "SMS")
                    {
                        string pathDocument = $"{hostUrl}/EMEMO_API/FileAttachment/PreviewMemoAttachmentHistory/{transactionData.DocumentNo + "-" + historyData.SequenceEditDocumentNo}";

                        string messageSMS = string.Empty;
                        string productCategory = Utility.CheckProductCategory(transactionData.PolicyNo, transactionData.SourceOfBusiness);
                        string policyNameSMS = string.Empty;

                        if (productCategory == "Individual" || productCategory == "PA")
                        {
                            if (Convert.ToInt32(transactionData.EntryAgeLifeAssured) > 19)
                            {
                                policyNameSMS = transactionData.LifeAssuredName;
                            }
                            else
                            {
                                policyNameSMS = transactionData.PolicyName + " ผู้ปกครองของ " + transactionData.LifeAssuredName;
                            }
                        }
                        else if (productCategory == "MRTA" || productCategory == "Group 1 Year Term")
                        {
                            policyNameSMS = transactionData.LifeAssuredName;
                        }
                        else
                        {
                            policyNameSMS = transactionData.PolicyName;
                        }

                        if (request.via == "Customer")
                        {
                            messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารจาก {policyNameSMS} เพิ่มเติมเพื่อพิจารณาการรับประกันกธ. { transactionData.PolicyNo} คลิก {pathDocument}";
                        }
                        else
                        {
                            messageSMS = $"อาคเนย์ประกันชีวิตขอเอกสารเพิ่มเติมเพื่อพิจารณาการรับประกันกธ. {transactionData.PolicyNo} คลิก {pathDocument}";

                        }

                        var responseSend = await _repository.sendMemo.SendSMS(request.information, messageSMS);
                        response.success = responseSend.resultCode == "1";

                        if (!response.success)
                        {
                            errorMessage = responseSend.message;
                            _log4net.Error(responseSend.message);
                        }
                    }
                    else
                    {
                        var coverPage = _repository.sendMemo.GenerateCoverPage(transactionData, memoDetailData, historyData.Note, historyData.TransactionDate);
                        var fileAttachment = await GenerateMemoAttachStream(coverPage, transactionData.ID);

                        List<string> emailTo = new List<string>();
                        List<string> emailCC = new List<string>();

                        emailTo.Add(request.information);
                        var bodyEmail = _repository.sendMemo.GetBodyEmail(request.via, transactionData);

                        if (fileAttachment.Length == 0)
                        {
                            response.success = false;
                        }
                        else
                        {
                            response.success = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                            if (!response.success)
                            {
                                errorMessage = "Send Email Failed";
                            }

                        }
                    }

                    List<TTDMemoTransactionHistory> transactionHistory = new List<TTDMemoTransactionHistory>();
                    List<UpdateStatusTransactionRequest> listUpdateStatus = new List<UpdateStatusTransactionRequest>();

                    TTDMemoTransactionHistory history = new TTDMemoTransactionHistory
                    {
                        MemoTransaction_ID = transactionData.ID,
                        Send_By = request.userID,
                        Send_DateTime = Utility.GetDateNowThai(),
                        SendTo = request.via,
                        Channel = request.channel,
                        AutoMemoSending = "N",
                        Description = request.information,
                    };

                    UpdateStatusTransactionRequest updateStatus = new UpdateStatusTransactionRequest
                    {
                        transactionID = transactionData.ID,
                        updateBy = request.userID,
                        updateDate = Utility.GetDateNowThai()
                    };

                    if (response.success)
                    {
                        updateStatus.ememoStatus = deliverySuccess.DeliveryStatus;
                        history.Send_Status = deliverySuccess.DeliveryStatus;

                        if (ebaoStatusPrinted.ID == transactionData.EBaoMemoStatus_ID && !transactionData.Round.HasValue)
                        {
                            var tempStaging = await _repository.eBaoMemoLetter.GetTempMemoLetterBy(c => c.DOCUMENT_NO == transactionData.DocumentNo);
                            await _repository.eBaoMemoLetter.UpdateMemoStatus(tempStaging, request.userID, "Issued");
                        }
                    }
                    else
                    {
                        updateStatus.ememoStatus = deliveryUnSuccess.DeliveryStatus;
                        history.Send_Status = deliveryUnSuccess.DeliveryStatus;
                    }

                    listUpdateStatus.Add(updateStatus);
                    transactionHistory.Add(history);

                    await _repository.memoTransaction.UpdateMemoTransactionStatusList(listUpdateStatus);
                    await _repository.memoTransaction.CreateMemoTransactionHistory(transactionHistory);

                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = response.success ? "Resend Success" : string.IsNullOrWhiteSpace(errorMessage) ? "Resend Failed" : errorMessage
                    };

                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.UnprocessableEntity).ToString(),
                        message = "ไม่พบข้อมูล Transaction History"
                    };

                }
            }
            catch (Exception ex)
            {
                _log4net.Error(ex.InnerException?.Message ?? ex.Message);
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> ReplyMemoItem(ReplyMemoItemRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                var memoDetailIDUpdate = request.memoReplyList.Select(c => c.memoDetailID);
                var memoDetailUpdateData = _repository.memoTransaction.GetMemoTransactionDetailBy(c => memoDetailIDUpdate.Contains(c.ID));
                var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.ID == request.memoTransactionID);

                foreach (var itemDetail in memoDetailUpdateData)
                {
                    var dataUpdateReply = request.memoReplyList.FirstOrDefault(c => c.memoDetailID == itemDetail.ID);

                    if (dataUpdateReply != null)
                    {
                        itemDetail.LetterStatus = dataUpdateReply.letterStatus == null ? string.Empty : dataUpdateReply.letterStatus;
                        itemDetail.Reply_By = request.userID;
                        itemDetail.Source = "eMemo";
                        itemDetail.Reply_Date = dataUpdateReply.replyDate;
                    }
                }

                response.success = await _repository.memoTransaction.UpdateMemoTransactionDetailList(memoDetailUpdateData);

                if (response.success)
                {
                    await _repository.memoTransaction.CreateMemoDocumentTransactionHistory(request.memoTransactionID, transactionData.DocumentNo, request.userID, transactionData.Note);
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "บันทึกข้อมูลสำเร็จ"
                    };
                }
                else
                {
                    response.status = new Model.Base.Status
                    {
                        code = ((int)HttpStatusCode.NotImplemented).ToString(),
                        message = "บันทึกข้อมูลไม่สำเร็จ"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<SaveDataResponse> SendAfterSave(SendAfterSaveRequest request)
        {
            SaveDataResponse response = new SaveDataResponse();

            try
            {
                string errorMessage = string.Empty;

                var transactionDetail = _repository.memoTransaction.GetMemoTransactionDetail(request.memoTransactionID);
                var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.ID == request.memoTransactionID);
                var deliveryStatus = _repository.memoTransaction.GetAllDeliveryStatus();
                var deliverySuccess = deliveryStatus.FirstOrDefault(c => c.DeliveryStatus == "Successfully");
                var deliveryUnSuccess = deliveryStatus.FirstOrDefault(c => c.DeliveryStatus == "Unsuccessfully");

                List<TTDMemoTransactionHistory> historyList = new List<TTDMemoTransactionHistory>();

                TTDMemoTransactionHistory history = new TTDMemoTransactionHistory
                {
                    MemoTransaction_ID = transactionData.ID,
                    Send_By = request.userID,
                    Send_DateTime = Utility.GetDateNowThai(),
                };

                List<MemoDetailData> memoDetailData = transactionDetail.Select(e => new MemoDetailData
                {
                    Comment = e.Comment,
                    DocumentNo = e.DocumentNo,
                    LetterStatus = e.LetterStatus,
                    MemoCode = e.MemoCode,
                    MemoCodeDescription = e.MemoCodeDescription,
                    MemoCode_Sequence = e.MemoCode_Sequence,
                    MemoType = e.MemoType,
                    MemoType_Sequence = e.MemoType_Sequence,
                    Reply_By = e.Reply_By,
                    Reply_Date = e.Reply_Date,
                    Source = e.Source,
                    MemoTransaction_ID = e.MemoTransaction_ID
                }).ToList();

                List<string> emailTo = new List<string>();
                List<string> emailCC = new List<string>();
                bool sendResult = false;
                var coverPage = _repository.sendMemo.GenerateCoverPage(transactionData, memoDetailData, transactionData.Note);
                Stream fileAttachment = await GenerateMemoAttachStream(coverPage, transactionData.ID);

                history.Channel = "E-Mail";

                if (request.sendTo == "Agent")
                {
                    history.SendTo = "Agent";
                    history.Description = transactionData.AgentEmail;
                    emailTo.Add(transactionData.AgentEmail);
                    var bodyEmail = _repository.sendMemo.GetBodyEmail("Agent", transactionData);

                    if (fileAttachment.Length == 0)
                    {
                        sendResult = false;
                    }
                    else
                    {
                        sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                        if (sendResult)
                        {
                            errorMessage = "Send Email Failed";
                        }
                    }
                }
                else
                {
                    history.SendTo = "Customer";
                    history.Description = transactionData.PHEmail;
                    emailTo.Add(transactionData.PHEmail);
                    var bodyEmail = _repository.sendMemo.GetBodyEmail("Customer", transactionData);

                    if (fileAttachment.Length == 0)
                    {
                        sendResult = false;
                    }
                    else
                    {
                        sendResult = _repository.sendMemo.SendEmail(emailTo, emailCC, bodyEmail, fileAttachment);
                        if (sendResult)
                        {
                            errorMessage = "Send Email Failed";
                        }
                    }
                }


                if (sendResult)
                {
                    history.Send_Status = deliverySuccess.DeliveryStatus;
                }
                else
                {
                    history.Send_Status = deliveryUnSuccess.DeliveryStatus;

                }

                historyList.Add(history);
                await _repository.memoTransaction.CreateMemoTransactionHistory(historyList);

                response.success = sendResult;
                response.status = new Status
                {
                    code = ((int)HttpStatusCode.OK).ToString(),
                    message = sendResult ? "Success" : !string.IsNullOrEmpty(errorMessage) ? errorMessage : "Failed"
                };
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        private async Task<Stream> GenerateMemoAttachStream(byte[] coverpage, int transactionID)
        {
            try
            {
                List<byte[]> allPDF = new List<byte[]>();
                var memoDetail = _repository.memoTransaction.GetMemoTransactionDetail(transactionID);
                var memoDetailIDList = memoDetail.OrderBy(e => e.MemoType_Sequence).ThenBy(d => d.MemoCode_Sequence).Select(c => c.ID);

                allPDF.Add(coverpage);

                var allMemoCodeAttach = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => c.MemoTransaction_ID == transactionID);

                foreach (var detailID in memoDetailIDList)
                {
                    var memoCodeAttach = allMemoCodeAttach.Where(e => e.MemoTransactionDetail_ID == detailID);
                    foreach (var itemAttach in memoCodeAttach.OrderBy(c => c.Created_Date))
                    {
                        var fileFromDMC = await _repository.dmcFile.GetFileDMC(itemAttach.File_Path);
                        var byteFile = Convert.FromBase64String(fileFromDMC.base64);

                        allPDF.Add(byteFile);
                    }
                }

                var mergePDF = Utility.ConcatAndAddContent(allPDF);
                return new MemoryStream(mergePDF);
            }
            catch(Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                throw ex;
            }        
        }

    }
}
