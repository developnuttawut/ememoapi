﻿using DinkToPdf.Contracts;
using eMemo.Helper;
using eMemo.Model.Base;
using eMemo.Model.Request.PrintMemoLetter;
using eMemo.Model.Response.PrintMemoLetter;
using eMemo.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Processes.Processes
{
    public class PrintMemoLetterProcess
    {
        private readonly Repository _repository;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(PrintMemoLetterProcess));

        public PrintMemoLetterProcess(Repository repository, IConverter converter)
        {
            _repository = repository;
        }

        public async Task<GetPrintMemoLetterResponse> GetPrintMemoLetter(GetPrintMemoLetterRequest request)
        {
            GetPrintMemoLetterResponse response = new GetPrintMemoLetterResponse();

            try
            {
                var result = await _repository.memoTransaction.GetMemoTransactionBy(c => c.EMemoStatus != "New");
                //var result = await _repository.memoTransaction.GetMemoTransactionMockUp();

                var eboaStatusList = await _repository.memoTransaction.GetAllEBaoStatus();
                var deliveryStatusList = _repository.memoTransaction.GetAllDeliveryStatus();

                if (!string.IsNullOrEmpty(request.productCategory))
                {
                    result = result.Where(w => w.ProductCategory.ToUpper().EndsWith(request.productCategory.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.fromDocumentNo) || !string.IsNullOrEmpty(request.toDocumentNo))
                {
                    if (!string.IsNullOrEmpty(request.fromDocumentNo) && string.IsNullOrEmpty(request.toDocumentNo))
                    {
                        result = result.Where(w => w.DocumentNo.ToUpper().EndsWith(request.fromDocumentNo.ToUpper())).ToList();
                    }
                    else if (!string.IsNullOrEmpty(request.fromDocumentNo) && !string.IsNullOrEmpty(request.toDocumentNo))
                    {
                        int documentFrom = int.Parse(request.fromDocumentNo);
                        int documentTo = int.Parse(request.toDocumentNo);

                        result = result.Where(w => getIntValue(w.DocumentNo.Substring(w.DocumentNo.Length - 4, 4)) >= documentFrom && getIntValue(w.DocumentNo.Substring(w.DocumentNo.Length - 4, 4)) <= documentTo).ToList();
                    }
                }

                if (request.transactionDateTo != null)
                {
                    result = result.Where(w => w.TransactionDate.Date >= request.transactionDateForm.Date && w.TransactionDate.Date <= request.transactionDateTo.GetValueOrDefault().Date).ToList();
                }
                else
                {
                    result = result.Where(w => w.TransactionDate.Date == request.transactionDateForm.Date).ToList();
                }


                if (!string.IsNullOrEmpty(request.fromPolicyNo) || !string.IsNullOrEmpty(request.toPolicyNo))
                {
                    if (!string.IsNullOrEmpty(request.fromPolicyNo) && string.IsNullOrEmpty(request.toPolicyNo))
                    {
                        result = result.Where(w => w.PolicyNo.ToUpper().EndsWith(request.fromPolicyNo.ToUpper())).ToList();
                    }
                    else if (!string.IsNullOrEmpty(request.fromPolicyNo) && !string.IsNullOrEmpty(request.fromPolicyNo))
                    {
                        int policyFrom = int.Parse(request.fromPolicyNo);
                        int policyTo = int.Parse(request.toPolicyNo);

                        result = result.Where(w => getIntValue(w.PolicyNo) >= policyFrom && getIntValue(w.PolicyNo) <= policyTo).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(request.fromProposalNo) || !string.IsNullOrEmpty(request.toProposalNo))
                {
                    if (!string.IsNullOrEmpty(request.fromProposalNo) && string.IsNullOrEmpty(request.toProposalNo))
                    {
                        result = result.Where(w => w.ProposalNo.ToUpper().EndsWith(request.fromProposalNo.ToUpper())).ToList();
                    }
                    else if (!string.IsNullOrEmpty(request.fromProposalNo) && !string.IsNullOrEmpty(request.toProposalNo))
                    {
                        int proposalFrom = int.Parse(request.fromProposalNo);
                        int proposalTo = int.Parse(request.toProposalNo);

                        result = result.Where(w => getIntValue(w.ProposalNo) >= proposalFrom && getIntValue(w.ProposalNo) <= proposalTo).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(request.ebaoMemoStatusID))
                {
                    result = result.Where(w => w.EBaoMemoStatus_ID == request.ebaoMemoStatusID).ToList();
                }

                if (!string.IsNullOrEmpty(request.sending))
                {
                    result = result.Where(w => w.DeliveryStatus_ID == request.sending).ToList();
                }

                if (!string.IsNullOrEmpty(request.citizenID))
                {
                    result = result.Where(w => w.CitizenID.ToUpper().Equals(request.citizenID.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.partyID))
                {
                    result = result.Where(w => w.PartyID.ToUpper().Equals(request.partyID.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.customerName) || !string.IsNullOrEmpty(request.customerSurname))
                {
                    result = result.Where(w => w.PolicyName.ToUpper().Contains($"{request.customerName.ToUpper()}{request.customerSurname.ToUpper()}")).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentCode))
                {
                    result = result.Where(w => w.AgentCode.ToUpper().Contains(request.agentCode.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentName))
                {
                    result = result.Where(w => w.AgentName.ToUpper().Contains($"{request.agentName.ToUpper()}")).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentSurname))
                {
                    result = result.Where(w => w.AgentName.ToUpper().Contains($"{request.agentSurname.ToUpper()}")).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentUnit))
                {
                    result = result.Where(w => w.AgentUnit.ToUpper().Contains(request.agentUnit.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(request.agentGroup))
                {
                    result = result.Where(w => w.AgentGroup.ToUpper().Contains(request.agentGroup.ToUpper())).ToList();
                }

                if (result.Any())
                {
                    var transactionIDList = result.Select(c => c.ID);
                    var allMemoDetail = _repository.memoTransaction.GetMemoTransactionDetailBy(c => transactionIDList.Contains(c.MemoTransaction_ID));

                    var data = result.Select(s => new PrintMemoLetterData()
                    {
                        transactionID = s.ID,
                        documentNo = s.DocumentNo,
                        transactionDate = s.TransactionDate.ToString("dd/MM/yyyy"),
                        policyNo = s.PolicyNo,
                        proposalNo = s.ProposalNo,
                        policyName = s.PolicyName,
                        agentCode = s.AgentCode,
                        agentName = s.AgentName,
                        agentUnit = s.AgentUnit,
                        agentGroup = s.AgentGroup,
                        eBaoMemoStatusID = s.EBaoMemoStatus_ID,
                        ebaoMemoStatus = eboaStatusList.FirstOrDefault(c => c.ID == s.EBaoMemoStatus_ID)?.EBaoMemoStatus,
                        sending = deliveryStatusList.FirstOrDefault(c => c.DeliveryStatus == s.EMemoStatus)?.DeliveryStatus,
                    }).ToList();

                    response.data = data;
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.OK).ToString(),
                        message = "ค้นหาข้อมูลสำเร็จ"
                    };
                }
                else
                {
                    response.data = new List<PrintMemoLetterData>();
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetFileAttachmentResponse> PrintMemoLetterOnly(string documentNo)
        {
            GetFileAttachmentResponse response = new GetFileAttachmentResponse();

            try
            {
                List<byte[]> allPDF = new List<byte[]>();
                var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.DocumentNo == documentNo);
                var memoDetail = _repository.memoTransaction.GetMemoTransactionDetail(transactionData.ID);

                if (transactionData != null && memoDetail != null && memoDetail.Any())
                {
                    List<MemoDetailData> memoDetailData = memoDetail.Select(e => new MemoDetailData
                    {
                        Comment = e.Comment,
                        DocumentNo = e.DocumentNo,
                        LetterStatus = e.LetterStatus,
                        MemoCode = e.MemoCode,
                        MemoCodeDescription = e.MemoCodeDescription,
                        MemoCode_Sequence = e.MemoCode_Sequence,
                        MemoType = e.MemoType,
                        MemoType_Sequence = e.MemoType_Sequence,
                        Reply_By = e.Reply_By,
                        Reply_Date = e.Reply_Date,
                        Source = e.Source,
                        MemoTransaction_ID = e.MemoTransaction_ID
                    }).ToList();

                    var coverPage = _repository.sendMemo.GenerateCoverPage(transactionData, memoDetailData,transactionData.Note);

                    response.fileContent = coverPage;
                }
                else
                {
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }


            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<GetFileAttachmentResponse> Print(string documentNo)
        {
            GetFileAttachmentResponse response = new GetFileAttachmentResponse();

            try
            {
                List<byte[]> allPDF = new List<byte[]>();
                var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.DocumentNo == documentNo);
                var memoDetail = _repository.memoTransaction.GetMemoTransactionDetail(transactionData.ID);
                var memoDetailIDList = memoDetail.OrderBy(e => e.MemoType_Sequence).ThenBy(d => d.MemoCode_Sequence).Select(c => c.ID);

                if (transactionData != null && memoDetail != null && memoDetail.Any())
                {
                    List<MemoDetailData> memoDetailData = memoDetail.Select(e => new MemoDetailData
                    {
                        Comment = e.Comment,
                        DocumentNo = e.DocumentNo,
                        LetterStatus = e.LetterStatus,
                        MemoCode = e.MemoCode,
                        MemoCodeDescription = e.MemoCodeDescription,
                        MemoCode_Sequence = e.MemoCode_Sequence,
                        MemoType = e.MemoType,
                        MemoType_Sequence = e.MemoType_Sequence,
                        Reply_By = e.Reply_By,
                        Reply_Date = e.Reply_Date,
                        Source = e.Source,
                        MemoTransaction_ID = e.MemoTransaction_ID
                    }).ToList();

                    var coverPage = _repository.sendMemo.GenerateCoverPage(transactionData, memoDetailData, transactionData.Note);
                    allPDF.Add(coverPage);

                    var allMemoCodeAttach = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => c.MemoTransaction_ID == transactionData.ID);

                    foreach (var detailID in memoDetailIDList)
                    {
                        var memoCodeAttach = allMemoCodeAttach.Where(e => e.MemoTransactionDetail_ID == detailID);
                        foreach (var itemAttach in memoCodeAttach.OrderBy(c => c.Created_Date))
                        {
                            var fileFromDMC = await _repository.dmcFile.GetFileDMC(itemAttach.File_Path);
                            var byteFile = Convert.FromBase64String(fileFromDMC.base64);

                            allPDF.Add(byteFile);
                        }
                    }

                    var mergePDF = Utility.ConcatAndAddContent(allPDF);

                    response.fileContent = mergePDF;
                }
                else
                {
                    response.status = new Status()
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล"
                    };
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        private decimal getIntValue(string s)
        {
            //string[] arr = s.Split(" ".ToCharArray());
            return Convert.ToDecimal(s);
        }
    }
}
