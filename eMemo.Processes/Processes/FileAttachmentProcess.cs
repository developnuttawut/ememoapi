﻿using DinkToPdf;
using DinkToPdf.Contracts;
using eMemo.Helper;
using eMemo.Model.Base;
using eMemo.Model.Entity;
using eMemo.Repositories;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static eMemo.Helper.AppEnum;

namespace eMemo.Processes.Processes
{
    public class FileAttachmentProcess
    {
        private readonly Repository _repository;
        private AppSettingHelper appSettingHelper;
        private readonly string fileTypeText = "text/plaintext; charset=UTF-8";
        private readonly IConverter _converter;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(FileAttachmentProcess));
        public FileAttachmentProcess(Repository repository)
        {
            _repository = repository;
            appSettingHelper = new AppSettingHelper();
        }

        public async Task<GetFileAttachmentResponse> GetFileAttachmentContent(string fileType, string referenceID)
        {
            GetFileAttachmentResponse response = new GetFileAttachmentResponse();

            try
            {
                if (fileType == FileAttachmentType.MEMOCODE.ToString())
                {
                    var attachmentData = _repository.memoMapping.FindMemoCodeAttachmentBy(c => c.Reference_ID == referenceID);
                    if (attachmentData != null)
                    {
                        var fileFromDMC = await _repository.dmcFile.GetFileDMC(attachmentData.File_Path);

                        if (fileFromDMC != null && fileFromDMC.responseCode.resultCode == "Ok")
                        {
                            response.fileContent = Convert.FromBase64String(fileFromDMC.base64);
                            if (attachmentData.File_Type == "text/plain")
                            {
                                response.fileType = fileTypeText;
                            }
                            else
                            {
                                response.fileType = attachmentData.File_Type;
                            }

                            response.fileName = attachmentData.File_Name;
                        }
                        else
                        {
                            response.status = new Status
                            {
                                code = ((int)HttpStatusCode.NotImplemented).ToString(),
                                message = fileFromDMC?.responseCode?.description ?? "Get File From DMC Failed"
                            };
                        }
                    }
                    else
                    {
                        response.status = new Status
                        {
                            code = ((int)HttpStatusCode.NotFound).ToString(),
                            message = "ไม่พบข้อมูล MemoCode Attachment"
                        };
                    }

                }
                else if (fileType == FileAttachmentType.MEMOCODETXN.ToString())
                {
                    var attachmentData = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => c.ID.ToString() == referenceID).FirstOrDefault();
                    if (attachmentData != null)
                    {
                        var fileFromDMC = await _repository.dmcFile.GetFileDMC(attachmentData.File_Path);

                        if (fileFromDMC != null && fileFromDMC.responseCode.resultCode == "Ok")
                        {
                            response.fileContent = Convert.FromBase64String(fileFromDMC.base64);
                            if (attachmentData.File_Type == "text/plain")
                            {
                                response.fileType = fileTypeText;
                            }
                            else
                            {
                                response.fileType = attachmentData.File_Type;
                            }

                            response.fileName = attachmentData.File_Name;
                        }
                        else
                        {
                            response.status = new Status
                            {
                                code = ((int)HttpStatusCode.NotImplemented).ToString(),
                                message = fileFromDMC?.responseCode?.description ?? "Get File From DMC Failed"
                            };
                        }
                    }
                    else
                    {
                        response.status = new Status
                        {
                            code = ((int)HttpStatusCode.NotFound).ToString(),
                            message = "ไม่พบข้อมูล Memo Transaction Attachment"
                        };
                    }
                }
                else
                {
                    response.status = new Status
                    {
                        code = ((int)HttpStatusCode.UnprocessableEntity).ToString(),
                        message = "fileType ไม่เข้าเงื่อนไข"
                    };
                }
            }
            catch (Exception ex)
            {
                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };

                _log4net.Error(JsonConvert.SerializeObject(ex));
            }

            return response;
        }

        public async Task<GetFileAttachmentResponse> PreviewMemoAttachment(string documentNo)
        {
            GetFileAttachmentResponse response = new GetFileAttachmentResponse();

            try
            {
                List<byte[]> allPDF = new List<byte[]>();
                var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.DocumentNo == documentNo);

                if (transactionData != null)
                {
                    var memoDetail = _repository.memoTransaction.GetMemoTransactionDetail(transactionData.ID);

                    if (memoDetail != null)
                    {
                        var memoDetailPending = memoDetail.Where(c => string.IsNullOrWhiteSpace(c.LetterStatus));
                        var memoDetailIDList = memoDetailPending.OrderBy(e => e.MemoType_Sequence).ThenBy(d => d.MemoCode_Sequence).Select(c => c.ID);

                        List<MemoDetailData> memoDetailData = memoDetailPending.Select(e => new MemoDetailData
                        {
                            Comment = e.Comment,
                            DocumentNo = e.DocumentNo,
                            LetterStatus = e.LetterStatus,
                            MemoCode = e.MemoCode,
                            MemoCodeDescription = e.MemoCodeDescription,
                            MemoCode_Sequence = e.MemoCode_Sequence,
                            MemoType = e.MemoType,
                            MemoType_Sequence = e.MemoType_Sequence,
                            Reply_By = e.Reply_By,
                            Reply_Date = e.Reply_Date,
                            Source = e.Source,
                            MemoTransaction_ID = e.MemoTransaction_ID
                        }).ToList();

                        var coverPage = _repository.sendMemo.GenerateCoverPage(transactionData, memoDetailData, transactionData.Note);

                        if (coverPage != null)
                        {
                            allPDF.Add(coverPage);

                            var allMemoCodeAttach = _repository.memoTransaction.GetMemoTransactionAttachmentBy(c => memoDetailIDList.Contains(c.MemoTransactionDetail_ID));

                            foreach (var detailID in memoDetailIDList)
                            {
                                var memoCodeAttach = allMemoCodeAttach.Where(e => e.MemoTransactionDetail_ID == detailID);
                                foreach (var itemAttach in memoCodeAttach.OrderBy(c => c.Created_Date))
                                {
                                    var fileFromDMC = await _repository.dmcFile.GetFileDMC(itemAttach.File_Path);
                                    if (fileFromDMC != null && fileFromDMC.responseCode.resultCode == "Ok")
                                    {
                                        var byteFile = Convert.FromBase64String(fileFromDMC.base64);
                                        allPDF.Add(byteFile);
                                    }
                                    else
                                    {
                                        _log4net.Error(fileFromDMC?.responseCode?.description ?? "Get File From DMC Failed");
                                    }

                                }
                            }

                            var mergePDF = Utility.ConcatAndAddContent(allPDF);

                            response.fileContent = mergePDF;
                        }
                        else
                        {
                            response.status = new Status
                            {
                                code = ((int)HttpStatusCode.NotImplemented).ToString(),
                                message = "Generate Coverpage failed"
                            };
                        }
                    }
                    else
                    {
                        response.status = new Status
                        {
                            code = ((int)HttpStatusCode.NotImplemented).ToString(),
                            message = "Get memoDetail failed"
                        };
                    }
                }
                else
                {
                    response.status = new Status
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = $"ไม่พบข้อมูลจาก documentNo {documentNo}"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }

        public async Task<GetFileAttachmentResponse> PreviewMemoAttachmentHistory(string documentNo)
        {
            GetFileAttachmentResponse response = new GetFileAttachmentResponse();

            try
            {
                List<byte[]> allPDF = new List<byte[]>();
                var historyData = await _repository.memoTransaction.FindMemoDocumentHistoryBy(c => c.EMEMODocumentNo + "-" + c.SequenceEditDocumentNo == documentNo);

                if (historyData != null)
                {
                    var transactionData = await _repository.memoTransaction.FindMemoTransactionBy(c => c.ID == historyData.MemoTransaction_ID);
                    var memoDetail = await _repository.memoTransaction.GetMemoDocumentHistoryDetailBy(c => c.MemoDocumentHistory_ID == historyData.ID);
                    var memoDetailIDList = memoDetail.OrderBy(e => e.MemoType_Sequence).ThenBy(d => d.MemoCode_Sequence).Select(c => c.ID);

                    List<MemoDetailData> memoDetailData = memoDetail.Select(e => new MemoDetailData
                    {
                        Comment = e.Comment,
                        DocumentNo = e.DocumentNo,
                        LetterStatus = e.LetterStatus,
                        MemoCode = e.MemoCode,
                        MemoCodeDescription = e.MemoCodeDescription,
                        MemoCode_Sequence = e.MemoCode_Sequence,
                        MemoType = e.MemoType,
                        MemoType_Sequence = e.MemoType_Sequence,
                        Reply_By = e.Reply_By,
                        Reply_Date = e.Reply_Date,
                        Source = e.Source,
                        MemoTransaction_ID = e.MemoTransaction_ID
                    }).ToList();

                    var coverPage = _repository.sendMemo.GenerateCoverPage(transactionData, memoDetailData, historyData.Note, historyData.TransactionDate);

                    if (coverPage != null)
                    {
                        allPDF.Add(coverPage);

                        var allMemoCodeAttach = await _repository.memoTransaction.GetMemoDocumentHistoryAttachmentBy(c => c.MemoDocumentHistory_ID == historyData.ID);

                        foreach (var detailID in memoDetailIDList)
                        {
                            var memoCodeAttach = allMemoCodeAttach.Where(e => e.MemoDocumentHistoryDetail_ID == detailID);
                            foreach (var itemAttach in memoCodeAttach.OrderBy(c => c.Created_Date))
                            {
                                var fileFromDMC = await _repository.dmcFile.GetFileDMC(itemAttach.File_Path);
                                if (fileFromDMC != null && fileFromDMC.responseCode.resultCode == "Ok")
                                {
                                    var byteFile = Convert.FromBase64String(fileFromDMC.base64);

                                    allPDF.Add(byteFile);
                                }
                                else
                                {
                                    _log4net.Error(fileFromDMC?.responseCode?.description ?? "Get File From DMC Failed");
                                }
                            }
                        }

                        var mergePDF = Utility.ConcatAndAddContent(allPDF);
                        response.fileContent = mergePDF;
                    }
                    else
                    {
                        response.status = new Status
                        {
                            code = ((int)HttpStatusCode.NotImplemented).ToString(),
                            message = "Generate Coverpage failed"
                        };
                    }

                }
                else
                {
                    response.status = new Status
                    {
                        code = ((int)HttpStatusCode.NotFound).ToString(),
                        message = "ไม่พบข้อมูล Transaction History"
                    };
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));

                response.status = new Model.Base.Status
                {
                    code = ((int)HttpStatusCode.InternalServerError).ToString(),
                    message = ex.InnerException?.Message ?? ex.Message
                };
            }

            return response;
        }
    }
}
