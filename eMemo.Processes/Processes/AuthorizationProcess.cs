﻿using eMemo.Helper;
using eMemo.Model.ExternalResponse;
using eMemo.Model.Response.Authen;
using eMemo.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Processes.Processes
{
    public class AuthorizationProcess
    {
        private readonly Repository _repository;
        private AppSettingHelper appSettingHelper;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(AuthorizationProcess));
        public AuthorizationProcess(Repository repository)
        {
            _repository = repository;
            appSettingHelper = new AppSettingHelper();
        }

        public async Task<VerifyTokenResponse> Authorization(string tokenID)
        {
            VerifyTokenResponse response = new VerifyTokenResponse();

            try
            {
                var responseAuthen = await _repository.authen.Authentication(tokenID);

                if (responseAuthen != null && !string.IsNullOrWhiteSpace(responseAuthen.accessToken))
                {
                    response.accessToken = responseAuthen.accessToken;

                    if (responseAuthen.userItem != null)
                    {
                        var userItem = responseAuthen.userItem;
                        var userData = userItem?.Users?.FirstOrDefault();
                        var listMenu = userData?.ApplicationMenu?.bus?.FirstOrDefault()?.groups?.FirstOrDefault()?.roles?.FirstOrDefault()?.mainmenu;

                        response.userItem = new UserItem
                        {
                            memberId = userItem.memberId,
                            userId = userItem.userId,
                            username = userItem.username,
                            userRole = userItem.userRole,
                            Users = new Users
                            {
                                FirstName = userData.FirstName,
                                ApplicationId = userData.ApplicationId,
                                BUCode = userData.BUCode,
                                BUSystemCode = userData.BUSystemCode,
                                ApplicationModule = userData.ApplicationModule,
                                BUSystemName = userData.BUSystemName,
                                Email = userData.Email,
                                EmployeeCode = userData.EmployeeCode,
                                GroupId = userData.GroupId,
                                GroupName = userData.GroupName,
                                LastName = userData.LastName,
                                NameEN = userData.NameEN,
                                NameTH = userData.NameTH,
                                RoleId = userData.RoleId,
                                RoleName = userData.RoleName,
                                ApplicationMenu = listMenu.Select(c => new ApplicationMenu
                                {
                                    Icon = c.Icon,
                                    MainMenuId = c.MainMenuId,
                                    MainName = c.MainName,
                                    OrderIndex = c.OrderIndex,
                                    submenu = c.submenu.Select(s => new Model.Response.Authen.Submenu
                                    {
                                        Description = s.Description,
                                        LinkURL = s.LinkURL,
                                        MainMenuId = s.MainMenuId,
                                        MenuId = s.MenuId,
                                        Name = s.Name,
                                        OrderIndex = s.OrderIndex
                                    }).ToList()
                                }).ToList()
                            }
                        };
                    }

                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
            }

            return response;
        }

        public async Task<AuthenticationResponse> VerifyToken(string tokenID)
        {
            AuthenticationResponse response = new AuthenticationResponse();

            try
            {
                response = await _repository.authen.Authentication(tokenID);
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
            }

            return response;
        }
    }
}
