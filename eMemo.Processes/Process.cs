﻿using DinkToPdf.Contracts;
using eMemo.Helper;
using eMemo.Processes.Processes;
using eMemo.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Processes
{
    public class Process
    {
        public MemoMappingProcess memoMapping;
        public FileAttachmentProcess fileAttachment;
        public MemoTransactionProcess memoTransaction;
        public MemoInquiryProcess memoInquiry;
        public ReportProcess report;
        public PrintMemoLetterProcess printMemoLetter;
        public AuthorizationProcess authorization;
        public Process(IHttpContextAccessor httpContext, Repository repository, IConverter converter, SensitiveDataHelper sensitiveData)
        {
            memoMapping = new MemoMappingProcess(repository, sensitiveData);
            fileAttachment = new FileAttachmentProcess(repository);
            memoTransaction = new MemoTransactionProcess(repository, converter, httpContext);
            memoInquiry = new MemoInquiryProcess(repository, httpContext);
            report = new ReportProcess(repository);
            printMemoLetter = new PrintMemoLetterProcess(repository, converter);
            authorization = new AuthorizationProcess(repository);
        }
    }
}
