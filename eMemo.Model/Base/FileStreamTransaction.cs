﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace eMemo.Model.Base
{
    public class FileStreamTransaction
    {
        public int transactionID { get; set; }
        public Stream fileStream { get; set; }
    }
}
