﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Base
{
    public class BaseResponse
    {
        public Status status { get; set; }
    }

    public class Status
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
