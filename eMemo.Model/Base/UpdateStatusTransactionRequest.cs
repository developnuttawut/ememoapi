﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Base
{
    public class UpdateStatusTransactionRequest
    {
        public int transactionID { get; set; }
        public string ememoStatus { get; set; }
        public string deliveryStatusID { get; set; }
        public string updateBy { get; set; }
        public DateTime updateDate { get; set; }
    }
}
