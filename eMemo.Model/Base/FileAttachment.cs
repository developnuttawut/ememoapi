﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Base
{
    public class FileAttachment
    {
        public string fileName { get; set; }
        public decimal? fileSize { get; set; }
        public string fileType { get; set; }
        public string filePath { get; set; }
    }
}
