﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Base
{
    public class GetFileAttachmentResponse : BaseResponse
    {
        public byte[] fileContent { get; set; }
        public string fileType { get; set; }
        public string fileName { get; set; }
    }
}
