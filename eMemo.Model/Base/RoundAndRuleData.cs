﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Base
{
    public class RoundAndRuleData
    {
        public int transactionID { get; set; }
        public int? round { get; set; }
        public string SecondTier { get; set; }
    }
}
