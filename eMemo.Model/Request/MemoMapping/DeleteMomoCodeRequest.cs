﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.MemoMapping
{
    public class DeleteMomoCodeRequest
    {
        public int memoCodeID { get; set; }
        public string userID { get; set; }
    }
}
