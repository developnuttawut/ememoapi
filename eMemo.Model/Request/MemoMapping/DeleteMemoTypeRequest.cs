﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.MemoMapping
{
    public class DeleteMemoTypeRequest
    {
        public int memoTypeID { get; set; }
        public string userID { get; set; }
    }
}
