﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Request.MemoMapping
{
    public class SaveMemoTypeRequest
    {
        public int? memoTypeID { get; set; }
        [Required]
        public string memoTypeName { get; set; }
        [Required]
        public int memoTypeSequence { get; set; }
        [Required]
        public string userID { get; set; }
    }
}
