﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Request.MemoMapping
{
    public class SaveMemoCodeRequest
    {
        public int? memoCodeID { get; set; }
        //[Required]
        public int memoTypeID { get; set; }
        public string memoType { get; set; }
        //[Required]
        public string memoCode { get; set; }
        //[Required]
        public int memoCodeSequence { get; set; }
        //[Required]
        public string description { get; set; }
        //[Required]
        public bool status { get; set; }
        [Required]
        public string userID { get; set; }

        public List<IFormFile> attachments { get; set; }
    }
}
