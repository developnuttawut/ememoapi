﻿using System;
using System.Collections.Generic;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class UpdateMemoTransactionRequest
    {
        public int ID { get; set; }
        public string documentNo { get; set; }
        public DateTime? transactionDate { get; set; }
        public string policyNo { get; set; }
        public string mainPlan { get; set; }
        public string proposalNo { get; set; }
        public DateTime? proposalDate { get; set; }
        public string policyName { get; set; }
        public string citizenID { get; set; }
        public string partyID { get; set; }
        public string policyStatus { get; set; }
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentUnit { get; set; }
        public string agentGroup { get; set; }
        public string memoStatus { get; set; }
        public string pdfFile { get; set; }
        public int round { get; set; }
        public DateTime? estimateTime { get; set; }
        public string deliveryStatus { get; set; }
        public string userID { get; set; }

        public List<UpdateMemoDetail> detail { get; set; }
    }

    public class UpdateMemoDetail
    {
        public int ID { get; set; }
        public string memoType { get; set; }
        public string memoCode { get; set; }
        public string comment { get; set; }
        public string memoStatus { get; set; }
        public string memoFile { get; set; }
    }
}
