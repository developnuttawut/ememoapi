﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class UpdateDeliveryStatusRequest
    {
        public List<UpdateDeliveryStatusData> updateData { get; set; }
        public string userID { get; set; }
    }

    public class UpdateDeliveryStatusData
    {
        public int transactionID { get; set; }
        public string deliveryStatus { get; set; }
    }

}
