﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class UpdateTempRequest
    {
        public string documentNo { get; set; }
        public string memoStatus { get; set; }
        public string isTransfer { get; set; }
    }
}
