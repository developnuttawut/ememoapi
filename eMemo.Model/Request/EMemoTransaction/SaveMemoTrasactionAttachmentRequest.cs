﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class SaveMemoTrasactionAttachmentRequest
    {
        public int memoTransactionDetail_ID { get; set; }
        public string filePath { get; set; }
        public string fileType { get; set; }
        public string fileName { get; set; }
        public int fileSize { get; set; }
        public string createdBy { get; set; }
    }
}
