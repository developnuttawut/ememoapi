﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class AddMemoDocumentRequest
    {
        public int memoTransactionID { get; set; }
        public string memoType { get; set; }
        public string memoCode { get; set; }
        public string userID { get; set; }
    }
}
