﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class DeleteMemoDocumentRequest
    {
        public int memoTransactionID { get; set; }
        public string memoCode { get; set; }
        public string userID { get; set; }
    }
}
