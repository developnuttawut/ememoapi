﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class CreateMemoTransactionRequest
    {
        public string documentNo { get; set; }
        public DateTime transactionDate { get; set; }
        public string policyNo { get; set; }
        public string mainPlan { get; set; }
        public string proposalNo { get; set; }
        public DateTime proposalDate { get; set; }
        public string policyName { get; set; }
        public string citizenID { get; set; }
        public string partyID { get; set; }
        public string policyStatus { get; set; }
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentUnit { get; set; }
        public string agentGroup { get; set; }
        public string memoStatus { get; set; }
        public string eBaoMemoStatus { get; set; }
        public int round { get; set; }
        public DateTime estimateTime { get; set; }
        public string deliveryStatus { get; set; }
        public string lifeAssuredName { get; set; }
        public string entryAgeLifeAssured { get; set; }
        public string phAddress { get; set; }
        public string phPhoneNo { get; set; }
        public string phEmail { get; set; }
        public string laAddress { get; set; }
        public string laPhoneNo { get; set; }
        public string laEmail { get; set; }
        public string userID { get; set; }
        public List<MemoDetail> detail { get; set; }
    }

    public class MemoDetail
    {
        public string memoType { get; set; }
        public string memoCode { get; set; }
        public string memoDescription { get; set; }
        public string memoComment { get; set; }
        //public int? sequence { get; set; }
        public int memoTypeSequence { get; set; }
        public int memoCodeSequence { get; set; }
    }
}
