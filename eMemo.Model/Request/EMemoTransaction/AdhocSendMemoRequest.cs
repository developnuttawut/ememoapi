﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class AdhocSendMemoRequest
    {
        public List<int> transactionIDList { get; set; }
        public string userID { get; set; }
    }
}
