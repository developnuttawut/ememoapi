﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class SaveMemoDocumentRequest
    {
        public int memoTransactionID { get; set; }
        public string userID { get; set; }
        public List<MemoDocumentData> memoDocumentData { get; set; }
        public List<string> noteList { get; set; }
    }

    public class MemoDocumentData
    {
        public int? memoDetailID { get; set; }
        public string memoType { get; set; }
        public string memoCode { get; set; }
        public string comment { get; set; }
        public bool add { get; set; }
        public bool edit { get; set; }
        public bool delete { get; set; }
        public List<FileContent> attachments { get; set; }
    }

    public class FileContent
    {
        public string fileName { get; set; }
        public string fileBase64 { get; set; }
        public decimal fileSize { get; set; }
        public string contentType { get; set; }
    }
}
