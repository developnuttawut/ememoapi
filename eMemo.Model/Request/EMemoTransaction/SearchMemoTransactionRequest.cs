﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.EMemoTransaction
{
    public class SearchMemoTransactionRequest
    {
        public string documentNo { get; set; }
        public DateTime? transactionDateForm { get; set; }
        public DateTime? transactionDateTo { get; set; }
        public string policyNo { get; set; }
        public string proposalNo { get; set; }
        public string ebaoMemoStatusID { get; set; }
        public string deliveryStatus { get; set; }
        public int? deliveryRound { get; set; }
        public string citizenID { get; set; }
        public string partyID { get; set; }
        public string customerName { get; set; }
        public string customerSurname { get; set; }
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentSurname { get; set; }
        public string agentUnit { get; set; }
        public string gentGroup { get; set; }
    }
}
