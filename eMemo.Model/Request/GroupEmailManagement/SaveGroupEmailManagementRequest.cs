﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.GroupEmailManagement
{
    public class SaveGroupEmailManagementRequest
    {
        public int? ID { get; set; }
        public string emailType { get; set; }
        public string sendGroup { get; set; }
        public int? businessUnitID { get; set; }
        public string businessUnit { get; set; }
        public string branchOfficeID { get; set; }
        public string branchOffice { get; set; }
        public bool status { get; set; }
        public List<GroupEmailInformation> groupEmailList { get; set; }
        public string userID { get; set; }
    }

    public class GroupEmailInformation
    {
        public string to_cc { get; set; }
        public string emailAddress { get; set; }
    }
}
