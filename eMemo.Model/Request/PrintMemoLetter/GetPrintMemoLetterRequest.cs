﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.PrintMemoLetter
{
    public class GetPrintMemoLetterRequest
    {
        public DateTime transactionDateForm { get; set; }
        public DateTime? transactionDateTo { get; set; }
        public string productCategory { get; set; }
        public string fromPolicyNo { get; set; }
        public string toPolicyNo { get; set; }
        public string fromProposalNo { get; set; }
        public string toProposalNo { get; set; }
        public string fromDocumentNo { get; set; }
        public string toDocumentNo { get; set; }
        public string ebaoMemoStatusID { get; set; }
        public string sending { get; set; }
        public string citizenID { get; set; }
        public string partyID { get; set; }
        public string customerName { get; set; }
        public string customerSurname { get; set; }
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentSurname { get; set; }
        public string agentUnit { get; set; }
        public string agentGroup { get; set; }
    }
}
