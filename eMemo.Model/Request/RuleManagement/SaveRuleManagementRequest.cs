﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Request.MemoMapping
{
    public class SaveRuleManagementRequest
    {
        public int? ruleManagementID { get; set; }
        [Required]
        public string channel { get; set; }
        public int? subChannelID { get; set; }
        public string subChannel { get; set; }
        public string sobID { get; set; }
        public string sob { get; set; }
        public int? businessUnitID { get; set; }
        public string businessUnit { get; set; }
        [Required]
        public string firstTier { get; set; }
        public string secondTier { get; set; }
        public string firstTierChannel { get; set; }
        public string secondTierChannel { get; set; }
        public int? spaceWorkingDays { get; set; }
        public string sendEmailToSaleSupport { get; set; }
        public string sendEmailToSaleUnderwriting { get; set; }
        public bool status { get; set; }
        [Required]
        public string userID { get; set; }
        public List<string> productCategory { get; set; }
    }
}
