﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.MemoMapping
{
    public class DeleteRuleManagementRequest
    {
        public int ruleManagementID { get; set; }
        public string userID { get; set; }
    }
}
