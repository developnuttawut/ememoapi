﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.Report
{
    public class PendingSummaryReportRequest
    {
        public DateTime submissionDateFrom { get; set; }
        public DateTime? submissionDateTo { get; set; }
        public string productCategory { get; set; }
    }
}
