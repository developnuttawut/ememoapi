﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.Report
{
    public class Report15DayAfterPrintedRequest
    {
        public DateTime memoPrintedDateFrom { get; set; }
        public DateTime? memoPrintedDateTo { get; set; }
       // public bool after15Days { get; set; }
        public string condition { get; set; }
        public int printedDay { get; set; }
    }
}
