﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.Report
{
    public class PendingReportRequest
    {
        public DateTime asOfDate { get; set; }
        public string productCategory { get; set; }
    }
}
