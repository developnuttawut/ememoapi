﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.Report
{
    public class SendingMemoReportRequest
    {
        public DateTime memoPrintedDateFrom { get; set; }
        public DateTime? memoPrintedDateTo { get; set; }
        public string deliveryStatus { get; set; }
        public string resendNeed { get; set; }
        public bool showLatest { get; set; }
    }
}
