﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.MemoInquiry
{
    public class SendAfterSaveRequest
    {
        public int memoTransactionID { get; set; }
        public string sendTo { get; set; }
        public string userID { get; set; }
    }
}
