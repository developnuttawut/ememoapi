﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.MemoInquiry
{
    public class ReplyMemoItemRequest
    {
        public int memoTransactionID { get; set; }
        public string userID { get; set; }
        public List<MemoReplyData> memoReplyList { get; set; }
    }

    public class MemoReplyData
    {
        public int memoDetailID { get; set; }
        public string memoCode { get; set; }
        public DateTime? replyDate { get; set; }
        public string letterStatus { get; set; }
    }
}
