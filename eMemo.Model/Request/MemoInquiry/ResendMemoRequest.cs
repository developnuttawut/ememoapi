﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Request.MemoInquiry
{
    public class ResendMemoRequest
    {
        public int memoDocumentHistoryID { get; set; }
        public string via { get; set; }
        public string channel { get; set; }
        public string information { get; set; }
        public string userID { get; set; }
    }
}
