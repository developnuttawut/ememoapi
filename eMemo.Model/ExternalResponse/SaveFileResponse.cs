﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.ExternalResponse
{
    public class SaveFileResponse
    {
        public List<File> files { get; set; }
        public string application { get; set; }
        public string receiptNumber { get; set; }
        public OperationStatus operationStatus { get; set; }
    }

    public class File
    {
        public string fileName { get; set; }
        public DateTime? updateTime { get; set; }
        public string extension { get; set; }
        public string data { get; set; }
    }

    public class OperationStatus
    {
        public int statusCode { get; set; }
        public string description { get; set; }
    }
}
