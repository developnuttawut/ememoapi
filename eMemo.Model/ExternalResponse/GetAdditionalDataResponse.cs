﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.ExternalResponse
{
    public class GetAdditionalDataResponse
    {
        public string channel { get; set; }
        public string subChannel { get; set; }
        public string sourceOfBusiness { get; set; }
        public string businessUnit { get; set; }
        public string productCategory { get; set; }
        public string applicationBranch { get; set; }
        public string servicingBranch { get; set; }
        public string agentEmail { get; set; }
        public string agentPhoneNumber { get; set; }
        public string agentGroup { get; set; }
        public string agentUnit { get; set; }
        public string partyID { get; set; }
        public string customerEmail { get; set; }
        public string customerPhoneNumber { get; set; }
        public string customerAge { get; set; }
    }
}
