﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.ExternalResponse
{
    public class AuthenticationResponse
    {
        public MemberModel userItem { get; set; }
        public string accessToken { get; set; }

    }

    public class MemberModel
    {
        public int memberId { get; set; }
        public int userId { get; set; }
        public string username { get; set; }
        public string userRole { get; set; }
        public IEnumerable<UsersModel> Users { get; set; }
    }

    public class UsersModel
    {
        public int UserId { get; set; }
        public int ApplicationId { get; set; }
        public int MemberId { get; set; }
        public string EmployeeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string BUSystemCode { get; set; }
        public string BUSystemName { get; set; }
        public string BUCode { get; set; }
        public object ApplicationModule { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public ApplicationViewModel ApplicationMenu { get; set; }
    }

    public class Submenu
    {
        public string MenuId { get; set; }
        public string Name { get; set; }
        public string LinkURL { get; set; }
        public string MainMenuId { get; set; }
        public string Description { get; set; }
        public int OrderIndex { get; set; }
    }
    public class Mainmenu
    {
        public string MainMenuId { get; set; }
        public string MainName { get; set; }
        public string Icon { get; set; }
        public int OrderIndex { get; set; }
        public List<Submenu> submenu { get; set; }
    }
    public class Role
    {
        public string RoleName { get; set; }
        public List<Mainmenu> mainmenu { get; set; }
    }
    public class Group
    {
        public string GroupName { get; set; }
        public List<Role> roles { get; set; }
    }
    public class Bus
    {
        public string BUSystemCode { get; set; }
        public string Description { get; set; }
        public string BUCode { get; set; }
        public List<Group> groups { get; set; }
    }

    public class ApplicationViewModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int ApplicationId { get; set; }
        public int MemberId { get; set; }
        public string fullName { get; set; }
        public List<Bus> bus { get; set; }
    }
}
