﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.ExternalResponse
{
    public class GetFileResponse
    {
        public ResponseCode responseCode { get; set; }
        public string base64 { get; set; }
        public string fileName { get; set; }
        public string extension { get; set; }
        public string path { get; set; }
    }

    public class ResponseCode
    {
        public string resultCode { get; set; }
        public string description { get; set; }
    }
}
