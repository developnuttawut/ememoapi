﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.ExternalResponse
{
    public class GetHolidayResponse
    {
        public int SeqNumber { get; set; }
        public string Date { get; set; }
        public bool IsHoliday { get; set; }
        public string Description { get; set; }
    }
}
