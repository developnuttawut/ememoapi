﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.MemoMapping
{
    public class GetMemoCodeResponse : BaseResponse
    {
        public List<MemoCodeData> data { get; set; }
    }

    public class MemoCodeData
    {
        public int memoTypeID { get; set; }
        public int memoCodeID { get; set; }
        public string memoCode { get; set; }
        public string description { get; set; }
    }
}
