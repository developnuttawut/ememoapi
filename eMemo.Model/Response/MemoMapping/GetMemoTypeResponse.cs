﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.MemoMapping
{
    public class GetMemoTypeResponse : BaseResponse
    {
        public List<MemoTypeData> data { get; set; }
    }

    public class MemoTypeData
    {
        public int memoTypeID { get; set; }
        public string memoTypeName { get; set; }
        public int memoTypeSequence { get; set; }
    }
}
