﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.MemoMapping
{
    public class GetMemoMappingResponse : BaseResponse
    {
        public List<MemoMappingData> data { get; set; }
    }

    public class MemoMappingData
    {
        public int memoCodeID { get; set; }
        public string memoCode { get; set; }
        public int memoCodeSequence { get; set; }
        public int memoTypeID { get; set; }
        public string memoTypeName { get; set; }
        public int memoTypeSequence { get; set; }
        public string description { get; set; }
        public bool status { get; set; }
        public List<FileAttachment> fileAttachment { get; set; }
    }
}
