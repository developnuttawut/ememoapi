﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.Report
{
    public class PreviewReportResponse : BaseResponse
    {
        public string htmlPreview { get; set; }
    }
}
