﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.MemoInquiry
{
    public class GetMemoInquiryResponse : BaseResponse
    {
        public List<MemoInquiryData> data { get; set; }
    }

    public class MemoInquiryData
    {
        public int transactionID { get; set; }
        public string transactionDate { get; set; }
        public string documentNo { get; set; }
        public string policyNo { get; set; }
        public string proposalNo { get; set; }
        public string policyName { get; set; }
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentUnit { get; set; }
        public string agentGroup { get; set; }
        public string eBaoMemoStatusID { get; set; }
        public string ebaoMemoStatus { get; set; }
        public int osMemo { get; set; }
        public string sending { get; set; }
    }
}
