﻿using eMemo.Model.Base;
using eMemo.Model.Response.EMemoTransaction;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.MemoInquiry
{
    public class GetEmemoInquiryDetailResponse : BaseResponse
    {
        public EmemoInquiryDetailData data { get; set; }
    }

    public class EmemoInquiryDetailData
    {
        public int memoTransactionID { get; set; }
        public InquiryBasicInfomation basicInformation { get; set; }
        public InquiryMemoDocumentInfomation memoDocumentInformation { get; set; }
        public InquiryMemoAmendmentInformation memoAmendmentInformation { get; set; }
        public List<MemoDocumentHistory> memoDocumentHistory { get; set; }
        public List<string> noteList { get; set; }
    }
    public class InquiryBasicInfomation
    {
        public string policyNo { get; set; }
        public string proposalNo { get; set; }
        public string proposalDate { get; set; }
        public string policyStatus { get; set; }
        public string policyName { get; set; }
        public string citizenID { get; set; }
        public string mainPlan { get; set; }
        public string agentName { get; set; }
        public string unit { get; set; }
        public string group { get; set; }
        public string agentEmail { get; set; }
        public string agentPhoneNumber { get; set; }
        public string customerEmail { get; set; }
        public string customerPhoneNumber { get; set; }
        public string serviceAgentEmail { get; set; }
        public string serviceAgentPhoneNumber { get; set; }
    }

    public class InquiryMemoDocumentInfomation
    {
        public string ebaoDocumentNo { get; set; }
        public string transactionDate { get; set; }
        public string createdUser { get; set; }
        public string ebaoMemoStatus { get; set; }
        public string ebaoRelpliedUser { get; set; }
        public string ebaoRelpliedDate { get; set; }
    }

    public class InquiryMemoAmendmentInformation
    {
        public List<InquiryMemoDocument> documents { get; set; }
        public List<InquiryMemoHistory> history { get; set; }
    }

    public class InquiryMemoDocument
    {
        public int memoDetailID { get; set; }
        public int? sequence { get; set; }
        public string memoCode { get; set; }
        public string memoDescription { get; set; }
        public string comment { get; set; }
        public string letterStatus { get; set; }
        public DateTime? replyDate { get; set; }
        public string replyBy { get; set; }
        public string source { get; set; }
        public string createdDate { get; set; }
        public string modifyDate { get; set; }
        public List<FileAttachment> fileAttachments { get; set; }
    }

    public class InquiryMemoHistory
    {
        public string sendTo { get; set; }
        public string channel { get; set; }
        public string description { get; set; }
        public string sendDate { get; set; }
        public string sendTime { get; set; }
        public string sendBy { get; set; }
        public string sendStatus { get; set; }
    }
}
