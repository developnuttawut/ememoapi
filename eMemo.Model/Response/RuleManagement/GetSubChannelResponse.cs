﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetSubChannelResponse : BaseResponse
    {
        public List<SubChannelData> data { get; set; }
    }

    public class SubChannelData
    {
        public int subChannelID { get; set; }
        public string description { get; set; }
        public int channelID { get; set; }
    }
}
