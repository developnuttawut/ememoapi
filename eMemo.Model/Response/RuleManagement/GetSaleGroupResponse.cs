﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetSaleGroupResponse : BaseResponse
    {
        public List<SaleGroupData> data { get; set; }
    }

    public class SaleGroupData
    {
        public int ID { get; set; }
        public string saleGroupName { get; set; }
    }
}
