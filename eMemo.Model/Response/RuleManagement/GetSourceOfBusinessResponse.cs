﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetSourceOfBusinessResponse : BaseResponse
    {
        public List<SourceOfBusinessData> data { get; set; }
    }

    public class SourceOfBusinessData
    {
        public string sourceOfBusinesID { get; set; }
        public string description { get; set; }
    }
}
