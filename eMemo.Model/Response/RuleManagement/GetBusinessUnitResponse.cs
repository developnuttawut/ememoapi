﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetBusinessUnitResponse : BaseResponse
    {
        public List<BusinessUnitData> data { get; set; }
    }

    public class BusinessUnitData
    {
        public string businessUnitID { get; set; }
        public string description { get; set; }
    }
}
