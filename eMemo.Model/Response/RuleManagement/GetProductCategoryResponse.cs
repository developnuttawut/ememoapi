﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetProductCategoryResponse : BaseResponse
    {
        public List<ProductCategoryData> data { get; set; }
    }

    public class ProductCategoryData
    {
        public string productCategoryID { get; set; }
        public string description { get; set; }
    }
}
