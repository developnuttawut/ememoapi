﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetChannelResponse : BaseResponse
    {
        public List<ChannelData> data { get; set; }
    }

    public class ChannelData
    {
        public string channelID { get; set; }
        public string description { get; set; }
    }
}
