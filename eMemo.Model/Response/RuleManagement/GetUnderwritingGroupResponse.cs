﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetUnderwritingGroupResponse : BaseResponse
    {
        public List<UnderwritingGroupData> data { get; set; }
    }
    public class UnderwritingGroupData
    {
        public int ID { get; set; }
        public string underwritingGroupName { get; set; }
    }
}
