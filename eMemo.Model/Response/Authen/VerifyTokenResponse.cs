﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.Authen
{
    public class VerifyTokenResponse
    {
        public UserItem userItem { get; set; }
        public string accessToken { get; set; }
    }

    public class Submenu
    {
        public string MenuId { get; set; }
        public string Name { get; set; }
        public string LinkURL { get; set; }
        public string MainMenuId { get; set; }
        public string Description { get; set; }
        public int OrderIndex { get; set; }
    }

    public class ApplicationMenu
    {
        public string MainMenuId { get; set; }
        public string MainName { get; set; }
        public string Icon { get; set; }
        public int OrderIndex { get; set; }
        public List<Submenu> submenu { get; set; }
    }

    public class Users
    {
        public int ApplicationId { get; set; }
        public string EmployeeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public object ApplicationModule { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string BUSystemCode { get; set; }
        public string BUSystemName { get; set; }
        public string BUCode { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public List<ApplicationMenu> ApplicationMenu { get; set; }
    }

    public class UserItem
    {
        public int memberId { get; set; }
        public int userId { get; set; }
        public string username { get; set; }
        public string userRole { get; set; }
        public Users Users { get; set; }
    }
}
