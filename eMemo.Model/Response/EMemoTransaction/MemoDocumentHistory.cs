﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EMemoTransaction
{
    public class MemoDocumentHistory
    {
        public int memoDocumentHistoryID { get; set; }
        public string documentNo { get; set; }
        public string transactionDate { get; set; }
        public int osMemo { get; set; }
        public string printedBy { get; set; }
    }
}
