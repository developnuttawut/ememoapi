﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EMemoTransaction
{
    public class GetAgentUnitResponse : BaseResponse
    {
        public List<AgentUnitDataResult> data { get; set; }
    }

    public class AgentUnitDataResult
    {
        public string agentUnitCode { get; set; }
        public string agentUnitName { get; set; }
    }
}
