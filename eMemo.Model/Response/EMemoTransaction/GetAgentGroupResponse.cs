﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EMemoTransaction
{
    public class GetAgentGroupResponse : BaseResponse
    {
        public List<AgentGroupDataResult> data { get; set; }
    }

    public class AgentGroupDataResult
    {
        public string agentGroupCode { get; set; }
        public string agentGroupName { get; set; }
    }
}
