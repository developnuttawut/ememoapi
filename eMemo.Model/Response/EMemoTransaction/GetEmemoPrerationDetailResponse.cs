﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EMemoTransaction
{
    public class GetEmemoPrerationDetailResponse : BaseResponse
    {
        public EmemoPrerationDetailData data { get; set; }
    }
    public class EmemoPrerationDetailData
    {
        public int memoTransactionID { get; set; }
        public PreparationBasicInfomation basicInformation { get; set; }
        public PreparationMemoDocumentInfomation memoDocumentInformation { get; set; }
        public PreparationMemoAmendmentInformation memoAmendmentInformation { get; set; }
        public List<MemoDocumentHistory> memoDocumentHistory { get; set; }
        public List<string> noteList { get; set; }
    }
    public class PreparationBasicInfomation
    {
        public string policyNo { get; set; }
        public string proposalNo { get; set; }
        public string proposalDate { get; set; }
        public string policyStatus { get; set; }
        public string policyName { get; set; }
        public string citizenID { get; set; }
        public string mainPlan { get; set; }
        public string agentName { get; set; }
        public string unit { get; set; }
        public string group { get; set; }
    }

    public class PreparationMemoDocumentInfomation
    {
        public string ebaoDocumentNo { get; set; }
        public string transactionDate { get; set; }
        public string createdUser { get; set; }
        public string ebaoMemoStatus { get; set; }
        public string ebaoRelpliedUser { get; set; }
        public string ebaoRelpliedDate { get; set; }
    }

    public class PreparationMemoAmendmentInformation
    {
        public List<PreparationMemoDocument> documents { get; set; }
        public List<PreparationMemoHistory> history { get; set; }
    }

    public class PreparationMemoDocument
    {
        public int memoDetailID { get; set; }
        public int? sequence { get; set; }
        public string memoCode { get; set; }
        public string memoDescription { get; set; }
        public string comment { get; set; }
        public List<FileAttachment> fileAttachments { get; set; }
    }

    public class PreparationMemoHistory
    {
        public string sendTo { get; set; }
        public string channel { get; set; }
        public string description { get; set; }
        public string sendDate { get; set; }
        public string sendTime { get; set; }
        public string sendBy { get; set; }
        public string sendStatus { get; set; }
    }
}
