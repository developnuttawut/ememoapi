﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EMemoTransaction
{
    public class GetDeliveryStatusResponse : BaseResponse
    {
        public List<DeliveryStatusData> data { get; set; }
    }
    public class DeliveryStatusData
    {
        public string deliveryStatusID { get; set; }
        public string description { get; set; }
    }
}
