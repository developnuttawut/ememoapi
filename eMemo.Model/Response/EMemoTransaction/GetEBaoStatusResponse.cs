﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EMemoTransaction
{
    public class GetEBaoStatusResponse : BaseResponse
    {
        public List<EBaoStatusData> data { get; set; }
    }
    public class EBaoStatusData
    {
        public string eBaoStatusID { get; set; }
        public string description { get; set; }
    }
}
