﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EMemoTransaction
{
    public class SearchMemoTransactionResponse : BaseResponse
    {
        public List<MemoTransactionData> data { get; set; }
    }

    public class MemoTransactionData
    {
        public int transactionID { get; set; }
        public string documentNo { get; set; }
        public DateTime transactionDate { get; set; }
        public string policyNo { get; set; }
        public string mainPlan { get; set; }
        public string proposalNo { get; set; }
        public DateTime proposalDate { get; set; }
        public string policyName { get; set; }
        public string citizenID { get; set; }
        public string policyStatus { get; set; }
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentUnit { get; set; }
        public string agentGroup { get; set; }
        public string memoStatus { get; set; }
        public string eBaoMemoStatusID { get; set; }
        public string eBaoMemoStatus { get; set; }
        public string pdfFile { get; set; }
        public int round { get; set; }
        public DateTime? estimateTime { get; set; }
        public string deliveryStatusID { get; set; }
        public string deliveryStatus { get; set; }
    }
}
