﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.RuleManagement
{
    public class GetBranchOfficeResponse : BaseResponse
    {
        public List<BranchOfficeData> data { get; set; }
    }

    public class BranchOfficeData
    {
        public string branchOfficeID { get; set; }
        public string description { get; set; }
    }
}
