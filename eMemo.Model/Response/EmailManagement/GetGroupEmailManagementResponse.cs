﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response.EmailManagement
{
    public class GetGroupEmailManagementResponse : BaseResponse
    {
        public List<GroupEmailManagementData> data { get; set; }
    }

    public class GroupEmailManagementData
    {
        public int groupEmailManagementID { get; set; }
        public string emailType { get; set; }
        public int? businessUnitID { get; set; }
        public string businessUnit { get; set; }
        public string branchOfficeID { get; set; }
        public string branchOffice { get; set; }
        public bool status { get; set; }
        public string sendGroup { get; set; }
        public List<string> toEmail { get; set; }
        public List<string> ccEmail { get; set; }
    }
}
