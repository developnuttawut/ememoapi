﻿using eMemo.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.Response
{
    public class SaveDataResponse : BaseResponse
    {
        public bool success { get; set; }
    }
}
