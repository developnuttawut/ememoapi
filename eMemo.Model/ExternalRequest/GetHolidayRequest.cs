﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.ExternalRequest
{
    public class GetHolidayRequest
    {
        public string ApplicationCode { get; set; }
        public string FunctionCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
