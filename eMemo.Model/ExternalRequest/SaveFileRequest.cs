﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Model.ExternalRequest
{
    public class SaveFileRequest
    {
        public string application { get; set; }
        public List<FileDetail> files { get; set; }
    }

    public class FileDetail
    {
        public string filename { get; set; }
        public string extension { get; set; }
        public string data { get; set; }
    }
}
