﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TTDMemoTransactionDetail
    {
        [Key]
        public int ID { get; set; }
        public int MemoTransaction_ID { get; set; }
        public string DocumentNo { get; set; }
        public string MemoType { get; set; }
        public string MemoCode { get; set; }
        public string MemoCodeDescription { get; set; }
        public string Comment { get; set; }
        public string LetterStatus { get; set; }
        public int? MemoType_Sequence { get; set; }
        public int? MemoCode_Sequence { get; set; }
        public string Reply_By { get; set; }
        public DateTime? Reply_Date { get; set; }
        public string Source { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Updated_By { get; set; }
        public DateTime? Updated_Date { get; set; }
    }
}
