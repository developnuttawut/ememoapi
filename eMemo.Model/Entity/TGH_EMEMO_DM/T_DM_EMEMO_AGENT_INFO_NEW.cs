﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.TGH_EMEMO_DM
{
    public class T_DM_EMEMO_AGENT_INFO_NEW
    {
        [Key]
        public Guid ID { get; set; }
        public string AGENT_CODE { get; set; }
        public string TITLE { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string AGENT_TYPE_CODE { get; set; }
        public string AGENT_TYPE_NAME { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? APPLICATION_DATE { get; set; }
        public DateTime? START_DATE { get; set; }
        public string AGENT_STATUS { get; set; }
        public string POSITION_CODE { get; set; }
        public string POSITION_NAME { get; set; }
        public string UNIT_CODE { get; set; }
        public string UNIT_NAME { get; set; }
        public string GROUP_CODE { get; set; }
        public string GROUP_NAME { get; set; }
        public string CHANNEL_CODE { get; set; }
        public string CHANNEL_NAME { get; set; }
        public string SUB_CHANNEL_CODE { get; set; }
        public string SUB_CHANNEL_NAME { get; set; }
        public string BRANCH_CODE { get; set; }
        public string BRANCH_NAME { get; set; }
        public string LICENSE_NO { get; set; }
        public DateTime? LICENSE_START_DATE { get; set; }
        public DateTime? LICENSE_EXPIRY_DATE { get; set; }
        public string MOBILE { get; set; }
        public string EMAIL { get; set; }
        public string CARD_ID { get; set; }
        public string CARD_TYPE_CODE { get; set; }
        public string CARD_TYPE_NAME { get; set; }
        public DateTime? RESIGN_OFFICIAL_DATE { get; set; }
        public string BUSINESS_UNIT_CODE { get; set; }
        public string BUSINESS_UNIT_NAME { get; set; }
        public string SOURCE_OF_BUSINESS_CODE { get; set; }
        public string SOURCE_OF_BUSINESS_NAME { get; set; }
        public string PRODUCT_CATEGORY_CODE { get; set; }
        public string PRODUCT_CATEGORY_NAME { get; set; }
        public string APPLICATION_BRANCH_CODE { get; set; }
        public string APPLICATION_BRANCH_NAME { get; set; }
        public string POLICY_CODE { get; set; }
        public string BRANCH_CO_CODE { get; set; }
    }
}
