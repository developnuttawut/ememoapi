﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.TGH_EMEMO_DM
{
    public class T_DM_EMEMO_AGENT_INFO
    {
        [Key]
        public string AGENT_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string SUB_CHANNEL { get; set; }
        public string AGENT_EMAIL { get; set; }
        public string AGENT_PHONE_NUMBER { get; set; }
        public string AGENT_GROUP { get; set; }
        public string AGENT_UNIT { get; set; }
    }
}
