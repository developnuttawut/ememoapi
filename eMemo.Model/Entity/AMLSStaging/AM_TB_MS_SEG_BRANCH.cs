﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.AMLSStaging
{
    public class AM_TB_MS_SEG_BRANCH
    {
        [Key]
        public decimal SEG_BRANCH_ID { get; set; }
        public string SEG_BRANCH_NAME { get; set; }
        public string SEG_BRANCH_CODE { get; set; }
        public string SEG_BRANCH_CO_CODE { get; set; }
    }
}
