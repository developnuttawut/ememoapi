﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.AMLSStaging
{
    public class AM_TB_MS_SUB_CHANNEL_BK
    {
        [Key]
        public decimal SUB_CHANNEL_ID { get; set; }
        public string SUB_CHANNEL_CODE { get; set; }
        public decimal? CHANNEL_ID { get; set; }
        public string SUB_CHANNEL_NAME { get; set; }
        public string STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public decimal? IS_DELETED { get; set; }
        public string UPDATED_BY { get; set; }
        public DateTime? UPDATED_DATE { get; set; }
    }
}
