﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.AMLSStaging
{
    public class AM_TB_MS_CHANNEL
    {
        [Key]
        public decimal CHANNEL_ID { get; set; }
        public string CHANNEL_BIZ_MEMO { get; set; }
        public string CHANNEL_CODE { get; set; }
        public DateTime? CHANNEL_EFFECTIVE_DATE { get; set; }
        public DateTime? CHANNEL_EXPIRY_DATE { get; set; }
        public string CHANNEL_NAME { get; set; }
        public string CHANNEL_STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public decimal? IS_DELETED { get; set; }
        public string UPDATED_BY { get; set; }
        public DateTime? UPDATED_DATE { get; set; }
    }
}
