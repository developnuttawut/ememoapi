﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMRuleManagementProductCategory
    {
        [Key]
        public int ID { get; set; }
        public int RuleManagement_ID { get; set; }
        public string ProductCategory { get; set; }
        public string Created_By { get; set; }
        public DateTime? Created_Date { get; set; }
    }
}
