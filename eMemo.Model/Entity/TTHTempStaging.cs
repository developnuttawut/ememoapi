﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TTHTempStaging
    {
        public int SequenceID { get; set; }
        [Key]
        public string DocumentNo { get; set; }
        public string PolicyNo { get; set; }
        public string MainPlan { get; set; }
        public string ProposalNo { get; set; }
        public DateTime ProposalDate { get; set; }
        public string PolicyName { get; set; }
        public string CitizenID { get; set; }
        public string PolicyStatus { get; set; }
        public string AgentNo { get; set; }
        public string AgentName { get; set; }
        public string IsTransfer { get; set; }
        public string MemoStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? RepliedDiscardDate { get; set; }
        public string RepliedUser { get; set; }
        public string SyncFlag { get; set; }
        public string LifeAssuredName { get; set; }
        public string EntryAgeLifeAssured { get; set; }
        public string PHAddress { get; set; }
        public string PHPhoneNo { get; set; }
        public string PHEmail { get; set; }
        public string LAAddress { get; set; }
        public string LAPhoneNo { get; set; }
        public string LAEmail { get; set; }
        public string PartyID { get; set; }
    }
}
