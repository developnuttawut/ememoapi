﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMMemoCodeAttachment
    {
        [Key]
        public int ID { get; set; }
        public int MemoCode_ID { get; set; }
        public string Reference_ID { get; set; }
        public string File_Path { get; set; }
        public string File_Type { get; set; }
        public int File_Size { get; set; }
        public string File_Name { get; set; }
        public string File_Extension { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
    }
}
