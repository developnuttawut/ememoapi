﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class PolicyStatus
    {
        [Key]
        public string policy_code { get; set; }
        public string status_desc { get; set; }
    }
}
