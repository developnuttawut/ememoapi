﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_DOCUMENT_JOIN
    {
        [Key]
        public string document_no { get; set; }
        public string template_name { get; set; }
    }
}
