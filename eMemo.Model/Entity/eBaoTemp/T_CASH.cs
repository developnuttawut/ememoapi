﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_CASH
    {
        [Key]
        public Guid ID { get; set; }
        public decimal? amount_recieved { get; set; }
    }
}
