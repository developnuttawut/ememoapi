﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_UW_EXCLUSION
    {
        [Key]
        public Guid ID { get; set; }
        public int policy_id { get; set; }
        public string exclusion_code { get; set; }
    }
}
