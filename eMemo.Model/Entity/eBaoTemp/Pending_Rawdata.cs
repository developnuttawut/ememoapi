﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class Pending_Rawdata
    {
        [Key]
        public int POLICY_ID { get; set; }
        public string POLICY_CODE { get; set; }
        public DateTime? SUBMISSION_DATE { get; set; }
        public DateTime? ISSUE_DATE { get; set; }
        public int? LIABILITY_STATE { get; set; }
        public string STATUS_DESC { get; set; }
        public int? END_CAUSE { get; set; }
        public string PROPOSAL_STATUS { get; set; }
        public string PEND_CAUSE { get; set; }
        public string RENEWAL_TYPE { get; set; }
        public int? pending_id { get; set; }
        public string unit_desc { get; set; }
        public string productCategory { get; set; }
        public string user_name { get; set; }
        public decimal? total_prem_af { get; set; }
        public int? Institute_code { get; set; }
        public decimal? APE { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string internal_id { get; set; }
        public string product_name { get; set; }
        public string exclusion_code { get; set; }
        public string stand_life { get; set; }
        public decimal? amount_recieved { get; set; }
        public DateTime? lca_printeddate { get; set; }
    }
}
