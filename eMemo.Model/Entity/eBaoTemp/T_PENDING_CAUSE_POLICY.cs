﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_PENDING_CAUSE_POLICY
    {
        [Key]
        public int policy_id { get; set; }
        public string policy_code { get; set; }
        public int? uw_pending { get; set; }
        public int? pending_id { get; set; }
        public string pend_cause { get; set; }
    }
}
