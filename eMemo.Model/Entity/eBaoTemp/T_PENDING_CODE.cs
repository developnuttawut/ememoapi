﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_PENDING_CODE
    {
        [Key]
        public int PENDING_ID { get; set; }
        public string PEND_CAUSE { get; set; }
    }
}
