﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class Institute_code
    {
        [Key]
        public int policy_id { get; set; }
        public string policy_code { get; set; }
    }
}
