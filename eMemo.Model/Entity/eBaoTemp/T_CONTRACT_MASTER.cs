﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_CONTRACT_MASTER
    {
        [Key]
        public int POLICY_ID { get; set; }
        public string POLICY_CODE { get; set; }
        public DateTime? SUBMISSION_DATE { get; set; }
        public DateTime? ISSUE_DATE { get; set; }
        public int? LIABILITY_STATE { get; set; }
        public int? END_CAUSE { get; set; }
    }
}
