﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class DataNotInMemo
    {
        [Key]
        public string POLICY_CODE { get; set; }
        public DateTime insert_time { get; set; }
        public string ProposerName { get; set; }
        public string LifeAssuredName { get; set; }
        public int? LifeAssuredAge { get; set; }
        public string document_no { get; set; }
        public string user_name { get; set; }
        public DateTime? update_time { get; set; }
    }
}
