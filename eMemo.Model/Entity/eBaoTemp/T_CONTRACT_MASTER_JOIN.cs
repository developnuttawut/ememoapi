﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_CONTRACT_MASTER_JOIN
    {
        [Key]
        public string policy_code { get; set; }
        public string internal_id { get; set; }
        public string product_name { get; set; }
        public string ins_type { get; set; }
        public string renewal_type { get; set; }
    }
}
