﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_MEMO_LETTER
    {
        [Key]
        public int LIST_ID { get; set; }
        public string DOCUMENT_NO { get; set; }
        public string POLICY_CODE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PROPOSAL_CODE { get; set; }
        public DateTime? PROPOSAL_DATE { get; set; }
        public string POLICY_NAME { get; set; }
        public string CITIZEN_ID { get; set; }
        public string POLICY_STATUS { get; set; }
        public string AGENT_CODE { get; set; }
        public string AGENT_NAME { get; set; }
        public string IS_TRANSFER { get; set; }
        public string MEMO_STATUS { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string CREATE_USER { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        public string UPDATE_USER { get; set; }
        public DateTime? STATUS_DATE { get; set; }
        public string REPLIED_USER { get; set; }
        public string SYNC_FLAG { get; set; }
        public string LA_CERTI_CODE { get; set; }
        public string LA_NAME { get; set; }
        public short LA_AGE { get; set; }
        public string LA_ADDRESS { get; set; }
        public string LA_PHONE { get; set; }
        public string LA_EMAIL { get; set; }
        public string PH_ADDRESS { get; set; }
        public string PH_PHONE { get; set; }
        public string PH_EMAIL { get; set; }
    }
}
