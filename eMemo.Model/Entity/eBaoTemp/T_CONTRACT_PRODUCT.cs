﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_CONTRACT_PRODUCT
    {
        [Key]
        public int POLICY_ID { get; set; }
        public string ISSUE_AGENT { get; set; }
        public decimal? TOTAL_PREM_AF { get; set; }
    }
}
