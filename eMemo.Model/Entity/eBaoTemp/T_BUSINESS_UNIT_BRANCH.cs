﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_BUSINESS_UNIT_BRANCH
    {
        public string BUSINESS_UNIT { get; set; }
        public string APPLICATION_BRANCH { get; set; }
        [Key]
        public string BRANCH_CODE { get; set; }
        public string BRANCH_NAME { get; set; }
    }
}
