﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_INSURED_LIST
    {
        [Key]
        public int list_id { get; set; }
        public int policy_id { get; set; }
        public string stand_life { get; set; }
    }
}
