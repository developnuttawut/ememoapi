﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_MEMO_LETTER_DETAIL
    {
        [Key]
        public Guid ID { get; set; }
        public string DOCUMENT_NO { get; set; }
        public string MEMO_ITEM_TYPE { get; set; }
        public string MEMO_CODE { get; set; }
        public string MEMO_DESCRIPTION { get; set; }
        public string COMMENTS { get; set; }
    }
}
