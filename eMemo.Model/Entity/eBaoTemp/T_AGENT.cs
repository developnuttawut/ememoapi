﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_AGENT
    {
        [Key]
        public string agent_code { get; set; }
        public string med_indi { get; set; }
    }
}
