﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_MASTER_POLICY
    {
        [Key]
        public string policy_code { get; set; }
        public string master_policy_no { get; set; }
    }
}
