﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_CONTRACT_PROPOSAL
    {
        [Key]
        public int POLICY_ID { get; set; }
        public string PROSPECT_PRODUCER { get; set; }
        public string PROPOSAL_STATUS { get; set; }
        public int? PENDING_CAUSE { get; set; }
        public DateTime? update_time { get; set; }
    }
}
