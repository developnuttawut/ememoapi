﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoTemp
{
    public class T_DOCUMENT
    {
        [Key]
        public string document_no { get; set; }
        public DateTime? insert_time { get; set; }
        public DateTime? end_time { get; set; }
    }
}
