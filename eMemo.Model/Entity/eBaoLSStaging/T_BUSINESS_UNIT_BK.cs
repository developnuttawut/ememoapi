﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoLSStaging
{
    public class T_BUSINESS_UNIT_BK
    {
        [Key]
        public string BUSINESS_UNIT { get; set; }
        public string UNIT_DESC { get; set; }
        public decimal? PARTY_ID { get; set; }
        public decimal? STATUS { get; set; }
    }
}
