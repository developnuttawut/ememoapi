﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity.eBaoLSStaging
{
    public class T_BUSINESS_SOURCE
    {
        [Key]
        public string BUSINESS_SOURCE { get; set; }
        public string SOURCE_DESC { get; set; }
    }
}
