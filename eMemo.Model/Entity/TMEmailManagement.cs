﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMEmailManagement
    {
        [Key]
        public int ID { get; set; }
        public int GroupEmailManagement_ID { get; set; }
        public string TO_CC { get; set; }
        public string EmailAddress { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }

    }
}
