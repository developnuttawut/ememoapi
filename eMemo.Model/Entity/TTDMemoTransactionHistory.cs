﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TTDMemoTransactionHistory
    {
        [Key]
        public int ID { get; set; }
        public int MemoTransaction_ID { get; set; }
        public string SendTo { get; set; }
        public string Channel { get; set; }
        public string Description { get; set; }
        public string Send_Status { get; set; }
        public string Send_By { get; set; }
        public DateTime Send_DateTime { get; set; }
        public string AutoMemoSending { get; set; }
        public string ResendNeeded { get; set; }
    }
}
