﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMGroupEmailManagement
    {
        [Key]
        public int ID { get; set; }
        public string EmailType { get; set; }
        public int? BusinessUnit_ID { get; set; }
        public string BusinessUnit { get; set; }
        public string BranchOffice_ID { get; set; }
        public string BranchOffice { get; set; }
        public bool Status { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Updated_By { get; set; }
        public DateTime? Updated_Date { get; set; }
        public string SendGroup { get; set; }

    }
}
