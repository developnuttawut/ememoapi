﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TTHMemoDocumentHistory
    {
        [Key]
        public int ID { get; set; }
        public int MemoTransaction_ID { get; set; }
        public string EMEMODocumentNo { get; set; }
        public int SequenceEditDocumentNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Note { get; set; }
    }
}
