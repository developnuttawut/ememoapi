﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMDeliveryStatus
    {
        [Key]
        public string ID { get; set; }
        public string DeliveryStatus { get; set; }
        public string Updated_By { get; set; }
        public DateTime? Updated_Date { get; set; }
    }
}
