﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMUnderwritingGroup
    {
        [Key]
        public int ID { get; set; }
        public string UnderwritingGroup_Name { get; set; }
        public bool Flag_Active { get; set; }
    }
}
