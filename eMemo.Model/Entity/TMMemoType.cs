﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMMemoType
    {
        [Key]
        public int ID { get; set; }
        public string MemoType_Name { get; set; }
        public int Sequence { get; set; }
        public bool Status { get; set; }
        public bool Flag_Delete { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Updated_By { get; set; }
        public DateTime? Updated_Date { get; set; }
    }
}
