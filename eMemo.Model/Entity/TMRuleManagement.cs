﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TMRuleManagement
    {
        [Key]
        public int ID { get; set; }
        public string Channel { get; set; }
        public int? SubChannel_ID { get; set; }
        public string SubChannel { get; set; }
        public string SOB_ID { get; set; }
        public string SOB { get; set; }
        public int? BusinessUnit_ID { get; set; }
        public string BusinessUnit { get; set; }
        public string FirstTier { get; set; }
        public string SecondTier { get; set; }
        public string FirstTierChannel { get; set; }
        public string SecondTierChannel { get; set; }
        public int? SpaceWorkingDays { get; set; }
        public string SendEmailToSaleSupport { get; set; }
        public string SendEmailToSaleUnderwriting { get; set; }
        public bool Status { get; set; }
        public bool Flag_Delete { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Updated_By { get; set; }
        public DateTime? Updated_Date { get; set; }

    }
}
