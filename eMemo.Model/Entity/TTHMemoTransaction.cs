﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TTHMemoTransaction
    {
        [Key]
        public int ID { get; set; }
        public string DocumentNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public string ProposalNo { get; set; }
        public DateTime ProposalDate { get; set; }
        public string PolicyNo { get; set; }
        public string CitizenID { get; set; }
        public string PartyID { get; set; }
        public string PolicyName { get; set; }
        public string PolicyStatus { get; set; }
        public string MainPlan { get; set; }
        public string Channel { get; set; }
        public string SubChannel { get; set; }
        public string SourceOfBusiness { get; set; }
        public string BusinessUnit { get; set; }
        public string ProductCategory { get; set; }
        public string BranchCode { get; set; }
        public string ApplicationBranch { get; set; }
        public string ServicingBranch { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AgentUnitCode { get; set; }
        public string AgentUnit { get; set; }
        public string AgentGroupCode { get; set; }
        public string ApplicationBranchCode { get; set; }
        public string AgentGroup { get; set; }
        public string AgentEmail { get; set; }
        public string AgentPhoneNumber { get; set; }
        public string EMemoStatus { get; set; }
        public string EBaoMemoStatus_ID { get; set; }
        public string EBaoRepliedUser { get; set; }
        public DateTime? EBaoRepliedDate { get; set; }
        public int? Round { get; set; }
        public DateTime? DatetimeSendSecondTier { get; set; }
        public string SecondTier { get; set; }
        public string SecondTierChannel { get; set; }
        public DateTime? EstimateTime { get; set; }
        public string DeliveryStatus_ID { get; set; }
        public string LifeAssuredName { get; set; }
        public string EntryAgeLifeAssured { get; set; }
        public string PHAddress { get; set; }
        public string PHPhoneNo { get; set; }
        public string PHEmail { get; set; }
        public string LAAddress { get; set; }
        public string LAPhoneNo { get; set; }
        public string LAEmail { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Updated_By { get; set; }
        public DateTime? Updated_Date { get; set; }
        public string BranchCOCode { get; set; }
        public string Note { get; set; }
    }
}
