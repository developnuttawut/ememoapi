﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace eMemo.Model.Entity
{
    public class TTDTempStagingDetail
    {
        [Key, Column(Order = 0)]
        public string DocumentNo { get; set; }
        public string MemoItemType { get; set; }
        public string MemoCode { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
    }
}
