﻿using eMemo.Model.Entity.eBaoLSStaging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Repositories.DBContext
{
    public class eBaoLSStagingDBContext : DbContext
    {
        public eBaoLSStagingDBContext(DbContextOptions<eBaoLSStagingDBContext> options) : base(options)
        {

        }

        public DbSet<T_BUSINESS_UNIT> T_BUSINESS_UNIT { get; set; }
        public DbSet<T_BUSINESS_SOURCE> T_BUSINESS_SOURCE { get; set; }
        public DbSet<T_BUSINESS_UNIT_BRANCH> T_BUSINESS_UNIT_BRANCH { get; set; }
        public DbSet<T_BUSINESS_UNIT_BK> T_BUSINESS_UNIT_BK { get; set; }
        public DbSet<T_BUSINESS_SOURCE_BK> T_BUSINESS_SOURCE_BK { get; set; }
        public DbSet<T_BUSINESS_UNIT_BRANCH_BK> T_BUSINESS_UNIT_BRANCH_BK { get; set; }
    }
}
