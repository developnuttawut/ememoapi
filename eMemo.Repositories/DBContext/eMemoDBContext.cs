﻿using eMemo.Model.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Repositories.DBContext
{
    public class eMemoDBContext : DbContext
    {
        public eMemoDBContext(DbContextOptions<eMemoDBContext> options) : base(options)
        {
        }

        public DbSet<TMMemoCode> TMMemoCode { get; set; }
        public DbSet<TMMemoCodeAttachment> TMMemoCodeAttachment { get; set; }
        public DbSet<TMMemoType> TMMemoType { get; set; }
        public DbSet<TMRuleManagement> TMRuleManagement { get; set; }
        public DbSet<TMRuleManagementProductCategory> TMRuleManagementProductCategory { get; set; }
        public DbSet<TMGroupEmailManagement> TMGroupEmailManagement { get; set; }
        public DbSet<TMEmailManagement> TMEmailManagement { get; set; }
        public DbSet<TMProductCategory> TMProductCategory { get; set; }
        public DbSet<TTHMemoTransaction> TTHMemoTransaction { get; set; }
        public DbSet<TTDMemoTransactionDetail> TTDMemoTransactionDetail { get; set; }
        public DbSet<TTDMemoTransactionAttachment> TTDMemoTransactionAttachment { get; set; }
        public DbSet<TTDMemoTransactionHistory> TTDMemoTransactionHistory { get; set; }
        public DbSet<TMEBaoMemoStatus> TMEBaoMemoStatus { get; set; }
        public DbSet<TMDeliveryStatus> TMDeliveryStatus { get; set; }
        public DbSet<TTHMemoDocumentHistory> TTHMemoDocumentHistory { get; set; }
        public DbSet<TTDMemoDocumentHistoryAttachment> TTDMemoDocumentHistoryAttachment { get; set; }
        public DbSet<TTDMemoDocumentHistoryDetail> TTDMemoDocumentHistoryDetail { get; set; }
        public DbSet<Transaction_View> Transaction_View { get; set; }
        public DbSet<Transaction_ViewHistory> Transaction_ViewHistory { get; set; }
        public DbSet<TMUnderwritingGroup> TMUnderwritingGroup { get; set; }
        public DbSet<TMSaleGroup> TMSaleGroup { get; set; }

    }
}
