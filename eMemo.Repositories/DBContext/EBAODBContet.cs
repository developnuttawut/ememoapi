﻿using eMemo.Model.Entity.eBaoTemp;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Repositories.DBContext
{
    public class EBAODBContet : DbContext
    {
        public EBAODBContet(DbContextOptions<EBAODBContet> options) : base(options)
        {

        }

        public DbSet<T_MEMO_LETTER> t_memo_letter { get; set; }
        public DbSet<T_MEMO_LETTER_DETAIL> t_memo_letter_detail { get; set; }
        public DbSet<T_CONTRACT_MASTER> t_contract_master { get; set; }
        public DbSet<T_CONTRACT_PRODUCT> t_contract_product { get; set; }
        public DbSet<T_CONTRACT_PROPOSAL> t_contract_proposal { get; set; }
        public DbSet<T_FMS_RATETABLE> T_FMS_RATETABLE { get; set; }
        public DbSet<API> API { get; set; }
        public DbSet<T_PENDING_CODE> T_PENDING_CODE { get; set; }
        public DbSet<T_DOCUMENT_JOIN> T_DOCUMENT_JOIN { get; set; }
        public DbSet<T_AGENT> T_AGENT { get; set; }
        public DbSet<T_CONTRACT_MASTER_JOIN> T_CONTRACT_MASTER_JOIN { get; set; }
        public DbSet<T_DOCUMENT> T_DOCUMENT { get; set; }
        public DbSet<T_INSURED_LIST> T_INSURED_LIST { get; set; }
        public DbSet<T_UW_EXCLUSION> T_UW_EXCLUSION { get; set; }
        public DbSet<T_CASH> T_CASH { get; set; }
        public DbSet<T_UW_POLICY> T_UW_POLICY { get; set; }
        public DbSet<T_BUSINESS_UNIT_BRANCH> T_BUSINESS_UNIT_BRANCH { get; set; }
        public DbSet<T_PENDING_CAUSE_POLICY> T_PENDING_CAUSE_POLICY { get; set; }
        public DbSet<T_MASTER_POLICY> T_MASTER_POLICY { get; set; }
        public DbSet<Pending_Summary_Rawdata> Pending_Summary_Rawdata { get; set; }
        public DbSet<Institute_code> Institute_code { get; set; }
        public DbSet<Pending_Rawdata> Pending_Rawdata { get; set; }
        public DbSet<PolicyStatus> PolicyStatus { get; set; }
        public DbSet<DataNotInMemo> DataNotInMemo { get; set; }

    }
}
