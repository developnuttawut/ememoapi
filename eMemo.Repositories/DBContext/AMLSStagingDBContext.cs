﻿using eMemo.Model.Entity.AMLSStaging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Repositories.DBContext
{
    public class AMLSStagingDBContext : DbContext
    {
        public AMLSStagingDBContext(DbContextOptions<AMLSStagingDBContext> options) : base(options)
        {

        }

        public DbSet<AM_TB_MS_CHANNEL> AM_TB_MS_CHANNEL { get; set; }
        public DbSet<AM_TB_MS_SUB_CHANNEL> AM_TB_MS_SUB_CHANNEL { get; set; }
        public DbSet<AM_TB_MS_CHANNEL_BK> AM_TB_MS_CHANNEL_BK { get; set; }
        public DbSet<AM_TB_MS_SUB_CHANNEL_BK> AM_TB_MS_SUB_CHANNEL_BK { get; set; }
        public DbSet<AM_TB_MS_SEG_BRANCH> AM_TB_MS_SEG_BRANCH { get; set; }
    }
}
