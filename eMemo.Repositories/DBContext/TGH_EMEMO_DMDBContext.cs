﻿using eMemo.Model.Entity.TGH_EMEMO_DM;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Repositories.DBContext
{
    public class TGH_EMEMO_DMDBContext : DbContext
    {
        public TGH_EMEMO_DMDBContext(DbContextOptions<TGH_EMEMO_DMDBContext> options) : base(options)
        {
        }

        public DbSet<T_DM_EMEMO_AGENT_INFO_NEW> T_DM_EMEMO_AGENT_INFO_NEW { get; set; }
        public DbSet<T_DM_EMEMO_AGENT_INFO> T_DM_EMEMO_AGENT_INFO { get; set; }
    }
}
