﻿using eMemo.Helper;
using eMemo.Model.ExternalRequest;
using eMemo.Model.ExternalResponse;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class DMCFileRepository
    {
        private readonly HttpRequestBuilder _requestBuilder;
        private readonly AppSettingHelper _appsettingHelper;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(DMCFileRepository));
        public DMCFileRepository(HttpRequestBuilder requestBuilder)
        {
            _requestBuilder = requestBuilder;
            _appsettingHelper = new AppSettingHelper();
        }

        public async Task<SaveFileResponse> SaveFileDMC(SaveFileRequest request)
        {
            SaveFileResponse contentResponse = new SaveFileResponse();

            try
            {
                _requestBuilder.Init(_appsettingHelper.GetConfiguration("APISaveFileDMC"), "POST")
                    .AddContent(new JsonContent(request));

                var response = await _requestBuilder.SendAsync();
              
                if (response.IsSuccessStatusCode)
                {
                    contentResponse = JsonConvert.DeserializeObject<SaveFileResponse>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return contentResponse;
        }


        public async Task<GetFileResponse> GetFileDMC(string filePath)
        {
            GetFileResponse contentResponse = new GetFileResponse();

            try
            {
                var url = _appsettingHelper.GetConfiguration("APIGetFileDMC");
                url = string.Format(url, filePath);

                _requestBuilder.Init(url, "GET");

                var response = await _requestBuilder.SendAsync();

                if (response.IsSuccessStatusCode)
                {
                    contentResponse = JsonConvert.DeserializeObject<GetFileResponse>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return contentResponse;
        }
    }
}
