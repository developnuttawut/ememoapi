﻿using eMemo.Helper;
using eMemo.Model.Entity;
using eMemo.Model.Request.GroupEmailManagement;
using eMemo.Model.Request.MemoMapping;
using eMemo.Model.Response.MemoMapping;
using eMemo.Model.Response.RuleManagement;
using eMemo.Repositories.DBContext;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class MasterDataRepository
    {
        private readonly eMemoDBContext _dbContext;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(MasterDataRepository));
        public MasterDataRepository(eMemoDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region Memo Type
        public async Task<bool> CreateMemoType(SaveMemoTypeRequest request)
        {
            try
            {
                TMMemoType newMemoType = new TMMemoType
                {
                    MemoType_Name = request.memoTypeName,
                    Sequence = request.memoTypeSequence,
                    Flag_Delete = false,
                    Status = true,
                    Created_By = request.userID,
                    Created_Date = Utility.GetDateNowThai()
                };

                _dbContext.TMMemoType.Add(newMemoType);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateMemoType(SaveMemoTypeRequest request)
        {
            try
            {
                var memoTypeData = _dbContext.TMMemoType.Find(request.memoTypeID);

                if (memoTypeData != null)
                {
                    memoTypeData.MemoType_Name = request.memoTypeName;
                    memoTypeData.Sequence = request.memoTypeSequence;
                    memoTypeData.Updated_By = request.userID;
                    memoTypeData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TMMemoType.Update(memoTypeData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoType(DeleteMemoTypeRequest request)
        {
            try
            {
                var memoTypeData = _dbContext.TMMemoType.Find(request.memoTypeID);

                if (memoTypeData != null)
                {
                    memoTypeData.Flag_Delete = true;
                    memoTypeData.Updated_By = request.userID;
                    memoTypeData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TMMemoType.Update(memoTypeData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public TMMemoType FindMemoTypeBy(Expression<Func<TMMemoType, bool>> expression)
        {
            try
            {
                return _dbContext.TMMemoType.Where(expression).AsNoTracking().FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public List<TMMemoType> GetMemoTypeBy(Expression<Func<TMMemoType, bool>> expression)
        {
            try
            {
                return _dbContext.TMMemoType.Where(expression).AsNoTracking().ToList();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMMemoType>> GetAllMemoType()
        {
            try
            {
                return await _dbContext.TMMemoType.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        #endregion

        public async Task<TMMemoCode> CreateMemoCode(SaveMemoCodeRequest request)
        {
            try
            {
                TMMemoCode newMemoCode = new TMMemoCode
                {
                    MemoType_ID = request.memoTypeID,
                    MemoType_Name = request.memoType,
                    MemoCode = request.memoCode,
                    Description = request.description,
                    Sequence = request.memoCodeSequence,
                    Flag_Delete = false,
                    Status = request.status,
                    Created_By = request.userID,
                    Created_Date = Utility.GetDateNowThai()
                };

                var entityMemoCode = _dbContext.TMMemoCode.Add(newMemoCode);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return entityMemoCode.Entity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateMemoCode(SaveMemoCodeRequest request)
        {
            try
            {
                var memoCodeData = _dbContext.TMMemoCode.Find(request.memoCodeID);

                if (memoCodeData != null)
                {

                    memoCodeData.MemoType_ID = request.memoTypeID;
                    memoCodeData.MemoType_Name = request.memoType;
                    memoCodeData.MemoCode = request.memoCode;
                    memoCodeData.Description = request.description;
                    memoCodeData.Sequence = request.memoCodeSequence;
                    memoCodeData.Status = request.status;
                    memoCodeData.Updated_By = request.userID;
                    memoCodeData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TMMemoCode.Update(memoCodeData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> InsertMemoCodeAttachment(List<TMMemoCodeAttachment> memoCodeAttashment)
        {
            try
            {
                _dbContext.TMMemoCodeAttachment.AddRange(memoCodeAttashment);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoCodeAttachment(int memoCodeID)
        {
            try
            {
                var memoCodeAttachment = _dbContext.TMMemoCodeAttachment.Where(c => c.MemoCode_ID == memoCodeID);
                _dbContext.TMMemoCodeAttachment.RemoveRange(memoCodeAttachment);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoCode(DeleteMomoCodeRequest request)
        {
            try
            {
                var memoCodeData = _dbContext.TMMemoCode.Find(request.memoCodeID);

                if (memoCodeData != null)
                {
                    memoCodeData.Flag_Delete = true;
                    memoCodeData.Updated_By = request.userID;
                    memoCodeData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TMMemoCode.Update(memoCodeData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMMemoCode>> GetMemoCodeBy(Expression<Func<TMMemoCode, bool>> expression)
        {
            try
            {
                return await _dbContext.TMMemoCode.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMMemoCode>> GetAllMemoCode()
        {
            try
            {
                return await _dbContext.TMMemoCode.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public TMMemoCode FindMemoCodeBy(Expression<Func<TMMemoCode, bool>> expression)
        {
            try
            {
                return _dbContext.TMMemoCode.Where(expression).AsNoTracking().FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<MemoMappingData>> GetMemoMappingList()
        {
            try
            {
                var allMemoCodeData = _dbContext.TMMemoCode.AsNoTracking();

                var memoCodeData = from data in allMemoCodeData
                                   where data.Flag_Delete == false
                                   join m in _dbContext.TMMemoType
                                   on data.MemoType_ID equals m.ID
                                   into joinMemoType
                                   from memoType in joinMemoType.DefaultIfEmpty()
                                   select new MemoMappingData
                                   {
                                       memoCodeID = data.ID,
                                       memoCode = data.MemoCode,
                                       description = data.Description,
                                       memoCodeSequence = data.Sequence,
                                       status = data.Status,
                                       memoTypeID = memoType != null ? memoType.ID : 0,
                                       memoTypeName = memoType != null ? memoType.MemoType_Name : string.Empty,
                                       memoTypeSequence = memoType != null ? memoType.Sequence : 0
                                   };

                return await memoCodeData.ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMMemoCodeAttachment>> GetMemoCodeAttachment(int memoCodeID)
        {
            try
            {
                return await _dbContext.TMMemoCodeAttachment
                     .Where(c => c.MemoCode_ID == memoCodeID)
                     .AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public List<TMMemoCodeAttachment> GetMemoCodeAttachmentBy(Expression<Func<TMMemoCodeAttachment, bool>> expression)
        {
            try
            {
                return _dbContext.TMMemoCodeAttachment.Where(expression).AsNoTracking().ToList();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public TMMemoCodeAttachment FindMemoCodeAttachmentBy(Expression<Func<TMMemoCodeAttachment, bool>> expression)
        {
            try
            {
                return _dbContext.TMMemoCodeAttachment.Where(expression).AsNoTracking().FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        #region 

        public async Task<List<ProductCategoryData>> FindProductCategoryByDescription(string param)
        {
            try
            {
                var allProductCategoryData = _dbContext.TMProductCategory.AsNoTracking();

                var productCategoryData = from data in allProductCategoryData.DefaultIfEmpty()
                                          where data.ProductCategory.ToUpper().Contains(param.ToUpper())
                                          select new ProductCategoryData
                                          {
                                              description = data.ProductCategory
                                          };

                return await productCategoryData.ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        public async Task<List<ProductCategoryData>> GetProductCategoryList()
        {
            try
            {
                var allProductCategoryData = _dbContext.TMProductCategory.AsNoTracking();

                var productCategoryData = from data in allProductCategoryData.DefaultIfEmpty()
                                          select new ProductCategoryData
                                          {
                                              description = data.ProductCategory
                                          };

                return await productCategoryData.ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<RuleManagementData>> GetRuleManagementList()
        {
            try
            {

                var allRuleManagement = _dbContext.TMRuleManagement.AsNoTracking();

                var ruleManagementData = from data in allRuleManagement
                                         where data.Flag_Delete == false
                                         select new RuleManagementData
                                         {
                                             ruleManagementID = data.ID,
                                             channel = data.Channel,
                                             subChannelID = data.SubChannel_ID != null ? data.SubChannel_ID : null,
                                             subChannel = data.SubChannel,
                                             sobID = data.SOB_ID,
                                             sob = data.SOB,
                                             businessUnitID = data.BusinessUnit_ID,
                                             businessUnit = data.BusinessUnit != null ? data.BusinessUnit : null,
                                             firstTier = data.FirstTier,
                                             secondTier = data.SecondTier,
                                             firstTierChannel = data.FirstTierChannel,
                                             secondTierChannel = data.SecondTierChannel,
                                             spaceWorkingDays = data.SpaceWorkingDays != null ? data.SpaceWorkingDays : null,
                                             sendEmailToSaleSupport = data.SendEmailToSaleSupport,
                                             sendEmailToSaleUnderwriting = data.SendEmailToSaleUnderwriting,
                                             status = data.Status,
                                         };

                return await ruleManagementData.ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMRuleManagementProductCategory>> GetRuleManagementProductCategory(int ruleManagementID)
        {
            try
            {
                return await _dbContext.TMRuleManagementProductCategory
                     .Where(c => c.RuleManagement_ID == ruleManagementID)
                     .AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMRuleManagementProductCategory>> GetRuleManagementProductCategoryBy(Expression<Func<TMRuleManagementProductCategory, bool>> expression)
        {
            try
            {
                return await _dbContext.TMRuleManagementProductCategory
                     .Where(expression)
                     .AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        public TMRuleManagement FindRuleManagementBy(Expression<Func<TMRuleManagement, bool>> expression)
        {
            try
            {
                return _dbContext.TMRuleManagement.Where(expression).AsNoTracking().FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        public async Task<TMRuleManagement> CreateRuleManagement(SaveRuleManagementRequest request)
        {
            try
            {
                TMRuleManagement newRuleManagement = new TMRuleManagement
                {
                    Channel = request.channel,
                    SubChannel_ID = request.subChannelID,
                    SubChannel = request.subChannel,
                    SOB_ID = request.sobID,
                    SOB = request.sob,
                    BusinessUnit_ID = request.businessUnitID,
                    BusinessUnit = request.businessUnit,
                    FirstTier = request.firstTier,
                    SecondTier = request.secondTier,
                    FirstTierChannel = request.firstTierChannel,
                    SecondTierChannel = request.secondTierChannel,
                    SpaceWorkingDays = request.spaceWorkingDays,
                    SendEmailToSaleSupport = request.sendEmailToSaleSupport,
                    SendEmailToSaleUnderwriting = request.sendEmailToSaleUnderwriting,
                    Flag_Delete = false,
                    Status = request.status,
                    Created_By = request.userID,
                    Created_Date = Utility.GetDateNowThai()
                };

                var entityRuleManagement = _dbContext.TMRuleManagement.Add(newRuleManagement);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return entityRuleManagement.Entity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        public async Task<bool> UpdateRuleManagement(SaveRuleManagementRequest request)
        {
            try
            {
                var ruleManagementData = _dbContext.TMRuleManagement.Find(request.ruleManagementID);

                if (ruleManagementData != null)
                {
                    ruleManagementData.Channel = request.channel;
                    ruleManagementData.SubChannel_ID = request.subChannelID;
                    ruleManagementData.SubChannel = request.subChannel;
                    ruleManagementData.SOB_ID = request.sobID;
                    ruleManagementData.SOB = request.sob;
                    ruleManagementData.BusinessUnit_ID = request.businessUnitID;
                    ruleManagementData.BusinessUnit = request.businessUnit;
                    ruleManagementData.FirstTier = request.firstTier;
                    ruleManagementData.SecondTier = request.secondTier;
                    ruleManagementData.FirstTierChannel = request.firstTierChannel;
                    ruleManagementData.SecondTierChannel = request.secondTierChannel;
                    ruleManagementData.SpaceWorkingDays = request.spaceWorkingDays;
                    ruleManagementData.SendEmailToSaleSupport = request.sendEmailToSaleSupport;
                    ruleManagementData.SendEmailToSaleUnderwriting = request.sendEmailToSaleUnderwriting;
                    ruleManagementData.Flag_Delete = false;
                    ruleManagementData.Status = request.status;
                    ruleManagementData.Updated_By = request.userID;
                    ruleManagementData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TMRuleManagement.Update(ruleManagementData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteRuleManagementProductCategory(int ruleManagementID)
        {
            try
            {
                var ruleManagementProductCategory = _dbContext.TMRuleManagementProductCategory.Where(c => c.RuleManagement_ID == ruleManagementID);
                _dbContext.TMRuleManagementProductCategory.RemoveRange(ruleManagementProductCategory);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        public async Task<bool> InsertRuleManagementProductCategory(List<TMRuleManagementProductCategory> ruleManagementProductCategory)
        {
            try
            {
                _dbContext.TMRuleManagementProductCategory.AddRange(ruleManagementProductCategory);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteRuleManagement(DeleteRuleManagementRequest request)
        {
            try
            {
                var ruleManagementeData = _dbContext.TMRuleManagement.Find(request.ruleManagementID);

                if (ruleManagementeData != null)
                {
                    ruleManagementeData.Flag_Delete = true;
                    ruleManagementeData.Updated_By = request.userID;
                    ruleManagementeData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TMRuleManagement.Update(ruleManagementeData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        #endregion

        #region Email Management

        public async Task<List<TMGroupEmailManagement>> GetGroupEmailManagementList()
        {
            try
            {
                return await _dbContext.TMGroupEmailManagement.AsNoTracking().ToListAsync();

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMEmailManagement>> GetEmailManagementListBy(Expression<Func<TMEmailManagement, bool>> expression)
        {
            try
            {
                return await _dbContext.TMEmailManagement.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<TMGroupEmailManagement> CreateGroupEmailManagement(SaveGroupEmailManagementRequest request)
        {
            try
            {
                TMGroupEmailManagement newGroupEmailManagement = new TMGroupEmailManagement
                {
                    BranchOffice = request.branchOffice,
                    BranchOffice_ID = request.branchOfficeID,
                    EmailType = request.emailType,
                    SendGroup = request.sendGroup,
                    BusinessUnit_ID = request.businessUnitID,
                    BusinessUnit = request.businessUnit,
                    Status = request.status,
                    Created_By = request.userID,
                    Created_Date = Utility.GetDateNowThai()
                };

                var entityGroupEmailManagement = _dbContext.TMGroupEmailManagement.Add(newGroupEmailManagement);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return entityGroupEmailManagement.Entity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateGroupEmailManagement(SaveGroupEmailManagementRequest request)
        {
            try
            {
                var groupEmailManagementData = _dbContext.TMGroupEmailManagement.Find(request.ID);

                if (groupEmailManagementData != null)
                {
                    groupEmailManagementData.BranchOffice = request.branchOffice;
                    groupEmailManagementData.BranchOffice_ID = request.branchOfficeID;
                    groupEmailManagementData.BusinessUnit = request.businessUnit;
                    groupEmailManagementData.BusinessUnit_ID = request.businessUnitID;
                    groupEmailManagementData.SendGroup = request.sendGroup;
                    groupEmailManagementData.EmailType = request.emailType;
                    groupEmailManagementData.Status = request.status;
                    groupEmailManagementData.Updated_By = request.userID;
                    groupEmailManagementData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TMGroupEmailManagement.Update(groupEmailManagementData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteEmailManagement(int groupEmailManagementID)
        {
            try
            {
                var data = _dbContext.TMEmailManagement.Where(c => c.GroupEmailManagement_ID == groupEmailManagementID);
                _dbContext.TMEmailManagement.RemoveRange(data);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> InsertEmailManagement(List<TMEmailManagement> listInsertData)
        {
            try
            {
                _dbContext.TMEmailManagement.AddRange(listInsertData);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        #endregion

        public async Task<List<TMSaleGroup>> GetSaleGroup(Expression<Func<TMSaleGroup, bool>> expression)
        {
            try
            {
                return await _dbContext.TMSaleGroup.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMUnderwritingGroup>> GetUnderwritingGroup(Expression<Func<TMUnderwritingGroup, bool>> expression)
        {
            try
            {
                return await _dbContext.TMUnderwritingGroup.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

    }
}
