﻿using eMemo.Helper;
using eMemo.Model.ExternalRequest;
using eMemo.Model.ExternalResponse;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class HolidayRepository
    {
        private readonly HttpRequestBuilder _requestBuilder;
        private readonly AppSettingHelper _appsettingHelper;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(HolidayRepository));
        public HolidayRepository(HttpRequestBuilder requestBuilder)
        {
            _requestBuilder = requestBuilder;
            _appsettingHelper = new AppSettingHelper();
        }

        public async Task<List<GetHolidayResponse>> GetHoliday(GetHolidayRequest request)
        {
            List<GetHolidayResponse> contentResponse = new List<GetHolidayResponse>();

            try
            {
                string userName = _appsettingHelper.GetConfiguration("UserName");
                string password = _appsettingHelper.GetConfiguration("Password");

                _requestBuilder.Init(_appsettingHelper.GetConfiguration("APIHolidayList"), "POST")
                    .AddAuth(userName, password)
                    .SetTokenEmpty()
                    .AddContent(new JsonContent(request));

                var response = await _requestBuilder.SendAsync();

                if (response.IsSuccessStatusCode)
                {
                    contentResponse = JsonConvert.DeserializeObject<List<GetHolidayResponse>>(await response.Content.ReadAsStringAsync());
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return contentResponse;
        }
    }
}
