﻿using eMemo.Helper;
using eMemo.Model.Base;
using eMemo.Model.Entity;
using eMemo.Model.Entity.TGH_EMEMO_DM;
using eMemo.Model.ExternalResponse;
using eMemo.Model.Request.EMemoTransaction;
using eMemo.Model.Request.MemoMapping;
using eMemo.Model.Response.EMemoTransaction;
using eMemo.Model.Response.MemoMapping;
using eMemo.Model.Response.RuleManagement;
using eMemo.Repositories.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class MemoTransactionRepository
    {
        private readonly eMemoDBContext _dbContext;
        private AppSettingHelper appSettingHelper;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(MemoTransactionRepository));
        public MemoTransactionRepository(eMemoDBContext dbContext)
        {
            _dbContext = dbContext;
            appSettingHelper = new AppSettingHelper();
        }

        public async Task<TTHMemoTransaction> CreateMemoTransactionHeader(CreateMemoTransactionRequest request, T_DM_EMEMO_AGENT_INFO_NEW additionalDaa)
        {
            try
            {
                TTHMemoTransaction newMemoTransaction = new TTHMemoTransaction
                {
                    DocumentNo = request.documentNo,
                    TransactionDate = request.transactionDate,
                    PolicyNo = request.policyNo,
                    MainPlan = request.mainPlan,
                    ProposalNo = request.proposalNo,
                    ProposalDate = request.proposalDate,
                    PolicyName = request.policyName,
                    CitizenID = request.citizenID,
                    PartyID = request.partyID,
                    PolicyStatus = request.policyStatus,
                    EBaoMemoStatus_ID = request.eBaoMemoStatus,
                    AgentCode = request.agentCode,
                    AgentName = request.agentName,
                    AgentUnitCode = additionalDaa?.UNIT_CODE,
                    AgentUnit = additionalDaa?.UNIT_NAME,
                    AgentGroupCode = additionalDaa?.GROUP_CODE,
                    AgentGroup = additionalDaa?.GROUP_NAME,
                    EMemoStatus = request.memoStatus,
                    Round = request.round,
                    EstimateTime = Utility.GetDateNowThai().AddMinutes(15),
                    DeliveryStatus_ID = request.deliveryStatus,
                    AgentEmail = additionalDaa?.EMAIL,
                    BusinessUnit = additionalDaa?.BUSINESS_UNIT_NAME,
                    AgentPhoneNumber = additionalDaa?.MOBILE,
                    ApplicationBranch = additionalDaa?.APPLICATION_BRANCH_NAME,
                    Channel = additionalDaa?.CHANNEL_NAME,
                    ProductCategory = additionalDaa?.PRODUCT_CATEGORY_NAME,
                    ServicingBranch = additionalDaa?.BRANCH_NAME,
                    BranchCode = additionalDaa?.BRANCH_CODE,
                    SourceOfBusiness = additionalDaa?.SOURCE_OF_BUSINESS_NAME,
                    SubChannel = additionalDaa?.SUB_CHANNEL_NAME,
                    EntryAgeLifeAssured = request.entryAgeLifeAssured,
                    ApplicationBranchCode = additionalDaa?.APPLICATION_BRANCH_CODE,
                    LAAddress = request.laAddress,
                    LAEmail = request.laEmail,
                    LAPhoneNo = request.laPhoneNo,
                    LifeAssuredName = request.lifeAssuredName,
                    PHAddress = request.phAddress,
                    PHEmail = request.phEmail,
                    PHPhoneNo = request.phPhoneNo,
                    BranchCOCode = additionalDaa?.BRANCH_CO_CODE,
                    Created_By = request.userID,
                    Created_Date = DateTime.Now,
                };

                var entityMemoTransaction = _dbContext.TTHMemoTransaction.Add(newMemoTransaction);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return entityMemoTransaction.Entity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateMemoTransactionHeader(TTHMemoTransaction dataUpdate)
        {
            try
            {
                var entityMemoTransaction = _dbContext.TTHMemoTransaction.Update(dataUpdate);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoTransactionHeader(int headerid)
        {
            try
            {
                var data = _dbContext.TTHMemoTransaction.Where(c => c.ID == headerid);
                _dbContext.TTHMemoTransaction.RemoveRange(data);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }


        public async Task<bool> UpdateMemoTransactionHeaderList(TTHMemoTransaction request)
        {
            try
            {
                _dbContext.TTHMemoTransaction.Update(request);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateMemoTransactionStatusList(List<UpdateStatusTransactionRequest> request)
        {
            try
            {
                var transactionIDList = request.Select(e => e.transactionID);
                var transactionUpdate = _dbContext.TTHMemoTransaction.Where(c => transactionIDList.Contains(c.ID));

                foreach (var itemTransaction in transactionUpdate)
                {
                    var dataUpdate = request.FirstOrDefault(c => c.transactionID == itemTransaction.ID);

                    if (!string.IsNullOrWhiteSpace(dataUpdate.ememoStatus))
                    {
                        itemTransaction.EMemoStatus = dataUpdate.ememoStatus;
                    }

                    if (!string.IsNullOrEmpty(dataUpdate.deliveryStatusID))
                    {
                        itemTransaction.DeliveryStatus_ID = dataUpdate.deliveryStatusID;
                    }

                    itemTransaction.Updated_Date = dataUpdate.updateDate;
                    itemTransaction.Updated_By = dataUpdate.updateBy;
                }

                _dbContext.TTHMemoTransaction.UpdateRange(transactionUpdate);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateMemoTransactionNote(int transactionID, string note, string userID)
        {
            try
            {
                var transactionUpdate = _dbContext.TTHMemoTransaction.FirstOrDefault(c => c.ID == transactionID);

                transactionUpdate.Note = note;
                transactionUpdate.Updated_By = userID;
                transactionUpdate.Updated_Date = Utility.GetDateNowThai();

                _dbContext.TTHMemoTransaction.UpdateRange(transactionUpdate);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> CreateMemoTransactionDetail(int headerid, CreateMemoTransactionRequest request)
        {
            try
            {
                var newMemoDetail = request.detail.Select(s => new TTDMemoTransactionDetail()
                {
                    MemoTransaction_ID = headerid,
                    DocumentNo = request.documentNo,
                    MemoType = s.memoType,
                    MemoCode = s.memoCode ?? string.Empty,
                    MemoCodeDescription = s.memoDescription,
                    LetterStatus = "",
                    Comment = s.memoComment,
                    MemoCode_Sequence = s.memoCodeSequence,
                    MemoType_Sequence = s.memoTypeSequence,
                    Created_By = request.userID,
                    Created_Date = Utility.GetDateNowThai()
                }).ToList();

                _dbContext.TTDMemoTransactionDetail.AddRange(newMemoDetail);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return false;
            }
        }

        public async Task<TTDMemoTransactionDetail> AddMemoTransactionDetail(TTDMemoTransactionDetail request)
        {
            try
            {
                var data = _dbContext.TTDMemoTransactionDetail.Add(request);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return data.Entity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(ex.InnerException?.Message ?? ex.Message);
                throw ex;
            }
        }

        public List<TTDMemoTransactionAttachment> GetMemoTransactionAttachmentBy(Expression<Func<TTDMemoTransactionAttachment, bool>> expression)
        {
            try
            {
                return _dbContext.TTDMemoTransactionAttachment.Where(expression).ToList();

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public List<TTDMemoTransactionDetail> GetMemoTransactionDetail(int memoHeaderID)
        {
            try
            {
                return _dbContext.TTDMemoTransactionDetail.Where(c => c.MemoTransaction_ID == memoHeaderID).AsNoTracking().ToList();

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public List<TTDMemoTransactionDetail> GetMemoTransactionDetailBy(Expression<Func<TTDMemoTransactionDetail, bool>> expression)
        {
            try
            {
                return _dbContext.TTDMemoTransactionDetail.Where(expression).ToList();

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateMemoTransactionDetailList(List<TTDMemoTransactionDetail> request)
        {
            try
            {
                _dbContext.TTDMemoTransactionDetail.UpdateRange(request);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TTHMemoTransaction>> GetMemoTransactionHeader()
        {
            try
            {
                var result = _dbContext.TTHMemoTransaction.AsNoTracking();
                return await result.ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateDeliveryStatus(UpdateDeliveryStatusData request, string userID)
        {
            try
            {
                var memoTxnData = _dbContext.TTHMemoTransaction.Find(request.transactionID);

                if (memoTxnData != null)
                {
                    memoTxnData.DeliveryStatus_ID = request.deliveryStatus;
                    memoTxnData.Updated_By = userID;
                    memoTxnData.Updated_Date = Utility.GetDateNowThai();

                    _dbContext.TTHMemoTransaction.Update(memoTxnData);

                    if (await _dbContext.SaveChangesAsync() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> SaveEMemoTransactionDetailAttachment(List<TTDMemoTransactionAttachment> data)
        {
            try
            {
                _dbContext.TTDMemoTransactionAttachment.AddRange(data);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public List<TMDeliveryStatus> GetAllDeliveryStatus()
        {
            try
            {
                return _dbContext.TMDeliveryStatus.AsNoTracking().ToList();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TMEBaoMemoStatus>> GetAllEBaoStatus()
        {
            try
            {
                return await _dbContext.TMEBaoMemoStatus.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<TTHMemoTransaction> FindMemoTransactionBy(Expression<Func<TTHMemoTransaction, bool>> expression)
        {
            try
            {
                return await _dbContext.TTHMemoTransaction.Where(expression).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TTHMemoTransaction>> GetMemoTransactionBy(Expression<Func<TTHMemoTransaction, bool>> expression)
        {
            try
            {
                return await _dbContext.TTHMemoTransaction.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }


        public async Task<List<TTHMemoTransaction>> GetMemoTransactionMockUp()
        {
            try
            {
                var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                var fullPath = path + @"\Mockup\Transaction.json";

                var jsonData = await System.IO.File.ReadAllTextAsync(fullPath);

                var response = JsonConvert.DeserializeObject<List<TTHMemoTransaction>>(jsonData);
                return response.Where(c => c.EMemoStatus != "New").ToList();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }


        public async Task<List<TTDMemoTransactionDetail>> FindMemoTransactionDetailBy(Expression<Func<TTDMemoTransactionDetail, bool>> expression)
        {
            try
            {
                return await _dbContext.TTDMemoTransactionDetail.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> CreateMemoTransactionHistory(List<TTDMemoTransactionHistory> request)
        {
            try
            {
                _dbContext.TTDMemoTransactionHistory.AddRange(request);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TTDMemoTransactionHistory>> FindMemoTransactionHistoryBy(Expression<Func<TTDMemoTransactionHistory, bool>> expression)
        {
            try
            {
                return await _dbContext.TTDMemoTransactionHistory.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<TTHMemoDocumentHistory> CreateMemoDocumentHistory(TTHMemoDocumentHistory request)
        {
            try
            {
                var entityMemoTransaction = _dbContext.TTHMemoDocumentHistory.Add(request);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return entityMemoTransaction.Entity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> SaveMemoDocumentHistoryAttchment(List<TTDMemoDocumentHistoryAttachment> data)
        {
            try
            {
                _dbContext.TTDMemoDocumentHistoryAttachment.AddRange(data);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoTransactionDetail(TTDMemoTransactionDetail data)
        {
            try
            {
                _dbContext.TTDMemoTransactionDetail.Remove(data);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoTransactionDetailList(List<TTDMemoTransactionDetail> data)
        {
            try
            {
                _dbContext.TTDMemoTransactionDetail.RemoveRange(data);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoTransactionAttachmentList(List<TTDMemoTransactionAttachment> data)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(appSettingHelper.GetConfiguration("ConnectionStrings:eMemoDB")))
                {
                    con.Open();
                    var queryParam = data.Select(d => d.ID.ToString()).Join(",");


                    using (SqlCommand command = new SqlCommand($"DELETE FROM TTDMemoTransactionAttachment  WHERE ID in ({queryParam})", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> DeleteMemoTransactionDetailAttachment(int memoTransactionDetailID)
        {
            try
            {
                var memoAttachment = _dbContext.TTDMemoTransactionAttachment.Where(c => c.MemoTransactionDetail_ID == memoTransactionDetailID);

                _dbContext.TTDMemoTransactionAttachment.RemoveRange(memoAttachment);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TTHMemoDocumentHistory>> GetMemoDocumentHistoryBy(Expression<Func<TTHMemoDocumentHistory, bool>> expression)
        {
            try
            {
                return await _dbContext.TTHMemoDocumentHistory.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<TTHMemoDocumentHistory> FindMemoDocumentHistoryBy(Expression<Func<TTHMemoDocumentHistory, bool>> expression)
        {
            try
            {
                return await _dbContext.TTHMemoDocumentHistory.Where(expression).AsNoTracking().FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TTDMemoDocumentHistoryAttachment>> GetMemoDocumentHistoryAttachmentBy(Expression<Func<TTDMemoDocumentHistoryAttachment, bool>> expression)
        {
            try
            {
                return await _dbContext.TTDMemoDocumentHistoryAttachment.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<TTDMemoDocumentHistoryDetail> CreateMemoDocumentHistoryDetail(TTDMemoDocumentHistoryDetail request)
        {
            try
            {
                var entityMemoTransaction = _dbContext.TTDMemoDocumentHistoryDetail.Add(request);

                if (await _dbContext.SaveChangesAsync() > 0)
                {
                    return entityMemoTransaction.Entity;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<TTDMemoDocumentHistoryDetail>> GetMemoDocumentHistoryDetailBy(Expression<Func<TTDMemoDocumentHistoryDetail, bool>> expression)
        {
            try
            {
                return await _dbContext.TTDMemoDocumentHistoryDetail.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> CreateMemoDocumentTransactionHistory(int memoTransactionID, string documentNo, string userID, string noteUpdate)
        {
            try
            {
                var memoDocumentHistory = await GetMemoDocumentHistoryBy(e => e.MemoTransaction_ID == memoTransactionID);
                var lastedMemoDocumentHistory = memoDocumentHistory.OrderByDescending(c => c.SequenceEditDocumentNo).FirstOrDefault();

                var memoDetailNotReply = GetMemoTransactionDetailBy(
                    c => c.MemoTransaction_ID == memoTransactionID && string.IsNullOrWhiteSpace(c.LetterStatus));


                TTHMemoDocumentHistory memoHistory = new TTHMemoDocumentHistory
                {
                    EMEMODocumentNo = documentNo,
                    MemoTransaction_ID = memoTransactionID,
                    SequenceEditDocumentNo = lastedMemoDocumentHistory != null ? lastedMemoDocumentHistory.SequenceEditDocumentNo + 1 : 1,
                    TransactionDate = Utility.GetDateNowThai(),
                    Note = noteUpdate,
                    Created_Date = Utility.GetDateNowThai(),
                    Created_By = userID
                };

                var saveHistoryResult = await CreateMemoDocumentHistory(memoHistory);

                foreach (var itemDetail in memoDetailNotReply)
                {
                    var memoAttachmentByID = GetMemoTransactionAttachmentBy(c => c.MemoTransactionDetail_ID == itemDetail.ID);

                    TTDMemoDocumentHistoryDetail historyDocumentDetail = new TTDMemoDocumentHistoryDetail
                    {
                        Comment = itemDetail.Comment,
                        Created_By = itemDetail.Created_By,
                        Created_Date = Utility.GetDateNowThai(),
                        DocumentNo = itemDetail.DocumentNo,
                        MemoCode = itemDetail.MemoCode,
                        MemoCodeDescription = itemDetail.MemoCodeDescription,
                        MemoCode_Sequence = itemDetail.MemoCode_Sequence,
                        MemoDocumentHistory_ID = saveHistoryResult.ID,
                        LetterStatus = itemDetail.LetterStatus,
                        MemoType = itemDetail.MemoType,
                        MemoType_Sequence = itemDetail.MemoType_Sequence,
                        Source = itemDetail.Source,
                        MemoTransaction_ID = itemDetail.MemoTransaction_ID,
                        Reply_By = itemDetail.Reply_By,
                        Reply_Date = itemDetail.Reply_Date
                    };

                    var saveDetailResponse = await CreateMemoDocumentHistoryDetail(historyDocumentDetail);

                    var memoHistoryAttachment = memoAttachmentByID.Select(c => new TTDMemoDocumentHistoryAttachment
                    {
                        MemoDocumentHistoryDetail_ID = saveDetailResponse.ID,
                        MemoDocumentHistory_ID = saveHistoryResult.ID,
                        File_Name = c.File_Name,
                        Reference_ID = c.Reference_ID,
                        File_Extension = c.File_Extension,
                        File_Path = c.File_Path,
                        File_Size = c.File_Size,
                        File_Type = c.File_Type,
                        Created_By = userID,
                        Created_Date = Utility.GetDateNowThai(),
                        Sequence = c.Sequence.GetValueOrDefault()
                    }).ToList();

                    await SaveMemoDocumentHistoryAttchment(memoHistoryAttachment);
                }

                return true;
            }
            catch(Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return false;
            }          
        }

        public async Task<List<Transaction_View>> GetTransactionReport(Expression<Func<Transaction_View, bool>> expression)
        {
            try
            {
                return await _dbContext.Transaction_View.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

        }

        public async Task<List<Transaction_View>> GetTransactionReportByPolicy(List<string> policyCodeList)
        {
            List<Transaction_View> response = new List<Transaction_View>();

            try
            {
                if (policyCodeList.Count > 1000)
                {
                    int totalRow = policyCodeList.Count;
                    var round = Math.Ceiling((decimal)policyCodeList.Count / 1000);

                    for (int i = 0; i < round; i++)
                    {
                        int getTotal = 0;
                        if (totalRow >= 1000)
                        {
                            getTotal = 1000;
                        }
                        else
                        {
                            getTotal = totalRow;
                        }
                        var policyQuery = policyCodeList.GetRange(1000 * i, getTotal);

                        var queryResponse = await _dbContext.Transaction_View.Where(c => policyQuery.Contains(c.PolicyNo)).AsNoTracking().ToListAsync();
                        response.AddRange(queryResponse);

                        totalRow -= 1000;
                    }
                }
                else
                {
                    var queryResponse = await _dbContext.Transaction_View.Where(c => policyCodeList.Contains(c.PolicyNo)).AsNoTracking().ToListAsync();
                    response.AddRange(queryResponse);
                }
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<Transaction_ViewHistory>> GetTransactionHistoryReport(Expression<Func<Transaction_ViewHistory, bool>> expression)
        {
            try
            {
                return await _dbContext.Transaction_ViewHistory.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

        }
    }
}

