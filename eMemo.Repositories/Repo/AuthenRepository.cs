﻿using eMemo.Helper;
using eMemo.Model.ExternalResponse;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class AuthenRepository
    {
        private readonly HttpRequestBuilder _requestBuilder;
        private readonly AppSettingHelper _appsettingHelper;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(AuthenRepository));
        public AuthenRepository(HttpRequestBuilder requestBuilder)
        {
            _requestBuilder = requestBuilder;
            _appsettingHelper = new AppSettingHelper();
        }

        public async Task<AuthenticationResponse> Authentication(string tokenID)
        {
            AuthenticationResponse response = new AuthenticationResponse();

            try
            {
                AuthenticationRequest request = new AuthenticationRequest
                {
                    AppId = "75", //รอสร้างบน Production
                    AppSecretKey = "YjRBRfYbbcSECRET_DCBR"
                };

                _requestBuilder.Init(_appsettingHelper.GetConfiguration("APIAuthen"), "POST")
                    .AddToken(tokenID)
                    .SetAuthEmpty()
                   .AddContent(new JsonContent(request));

                var result = await _requestBuilder.SendAsync();

                if (result.IsSuccessStatusCode)
                {
                    response = JsonConvert.DeserializeObject<AuthenticationResponse>(await result.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task Logout(string tokenID)
        {
            AuthenticationResponse response = new AuthenticationResponse();

            try
            {
                _requestBuilder.Init(_appsettingHelper.GetConfiguration("APIAuthen"), "GET")
                    .AddToken(tokenID)
                    .SetAuthEmpty();

                var result = await _requestBuilder.SendAsync();

                if (result.IsSuccessStatusCode)
                {
                    response = JsonConvert.DeserializeObject<AuthenticationResponse>(await result.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            await Task.CompletedTask;
        }
    }

    public class AuthenticationRequest
    {
        public string AppId { get; set; }
        public string AppSecretKey { get; set; }
    }
}
