﻿using eMemo.Helper;
using eMemo.Model.Entity.eBaoTemp;
using eMemo.Model.Request.EMemoTransaction;
using eMemo.Model.Request.Report;
using eMemo.Repositories.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class EBaoMemoLetterRepository
    {
        private readonly EBAODBContet _dbContext;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(EBaoMemoLetterRepository));
        private AppSettingHelper appSettingHelper;
        private string queryPending = $" SELECT DISTINCT  " +
                    $"m.submission_date, " +
                    $"m.policy_code, " +
                    $"m.policy_id," +
                    $"m.liability_state," +
                    $"m.END_CAUSE," +
                    $"m.issue_date , " +
                    $"c.pending_id, " +
                    $"c.pend_cause, " +
                    $"(select sum(p.total_prem_af) as Polprem from t_contract_product p where p.policy_id = m.policy_id group by policy_id) total_prem_af," +
                    $" p.renewal_type , " +
                    $"(select td.update_time from t_document td where list_id = (SELECT max(list_id) from t_document where template_id = 1 and policy_id = m.policy_id) ) lca_printeddate," +
                    $"(select create_date from t_memo_letter where list_id = (SELECT max(list_id) from t_memo_letter where policy_code = m.policy_code group by policy_code)) create_date," +
                    $"(select distinct update_date from t_memo_letter where list_id = (SELECT max(list_id) from t_memo_letter where policy_code = m.policy_code group by policy_code)) update_date," +
                    $"li.internal_id, " +
                    $"li.product_name," +
                    $"uwe.exclusion_code, " +
                    $"ins.stand_life, " +
                    $"(select SUM(c.pay_balance) from t_cash c where c.FEE_STATUS = '1' and c.FEE_TYPE = '11' and c.prem_purpose = '1' and policy_id = m.policy_id) amount_recieved," +
                    $"(CASE WHEN M.Institute_code is null and p.renewal_type = '4' " +
                    $"THEN 1 " +
                    $"ELSE  0 END) Institute_code, " +
                    $"(CASE WHEN p.renewal_type = 2  " +
                    $"THEN((select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) * 2) " +
                    $"WHEN p.renewal_type = 3 THEN((select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) * 4) " +
                    $"WHEN p.renewal_type = 4 THEN((select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) * 12) " +
                    $"ELSE(select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) END) APE," +
                    $"bu.unit_desc, " +
                    $"us.user_name, " +
                    $"'' as productCategory, " +
                    $"(CASE WHEN M.LIABILITY_STATE = 0 " +
                    $"THEN(SELECT T_PROPOSAL_STATUS.PROPOSAL_STATUS " +
                    $"FROM T_CONTRACT_PROPOSAL INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS " +
                    $"WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) " +
                    $"ELSE(SELECT T_LIABILITY_STATUS.STATUS_ID FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE)  END) PROPOSAL_STATUS,  " +
                    $"(CASE WHEN M.LIABILITY_STATE = 0 " +
                    $"THEN(SELECT T_PROPOSAL_STATUS.STATUS_DESC " +
                    $"FROM T_CONTRACT_PROPOSAL INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS " +
                    $"WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) " +
                    $"ELSE(SELECT T_LIABILITY_STATUS.STATUS_NAME " +
                    $"FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE) END) STATUS_DESC " +
                    $"FROM T_CONTRACT_MASTER M " +
                    $"left outer join t_contract_proposal n on n.policy_id = m.policy_id " +
                    $"left outer join t_proposal_status q on q.proposal_status = n.proposal_status " +
                    $"INNER JOIN T_CONTRACT_PRODUCT P ON M.POLICY_ID = P.POLICY_ID " +
                    $"left outer join t_product_life li on P.product_id = li.product_id  " +
                    $"INNER JOIN T_AGENT A ON M.SERVICE_AGENT = A.AGENT_ID " +
                    $"left outer join (SELECT distinct policy_id, UW_PENDING FROM t_uw_policy Where UNDERWRITE_ID in (SELECT max(UNDERWRITE_ID) from t_uw_policy group by policy_id )) unp on unp.policy_id = m.policy_id " +
                    $"left outer join t_pending_code c on c.pending_id = unp.UW_PENDING " +
                    $"LEFT JOIN T_CUSTOMER Ca ON A.AGENT_ID = Ca.CUSTOMER_ID " +
                    $"LEFT JOIN T_COMPANY_CUSTOMER CO_INF ON M.SERVICE_AGENT = CO_INF.COMPANY_ID " +
                    $"left OUTER JOIN t_uw_exclusion uwe ON uwe.policy_id = m.policy_id " +
                    $"left OUTER JOIN t_insured_list ins ON ins.policy_id = m.policy_id " +
                    $"LEFT JOIN T_CASH C ON P.POLICY_ID = C.POLICY_ID " +
                    $"AND C.PREM_PURPOSE = 1 " +
                    $"AND EXISTS(SELECT Cs.Policy_Id, MIN(Cs.Insert_Time) as Insert_Time " +
                    $"FROM T_CASH Cs WHERE Cs.Prem_Purpose = 1 AND Policy_Id = C.POLICY_ID GROUP BY Cs.Policy_Id HAVING MIN(Cs.Insert_Time) = C.Insert_Time) " +
                    $"left outer join(select a.* from T_INSURED_LIST a " +
                    $"where relation_to_ph = (select max(relation_to_ph) from T_INSURED_LIST where policy_id = a.policy_id  ) and STAND_LIFE = 'Y' ) I " +
                    $"ON M.POLICY_ID = I.POLICY_ID " +
                    $"INNER JOIN T_PARTY Pt ON I.PARTY_ID = Pt.Party_Id " +
                    $"INNER JOIN T_CUSTOMER Ci ON Pt.PARTY_ID = Ci.CUSTOMER_ID " +
                    $"INNER JOIN T_CHARGE_MODE G ON P.RENEWAL_TYPE = G.CHARGE_TYPE " +
                    $"inner join t_user us on m.updated_by = us.user_id " +
                    $"INNER JOIN T_PRODUCT_LIFE L ON P.PRODUCT_ID = L.PRODUCT_ID AND L.INS_TYPE = 1 " +
                    $"LEFT JOIN T_INSTITUTE_CODE TIC ON TIC.INSTITUTE_CODE = M.INSTITUTE_CODE " +
                    $"LEFT JOIN T_BUSINESS_UNIT Bu ON M.BUSINESS_UNIT = Bu.BUSINESS_UNIT ";

        public EBaoMemoLetterRepository(EBAODBContet dbContext)
        {
            _dbContext = dbContext;
            appSettingHelper = new AppSettingHelper();
        }

        public async Task<List<T_MEMO_LETTER_DETAIL>> GetTempStagingDetailBy(Expression<Func<T_MEMO_LETTER_DETAIL, bool>> expression)
        {
            try
            {
                return await _dbContext.t_memo_letter_detail.FromSql("select sys_guid() as ID,DOCUMENT_NO,MEMO_ITEM_TYPE,MEMO_CODE,MEMO_DESCRIPTION,COMMENTS from t_memo_letter_detail")
                    .Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_MEMO_LETTER_DETAIL>> GetTempStagingDetailByDocument(List<string> documentList)
        {
            List<T_MEMO_LETTER_DETAIL> response = new List<T_MEMO_LETTER_DETAIL>();

            try
            {
                if (documentList.Any())
                {
                    if (documentList.Count > 1000)
                    {
                        int totalRow = documentList.Count;
                        var round = Math.Ceiling((decimal)documentList.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = documentList.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            string query = $"select sys_guid() as ID,DOCUMENT_NO,MEMO_ITEM_TYPE,MEMO_CODE,MEMO_DESCRIPTION,COMMENTS from t_memo_letter_detail " +
                            $"WHERE DOCUMENT_NO in ({queryParam})";

                            var queryResponse = await _dbContext.t_memo_letter_detail.FromSql(query).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = documentList.Select(d => "'" + d + "'").Join(",");
                        string query = $"select sys_guid() as ID,DOCUMENT_NO,MEMO_ITEM_TYPE,MEMO_CODE,MEMO_DESCRIPTION,COMMENTS from t_memo_letter_detail " +
                                    $"WHERE DOCUMENT_NO in ({queryParam})";

                        response = await _dbContext.t_memo_letter_detail.FromSql(query).ToListAsync();
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_MEMO_LETTER>> GetTempMemoLetterBy(Expression<Func<T_MEMO_LETTER, bool>> expression)
        {
            try
            {
                return await _dbContext.t_memo_letter.FromSql("select * from t_memo_letter")
                        .Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_MEMO_LETTER>> GetTempMemoLetterByPolicyCodeList(List<string> policyCodeList)
        {
            List<T_MEMO_LETTER> response = new List<T_MEMO_LETTER>();
           
            try
            {
                if (policyCodeList.Any())
                {
                    if (policyCodeList.Count > 1000)
                    {
                        int totalRow = policyCodeList.Count;
                        var round = Math.Ceiling((decimal)policyCodeList.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyCodeList.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            string query = $"select * from t_memo_letter " +
                            $"WHERE POLICY_CODE in ({queryParam})";

                            var queryResponse = await _dbContext.t_memo_letter.FromSql(query).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyCodeList.Select(d => "'" + d + "'").Join(",");
                        string query = $"select * from t_memo_letter " +
                            $"WHERE POLICY_CODE in ({queryParam})";

                        response = await _dbContext.t_memo_letter.FromSql(query).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_MEMO_LETTER>> GetTempMemoLetterNotTransfer()
        {
            try
            {
                return await _dbContext.t_memo_letter.FromSql($"select * from t_memo_letter where IS_TRANSFER <> 'Y' and MEMO_STATUS = 'Printed' and POLICY_STATUS = 'Underwriting in Progress'").ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_MEMO_LETTER>> GetTempMemoLetterUpdateStatus()
        {
            try
            {
                return await _dbContext.t_memo_letter.FromSql($"select* from t_memo_letter where IS_TRANSFER = 'Y' and (MEMO_STATUS <> 'Printed' or POLICY_STATUS <> 'Underwriting in Progress')").ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
        
        public async Task<List<T_CONTRACT_MASTER>> GetContractMasterByPolicyCode(List<string> policyCodeList)
        {
            List<T_CONTRACT_MASTER> response = new List<T_CONTRACT_MASTER>();

            try
            {
                if (policyCodeList.Any())
                {
                    if (policyCodeList.Count > 1000)
                    {
                        int totalRow = policyCodeList.Count;
                        var round = Math.Ceiling((decimal)policyCodeList.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyCodeList.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select  m.policy_id,m.policy_code,m.submission_date,m.liability_state,m.end_cause,m.issue_date from t_contract_master m WHERE policy_code in ({queryParam})";

                            var queryResponse = await _dbContext.t_contract_master.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyCodeList.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select  m.policy_id,m.policy_code,m.submission_date,m.liability_state,m.end_cause,m.issue_date from t_contract_master m WHERE policy_code in ({queryParam})";

                        response = await _dbContext.t_contract_master.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_CONTRACT_MASTER>> GetContractMasterBy(Expression<Func<T_CONTRACT_MASTER, bool>> expression)
        {
            try
            {
                var queryString = $"select m.policy_id,m.policy_code,m.submission_date,m.liability_state,m.end_cause,m.issue_date from t_contract_master m";
                return await _dbContext.t_contract_master.FromSql(queryString).Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_CONTRACT_PRODUCT>> GetContractProductByPolicyID(List<int> policyIDList)
        {
            List<T_CONTRACT_PRODUCT> response = new List<T_CONTRACT_PRODUCT>();
            try
            {
                if (policyIDList.Any())
                {
                    if (policyIDList.Count > 1000)
                    {
                        int totalRow = policyIDList.Count;
                        var round = Math.Ceiling((decimal)policyIDList.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyIDList.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select p.policy_id,p.issue_agent,p.total_prem_af from t_contract_product p where p.policy_id in ({queryParam})";

                            var queryResponse = await _dbContext.t_contract_product.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyIDList.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select p.policy_id,p.issue_agent,p.total_prem_af from t_contract_product p where p.policy_id in ({queryParam})";

                        response = await _dbContext.t_contract_product.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_CONTRACT_PROPOSAL>> GetContractProposalByPolicyID(List<int> policyIDList)
        {
            List<T_CONTRACT_PROPOSAL> response = new List<T_CONTRACT_PROPOSAL>();

            try
            {
                if (policyIDList.Any())
                {
                    if (policyIDList.Count > 1000)
                    {
                        int totalRow = policyIDList.Count;
                        var round = Math.Ceiling((decimal)policyIDList.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyIDList.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select p.policy_id,p.prospect_producer,p.proposal_status,p.pending_cause,p.update_time  from t_contract_proposal p where p.policy_id in ({queryParam})";

                            var queryResponse = await _dbContext.t_contract_proposal.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyIDList.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select p.policy_id,p.prospect_producer,p.proposal_status,p.pending_cause,p.update_time  from t_contract_proposal p where p.policy_id in ({queryParam})";

                        response = await _dbContext.t_contract_proposal.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<T_FMS_RATETABLE> GetPeriod(int policyID)
        {
            try
            {
                var queryString = $"select sys_guid() as ID,result_value_1 from t_fms_ratetable where column_value = (select CONCAT(col_value_id, '-*-*') from t_fms_rt_col_value where rt_column_id = 'CRSPK_100000000000484' and col_value = (select p.product_id from t_contract_master m join t_contract_product p on m.policy_id = p.policy_id and p.master_id is null where m.policy_id ={policyID}));";

                var data = await _dbContext.T_FMS_RATETABLE.FromSql(queryString).ToListAsync();
                return data.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_PENDING_CODE>> GetPendingCode()
        {
            try
            {
                var queryString = $"select pending_id,pend_cause from t_pending_code";
                return await _dbContext.T_PENDING_CODE.FromSql(queryString).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<API> GetAPI(int policyID)
        {
            try
            {
                var queryString = $"select distinct p.policy_id, sum(p.total_prem_af) as Polprem from t_contract_product p where p.policy_id = {policyID} group by policy_id;";

                var data = await _dbContext.API.FromSql(queryString).ToListAsync();
                return data.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }


        public async Task<bool> UpdateTransferFlag(List<T_MEMO_LETTER> data, string userID)
        {
            try
            {
                var connectionOracle = appSettingHelper.GetConfiguration("ConnectionStrings:eBaoTempDBOR");

                using (OracleConnection con = new OracleConnection(connectionOracle))
                {
                    con.Open();
                    OracleCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    //cmd.CommandText = $"UPDATE t_memo_letter SET is_transfer = 'N' WHERE document_no = '2110050000015'";
                    //cmd.ExecuteNonQuery();

                    foreach (var itemData in data)
                    {
                        cmd.CommandText = $"UPDATE t_memo_letter SET is_transfer = 'Y',update_user = '{userID}',update_date = '{Utility.GetDateNowThai().Date.ToString("dd-MMM-yyyy")}' WHERE document_no = '{itemData.DOCUMENT_NO}'";
                        await cmd.ExecuteNonQueryAsync();
                    }

                    con.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateTransferFlagPeritem(T_MEMO_LETTER itemData, string userID)
        {
            try
            {
                var connectionOracle = appSettingHelper.GetConfiguration("ConnectionStrings:eBaoTempDBOR");

                using (OracleConnection con = new OracleConnection(connectionOracle))
                {
                    con.Open();
                    OracleCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    //cmd.CommandText = $"UPDATE t_memo_letter SET is_transfer = 'N' WHERE document_no = '2110050000015'";
                    //cmd.ExecuteNonQuery();

                    cmd.CommandText = $"UPDATE t_memo_letter SET is_transfer = 'Y',update_user = '{userID}',update_date = '{Utility.GetDateNowThai().Date.ToString("dd-MMM-yyyy")}' WHERE document_no = '{itemData.DOCUMENT_NO}'";
                    await cmd.ExecuteNonQueryAsync();


                    con.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateMemoStatus(List<T_MEMO_LETTER> data, string userID, string memoStatus)
        {
            try
            {
                var connectionOracle = appSettingHelper.GetConfiguration("ConnectionStrings:eBaoTempDBOR");

                using (OracleConnection con = new OracleConnection(connectionOracle))
                {
                    con.Open();
                    OracleCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;

                    foreach (var itemData in data)
                    {
                        cmd.CommandText = $"UPDATE t_memo_letter SET MEMO_STATUS = '{memoStatus}',update_user = '{userID}',update_date = '{Utility.GetDateNowThai().Date.ToString("dd-MMM-yyyy")}' WHERE document_no = '{itemData.DOCUMENT_NO}'";
                        await cmd.ExecuteNonQueryAsync();
                    }

                    con.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateTransferFlagN(string documentNo)
        {
            try
            {
                var connectionOracle = appSettingHelper.GetConfiguration("ConnectionStrings:eBaoTempDBOR");

                using (OracleConnection con = new OracleConnection(connectionOracle))
                {
                    con.Open();
                    OracleCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = $"UPDATE t_memo_letter SET is_transfer = 'N', MEMO_STATUS = 'Printed' WHERE document_no = '{documentNo}'";
                    cmd.ExecuteNonQuery();

                    con.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<bool> UpdateTempTable(UpdateTempRequest request)
        {
            try
            {
                var connectionOracle = appSettingHelper.GetConfiguration("ConnectionStrings:eBaoTempDBOR");

                using (OracleConnection con = new OracleConnection(connectionOracle))
                {
                    con.Open();
                    OracleCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = $"UPDATE t_memo_letter SET is_transfer = '{request.isTransfer}', MEMO_STATUS = '{request.memoStatus}' WHERE document_no = '{request.documentNo}'";
                    cmd.ExecuteNonQuery();

                    con.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_DOCUMENT_JOIN>> GetDocumentJoin(List<string> documentList)
        {
            List<T_DOCUMENT_JOIN> response = new List<T_DOCUMENT_JOIN>();
            try
            {
                if (documentList.Any())
                {
                    if (documentList.Count > 1000)
                    {
                        int totalRow = documentList.Count;
                        var round = Math.Ceiling((decimal)documentList.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = documentList.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select td.document_no,t.template_name from t_document td join t_template t on t.template_id = td.template_id where td.template_id in  ('10018','1') and td.document_no in ({queryParam})";

                            var queryResponse = await _dbContext.T_DOCUMENT_JOIN.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = documentList.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select td.document_no,t.template_name from t_document td join t_template t on t.template_id = td.template_id where td.template_id in  ('10018','1') and td.document_no in ({queryParam})";

                        response = await _dbContext.T_DOCUMENT_JOIN.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_AGENT>> GetAgent(List<string> agentCode)
        {
            try
            {
                if (agentCode.Any())
                {
                    var queryParam = agentCode.Select(d => "'" + d + "'").Join(",");
                    var queryString = $"select a.agent_code, a.med_indi from t_agent a where a.agent_code in ({queryParam})";
                    return await _dbContext.T_AGENT.FromSql(queryString).ToListAsync();
                }
                else
                {
                    return new List<T_AGENT>();
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_CONTRACT_MASTER_JOIN>> GetContractMasterJoin(List<string> policyCode)
        {
            List<T_CONTRACT_MASTER_JOIN> response = new List<T_CONTRACT_MASTER_JOIN>();

            try
            {
                if (policyCode.Any())
                {
                    if (policyCode.Count > 1000)
                    {
                        int totalRow = policyCode.Count;
                        var round = Math.Ceiling((decimal)policyCode.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyCode.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select a.policy_code, c.internal_id, c.product_name, c.ins_type, b.renewal_type from t_contract_master a " +
                                $"left outer join t_contract_product b on b.policy_id = a.policy_id " +
                                $"left outer join t_product_life c on b.product_id = c.product_id " +
                                $"where a.policy_code in ({queryParam})";

                            var queryResponse = await _dbContext.T_CONTRACT_MASTER_JOIN.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyCode.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select a.policy_code, c.internal_id, c.product_name, c.ins_type, b.renewal_type from t_contract_master a " +
                    $"left outer join t_contract_product b on b.policy_id = a.policy_id " +
                    $"left outer join t_product_life c on b.product_id = c.product_id " +
                    $"where a.policy_code in ({queryParam})";

                        response = await _dbContext.T_CONTRACT_MASTER_JOIN.FromSql(queryString).ToListAsync();
                    }
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_DOCUMENT>> GetDocumentData(List<string> documentList)
        {
            List<T_DOCUMENT> response = new List<T_DOCUMENT>();
            try
            {
                if (documentList.Any())
                {
                    if (documentList.Count > 1000)
                    {
                        int totalRow = documentList.Count;
                        var round = Math.Ceiling((decimal)documentList.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = documentList.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select d.document_no,d.insert_time, d.end_time from T_DOCUMENT d where d.document_no in ({queryParam})";

                            var queryResponse = await _dbContext.T_DOCUMENT.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = documentList.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select d.document_no,d.insert_time, d.end_time from T_DOCUMENT d where d.document_no in ({queryParam})";

                        response = await _dbContext.T_DOCUMENT.FromSql(queryString).ToListAsync();
                    }
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_INSURED_LIST>> GetInsuredList(List<int> policyID)
        {
            List<T_INSURED_LIST> response = new List<T_INSURED_LIST>();

            try
            {
                if (policyID.Any())
                {
                    if (policyID.Count > 1000)
                    {
                        int totalRow = policyID.Count;
                        var round = Math.Ceiling((decimal)policyID.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyID.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select list_id,policy_id, stand_life from t_insured_list where policy_id in ({queryParam})";

                            var queryResponse = await _dbContext.T_INSURED_LIST.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyID.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select list_id,policy_id, stand_life from t_insured_list where policy_id in ({queryParam})";

                        response = await _dbContext.T_INSURED_LIST.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_UW_EXCLUSION>> GetUWExclusionList(List<int> policyID)
        {
            List<T_UW_EXCLUSION> response = new List<T_UW_EXCLUSION>();

            try
            {
                if (policyID.Any())
                {
                    if (policyID.Count > 1000)
                    {
                        int totalRow = policyID.Count;
                        var round = Math.Ceiling((decimal)policyID.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyID.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select sys_guid() as ID, policy_id,exclusion_code from t_uw_exclusion where policy_id in ({queryParam})";

                            var queryResponse = await _dbContext.T_UW_EXCLUSION.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyID.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select sys_guid() as ID, policy_id,exclusion_code from t_uw_exclusion where policy_id in ({queryParam})";

                        response = await _dbContext.T_UW_EXCLUSION.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<T_CASH> GetCashRecieved(int policyID)
        {
            try
            {
                var queryString = $"select sys_guid() as ID, SUM(c.pay_balance) AS amount_recieved from t_cash c where c.FEE_STATUS = '1' and c.FEE_TYPE = '11' and c.prem_purpose = '1' and policy_id = {policyID}";

                var data = await _dbContext.T_CASH.FromSql(queryString).ToListAsync();
                return data.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_UW_POLICY>> GetUWPolicyList(List<int> policyID)
        {
            List<T_UW_POLICY> response = new List<T_UW_POLICY>();

            try
            {
                if (policyID.Any())
                {
                    if (policyID.Count > 1000)
                    {
                        int totalRow = policyID.Count;
                        var round = Math.Ceiling((decimal)policyID.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyID.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select sys_guid() as ID, p.policy_id, p.underwrite_time from t_uw_policy p where p.policy_id in ({queryParam})";

                            var queryResponse = await _dbContext.T_UW_POLICY.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyID.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select sys_guid() as ID, p.policy_id, p.underwrite_time from t_uw_policy p where p.policy_id in ({queryParam})";

                        response = await _dbContext.T_UW_POLICY.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<T_BUSINESS_UNIT_BRANCH>> GetBranchBy(Expression<Func<T_BUSINESS_UNIT_BRANCH, bool>> expression)
        {
            try
            {

                var queryString = $"select * from t_business_unit_branch";
                return await _dbContext.T_BUSINESS_UNIT_BRANCH.FromSql(queryString).Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_PENDING_CAUSE_POLICY>> GetPendingCausePolicyList(List<int> policyID)
        {
            try
            {
                if (policyID.Any())
                {
                    var queryParam = policyID.Select(d => "'" + d.ToString() + "'").Join(",");
                    var queryString = $"select a.policy_id, b.policy_code, unp.UW_PENDING, c.pending_id, c.pend_cause from t_contract_proposal a " +
                        $"left outer join t_Contract_Master b on b.policy_id = a.policy_id " +
                        $"left outer join t_uw_policy unp on unp.policy_id = a.policy_id " +
                        $"left outer join t_pending_code c on c.pending_id = unp.UW_PENDING " +
                        $"where a.policy_id in ({queryParam})";

                    return await _dbContext.T_PENDING_CAUSE_POLICY.FromSql(queryString).ToListAsync();
                }
                else
                {
                    return new List<T_PENDING_CAUSE_POLICY>();
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public T_MASTER_POLICY GetMasterPolicy(string policyNo)
        {
            try
            {
                var queryString = $"select b.master_policy_no, a.policy_code from t_Contract_Master a " +
                    $"join t_master_policy b on b.master_policy_id = a.master_policy_id" +
                    $"where a.policy_Code = '{policyNo}'";

                return _dbContext.T_MASTER_POLICY.FromSql(queryString).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Pending_Summary_Rawdata>> GetPendingSummaryData(DateTime submissionDateFrom, DateTime? submissionDateTo)
        {
            try
            {
                string subnitdateFromStr = submissionDateFrom.ToString("yyyyMMdd");

                string query = $"SELECT DISTINCT " +
                    $"m.submission_date, m.policy_code, m.policy_id, m.liability_state,m.END_CAUSE,m.issue_date , c.pending_id, c.pend_cause, p.renewal_type ,bu.unit_desc, us.user_name, '' as productCategory, " +
                    $"(select sum(p.total_prem_af) as Polprem from t_contract_product p where p.policy_id = m.policy_id group by policy_id) total_prem_af, " +
                    $"(CASE WHEN M.Institute_code is null and p.renewal_type = '4' " +
                    $"THEN 1 " +
                    $"ELSE  0 END) Institute_code," +
                    $"(CASE WHEN p.renewal_type = 2  " +
                    $"THEN((select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) * 2) " +
                    $"WHEN p.renewal_type = 3 THEN((select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) * 4) " +
                    $"WHEN p.renewal_type = 4 THEN((select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) * 12) " +
                    $"ELSE(select distinct sum(p.total_prem_af)from t_contract_product p where p.policy_id = m.policy_id group by policy_id) END) APE," +
                    $"(CASE WHEN M.LIABILITY_STATE = 0 THEN (SELECT T_PROPOSAL_STATUS.PROPOSAL_STATUS FROM T_CONTRACT_PROPOSAL " +
                    $"INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) " +
                    $"ELSE  (SELECT T_LIABILITY_STATUS.STATUS_ID FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE)  END) PROPOSAL_STATUS, " +
                    $"(CASE WHEN M.LIABILITY_STATE = 0 THEN (SELECT T_PROPOSAL_STATUS.STATUS_DESC FROM T_CONTRACT_PROPOSAL " +
                    $"INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) " +
                    $"ELSE (SELECT T_LIABILITY_STATUS.STATUS_NAME FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE) END) STATUS_DESC " +
                    $"FROM T_CONTRACT_MASTER M " +
                    $"INNER JOIN T_CONTRACT_PRODUCT P ON M.POLICY_ID = P.POLICY_ID " +
                    $"INNER JOIN T_AGENT A ON M.SERVICE_AGENT = A.AGENT_ID " +
                    $"left outer join (SELECT distinct policy_id, UW_PENDING FROM t_uw_policy Where UNDERWRITE_ID in (SELECT max(UNDERWRITE_ID) from t_uw_policy group by policy_id )) unp on unp.policy_id = m.policy_id " +
                    $"left outer join t_pending_code c on c.pending_id = unp.UW_PENDING " +
                    $"LEFT JOIN T_CUSTOMER Ca ON A.AGENT_ID = Ca.CUSTOMER_ID " +
                    $"LEFT JOIN T_COMPANY_CUSTOMER CO_INF ON M.SERVICE_AGENT = CO_INF.COMPANY_ID " +
                    $"LEFT JOIN T_CASH C ON P.POLICY_ID = C.POLICY_ID " +
                    $"AND C.PREM_PURPOSE = 1 " +
                    $"AND EXISTS(SELECT Cs.Policy_Id, MIN(Cs.Insert_Time) as Insert_Time " +
                    $"FROM T_CASH Cs WHERE Cs.Prem_Purpose = 1 AND Policy_Id = C.POLICY_ID GROUP BY Cs.Policy_Id HAVING MIN(Cs.Insert_Time) = C.Insert_Time) " +
                    $"left outer join(select a.* from T_INSURED_LIST a " +
                    $"where relation_to_ph = (select max(relation_to_ph) from T_INSURED_LIST where policy_id = a.policy_id  ) and STAND_LIFE = 'Y' ) I " +
                    $"ON M.POLICY_ID = I.POLICY_ID " +
                    $"INNER JOIN T_PARTY Pt ON I.PARTY_ID = Pt.Party_Id " +
                    $"INNER JOIN T_CUSTOMER Ci ON Pt.PARTY_ID = Ci.CUSTOMER_ID " +
                    $"INNER JOIN T_CHARGE_MODE G ON P.RENEWAL_TYPE = G.CHARGE_TYPE " +
                    $"inner join t_user us on m.updated_by = us.user_id " +
                    $"INNER JOIN T_PRODUCT_LIFE L ON P.PRODUCT_ID = L.PRODUCT_ID AND L.INS_TYPE = 1 " +
                    $"LEFT JOIN T_INSTITUTE_CODE TIC ON TIC.INSTITUTE_CODE = M.INSTITUTE_CODE " +
                    $"LEFT JOIN T_BUSINESS_UNIT Bu ON M.BUSINESS_UNIT = Bu.BUSINESS_UNIT WHERE M.LIABILITY_STATE <> 2 ";

                if (submissionDateTo.HasValue)
                {
                    string subnitdateToStr = submissionDateTo.GetValueOrDefault().ToString("yyyyMMdd");
                    query += $"and to_char(m.submission_date, 'yyyymmdd') >= '{subnitdateFromStr}' and to_char(m.submission_date, 'yyyymmdd') <= '{subnitdateToStr}'";
                }
                else
                {
                    query += $"and to_char(m.submission_date, 'yyyymmdd') = '{subnitdateFromStr}'";
                }

                return await _dbContext.Pending_Summary_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Institute_code>> GetInstitute_code(List<int> policyID)
        {
            List<Institute_code> response = new List<Institute_code>();

            try
            {
                if (policyID.Any())
                {
                    if (policyID.Count > 1000)
                    {
                        int totalRow = policyID.Count;
                        var round = Math.Ceiling((decimal)policyID.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyID.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"select a.policy_id,a.policy_Code from t_contract_master a  " +
                        $"join t_contract_product b on b.policy_id = a.policy_id " +
                        $"where a.Institute_code is null and b.renewal_type = '4' and a.policy_id in ({queryParam})";

                            var queryResponse = await _dbContext.Institute_code.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyID.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"select a.policy_id,a.policy_Code from t_contract_master a  " +
                        $"join t_contract_product b on b.policy_id = a.policy_id " +
                        $"where a.Institute_code is null and b.renewal_type = '4' and a.policy_id in ({queryParam})";

                        response = await _dbContext.Institute_code.FromSql(queryString).ToListAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public async Task<List<Pending_Rawdata>> GetPendingDataAllSubmit(PendingReportRequest request)
        {
            try
            {
                string asOfDateStr = request.asOfDate.ToString("yyyyMMdd");
                string query = queryPending + $"WHERE M.LIABILITY_STATE <> 2 and to_char(m.submission_date, 'yyyymmdd') = '{asOfDateStr}'";

                //if (!string.IsNullOrWhiteSpace(request.productCategory))
                //{
                //    if(request.productCategory == "Individual")
                //    {
                //        query += " and SUBSTR(m.POLICY_CODE, 3, 1) not in ('2','3','4')";
                //    }
                //    else if (request.productCategory == "Srisawad" || request.productCategory == "SECAP")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc = '{request.productCategory}'"; 
                //    }
                //    else if (request.productCategory == "MRTA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc not in ('Srisawad','SECAP')";
                //    }
                //    else if (request.productCategory == "Group 1 Year Term")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('3')";
                //    }
                //    else if (request.productCategory == "PA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('4')";
                //    }
                //}

                return await _dbContext.Pending_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Pending_Rawdata>> GetPendingDataOtherData(PendingReportRequest request)
        {
            try
            {
                string asOfDateStr = request.asOfDate.ToString("dd/MM/yyyy");
                string query = queryPending + $"where trunc(M.SUBMISSION_DATE) < TO_DATE('{asOfDateStr}', 'DD/MM/YYYY') " +
                    $"and m.liability_state = 0 and(q.proposal_status = 10 or  q.proposal_status = 11 or  q.proposal_status = 20 or  q.proposal_status = 21 or  q.proposal_status = 31 or  q.proposal_status = 40)";

                //if (!string.IsNullOrWhiteSpace(request.productCategory))
                //{
                //    if (request.productCategory == "Individual")
                //    {
                //        query += " and SUBSTR(m.POLICY_CODE, 3, 1) not in ('2','3','4')";
                //    }
                //    else if (request.productCategory == "Srisawad" || request.productCategory == "SECAP")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc = '{request.productCategory}'";
                //    }
                //    else if (request.productCategory == "MRTA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc not in ('Srisawad','SECAP')";
                //    }
                //    else if (request.productCategory == "Group 1 Year Term")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('3')";
                //    }
                //    else if (request.productCategory == "PA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('4')";
                //    }
                //}

                return await _dbContext.Pending_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Pending_Rawdata>> GetPendingDataConditionAccept(PendingReportRequest request)
        {
            try
            {
                string asOfDateStr = request.asOfDate.ToString("dd/MM/yyyy");
                string query = queryPending +
                   $"where trunc(M.SUBMISSION_DATE) < TO_DATE('{asOfDateStr}', 'DD/MM/YYYY') " +
                    $"and m.liability_state = 0 and (q.proposal_status = 81)";

                //if (!string.IsNullOrWhiteSpace(request.productCategory))
                //{
                //    if (request.productCategory == "Individual")
                //    {
                //        query += " and SUBSTR(m.POLICY_CODE, 3, 1) not in ('2','3','4')";
                //    }
                //    else if (request.productCategory == "Srisawad" || request.productCategory == "SECAP")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc = '{request.productCategory}'";
                //    }
                //    else if (request.productCategory == "MRTA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc not in ('Srisawad','SECAP')";
                //    }
                //    else if (request.productCategory == "Group 1 Year Term")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('3')";
                //    }
                //    else if (request.productCategory == "PA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('4')";
                //    }
                //}

                return await _dbContext.Pending_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Pending_Rawdata>> GetPendingDataAccept(PendingReportRequest request)
        {
            try
            {
                string asOfDateStr = request.asOfDate.ToString("dd/MM/yyyy");
                string query = queryPending +
                   $"where trunc(M.SUBMISSION_DATE) < TO_DATE('{asOfDateStr}', 'DD/MM/YYYY') " +
                    $"and m.liability_state = 0 and (q.proposal_status = 80)";

                //if (!string.IsNullOrWhiteSpace(request.productCategory))
                //{
                //    if (request.productCategory == "Individual")
                //    {
                //        query += " and SUBSTR(m.POLICY_CODE, 3, 1) not in ('2','3','4')";
                //    }
                //    else if (request.productCategory == "Srisawad" || request.productCategory == "SECAP")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc = '{request.productCategory}'";
                //    }
                //    else if (request.productCategory == "MRTA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc not in ('Srisawad','SECAP')";
                //    }
                //    else if (request.productCategory == "Group 1 Year Term")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('3')";
                //    }
                //    else if (request.productCategory == "PA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('4')";
                //    }
                //}

                return await _dbContext.Pending_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Pending_Rawdata>> GetPendingDataUnderwritingInprogress(PendingReportRequest request)
        {
            try
            {
                string asOfDateStr = request.asOfDate.ToString("dd/MM/yyyy");
                string query = queryPending +
                    $"where trunc(M.SUBMISSION_DATE) < TO_DATE('{asOfDateStr}', 'DD/MM/YYYY') " +
                    $"and m.liability_state = 0 and (q.proposal_status = 32)";

                //if (!string.IsNullOrWhiteSpace(request.productCategory))
                //{
                //    if (request.productCategory == "Individual")
                //    {
                //        query += " and SUBSTR(m.POLICY_CODE, 3, 1) not in ('2','3','4')";
                //    }
                //    else if (request.productCategory == "Srisawad" || request.productCategory == "SECAP")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc = '{request.productCategory}'";
                //    }
                //    else if (request.productCategory == "MRTA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc not in ('Srisawad','SECAP')";
                //    }
                //    else if (request.productCategory == "Group 1 Year Term")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('3')";
                //    }
                //    else if (request.productCategory == "PA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('4')";
                //    }
                //}

                return await _dbContext.Pending_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Pending_Rawdata>> GetPendingDataUpdateAt(PendingReportRequest request)
        {
            try
            {
                string asOfDateStr = request.asOfDate.ToString("dd/MM/yyyy");
                string query = queryPending +
                   $"where trunc(n.UPDATE_TIME) = TO_DATE('{asOfDateStr}','DD/MM/YYYY')";

                //if (!string.IsNullOrWhiteSpace(request.productCategory))
                //{
                //    if (request.productCategory == "Individual")
                //    {
                //        query += " and SUBSTR(m.POLICY_CODE, 3, 1) not in ('2','3','4')";
                //    }
                //    else if (request.productCategory == "Srisawad" || request.productCategory == "SECAP")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc = '{request.productCategory}'";
                //    }
                //    else if (request.productCategory == "MRTA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc not in ('Srisawad','SECAP')";
                //    }
                //    else if (request.productCategory == "Group 1 Year Term")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('3')";
                //    }
                //    else if (request.productCategory == "PA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('4')";
                //    }
                //}

                return await _dbContext.Pending_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<Pending_Rawdata>> GetPendingDataUpdateLiabilityAt(PendingReportRequest request)
        {
            try
            {
                string asOfDateStr = request.asOfDate.ToString("dd/MM/yyyy");
                string query = queryPending +
                   $"where trunc(m.LIABILITY_STATE_DATE) = TO_DATE('{asOfDateStr}','DD/MM/YYYY')";

                //if (!string.IsNullOrWhiteSpace(request.productCategory))
                //{
                //    if (request.productCategory == "Individual")
                //    {
                //        query += " and SUBSTR(m.POLICY_CODE, 3, 1) not in ('2','3','4')";
                //    }
                //    else if (request.productCategory == "Srisawad" || request.productCategory == "SECAP")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc = '{request.productCategory}'";
                //    }
                //    else if (request.productCategory == "MRTA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('2') and bu.unit_desc not in ('Srisawad','SECAP')";
                //    }
                //    else if (request.productCategory == "Group 1 Year Term")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('3')";
                //    }
                //    else if (request.productCategory == "PA")
                //    {
                //        query += $" and SUBSTR(m.POLICY_CODE, 3, 1) in ('4')";
                //    }
                //}

                return await _dbContext.Pending_Rawdata.FromSql(query).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<PolicyStatus>> GetPolicyStatus(List<string> policyCode)
        {
            List<PolicyStatus> response = new List<PolicyStatus>();

            try
            {
                if (policyCode.Any())
                {
                    if (policyCode.Count > 1000)
                    {
                        int totalRow = policyCode.Count;
                        var round = Math.Ceiling((decimal)policyCode.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyCode.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            var queryString = $"SELECT m.policy_code, (CASE WHEN M.LIABILITY_STATE = 0 THEN (SELECT T_PROPOSAL_STATUS.PROPOSAL_STATUS  " +
                                $"FROM T_CONTRACT_PROPOSAL INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS " +
                                $"WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) " +
                                $"ELSE(SELECT T_LIABILITY_STATUS.STATUS_ID FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE)  END) PROPOSAL_STATUS, " +
                                $"(CASE WHEN M.LIABILITY_STATE = 0 THEN(SELECT T_PROPOSAL_STATUS.STATUS_DESC FROM T_CONTRACT_PROPOSAL " +
                                $"INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS " +
                                $"WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) ELSE(SELECT T_LIABILITY_STATUS.STATUS_NAME " +
                                $"FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE) END) STATUS_DESC " +
                                $"FROM T_CONTRACT_MASTER M INNER JOIN T_CONTRACT_PRODUCT P ON M.POLICY_ID = P.POLICY_ID " +
                                $"Where M.policy_code in ({queryParam})";

                            var queryResponse = await _dbContext.PolicyStatus.FromSql(queryString).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyCode.Select(d => "'" + d + "'").Join(",");
                        var queryString = $"SELECT m.policy_code, (CASE WHEN M.LIABILITY_STATE = 0 THEN (SELECT T_PROPOSAL_STATUS.PROPOSAL_STATUS  " +
                                $"FROM T_CONTRACT_PROPOSAL INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS " +
                                $"WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) " +
                                $"ELSE(SELECT T_LIABILITY_STATUS.STATUS_ID FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE)  END) PROPOSAL_STATUS, " +
                                $"(CASE WHEN M.LIABILITY_STATE = 0 THEN(SELECT T_PROPOSAL_STATUS.STATUS_DESC FROM T_CONTRACT_PROPOSAL " +
                                $"INNER JOIN T_PROPOSAL_STATUS ON T_CONTRACT_PROPOSAL.PROPOSAL_STATUS = T_PROPOSAL_STATUS.PROPOSAL_STATUS " +
                                $"WHERE T_CONTRACT_PROPOSAL.POLICY_ID = M.POLICY_ID) ELSE(SELECT T_LIABILITY_STATUS.STATUS_NAME " +
                                $"FROM T_LIABILITY_STATUS WHERE T_LIABILITY_STATUS.STATUS_ID = M.LIABILITY_STATE) END) STATUS_DESC " +
                                $"FROM T_CONTRACT_MASTER M INNER JOIN T_CONTRACT_PRODUCT P ON M.POLICY_ID = P.POLICY_ID " +
                                $"Where M.policy_code in ({queryParam})";

                        response = await _dbContext.PolicyStatus.FromSql(queryString).ToListAsync();
                    }
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
            return response;
        }

        public async Task<List<DataNotInMemo>> GetDataNotInMemo(List<string> policyCode)
        {
            List<DataNotInMemo> response = new List<DataNotInMemo>();

            try
            {
                if (policyCode.Any())
                {
                    if (policyCode.Count > 1000)
                    {
                        int totalRow = policyCode.Count;
                        var round = Math.Ceiling((decimal)policyCode.Count / 1000);

                        for (int i = 0; i < round; i++)
                        {
                            int getTotal = 0;
                            if (totalRow >= 1000)
                            {
                                getTotal = 1000;
                            }
                            else
                            {
                                getTotal = totalRow;
                            }
                            var policyQuery = policyCode.GetRange(1000 * i, getTotal);

                            var queryParam = policyQuery.Select(d => "'" + d + "'").Join(",");
                            string query = $"SELECT cm.policy_code,cm.insert_time,hd.PolicyHoderName  ProposerName,li.insuredName LifeAssuredName,li.Age LifeAssuredAge, " +
                                $"td.document_no   ,u.user_name ,td.update_time " +
                            $"FROM t_contract_master cm " +
                            $"inner join t_contract_proposal cps on cm.policy_id = cps.policy_id " +
                            $"inner join t_user u on cm.updated_by = u.user_id " +
                            $"left join(select nvl(t.title_desc,'') || ' ' || cu.first_name || ' ' || cu.last_name as PolicyHoderName ,h.policy_id " +
                            $"from t_policy_holder h inner join t_customer cu on h.party_id = cu.customer_id " +
                            $"left join t_title t on cu.title = t.title_code and cu.gender = t.gender) hd on  hd.policy_id = cm.policy_id " +
                            $"left join(select nvl(t.title_desc,'') || ' ' || cu.first_name || ' ' || cu.last_name as insuredName ,   trunc(months_between(sysdate, cu.birthday) / 12) Age,cu.birthday , il.policy_id " +
                            $"from t_insured_list il inner join t_customer cu  on il.party_id = cu.customer_id  and il.active_status = 1 " +
                            $"left join t_title t on cu.title = t.title_code and cu.gender = t.gender) li " +
                            $"on   li.policy_id = cm.policy_id " +
                            $"left join t_document td on cm.policy_id = td.policy_id " +
                            $"WHERE cm.policy_code in ({queryParam})";

                            var queryResponse = await _dbContext.DataNotInMemo.FromSql(query).ToListAsync();
                            response.AddRange(queryResponse);

                            totalRow -= 1000;
                        }
                    }
                    else
                    {
                        var queryParam = policyCode.Select(d => "'" + d + "'").Join(",");
                        string query = $"SELECT cm.policy_code,cm.insert_time,hd.PolicyHoderName  ProposerName,li.insuredName LifeAssuredName,li.Age LifeAssuredAge, " +
                            $"td.document_no   ,u.user_name ,td.update_time " +
                            $"FROM t_contract_master cm " +
                            $"inner join t_contract_proposal cps on cm.policy_id = cps.policy_id " +
                            $"inner join t_user u on cm.updated_by = u.user_id " +
                            $"left join(select nvl(t.title_desc,'') || ' ' || cu.first_name || ' ' || cu.last_name as PolicyHoderName ,h.policy_id " +
                            $"from t_policy_holder h inner join t_customer cu on h.party_id = cu.customer_id " +
                            $"left join t_title t on cu.title = t.title_code and cu.gender = t.gender) hd on  hd.policy_id = cm.policy_id " +
                            $"left join(select nvl(t.title_desc,'') || ' ' || cu.first_name || ' ' || cu.last_name as insuredName ,   trunc(months_between(sysdate, cu.birthday) / 12) Age,cu.birthday , il.policy_id " +
                            $"from t_insured_list il inner join t_customer cu  on il.party_id = cu.customer_id  and il.active_status = 1 " +
                            $"left join t_title t on cu.title = t.title_code and cu.gender = t.gender) li " +
                            $"on   li.policy_id = cm.policy_id " +
                            $"left join t_document td on cm.policy_id = td.policy_id " +
                            $"WHERE cm.policy_code in ({queryParam})";

                        response = await _dbContext.DataNotInMemo.FromSql(query).ToListAsync();
                    }
                }

                return response;

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
    }
}
