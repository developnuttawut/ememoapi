﻿using eMemo.Model.Entity.AMLSStaging;
using eMemo.Repositories.DBContext;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class AMLSStagingRepository
    {
        private readonly AMLSStagingDBContext _dbContext;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(AMLSStagingRepository));
        public AMLSStagingRepository(AMLSStagingDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<AM_TB_MS_CHANNEL>> GetChannel(Expression<Func<AM_TB_MS_CHANNEL, bool>> expression)
        {
            try
            {
                return await _dbContext.AM_TB_MS_CHANNEL.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<AM_TB_MS_CHANNEL_BK>> GetChannelBK(Expression<Func<AM_TB_MS_CHANNEL_BK, bool>> expression)
        {
            try
            {
                return await _dbContext.AM_TB_MS_CHANNEL_BK.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<AM_TB_MS_SUB_CHANNEL>> GetSubChannel(Expression<Func<AM_TB_MS_SUB_CHANNEL, bool>> expression)
        {
            try
            {
                return await _dbContext.AM_TB_MS_SUB_CHANNEL.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<AM_TB_MS_SUB_CHANNEL_BK>> GetSubChannelBK(Expression<Func<AM_TB_MS_SUB_CHANNEL_BK, bool>> expression)
        {
            try
            {
                return await _dbContext.AM_TB_MS_SUB_CHANNEL_BK.Where(expression).AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<AM_TB_MS_SEG_BRANCH>> GetSEGBranch()
        {
            try
            {
                return await _dbContext.AM_TB_MS_SEG_BRANCH.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
    }
}
