﻿using eMemo.Model.Entity.eBaoLSStaging;
using eMemo.Repositories.DBContext;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class EbaoLSStagingRepository
    {
        private readonly eBaoLSStagingDBContext _dbContext;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(EbaoLSStagingRepository));
        public EbaoLSStagingRepository(eBaoLSStagingDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<T_BUSINESS_UNIT>> GetBusinessUnit()
        {
            try
            {
                return await _dbContext.T_BUSINESS_UNIT.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_BUSINESS_SOURCE>> GetSourceOfBusiness()
        {
            try
            {
                return await _dbContext.T_BUSINESS_SOURCE.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_BUSINESS_UNIT_BRANCH>> GetBranchOffice()
        {
            try
            {
                return await _dbContext.T_BUSINESS_UNIT_BRANCH.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_BUSINESS_UNIT_BK>> GetBusinessUnitBK()
        {
            try
            {
                return await _dbContext.T_BUSINESS_UNIT_BK.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_BUSINESS_SOURCE_BK>> GetSourceOfBusinessBK()
        {
            try
            {
                return await _dbContext.T_BUSINESS_SOURCE_BK.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }

        public async Task<List<T_BUSINESS_UNIT_BRANCH_BK>> GetBranchOfficeBK()
        {
            try
            {
                return await _dbContext.T_BUSINESS_UNIT_BRANCH_BK.AsNoTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                 _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }
        }
    }
}
