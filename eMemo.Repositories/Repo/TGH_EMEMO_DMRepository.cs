﻿using eMemo.Model.Entity.TGH_EMEMO_DM;
using eMemo.Model.ExternalResponse;
using eMemo.Repositories.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class TGH_EMEMO_DMRepository
    {
        private readonly TGH_EMEMO_DMDBContext _dbContext;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(TGH_EMEMO_DMRepository));
        public TGH_EMEMO_DMRepository(TGH_EMEMO_DMDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public T_DM_EMEMO_AGENT_INFO_NEW GetAdditionalData(string agentCode, string policyNo)
        {
            try
            {
                var dataQuery = _dbContext.T_DM_EMEMO_AGENT_INFO_NEW.FromSql($"SELECT NEWID() as ID,* FROM [TGH_EMEMO_DM].[dbo].[T_DM_EMEMO_AGENT_INFO_NEW] Where AGENT_CODE = {agentCode} and POLICY_CODE = {policyNo}").ToList();
                return dataQuery.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return null;
            }
        }

        public T_DM_EMEMO_AGENT_INFO_NEW GetAdditionalDataByPolicyNo(string policyNo)
        {
            try
            {
                var dataQuery = _dbContext.T_DM_EMEMO_AGENT_INFO_NEW.FromSql($"SELECT NEWID() as ID,* FROM [TGH_EMEMO_DM].[dbo].[T_DM_EMEMO_AGENT_INFO_NEW] Where POLICY_CODE = {policyNo}").ToList();
                return dataQuery.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return null;
            }
        }

        public T_DM_EMEMO_AGENT_INFO_NEW GetAdditionalDataBy(Expression<Func<T_DM_EMEMO_AGENT_INFO_NEW, bool>> expression)
        {
            try
            {
                var dataQuery = _dbContext.T_DM_EMEMO_AGENT_INFO_NEW.Where(expression).ToList();
                return dataQuery.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return null;
            }
        }

        public async Task<List<T_DM_EMEMO_AGENT_INFO_NEW>> GetAdditionalDataByPolicyNoList(List<string> policyNo)
        {
            try
            {
                var queryParam = policyNo.Join(",");
                var queryString = $"SELECT NEWID() as ID,* FROM [TGH_EMEMO_DM].[dbo].[T_DM_EMEMO_AGENT_INFO_NEW] Where POLICY_CODE in ({queryParam})";
                return await _dbContext.T_DM_EMEMO_AGENT_INFO_NEW.FromSql(queryString).ToListAsync();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return null;
            }
        }

        public List<T_DM_EMEMO_AGENT_INFO_NEW> GetT_DM_EMEMO_AGENT_INFO_NEWBy(Expression<Func<T_DM_EMEMO_AGENT_INFO_NEW, bool>> expression)
        {
            try
            {
                return _dbContext.T_DM_EMEMO_AGENT_INFO_NEW.Where(expression).ToList();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return null;
            }
        }

        public List<T_DM_EMEMO_AGENT_INFO> GetAgentInfoBy(Expression<Func<T_DM_EMEMO_AGENT_INFO, bool>> expression)
        {
            try
            {
                return _dbContext.T_DM_EMEMO_AGENT_INFO.Where(expression).ToList();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return null;
            }
        }
    }
}
