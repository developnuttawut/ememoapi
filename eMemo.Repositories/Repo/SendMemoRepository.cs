﻿using DinkToPdf.Contracts;
using eMemo.Helper;
using eMemo.Model.Base;
using eMemo.Model.Entity;
using eMemo.Model.Entity.eBaoTemp;
using eMemo.Repositories.DBContext;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace eMemo.Repositories.Repo
{
    public class SendMemoRepository
    {
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(SendMemoRepository));
        private readonly HttpRequestBuilder _requestBuilder;
        private readonly AppSettingHelper _appsettingHelper;
        //private readonly string destinationPath = string.Empty;
        //HelperConfig.GET_API_SMS_PROVIDER;
        private readonly string authCodeForApiOwner = string.Empty;
        //HelperConfig.GET_API_SMS_AUTHEN_OWNER;
        private readonly string baseURIsms = string.Empty;
        //HelperConfig.GET_API_SMS_PROVIDER;
        private readonly string message = string.Empty;
        private readonly string notifiUrl = string.Empty;
        private readonly string smsFrom = string.Empty;
        private readonly IConverter _converter;
        private readonly eMemoDBContext _dbContext;
        private readonly EBAODBContet _ebaoDbContext;
        private List<string> productCo = new List<string>()
        {
                "CCO.10",
                "CCO.11",
                "CCO.12",
                "CCO.13",
                "CCO.14",
                "CCO.15",
                "CCO.16",
                "CCO.17",
                "CCO.18",
                "CCO.19",
                "CCO.21",
                "MR0300",
                "MR0100",
                "MR0100",
                "MR0320",
                "ML0115",
                "MR0300",
                "MR0300",
                "MR0300",
                "MR0110",
                "MR0320",
                "MR0120",
                "MR0110",
                "MR0320",
                "MR0100",
                "MR0300",
                "MR0300",
                "ML0100",
                "MR0300",
                "ML0100",
                "MR0300",
                "ML0100",
                "ML0100",
                "MR0300",
                "ML0100",
                "MR0300",
                "ML0110",
                "ML0100",
                "MR0310",
                "ML0100",
                "MR0110",
                "MR0325",
                "ML0125",
                "MR0320",
                "ML0120",
                "MR0100",
                "ML0100",
                "ML0100",
                "CCO.22D",
                "CCO.23L",
                "CCO.24L",
                "CCO.25D",
                "CCO.26L",
                "CCO.27D",
                "CCO.28D",
                "CCO.29D",
                "CCO.30D",
                "CCO.31D",
                "CCO.32L",
                "CCO.33D",
                "CCO.34D",
                "CCO.35L",
                "CCO.36D",
                "CCO.37D",
                "LSE.01L",
                "CCO.38D",
                "CCO.39L",
                "CCO.40D",
                "CCO.41L",
                "CCO.42L",
                "CCO.43D",
                "CCO.44L",
                "CCO.45D",
                "CCO.46L",
                "CCO.47L",
                "CCO.48D",
                "CCO.49L",
                "CCO.50D",
                "CCO.51D",
                "CCO.52L",
                "CCO.53D",
                "CCO.54L",
                "CCO.55D",
                "CCO.56L",
                "CCO.57L"


        };
        private List<string> productSC = new List<string>()
        {
            "SC.01D",
            "SC.02L",
            "AC.01D",
            "AC.02L",
            "AC.03D",
            "AC.04L",
            "AC.05D",
            "AC.06L",
            "AC.07D",
            "AC.08L",
            "AC.09D",
            "AC.10L",
            "AC.11D",
            "AC.12L",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "MRA0100",
            "MLA0100",
            "AC.13D",
            "AC.14L",
            "AC.15D",
            "AC.16L",
            "AC.17D",
            "AC.18L",
            "AC.19D",
            "AC.20L",
            "AC.21D",
            "AC.22D",
            "AC.23D",
            "AC.24L",
            "AC.25D",
            "AC.26L",
            "AC.27D",
            "AC.28L",
            "AC.29D",
            "AC.30L",
            "AC.31D",
            "AC.32L",
            "AC.33D",
            "AC.34L",
            "AC.35D",
            "AC.36L"
        };
        public SendMemoRepository(HttpRequestBuilder requestBuilder, IConverter converter,
            eMemoDBContext dbContext, EBAODBContet ebaoDbContext)
        {
            _dbContext = dbContext;
            _ebaoDbContext = ebaoDbContext;
            _converter = converter;
            _requestBuilder = requestBuilder;
            _appsettingHelper = new AppSettingHelper();
            authCodeForApiOwner = "RmluYW5jZS5PcGVyYXRpb24uZGNicjpTZWxpYzIwMjA=";
            smsFrom = "SOUTHEAST";
            baseURIsms = "https://notification.segroup.co.th/notification/sms/multi/";
        }

        public bool SendEmail(List<string> emailTo, List<string> emailCC, string bodyEmail, Stream fileAttachment)
        {
            try
            {
                if (emailTo.Any())
                {
                    _log4net.Info($"Send Email To {emailTo.Join(",")} CC To {emailCC.Join(",")} ");
                    string smtpHost = _appsettingHelper.GetConfiguration("EmailSetting:SmtpHost");

                    MailMessage mailMessage = new MailMessage();

                    string htmlBody = bodyEmail;
                    mailMessage.From = new MailAddress("noreply_ememo@tgh.co.th", "noreply_ememo@tgh.co.th");

                    foreach (var email in emailTo)
                    {
                        mailMessage.To.Add(email);
                    }

                    foreach (var email in emailCC)
                    {
                        mailMessage.CC.Add(email);
                    }

                    mailMessage.Subject = "ขอเอกสารเพิ่มเพื่อพิจารณาการรับประกัน";
                    mailMessage.Body = htmlBody;
                    mailMessage.IsBodyHtml = true;

                    string filename = "MemoReports_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";

                    System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
                    System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(fileAttachment, ct);
                    attach.ContentDisposition.FileName = filename;
                    mailMessage.Attachments.Add(attach);

                    mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                    mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    mailMessage.BodyEncoding = Encoding.UTF8;

                    using (var smtp = new SmtpClient())
                    {
                        smtp.Host = smtpHost;
                        smtp.UseDefaultCredentials = true;
                        smtp.Send(mailMessage);
                    }

                    _log4net.Info("Send Result Success");
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                _log4net.Error(ex.InnerException?.Message ?? ex.Message);
                return false;
            }

        }

        public async Task<responseSMS> SendSMS(string smsTo, string message)
        {
            responseSMS response = new responseSMS();

            smsTo = smsTo.Replace("-", "");

            try
            {
                _log4net.Info($"Send SMS To {smsTo} Message {message}");

                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("authCodeForApiOwner", authCodeForApiOwner);

                    var msgs = new SmsContent
                    {
                        messages = new List<SmsMessage> {
                            new SmsMessage
                            {
                                from = smsFrom,
                                destinations = new List<SmsTo>() {
                                    new SmsTo {
                                        to = smsTo
                                    }
                                },
                                callbackData = "callbackData",
                                message = message,
                                notificationUrl = "",
                                notifyContentType = "application/json"
                            }
                        },
                        tracking = new Tracking
                        {
                            track = "URL",
                            type = "SOCIAL_INVITES"

                        }
                    };
                    string smsMessage = JsonConvert.SerializeObject(msgs);

                    using (var strContent = new StringContent(smsMessage, Encoding.UTF8, "application/json"))
                    {
                        client.BaseAddress = new Uri(baseURIsms);
                        var responseTask = client.PostAsync(baseURIsms, strContent);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        var resultContent = result.Content.ReadAsStringAsync();
                        resultContent.Wait();
                        string r = resultContent.Result;

                        response = JsonConvert.DeserializeObject<responseSMS>(r);
                    }
                }
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex)); throw ex;
            }

            return response;
        }

        public string GetBodyEmail(string type, TTHMemoTransaction transactionData)
        {

            var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            string fullPath = string.Empty;
            string content = string.Empty;
            string productCategory = Utility.CheckProductCategory(transactionData.PolicyNo, transactionData.SourceOfBusiness);

            if (type == "Agent")
            {
                fullPath = path + @"\MailTemplate\MailTemplate_Agent.html";
                content = GetHTMLString(fullPath);

                string PolicyAgentFullName = transactionData.AgentName;
                string PolicyNumber = transactionData.PolicyNo;
                string PolicyHolderFullName = transactionData.PolicyName;

                if (productCategory == "Individual" || productCategory == "PA")
                {
                    if (Convert.ToInt32(transactionData.EntryAgeLifeAssured) > 19)
                    {
                        PolicyHolderFullName = transactionData.LifeAssuredName;
                    }
                    else
                    {
                        PolicyHolderFullName = transactionData.PolicyName + " ผู้ปกครองของ " + transactionData.LifeAssuredName;
                    }
                }
                else if (productCategory == "MRTA" || productCategory == "Group 1 Year Term")
                {
                    PolicyHolderFullName = transactionData.LifeAssuredName;
                }

                content = content.Replace("LifeAssuredFullName", transactionData.LifeAssuredName);
                content = content.Replace("PolicyAgentFullName", PolicyAgentFullName);
                content = content.Replace("PolicyNumber", PolicyNumber);
                content = content.Replace("PolicyHolderFullName", PolicyHolderFullName);
            }
            else if (type == "Customer")
            {
                fullPath = path + @"\MailTemplate\MailTemplate_Customer.html";
                content = GetHTMLString(fullPath);

                string PolicyNumber = transactionData.PolicyNo;
                string PolicyProposerFullName = transactionData.PolicyName;

                if (productCategory == "Individual" || productCategory == "PA")
                {
                    if (Convert.ToInt32(transactionData.EntryAgeLifeAssured) > 19)
                    {
                        PolicyProposerFullName = transactionData.LifeAssuredName;
                    }
                    else
                    {
                        PolicyProposerFullName = transactionData.PolicyName + " ผู้ปกครองของ " + transactionData.LifeAssuredName;
                    }
                }
                else if (productCategory == "MRTA" || productCategory == "Group 1 Year Term")
                {
                    PolicyProposerFullName = transactionData.LifeAssuredName;
                }
                else
                {
                    if (Convert.ToInt32(transactionData.EntryAgeLifeAssured) >= 0 && Convert.ToInt32(transactionData.EntryAgeLifeAssured) <= 19)
                    {
                        PolicyProposerFullName = transactionData.PolicyName + " ผู้ปกครองของ " + transactionData.LifeAssuredName;//ผู้ปกครองของคุณ ตามด้วย ชื่อ-สกุลของผู้ขอเอาประกัน
                    }
                }

                PolicyProposerFullName += " ผู้ขอเอาประกันภัย";
                content = content.Replace("PolicyNumber", PolicyNumber);
                content = content.Replace("PolicyProposerFullName", PolicyProposerFullName);
            }
            else if (type == "SaleSupport" || type == "Service Agent")
            {
                fullPath = path + @"\MailTemplate\MailTemplate_SaleSupport.html";
                content = GetHTMLString(fullPath);

                string PolicyAgentFullName = transactionData.AgentName;
                string PolicyNumber = transactionData.PolicyNo;
                string PolicyHolderFullName = transactionData.PolicyName;

                if (productCategory == "Individual" || productCategory == "PA")
                {
                    if (Convert.ToInt32(transactionData.EntryAgeLifeAssured) > 19)
                    {
                        PolicyHolderFullName = transactionData.LifeAssuredName;
                    }
                    else
                    {
                        PolicyHolderFullName = transactionData.PolicyName + " ผู้ปกครองของ " + transactionData.LifeAssuredName;
                    }
                }
                else if (productCategory == "MRTA" || productCategory == "Group 1 Year Term")
                {
                    PolicyHolderFullName = transactionData.LifeAssuredName;
                }

                string PolicyLifeAssuredFullName = transactionData.LifeAssuredName;
                content = content.Replace("PolicyAgentFullName", PolicyAgentFullName);
                content = content.Replace("PolicyNumber", PolicyNumber);
                content = content.Replace("PolicyHolderFullName", PolicyHolderFullName);
                content = content.Replace("PolicyLifeAssuredFullName", PolicyLifeAssuredFullName);
            }

            return content;
        }

        public byte[] GenerateCoverPage(TTHMemoTransaction transactionData, List<MemoDetailData> transactionDetailData,
            string note = null, DateTime? printDate = null)
        {
            try
            {
                var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);

                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");
                DateTime dateThai = Convert.ToDateTime(DateTime.Now, _cultureTHInfo);

                string PrintDate = string.Empty;
                var masterPolicy = GetMasterPolicy(transactionData.PolicyNo);
                var mainProduct = masterPolicy?.master_policy_no ?? transactionData.MainPlan;
                string PolicyDay = "15";
                string productCategory = Utility.CheckProductCategory(transactionData.PolicyNo, transactionData.SourceOfBusiness);

                if (printDate.HasValue)
                {
                    PrintDate = printDate.GetValueOrDefault().ToString("d MMMM yyyy", _cultureTHInfo);
                }
                else
                {
                    var memoHistory = _dbContext.TTHMemoDocumentHistory.Where(c => c.MemoTransaction_ID == transactionData.ID).OrderByDescending(e => e.SequenceEditDocumentNo).FirstOrDefault();
                    PrintDate = memoHistory.TransactionDate.ToString("d MMMM yyyy", _cultureTHInfo);
                }

                string PolicyNumber = transactionData.PolicyNo;
                string PolicyOwner = transactionData.PolicyName;
                string[] PolicyPHAddress = transactionData.PHAddress.Split("|");
                string[] PolicyLAAddress = transactionData.LAAddress != null ? transactionData.LAAddress.Split("|") : new string[0];

                string Address1 = PolicyPHAddress.Length > 0 ? PolicyPHAddress[0] : "";
                string Address2 = PolicyPHAddress.Length > 1 ? PolicyPHAddress[1] : "";
                string Address3 = PolicyPHAddress.Length > 2 ? PolicyPHAddress[2] : "";

                string PolicyMemo = string.Empty;
                string PolicyAgent = transactionData.AgentName + " (" + transactionData.ServicingBranch + ")";

                if (productCategory == "MRTA")
                {
                    PolicyNumber = $"{mainProduct} {transactionData.PolicyNo}";
                }

                if (productCategory == "MRTA" || productCategory == "Group 1 Year Term")
                {
                    PolicyAgent = transactionData.PolicyName;
                    PolicyDay = "30";
                    PolicyOwner = transactionData.LifeAssuredName;
                }
                else if (Convert.ToInt32(transactionData.EntryAgeLifeAssured) <= 19
                    && transactionData.PolicyName != transactionData.LifeAssuredName)
                {
                    PolicyOwner = transactionData.PolicyName + " ผู้ปกครองของ " + transactionData.LifeAssuredName;//ผู้ปกครองของคุณ ตามด้วย ชื่อ-สกุลของผู้ขอเอาประกัน
                }

                if (Convert.ToInt32(transactionData.EntryAgeLifeAssured) > 15 || productCategory == "MRTA" || productCategory == "Group 1 Year Term")
                {
                    //กรณีอายุมากกว่า 15 ปีใช้ที่อยู่ผู้เอาประกัน
                    Address1 = PolicyLAAddress.Length > 0 ? PolicyLAAddress[0] : "";
                    Address2 = PolicyLAAddress.Length > 1 ? PolicyLAAddress[1] : "";
                    Address3 = PolicyLAAddress.Length > 2 ? PolicyLAAddress[2] : "";
                }

                Dictionary<string, string> printDict = new Dictionary<string, string>(2);
                printDict.Add("PrintDate", PrintDate);

                printDict.Add("PolicyOwner", PolicyOwner);
                printDict.Add("PolicyAddress1", Address1);
                printDict.Add("PolicyAddress2", Address2);
                printDict.Add("PolicyAddress3", Address3);
                printDict.Add("PolicyDay", PolicyDay);

                if (productCategory != "Individual"
                    && productCategory != "PA"
                    && productCategory != "Group 1 Year Term")
                {
                    if (productCategory == "MRTA")
                    {
                        if (mainProduct == "GHB.01" || mainProduct == "GHB.01DR")
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายการตลาดสถาบัน 1");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9178, 9187");
                        }
                        else if (mainProduct == "BAAC.01LR")
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายสนับสนุนงานบริหารลูกค้าสถาบัน");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9436, 9429");
                        }
                        else if (mainProduct == "SELIC.01" || mainProduct == "SECAP.03D"
                            || mainProduct == "SEMN01D" || mainProduct == "SEMN.01D"
                            || mainProduct == "ML0120" || mainProduct == "SEMN.02L")
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายกลยุทธ์และงานสนับสนุนงานธุรกิจในเครือ TCC");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 5643");
                        }
                        else if (productCo.Contains(mainProduct))
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายสนับสนุนงานบริหารลูกค้าสถาบัน");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9413");
                        }
                        else if (productSC.Contains(mainProduct))
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายสนับสนุนงานตัวแทน");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9472");
                        }
                        else
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายรับประกันธุรกิจองค์กร");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9141");
                        }
                    }
                    else
                    {
                        if (mainProduct == "GHB.01" || mainProduct == "GHB.01DR")
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายการตลาดสถาบัน 1");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9178, 9187");
                        }
                        else if (mainProduct == "BAAC.01LR")
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายตัวแทน / ธุรกิจองค์กร");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9436, 9429");
                        }
                        else if (mainProduct == "SELIC.01")
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายกลยุทธ์และงานสนับสนุนงานธุรกิจในเครือ TCC");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 5643");
                        }
                        else if (mainProduct.StartsWith("CCO"))
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายสนับสนุนงานบริหารลูกค้าสถาบัน 2");
                            printDict.Add("Footer2", "หมายเลขโทร.02-631-1331 ต่อ 9413");
                        }
                        else
                        {
                            printDict.Add("Footer1", "หากท่านมีข้อสงสัยประการใด โปรดติดต่อที่ฝ่ายรับประกัน");
                            printDict.Add("Footer2", "หมายเลขโทร. 02-6311331 ต่อ 9217,9210");
                        }
                    }

                }
                printDict.Add("PolicyNumber", "กรมธรรม์เลขที่ " + PolicyNumber);
                printDict.Add("PolicyAgent", PolicyAgent);

                byte[] file = Generate(printDict, transactionData, transactionDetailData, note, productCategory);

                return file;
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                throw ex;
            }

        }
        //public byte[] Generate(Dictionary<string, string> testDict, TTHMemoTransaction transactionData, List<MemoDetailData> transactionDetailData, string note, string productCategory)
        //{

        //    try
        //    {
        //        var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);

        //        string fullPath = string.Empty;

        //        if (productCategory == "MRTA")
        //        {
        //            fullPath = path + @"\MailTemplate\LetterTemplate.pdf";
        //        }
        //        else
        //        {
        //            fullPath = path + @"\MailTemplate\LetterTemplate-2.pdf";
        //        }

        //        string fontpath = path + @"\MailTemplate\CORDIA.ttf";

        //        MemoryStream mem = new MemoryStream();
        //        PdfReader pdfReader = new PdfReader(fullPath);

        //        PdfStamper pdfStamper = new PdfStamper(pdfReader, mem);
        //        AcroFields pdfFormFields = pdfStamper.AcroFields;
        //        BaseFont font = BaseFont.CreateFont(fontpath,
        //                                   BaseFont.IDENTITY_H,
        //                                   BaseFont.EMBEDDED);

        //        PdfContentByte overContent = pdfStamper.GetOverContent(1);
        //        overContent.SaveState();

        //        transactionDetailData = transactionDetailData.OrderBy(c => c.MemoType_Sequence).ThenBy(e => e.MemoCode_Sequence).ToList();

        //        int y = 465;
        //        int newline = 0;
        //        int endLine = 0;
        //        iTextSharp.text.Font font2 = new iTextSharp.text.Font(font, 14);

        //        for (int i = 0; i < transactionDetailData.Where(c => c.MemoType != "Prepation for physical exam").Count(); i++)
        //        {
        //            var data = transactionDetailData.Where(c => c.MemoType != "Prepation for physical exam").ToList()[i];
        //            string comment = data.Comment;
        //            string description = data.MemoCodeDescription;
        //            string memoCode = data.MemoCode;
        //            string memoType = data.MemoType;

        //            string textMemo = $"{i + 1}. ";

        //            textMemo += !string.IsNullOrWhiteSpace(comment) ? comment : !string.IsNullOrWhiteSpace(description)
        //                ? description : !string.IsNullOrWhiteSpace(memoCode) ? memoCode : memoType;

        //            endLine = y + newline;

        //            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(50, y + newline, 560, 200);
        //            ColumnText ct = new ColumnText(overContent);
        //            ct.SetSimpleColumn(rect);
        //            ct.AddElement(new Paragraph(textMemo, font2));
        //            ct.Go();

        //            //overContent.ShowText(itemText);
        //            overContent.EndText();

        //            var multiNewline = textMemo.Length / 106.00M;

        //            if (multiNewline > 0)
        //            {
        //                newline -= (int)(20 * Math.Ceiling(multiNewline));
        //            }
        //            else
        //            {
        //                newline -= 20;
        //            }




        //            //                if (textMemo.Length > 106)
        //            //                {
        //            //                    var spritText = Split(textMemo, 106);
        //            //                    foreach (var itemText in spritText)
        //            //                    {

        //            //                        overContent.BeginText();
        //            //                        overContent.SetFontAndSize(font, 14.0f);
        //            //                        overContent.SetTextMatrix(50, y);
        //            //                        if (itemText == spritText.FirstOrDefault())
        //            //                        {
        //            //                            overContent.SetTextMatrix(50, y);
        //            //                        }
        //            //                        else
        //            //                        {
        //            //                            overContent.SetTextMatrix(60, y);
        //            //                        }

        //            //                        float llx = 36;
        //            //                        float lly = 700;
        //            //                        float urx = 300;
        //            //                        float ury = 806;

        //            //                        Rectangle rect = new Rectangle(
        //            //50, 445,
        //            //550, 200);
        //            //                        ColumnText ct = new ColumnText(overContent);
        //            //                        ct.SetSimpleColumn(rect);
        //            //                        ct.AddElement(new Paragraph(itemText));
        //            //                        ct.Go();

        //            //                        //overContent.ShowText(itemText);
        //            //                        overContent.EndText();
        //            //                        y -= 20;
        //            //                    }
        //            //                }
        //            //                else
        //            //                {
        //            //                    overContent.BeginText();
        //            //                    overContent.SetFontAndSize(font, 14.0f);
        //            //                    overContent.SetTextMatrix(50, y);
        //            //                    overContent.ShowText(textMemo);
        //            //                    overContent.EndText();
        //            //                    y -= 20;
        //            //                }

        //        }

        //        string PolicyMemo = string.Empty;
        //        for (int i = 0; i < transactionDetailData.Where(c => c.MemoType == "Prepation for physical exam").Count(); i++)
        //        {
        //            var data = transactionDetailData.Where(c => c.MemoType == "Prepation for physical exam").ToList()[i];
        //            string comment = data.Comment;
        //            string description = data.MemoCodeDescription;
        //            string memoCode = data.MemoCode;
        //            string memoType = data.MemoType;

        //            string textMemo = string.Empty;
        //            if (i == 0)
        //            {
        //                textMemo += $"*หมายเหตุ   ";
        //            }
        //            else
        //            {
        //                textMemo += "                   ";
        //            }

        //            textMemo += !string.IsNullOrWhiteSpace(comment) ? comment : !string.IsNullOrWhiteSpace(description)
        //                ? description : !string.IsNullOrWhiteSpace(memoCode) ? memoCode : memoType;

        //            endLine = y + newline;
        //            iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(50, y + newline, 560, 200);
        //            ColumnText ct = new ColumnText(overContent);
        //            ct.SetSimpleColumn(rect);
        //            ct.AddElement(new Paragraph(textMemo, font2));
        //            ct.Go();

        //            //overContent.ShowText(itemText);
        //            overContent.EndText();

        //            var multiNewline = textMemo.Length / 106.00M;

        //            if (multiNewline > 0)
        //            {
        //                newline -= (int)(20 * Math.Ceiling(multiNewline));
        //            }
        //            else
        //            {
        //                newline -= 20;
        //            }

        //            //if (textMemo.Length > 101)
        //            //{
        //            //    var spritText = Split(textMemo, 101);
        //            //    foreach (var itemText in spritText)
        //            //    {
        //            //        overContent.BeginText();
        //            //        overContent.SetFontAndSize(font, 14.0f);
        //            //        overContent.SetTextMatrix(50, y);
        //            //        overContent.ShowText(itemText);
        //            //        overContent.EndText();
        //            //        y -= 20;
        //            //    }
        //            //}
        //            //else
        //            //{
        //            //    overContent.BeginText();
        //            //    overContent.SetFontAndSize(font, 14.0f);
        //            //    overContent.SetTextMatrix(50, y);
        //            //    overContent.ShowText(textMemo);
        //            //    overContent.EndText();
        //            //    y -= 20;
        //            //}


        //            PolicyMemo += textMemo;

        //        }

        //        if (!string.IsNullOrWhiteSpace(note))
        //        {
        //            newline += 20;
        //            List<string> noteList = new List<string>();

        //            noteList = note.Split("|").ToList();

        //            for (int i = 0; i < noteList.Count; i++)
        //            {
        //                string textMemo = string.Empty;
        //                if (i == 0 && !PolicyMemo.Contains("*หมายเหตุ"))
        //                {
        //                    textMemo += $"*หมายเหตุ   ";
        //                }
        //                else
        //                {
        //                    textMemo += "                   ";
        //                }

        //                textMemo += noteList[i];
        //                endLine = y + newline;
        //                iTextSharp.text.Rectangle rect = new iTextSharp.text.Rectangle(50, y + newline, 560, 200);
        //                ColumnText ct = new ColumnText(overContent);
        //                ct.SetSimpleColumn(rect);
        //                ct.AddElement(new Paragraph(textMemo, font2));
        //                ct.Go();

        //                //overContent.ShowText(itemText);
        //                overContent.EndText();

        //                var multiNewline = textMemo.Length / 106.00M;

        //                if (multiNewline > 0)
        //                {
        //                    newline -= (int)(20 * Math.Ceiling(multiNewline));
        //                }
        //                else
        //                {
        //                    newline -= 20;
        //                }

        //                //if (textMemo.Length > 101)
        //                //{
        //                //    var spritText = Split(textMemo, 101);
        //                //    foreach (var itemText in spritText)
        //                //    {
        //                //        overContent.BeginText();
        //                //        overContent.SetFontAndSize(font, 14.0f);
        //                //        overContent.SetTextMatrix(50, y);
        //                //        overContent.ShowText(itemText);
        //                //        overContent.EndText();
        //                //        y -= 20;
        //                //    }
        //                //}
        //                //else
        //                //{
        //                //    overContent.BeginText();
        //                //    overContent.SetFontAndSize(font, 14.0f);
        //                //    overContent.SetTextMatrix(50, y);
        //                //    overContent.ShowText(textMemo);
        //                //    overContent.EndText();
        //                //    y -= 20;
        //                //}

        //            }
        //        }

        //        if (endLine == 0)
        //        {
        //            endLine = 385;
        //        }
        //        else
        //        {
        //            endLine -= 90;
        //        }



        //        overContent.BeginText();
        //        overContent.SetFontAndSize(font, 14.0f);
        //        overContent.SetTextMatrix(325, endLine);
        //        overContent.ShowText("จดหมายฉบับนี้ออกจากระบบอิเล็กทรอนิกส์จึงไม่มีลายมือชื่อ");
        //        overContent.EndText();

        //        overContent.BeginText();
        //        overContent.SetFontAndSize(font, 14.0f);
        //        overContent.SetTextMatrix(410, endLine - 20);
        //        overContent.ShowText("ฝ่ายรับประกัน");
        //        overContent.EndText();

        //        overContent.RestoreState();
        //        foreach (KeyValuePair<string, string> item in testDict)
        //        {
        //            if (item.Key.Contains("Footer"))
        //            {
        //                pdfFormFields.SetFieldProperty(item.Key, "textsize", 10, null);
        //            }
        //            else
        //            {
        //                pdfFormFields.SetFieldProperty(item.Key, "textsize", 14, null);
        //            }
        //            pdfFormFields.SetFieldProperty(item.Key, "textfont", font, null);

        //            pdfFormFields.SetFieldProperty(item.Key, "textcolor", BaseColor.BLACK, null);
        //            pdfFormFields.SetField(item.Key, item.Value);
        //        }
        //        pdfFormFields.SetFieldProperty("text1", "textsize", 14, null);
        //        pdfFormFields.SetFieldProperty("text2", "textsize", 14, null);
        //        pdfFormFields.SetFieldProperty("text3", "textsize", 14, null);
        //        pdfFormFields.SetFieldProperty("text1", "textfont", font, null);
        //        pdfFormFields.SetFieldProperty("text2", "textfont", font, null);
        //        pdfFormFields.SetFieldProperty("text3", "textfont", font, null);
        //        pdfStamper.FormFlattening = true;

        //        // close the pdf  
        //        pdfStamper.Close();
        //        pdfReader.Close();
        //        mem.Dispose();

        //        return mem.ToArray();
        //    }
        //    catch (Exception ex)
        //    {
        //        _log4net.Error(JsonConvert.SerializeObject(ex));
        //        throw ex;

        //    }
        //}

        public byte[] Generate(Dictionary<string, string> testDict, TTHMemoTransaction transactionData, List<MemoDetailData> transactionDetailData, string noteHistory, string productCategory)
        {

            try
            {
                var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);

                string fullPath = string.Empty;

                if (productCategory == "MRTA")
                {
                    fullPath = path + @"\MailTemplate\LetterTemplate.pdf";
                }
                else
                {
                    fullPath = path + @"\MailTemplate\LetterTemplate-2.pdf";
                }

                string fontpath = path + @"\MailTemplate\CORDIA.ttf";

                MemoryStream mem = new MemoryStream();
                PdfReader pdfReader = new PdfReader(fullPath);

                PdfStamper pdfStamper = new PdfStamper(pdfReader, mem);
                AcroFields pdfFormFields = pdfStamper.AcroFields;
                BaseFont font = BaseFont.CreateFont(fontpath,
                                           BaseFont.IDENTITY_H,
                                           BaseFont.EMBEDDED);

                PdfContentByte overContent = pdfStamper.GetOverContent(1);
                overContent.SaveState();

                transactionDetailData = transactionDetailData.OrderBy(c => c.MemoType_Sequence).ThenBy(e => e.MemoCode_Sequence).ToList();




                PdfPTable table = new PdfPTable(1);
                table.TotalWidth = 510f;

                int y = 425;
                string textMemo = string.Empty;
                for (int i = 0; i < transactionDetailData.Where(c => c.MemoType != "Prepation for physical exam").Count(); i++)
                {
                    var data = transactionDetailData.Where(c => c.MemoType != "Prepation for physical exam").ToList()[i];
                    string comment = data.Comment;
                    string description = data.MemoCodeDescription;
                    string memoCode = data.MemoCode;
                    string memoType = data.MemoType;

                    textMemo = $"{i + 1}. ";

                    textMemo += !string.IsNullOrWhiteSpace(comment) ? comment : !string.IsNullOrWhiteSpace(description)
                        ? description : !string.IsNullOrWhiteSpace(memoCode) ? memoCode : memoType;

                    table.AddCell(GetCell(textMemo, 0));
                }


                string PolicyMemo = string.Empty;
                for (int i = 0; i < transactionDetailData.Where(c => c.MemoType == "Prepation for physical exam").Count(); i++)
                {
                    var data = transactionDetailData.Where(c => c.MemoType == "Prepation for physical exam").ToList()[i];
                    string comment = data.Comment;
                    string description = data.MemoCodeDescription;
                    string memoCode = data.MemoCode;
                    string memoType = data.MemoType;

                    textMemo = string.Empty;
                    if (i == 0)
                    {
                        textMemo += $"*หมายเหตุ   ";
                    }
                    else
                    {
                        textMemo += "                   ";
                    }

                    textMemo += !string.IsNullOrWhiteSpace(comment) ? comment : !string.IsNullOrWhiteSpace(description)
                        ? description : !string.IsNullOrWhiteSpace(memoCode) ? memoCode : memoType;

                    table.AddCell(GetCell(textMemo, 0));

                }

                if (!string.IsNullOrWhiteSpace(transactionData.Note) || !string.IsNullOrWhiteSpace(noteHistory))
                {
                    List<string> noteList = new List<string>();

                    if (!string.IsNullOrWhiteSpace(noteHistory))
                    {
                        noteList = noteHistory.Split("|").ToList();
                    }
                    else
                    {
                        noteList = transactionData.Note.Split("|").ToList();
                    }

                    for (int i = 0; i < noteList.Count; i++)
                    {
                        textMemo = string.Empty;
                        if (i == 0 && !PolicyMemo.Contains("*หมายเหตุ"))
                        {
                            textMemo += $"*หมายเหตุ   ";
                        }
                        else
                        {
                            textMemo += "                   ";
                        }

                        textMemo += noteList[i];

                        table.AddCell(GetCell(textMemo, 0));

                    }
                }

                table.AddCell(GetCell("\n\n\nจดหมายฉบับนี้ออกจากระบบอิเล็กทรอนิกส์จึงไม่มีลายมือชื่อ", 3));
                table.AddCell(GetCell("                                                                                                                                                ฝ่ายรับประกัน", 0));

                table.WriteSelectedRows(0, -1, 50, 460, overContent);


                overContent.RestoreState();
                foreach (KeyValuePair<string, string> item in testDict)
                {
                    if (item.Key.Contains("Footer"))
                    {
                        pdfFormFields.SetFieldProperty(item.Key, "textsize", 10, null);
                    }
                    else
                    {
                        pdfFormFields.SetFieldProperty(item.Key, "textsize", 14, null);
                    }
                    pdfFormFields.SetFieldProperty(item.Key, "textfont", font, null);

                    pdfFormFields.SetFieldProperty(item.Key, "textcolor", BaseColor.BLACK, null);
                    pdfFormFields.SetField(item.Key, item.Value);
                }
                pdfFormFields.SetFieldProperty("text1", "textsize", 14, null);
                pdfFormFields.SetFieldProperty("text2", "textsize", 14, null);
                pdfFormFields.SetFieldProperty("text3", "textsize", 14, null);
                pdfFormFields.SetFieldProperty("text1", "textfont", font, null);
                pdfFormFields.SetFieldProperty("text2", "textfont", font, null);
                pdfFormFields.SetFieldProperty("text3", "textfont", font, null);
                pdfStamper.FormFlattening = true;

                // close the pdf  
                pdfStamper.Close();
                pdfReader.Close();
                mem.Dispose();

                return mem.ToArray();
            }
            catch (Exception e)
            {
                return null;

            }
        }


        private PdfPCell GetCell(string text, int alignment)
        {
            var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            string fontpath = path + @"\MailTemplate\CORDIA.ttf";

            BaseFont font = BaseFont.CreateFont(fontpath,
                                     BaseFont.IDENTITY_H,
                                     BaseFont.EMBEDDED);
            var normal = new Font(font, 14);

            PdfPCell cell = new PdfPCell(new Phrase(text, normal));
            cell.HorizontalAlignment = alignment == 0 ? Element.ALIGN_LEFT : alignment == 1 ? Element.ALIGN_CENTER : Element.ALIGN_RIGHT;
            cell.Border = Rectangle.NO_BORDER;
            cell.PaddingTop = 0;
            return cell;
        }

        public string GetHTMLString(string path)
        {

            String line = String.Empty;
            String htmlString = String.Empty;
            using (StreamReader reader = new StreamReader(path))
            {

                while ((line = reader.ReadLine()) != null)
                {
                    htmlString += line;
                }
            }
            var sb = new StringBuilder();
            sb.Append(htmlString);
            return sb.ToString();
        }

        public List<string> Split(string str, int chunkSize)
        {
            List<string> textSplit = new List<string>();
            int textLength = str.Length;
            var roundText = Math.Ceiling(textLength / Convert.ToDecimal(chunkSize));
            int lengthSplit = textLength;

            for (int i = 0; i < roundText; i++)
            {
                if (lengthSplit >= chunkSize)
                {
                    var subString = str.Substring(i * chunkSize, chunkSize);
                    textSplit.Add(subString);
                }
                else
                {
                    var subString = str.Substring(i * chunkSize, lengthSplit);
                    textSplit.Add(subString);
                }

                lengthSplit -= chunkSize;
            }

            return textSplit;
        }

        public T_MASTER_POLICY GetMasterPolicy(string policyNo)
        {
            try
            {
                var queryString = $"select a.policy_code,b.master_policy_no from t_contract_master a join t_master_policy b on b.master_policy_id = a.master_policy_id where a.policy_Code = '{policyNo}';";

                var data = _ebaoDbContext.T_MASTER_POLICY.FromSql(queryString).ToList();
                return data.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return null;
            }
        }
    }

    public class SmsContent
    {
        public List<SmsMessage> messages { get; set; }
        public Tracking tracking { get; set; }
    }

    public class SmsMessage
    {
        public string from { get; set; }
        public List<SmsTo> destinations { get; set; }
        public string message { get; set; }
        public string callbackData { get; set; }
        public string notifyContentType { get; set; }
        public string notificationUrl { get; set; }
    }
    public class SmsTo
    {
        public string to { get; set; }
    }

    public class responseSMS
    {
        public string message { get; set; }
        public responseResultSMS result { get; set; }
        public string resultCode { get; set; }
    }

    public class responseResultSMS
    {
        public string bulkId { get; set; }
        public List<responseMessageSMS> messages { get; set; }
        public string trackingProcessKey { get; set; }
    }
    public class responseMessageSMS
    {
        public string callbackData { get; set; }
        public string messageId { get; set; }
        public string notifyUrl { get; set; }
        public string smsCount { get; set; }
        public responseStatusSMS status { get; set; }
        private string _to;
        public string to
        {
            get
            {
                string nobileno = Regex.Replace(this._to, @"\d(?!\d{0,3}$)", "X");
                return nobileno;
            }
            set
            {
                this._to = value;
            }
        }

    }
    public class responseStatusSMS
    {
        public string action { get; set; }
        public string description { get; set; }
        public string groupId { get; set; }
        public string groupName { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Tracking
    {
        public string track { get; set; }
        public string type { get; set; }
        public string baseUrl { get; set; }
    }
}
