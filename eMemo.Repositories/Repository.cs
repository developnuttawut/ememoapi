﻿using DinkToPdf.Contracts;
using eMemo.Helper;
using eMemo.Repositories.DBContext;
using eMemo.Repositories.Repo;
using System;
using System.Collections.Generic;
using System.Text;

namespace eMemo.Repositories
{
    public class Repository
    {
        public MasterDataRepository memoMapping;
        public MemoTransactionRepository memoTransaction;
        public DMCFileRepository dmcFile;
        public SendMemoRepository sendMemo;
        public TGH_EMEMO_DMRepository tghEmemo;
        public EbaoLSStagingRepository ebaoLSStaging;
        public AMLSStagingRepository almStaging;
        public EBaoMemoLetterRepository eBaoMemoLetter;
        public HolidayRepository holiday;
        public AuthenRepository authen;
        public Repository(
            eMemoDBContext dbContext,
            TGH_EMEMO_DMDBContext tghEmemoContext,
            eBaoLSStagingDBContext eBaoLSStagingContext,
            AMLSStagingDBContext almStagingContext,
            EBAODBContet ebContrext,
            HttpRequestBuilder requestBuilder,
            IConverter converter)
        {
            memoMapping = new MasterDataRepository(dbContext);
            memoTransaction = new MemoTransactionRepository(dbContext);
            dmcFile = new DMCFileRepository(requestBuilder);
            sendMemo = new SendMemoRepository(requestBuilder, converter, dbContext,ebContrext);
            tghEmemo = new TGH_EMEMO_DMRepository(tghEmemoContext);
            ebaoLSStaging = new EbaoLSStagingRepository(eBaoLSStagingContext);
            almStaging = new AMLSStagingRepository(almStagingContext);
            eBaoMemoLetter = new EBaoMemoLetterRepository(ebContrext);
            holiday = new HolidayRepository(requestBuilder);
            authen = new AuthenRepository(requestBuilder);
        }
    }
}
