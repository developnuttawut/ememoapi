﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace eMemo.Helper
{
    public class HttpRequestBuilder
    {
        private HttpMethod _method = null;
        private string _requestUri = string.Empty;
        private string _auth = null;
        private string _token = null;
        private HttpContent _content = null;
        private readonly IHttpClientFactory _clientFactory;
        public HttpRequestBuilder(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public HttpRequestBuilder Init(string url, string method)
        {
            _requestUri = url;

            if (method == "POST")
            {
                _method = HttpMethod.Post;
            }
            else if (method == "GET")
            {
                _method = HttpMethod.Get;
            }

            return this;
        }

        public HttpRequestBuilder AddContent(HttpContent content)
        {
            _content = content;

            return this;
        }

        public HttpRequestBuilder AddAuth(string userName, string password)
        {
            var authString = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{userName}:{password}"));
            _auth = authString;

            return this;
        }

        public HttpRequestBuilder SetAuthEmpty()
        {
            _auth = string.Empty;
            return this;
        }

        public HttpRequestBuilder SetTokenEmpty()
        {
            _token = string.Empty;
            return this;
        }

        public HttpRequestBuilder AddToken(string token)
        {
            _token = token;
            return this;
        }

        public async Task<HttpResponseMessage> SendAsync()
        {
            var request = new HttpRequestMessage
            {
                Method = _method,
                RequestUri = new Uri(_requestUri)
            };

            if (_content != null)
            {
                request.Content = _content;
                request.Content.Headers.ContentType.CharSet = string.Empty;
            }

            var client = _clientFactory.CreateClient("EMEMOAPI");

            if (!string.IsNullOrWhiteSpace(_auth))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", _auth);
            }

            if (!string.IsNullOrWhiteSpace(_token))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _token);
            }

            var response = await client.SendAsync(request);

            return response;
        }
    }
}
