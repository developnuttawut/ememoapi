﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace eMemo.Helper
{
    public class Utility
    {
        public static DateTime GetDateNowThai()
        {
            var dateUtc = DateTime.UtcNow;
            return dateUtc.AddHours(7);
        }

        public static byte[] ConcatAndAddContent(List<byte[]> pdfByteContent)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    using (var doc = new Document())
                    {
                        using (var copy = new PdfSmartCopy(doc, ms))
                        {
                            doc.Open();

                            //Loop through each byte array
                            foreach (var p in pdfByteContent)
                            {

                                try
                                {
                                    //Create a PdfReader bound to that byte array
                                    using (var reader = new PdfReader(p))
                                    {

                                        //Add the entire document instead of page-by-page
                                        copy.AddDocument(reader);
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }

                            doc.Close();
                        }
                    }

                    //Return just before disposing
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }       
        }

        public static string CheckProductCategory(string policyNo, string businessUnit)
        {
            var policyDigit3 = policyNo.Substring(2, 1);
            string productCategory = string.Empty;

            if (policyDigit3 == "1")
            {
                productCategory = "Individual";
            }
            else if (policyDigit3 == "2")
            {
                if (businessUnit != "SECAP" && businessUnit != "Srisawad")
                {
                    productCategory = "MRTA";
                }
                else if (businessUnit == "SECAP")
                {
                    productCategory = "SECAP";
                }
                else if (businessUnit == "Srisawad")
                {
                    productCategory = "Srisawad";
                }
            }
            else if (policyDigit3 == "3")
            {
                productCategory = "Group 1 Year Term";
            }
            else if (policyDigit3 == "4")
            {
                productCategory = "PA";
            }
            else
            {
                productCategory = "Individual";
            }

            return productCategory;
        }
    }
}
