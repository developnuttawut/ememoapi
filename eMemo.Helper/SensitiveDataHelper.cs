﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace eMemo.Helper
{
    public class SensitiveDataHelper
    {
        private readonly IConfiguration _config;
        public SensitiveDataHelper(IConfiguration config)
        {
            _config = config;
        }

        public string SetSensitiveData(string jsonString)
        {
            try
            {
                Dictionary<object, object> JsonStringConvert = JsonConvert.DeserializeObject<Dictionary<object, object>>(jsonString);
                Dictionary<object, object> jsonObject = new Dictionary<object, object>();

                object dataObject = null;
                JsonStringConvert.TryGetValue("data", out dataObject);

                if (dataObject == null)
                {
                    JsonStringConvert.TryGetValue("Data", out dataObject);
                }

                // Blur ข้อมูลใน object data
                if (dataObject != null)
                {
                    if (dataObject.GetType().Name == "JArray")
                    {
                        List<Dictionary<object, object>> dataObjetArray = new List<Dictionary<object, object>>();
                        JArray jsonArray = JArray.Parse(dataObject.ToString());
                        foreach (var itemData in jsonArray)
                        {
                            var jObject = CheckAndSetSensitiveData(itemData);
                            dataObjetArray.Add(jObject);
                        }
                        jsonObject.TryAdd("data", dataObjetArray);
                    }
                    else
                    {
                        Dictionary<object, object> jsonObjectData = CheckAndSetSensitiveData(dataObject);

                        jsonObject.TryAdd("data", jsonObjectData);
                    }
                }

                foreach (var item in JsonStringConvert.Where(c => c.Key.ToString().ToLower() != "data"))
                {
                    if (item.Value != null)
                    {
                        var typeValue = item.Value.GetType().Name;

                        string configField = _config["SensitiveData:" + item.Key.ToString().ToLower()];
                        if (configField != null)
                        {
                            jsonObject.TryAdd(item.Key, configField);
                        }
                        else
                        {
                            jsonObject.TryAdd(item.Key, item.Value);
                        }



                    }
                    else
                    {
                        jsonObject.TryAdd(item.Key, item.Value);
                    }
                }

                jsonString = JsonConvert.SerializeObject(jsonObject);
            }
            catch (Exception ex)
            {
            }

            return jsonString;
        }

        private Dictionary<object, object> CheckAndSetSensitiveData(object value)
        {
            var jsonData = JsonConvert.SerializeObject(value);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<object, object>>(jsonData);

            Dictionary<object, object> jsonObjectData = new Dictionary<object, object>();

            if(dictionary != null)
            {
                foreach (var item in dictionary)
                {
                    if (item.Value != null)
                    {
                        var type = item.Value.GetType().Name;

                        if (type == "JObject")
                        {
                            var jObject = CheckAndSetSensitiveData(item.Value);
                            jsonObjectData.TryAdd(item.Key, jObject);
                        }
                        else if (type == "JArray")
                        {
                            JArray app = JArray.Parse(item.Value.ToString());
                            List<Dictionary<object, object>> dataArray = new List<Dictionary<object, object>>();

                            foreach (var itemData in app)
                            {
                                var jObject = CheckAndSetSensitiveData(itemData);
                                dataArray.Add(jObject);
                            }

                            jsonObjectData.TryAdd(item.Key, dataArray);
                        }
                        else
                        {
                            string configField = _config["SensitiveData:" + item.Key.ToString().ToLower()];
                            if (configField != null)
                            {
                                jsonObjectData.TryAdd(item.Key, configField);
                            }
                            else
                            {
                                jsonObjectData.TryAdd(item.Key, item.Value);
                            }
                        }
                    }
                    else
                    {
                        jsonObjectData.TryAdd(item.Key, item.Value);
                    }

                }
            }
           

            return jsonObjectData;
        }

        private string BlurData(string value)
        {
            if (!string.IsNullOrWhiteSpace(value) && value.Length > 4)
            {
                string blurValue = string.Empty;
                int index = 0;

                for (int i = 0; i < value.Length - 4; i++)
                {
                    blurValue += "x";
                    index++;
                }

                for (int i = index; i < value.Length; i++)
                {
                    blurValue += value[i];
                }

                return blurValue;
            }
            else
            {
                return value;
            }
        }
    }
}
